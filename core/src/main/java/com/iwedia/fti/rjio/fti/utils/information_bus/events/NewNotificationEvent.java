package com.iwedia.fti.rjio.fti.utils.information_bus.events;

import com.iwedia.fti.rjio.fti.utils.information_bus.Event;

/**
 * New notification event
 *
 * @author Dejan Nadj
 */
public class NewNotificationEvent extends Event {

    /**
     * New notification event
     *
     * @param notification  new notification
     */
    public NewNotificationEvent(Object notification) {
        super(Events.NEW_NOTIFICATION, notification);
    }
}
