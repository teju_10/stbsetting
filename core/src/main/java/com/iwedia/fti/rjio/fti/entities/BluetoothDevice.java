package com.iwedia.fti.rjio.fti.entities;

/**
 * Bluetooth device
 *
 * @author Dejan Nadj
 */
public class BluetoothDevice {

    /**
     * Device id
     */
    private String id;

    /**
     * Device name
     */
    private String deviceName;

    /**
     * Default constructor
     */
    public BluetoothDevice() {}

    /**
     * Constructor
     *
     * @param deviceId   device id
     * @param deviceName device name
     */
    public BluetoothDevice(String deviceId, String deviceName) {
        this.id = deviceId;
        this.deviceName = deviceName;
    }

    /**
     * Get device name
     *
     * @return device name
     */
    public String getDeviceName() {
        return deviceName;
    }

    /**
     * Get device id
     *
     * @return device id
     */
    public String getDeviceId() {
        return id;
    }
}
