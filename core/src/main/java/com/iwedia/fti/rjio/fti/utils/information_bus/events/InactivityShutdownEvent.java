package com.iwedia.fti.rjio.fti.utils.information_bus.events;

import com.iwedia.fti.rjio.fti.utils.information_bus.Event;

/**
 * Inactivity shutdown event
 *
 * @author Nikola Aleksic
 */
public class InactivityShutdownEvent extends Event {

    /**
     * Constructor
     */
    public InactivityShutdownEvent() {
        super(Events.INACTIVITY_SHUTDOWN);
    }
}
