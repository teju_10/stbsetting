package com.iwedia.fti.rjio.fti.api;

import com.iwedia.fti.rjio.fti.entities.User;

/**
 * User profile api
 *
 * @author Dragan Krnjaic
 */
public interface UserProfileAPI<T extends User> {

    /**
     * Request user data
     *
     * @param user     user
     * @param callback callback
     */
    void requestUserData(T user, AsyncReceiver callback);
}
