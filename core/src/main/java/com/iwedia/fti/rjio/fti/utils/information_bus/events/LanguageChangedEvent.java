package com.iwedia.fti.rjio.fti.utils.information_bus.events;

import com.iwedia.fti.rjio.fti.entities.Language;
import com.iwedia.fti.rjio.fti.utils.information_bus.Event;

/**
 * Language changed event
 *
 * @author Dejan Nadj
 */
public class LanguageChangedEvent extends Event {

    /**
     * Constructor
     *
     * @param language language
     */
    public LanguageChangedEvent(Language language) {
        super(Events.LANGUAGE_CHANGED, language);
    }
}
