package com.iwedia.fti.rjio.fti.handler;

import com.iwedia.fti.rjio.fti.api.SignInAPI;
import com.iwedia.fti.rjio.fti.api.SignUpAPI;
import com.iwedia.fti.rjio.fti.entities.User;

/**
 * Sign up/in handler
 *
 * @author Veljko Ilkic
 */
public abstract class AccountHandler<T extends User> implements SignInAPI<T>, SignUpAPI<T> {

    /**
     * Current user
     */
    protected T user;

    /**
     * Get user
     *
     * @return user
     */
    public T getUser() {
        return user;
    }
}
