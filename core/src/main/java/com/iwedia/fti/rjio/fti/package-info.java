/**
 * The intention of this documentation is to provide the process of implementation RJIO Launcher core module.
 * <p>
 * <p>
 * Core module architecture implements base API functionalities and entity models, independent from Android system.<br>
 * Core module holds api package, entities package and handler package.
 * <p>
 * <b>API</b> package with these interfaces:
 * <p>
 * <code>AppsAPI</code> with methods:
 * </ul>
 * <li><code><font color="#4169E1">requestFavoriteApps(AsyncDataReceiver<ArrayList<T>> callback)</font></code></li>
 * <li><code><font color="#4169E1">void requestInstalledApps(AsyncDataReceiver<ArrayList<T>> callback)</font></code></li>
 * <li><code><font color="#4169E1">void requestInstalledGames(AsyncDataReceiver<ArrayList<T>> callback)</font></code></li>
 * <li><code><font color="#4169E1">void launchApp(T appItem, AsyncReceiver callback)</font></code></li>
 * <li><code><font color="#4169E1">void removeApp(T appItem, AsyncReceiver callback)</font></code></li>
 * </ul>
 * <p>
 * <code>AsyncDataReceiver</code> with methods:
 * </ul>
 * <li><code><font color="#4169E1">void onSuccess(T data)</font></code></li>
 * <li><code><font color="#4169E1">void onFailed()</font></code></li>
 * </ul>
 * <p>
 * <code>AsyncReceiver</code> with methods:
 * </ul>
 * <li><code><font color="#4169E1">void onSuccess</font></code></li>
 * <li><code><font color="#4169E1">void onFailed()</font></code></li>
 * </ul>
 * <p>
 * <code>DeviceAPI</code> with methods:
 * </ul>
 * <li><code><font color="#4169E1">void onMounted(T device)</font></code></li>
 * <li><code><font color="#4169E1">void onUnmounted(T device)</font></code></li>
 * </ul>
 * <p>
 * <code>NetworkAPI</code> with methods:
 * </ul>
 * <li><code><font color="#4169E1">void requestNetworks(AsyncDataReceiver<List<T>> callback)</font></code></li>
 * <li><code><font color="#4169E1">void connectToWiFi(String wifiName, String wifiPassword, AsyncReceiver callback)</font></code></li>
 * </ul>
 * <p>
 * <code>NotificationAPI</code> with methods:
 * </ul>
 * <li><code><font color="#4169E1">void requestNotifications(AsyncDataReceiver<ArrayList<T>> callback)</font></code></li>
 * </ul>
 * <p>
 * <code>OverscanAPI</code> with methods:
 * </ul>
 * <li><code><font color="#4169E1">void onProgressValues(int scale, int horizontal, int vertical)</font></code></li>
 * </ul>
 * <p>
 * <code>RecommendedAPI</code> with methods:
 * </ul>
 * <li><code><font color="#4169E1">void requestRecommendedItems(AsyncDataReceiver<ArrayList<T>> callback)</font></code></li>
 * <li><code><font color="#4169E1">void requestPromotedItems(AsyncDataReceiver<ArrayList<T>> callback)</font></code></li>
 * <li><code><font color="#4169E1">void startRecommendedItem(T item)</font></code></li>
 * <li><code><font color="#4169E1">void startPromotedItem(T item)</font></code></li>
 * </ul>
 * <p>
 * <code>SignInAPI</code> with methods:
 * </ul>
 * <li><code><font color="#4169E1">void signIn(T user, AsyncReceiver callback)</font></code></li>
 * <li><code><font color="#4169E1">void signOut(T user, AsyncReceiver callback)</font></code></li>
 * </ul>
 * <p>
 * <code>SignUpAPI</code> with methods:
 * </ul>
 * <li><code><font color="#4169E1">void signUp(T user, AsyncReceiver callback)</font></code></li>
 * </ul>
 * <p>
 * <code>TimerAPI</code> with methods:
 * </ul>
 * <li><code><font color="#4169E1">void setTimeoutPeriod(int seconds)</font></code></li>
 * <li><code><font color="#4169E1">void startTimer()</font></code></li>
 * <li><code><font color="#4169E1">void resetTimer()</font></code></li>
 * <li><code><font color="#4169E1">void stopTimer()</font></code></li>
 * </ul>
 * <p>
 * <code>UpdateAPI</code> with methods:
 * </ul>
 * <li><code><font color="#4169E1">void requestUpdateAvailable(AsyncReceiver callback)</font></code></li>
 * <li><code><font color="#4169E1">void requestUpdateStart(AsyncReceiver callback)</font></code></li>
 * <li><code><font color="#4169E1">void requestUpdateCancel(AsyncReceiver callback)</font></code></li>
 * <li><code><font color="#4169E1">void requestUpdateApply(AsyncReceiver callback)</font></code></li>
 * </ul>
 * <p>
 * <code>UserProfileAPI</code> with methods:
 * </ul>
 * <li><code><font color="#4169E1">void requestUserData(T user, AsyncReceiver callback)</font></code></li>
 * </ul>
 * <p>
 * <p>
 * <b>Entities</b> package with these base models:
 * <p>
 * <code>AppItem</code> class is model entity for application item with methods:
 * <ul>
 * <li><code><font color="#4169E1">int getId()</font></code> - Returns application id</li>
 * <li><code><font color="#4169E1">String getName()</font></code> - Returns application name</li>
 * <li><code><font color="#4169E1">String getPackageName()</font></code> - Returns application package name</li>
 * <li><code><font color="#4169E1">T getIcon()</font></code> - Returns application icon</li>
 * <li><code><font color="#4169E1">int getType()</font></code> - Returns application type</li>
 * <li><code><font color="#4169E1">void setData(Object data)</font></code> - Sets application data</li>
 * <li><code><font color="#4169E1">Object getData()</font></code> - Returns application data</li>
 * <li><code><font color="#4169E1">String getBackgroundUrl()</font></code> - Returns application background url image</li>
 * </ul>
 * <p>
 * <code>DeviceEntity</code> class is model entity for usb device item with methods:
 * <ul>
 * <li><code><font color="#4169E1">int getId()</font></code> - Returns device id</li>
 * <li><code><font color="#4169E1">String getName()</font></code> - Returns device name</li>
 * <li><code><font color="#4169E1">String getProduct()</font></code> - Returns device product name</li>
 * <li><code><font color="#4169E1">String getVersion()</font></code> - Returns device version</li>
 * <li><code><font color="#4169E1">String getSerialNumber()</font></code> - Returns device serial number</li>
 * <li><code><font color="#4169E1">void setData(Object data)</font></code> - Sets device data</li>
 * <li><code><font color="#4169E1">Object getData()</font></code> - Returns device data</li>
 * </ul>
 * <p>
 * <code>User</code> class is model entity for User account with methods:
 * <ul>
 * <li><code><font color="#4169E1">int getId()</font></code> - Returns user's id</li>
 * <li><code><font color="#4169E1">String getUsername()</font></code> - Returns user's username</li>
 * <li><code><font color="#4169E1">String getPassword()</font></code> - Returns user's password</li>
 * <li><code><font color="#4169E1">void setData(Object data)</font></code> - Sets user's data</li>
 * <li><code><font color="#4169E1">Object getData()</font></code> - Returns user's data</li>
 * </ul>
 * <p>
 * <code>Notification</code> class is model entity for notifications with methods:
 * <ul>
 * <li><code><font color="#4169E1">int getId()</font></code> - Returns notification id</li>
 * <li><code><font color="#4169E1">String getText()</font></code> - Returns notification text</li>
 * <li><code><font color="#4169E1">T getIcon()</font></code> - Returns notification icon</li>
 * <li><code><font color="#4169E1">void setData(Object data)</font></code> - Sets notification data</li>
 * <li><code><font color="#4169E1">Object getData()</font></code> - Returns notification data</li>
 * </ul>
 * <p>
 * <code>RecommendedItem</code> class is model entity for recommended items with methods:
 * <ul>
 * <li><code><font color="#4169E1">int getId()</font></code> - Returns recommended item id</li>
 * <li><code><font color="#4169E1">String getName()</font></code> - Returns recommended item name</li>
 * <li><code><font color="#4169E1">T getIcon()</font></code> - Returns recommended item icon</li>
 * <li><code><font color="#4169E1">String getDescription()</font></code> - Returns recommended item description</li>
 * <li><code><font color="#4169E1">T getSourceIcon()</font></code> - Returns recommended item source icon</li>
 * <li><code><font color="#4169E1">RecommendedType getType()</font></code> - Returns recommended item type</li>
 * <li><code><font color="#4169E1">void setData(Object data)</font></code> - Sets recommended item data</li>
 * <li><code><font color="#4169E1">Object getData()</font></code> - Returns recommended item data</li>
 * </ul>
 * <p>
 * <code>WifiItem</code> class is model entity for wifi network with methods:
 * <ul>
 * <li><code><font color="#4169E1">int getId()</font></code> - Returns wifi id</li>
 * <li><code><font color="#4169E1">String getName()</font></code> - Returns wifi name</li>
 * <li><code><font color="#4169E1">boolean isConnected()</font></code> - Checks wifi connection</li>
 * <li><code><font color="#4169E1">void setConnected(boolean isConnected)</font></code> - Sets wifi connection</li>
 * <li><code><font color="#4169E1">void setData(Object data)</font></code> - Sets wifi data</li>
 * <li><code><font color="#4169E1">Object getData()</font></code> - Returns wifi data</li>
 * <li><code><font color="#4169E1">void setPassword(String password)</font></code> - Sets wifi password</li>
 * <li><code><font color="#4169E1">String getPassword()</font></code> - Returns wifi password</li>
 * </ul>
 * <p>
 * <b>Handler</b> package with these handlers:
 * <p>
 * </ul>
 * <li><code>AccountHandler</code></li>
 * <li><code>AppsHandler</code></li>
 * <li><code>DeviceHandler</code></li>
 * <li><code>NetworkHandler</code></li>
 * <li><code>NotificationsHandler</code></li>
 * <li><code>OverscanHandler</code></li>
 * <li><code>RecommendedHandler</code></li>
 * <li><code>TimerHandler</code></li>
 * <li><code>UpdateHandler</code></li>
 * <li><code>UserProfileHandler</code></li>
 * </ul>
 * <p>
 * <b>Utils</b> package with information bus feature:
 * <code>InformationBus</code> provides support for sending and receiving events between different app modules.
 * <p>
 * Every <code>Event</code> has id and data object.
 * Event can be submitted and received by using InformationBus
 *
 * @see com.iwedia.fti.rjio.fti.api.AppsAPI
 * @see com.iwedia.fti.rjio.fti.api.AsyncDataReceiver
 * @see com.iwedia.fti.rjio.fti.api.AsyncReceiver
 * @see com.iwedia.fti.rjio.fti.api.DeviceAPI
 * @see com.iwedia.fti.rjio.fti.api.OverscanAPI
 * @see com.iwedia.fti.rjio.fti.api.NetworkAPI
 * @see com.iwedia.fti.rjio.fti.api.NotificationsAPI
 * @see com.iwedia.fti.rjio.fti.api.RecommendedAPI
 * @see com.iwedia.fti.rjio.fti.api.SignInAPI
 * @see com.iwedia.fti.rjio.fti.api.SignUpAPI
 * @see com.iwedia.fti.rjio.fti.api.TimerAPI
 * @see com.iwedia.fti.rjio.fti.api.UpdateAPI
 * @see com.iwedia.fti.rjio.fti.api.UserProfileAPI
 * @see com.iwedia.fti.rjio.fti.entities.AppItem
 * @see com.iwedia.fti.rjio.fti.entities.DeviceEntity
 * @see com.iwedia.fti.rjio.fti.entities.Notification
 * @see com.iwedia.fti.rjio.fti.entities.RecommendedItem
 * @see com.iwedia.fti.rjio.fti.entities.User
 * @see com.iwedia.fti.rjio.fti.entities.WiFiItem
 * @see com.iwedia.fti.rjio.fti.handler.AccountHandler
 * @see com.iwedia.fti.rjio.fti.handler.AppsHandler
 * @see com.iwedia.fti.rjio.fti.handler.DeviceHandler
 * @see com.iwedia.fti.rjio.fti.handler.NetworkHandler
 * @see com.iwedia.fti.rjio.fti.handler.NotificationsHandler
 * @see com.iwedia.fti.rjio.fti.handler.RecommendedHandler
 * @see com.iwedia.fti.rjio.fti.handler.OverscanHandler
 * @see com.iwedia.fti.rjio.fti.handler.TimerHandler
 * @see com.iwedia.fti.rjio.fti.handler.UpdateHandler
 * @see com.iwedia.fti.rjio.fti.handler.UserProfileHandler
 * @see com.iwedia.fti.rjio.fti.utils.information_bus.events.Events
 * @see com.iwedia.fti.rjio.fti.utils.information_bus.events.InactivityRemainingTimeEvent
 * @see com.iwedia.fti.rjio.fti.utils.information_bus.events.InactivityShutdownEvent
 * @see com.iwedia.fti.rjio.fti.utils.information_bus.events.InactivityWarningEvent
 * @see com.iwedia.fti.rjio.fti.utils.information_bus.events.NewNotificationEvent
 * @see com.iwedia.fti.rjio.fti.utils.information_bus.events.UpdateProgressEvent
 * @see com.iwedia.fti.rjio.fti.utils.information_bus.Event
 * @see com.iwedia.fti.rjio.fti.utils.information_bus.EventListener
 * @see com.iwedia.fti.rjio.fti.utils.information_bus.InformationBus
 * @since 1.0
 */
package com.iwedia.fti.rjio.fti;