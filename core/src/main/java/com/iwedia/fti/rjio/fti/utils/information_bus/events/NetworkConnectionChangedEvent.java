package com.iwedia.fti.rjio.fti.utils.information_bus.events;


import com.iwedia.fti.rjio.fti.utils.information_bus.Event;

/**
 * Connectivity changed event
 *
 * @author Dragan Krnjaic
 */
public class NetworkConnectionChangedEvent extends Event {

    /**
     * Constructor
     *
     * @param isConnected is device connected to internet
     */
    public NetworkConnectionChangedEvent(boolean isConnected) {
        super(Events.NETWORK_CONNECTION, isConnected);
    }
}
