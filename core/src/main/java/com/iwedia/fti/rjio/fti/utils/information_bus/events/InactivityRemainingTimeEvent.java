package com.iwedia.fti.rjio.fti.utils.information_bus.events;

import com.iwedia.fti.rjio.fti.utils.information_bus.Event;

/**
 * Inactivity remaining time event
 *
 * @author Nikola Aleksic
 */
public class InactivityRemainingTimeEvent extends Event {

    /**
     * Constructor
     *
     * @param remainingTime remaining time
     */
    public InactivityRemainingTimeEvent(int remainingTime) {
        super(Events.INACTIVITY_REMAINING_TIME, remainingTime);
    }
}
