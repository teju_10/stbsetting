package com.iwedia.fti.rjio.fti.entities;

/**
 * Device Entity
 *
 * @author Nikola Aleksic
 */
public class DeviceEntity {

    /**
     * Id
     */
    protected int id;

    /**
     * Name
     */
    protected String name;

    /**
     * Product name
     */
    protected String product;

    /**
     * Device version
     */
    protected String version;

    /**
     * Device serial number
     */
    protected String serialNumber;

    /**
     * Data
     */
    protected Object data;

    /**
     * Constructor
     *
     * @param id           id
     * @param name         name
     * @param product      product
     * @param version      version
     * @param serialNumber serial number
     */
    public DeviceEntity(int id, String name, String product, String version, String serialNumber) {
        this.id = id;
        this.name = name;
        this.product = product;
        this.version = version;
        this.serialNumber = serialNumber;
    }

    /**
     * Get id
     *
     * @return id
     */
    public int getId() {
        return id;
    }

    /**
     * Get name
     *
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * Get product name
     *
     * @return product
     */
    public String getProduct() {
        return product;
    }

    /**
     * Get version
     *
     * @return version
     */
    public String getVersion() {
        return version;
    }

    /**
     * Get serial number
     *
     * @return serial number
     */
    public String getSerialNumber() {
        return serialNumber;
    }

    /**
     * Set data
     *
     * @param data data
     */
    public void setData(Object data) {
        this.data = data;
    }

    /**
     * Get data
     *
     * @return data
     */
    public Object getData() {
        return data;
    }
}
