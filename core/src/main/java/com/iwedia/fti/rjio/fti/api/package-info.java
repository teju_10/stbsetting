/**
 * Provides base interface structure independent from Android system.
 * <p>
 * <p>
 * Api package holds these interfaces:
 * <p>
 * <code>AppsAPI</code> with methods:
 * </ul>
 * <li><code><font color="#4169E1">requestFavoriteApps(AsyncDataReceiver<ArrayList<T>> callback)</font></code></li>
 * <li><code><font color="#4169E1">void requestInstalledApps(AsyncDataReceiver<ArrayList<T>> callback)</font></code></li>
 * <li><code><font color="#4169E1">void requestInstalledGames(AsyncDataReceiver<ArrayList<T>> callback)</font></code></li>
 * <li><code><font color="#4169E1">void launchApp(T appItem, AsyncReceiver callback)</font></code></li>
 * <li><code><font color="#4169E1">void removeApp(T appItem, AsyncReceiver callback)</font></code></li>
 * </ul>
 * <p>
 * <code>AsyncDataReceiver</code> with methods:
 * </ul>
 * <li><code><font color="#4169E1">void onSuccess(T data)</font></code></li>
 * <li><code><font color="#4169E1">void onFailed()</font></code></li>
 * </ul>
 * <p>
 * <code>AsyncReceiver</code> with methods:
 * </ul>
 * <li><code><font color="#4169E1">void onSuccess</font></code></li>
 * <li><code><font color="#4169E1">void onFailed()</font></code></li>
 * </ul>
 * <p>
 * <code>DeviceAPI</code> with methods:
 * </ul>
 * <li><code><font color="#4169E1">void onMounted(T device)</font></code></li>
 * <li><code><font color="#4169E1">void onUnmounted(T device)</font></code></li>
 * </ul>
 * <p>
 * <code>NetworkAPI</code> with methods:
 * </ul>
 * <li><code><font color="#4169E1">void requestNetworks(AsyncDataReceiver<List<T>> callback)</font></code></li>
 * <li><code><font color="#4169E1">void connectToWiFi(String wifiName, String wifiPassword, AsyncReceiver callback)</font></code></li>
 * </ul>
 * <p>
 * <code>NotificationAPI</code> with methods:
 * </ul>
 * <li><code><font color="#4169E1">void requestNotifications(AsyncDataReceiver<ArrayList<T>> callback)</font></code></li>
 * </ul>
 * <p>
 * <code>OverscanAPI</code> with methods:
 * </ul>
 * <li><code><font color="#4169E1">void onProgressValues(int scale, int horizontal, int vertical)</font></code></li>
 * </ul>
 * <p>
 * <code>RecommendedAPI</code> with methods:
 * </ul>
 * <li><code><font color="#4169E1">void requestRecommendedItems(AsyncDataReceiver<ArrayList<T>> callback)</font></code></li>
 * <li><code><font color="#4169E1">void requestPromotedItems(AsyncDataReceiver<ArrayList<T>> callback)</font></code></li>
 * <li><code><font color="#4169E1">void startRecommendedItem(T item)</font></code></li>
 * <li><code><font color="#4169E1">void startPromotedItem(T item)</font></code></li>
 * </ul>
 * <p>
 * <code>SignInAPI</code> with methods:
 * </ul>
 * <li><code><font color="#4169E1">void signIn(T user, AsyncReceiver callback)</font></code></li>
 * <li><code><font color="#4169E1">void signOut(T user, AsyncReceiver callback)</font></code></li>
 * </ul>
 * <p>
 * <code>SignUpAPI</code> with methods:
 * </ul>
 * <li><code><font color="#4169E1">void signUp(T user, AsyncReceiver callback)</font></code></li>
 * </ul>
 * <p>
 * <code>TimerAPI</code> with methods:
 * </ul>
 * <li><code><font color="#4169E1">void setTimeoutPeriod(int seconds)</font></code></li>
 * <li><code><font color="#4169E1">void startTimer()</font></code></li>
 * <li><code><font color="#4169E1">void resetTimer()</font></code></li>
 * <li><code><font color="#4169E1">void stopTimer()</font></code></li>
 * </ul>
 * <p>
 * <code>UpdateAPI</code> with methods:
 * </ul>
 * <li><code><font color="#4169E1">void requestUpdateAvailable(AsyncReceiver callback)</font></code></li>
 * <li><code><font color="#4169E1">void requestUpdateStart(AsyncReceiver callback)</font></code></li>
 * <li><code><font color="#4169E1">void requestUpdateCancel(AsyncReceiver callback)</font></code></li>
 * <li><code><font color="#4169E1">void requestUpdateApply(AsyncReceiver callback)</font></code></li>
 * </ul>
 * <p>
 * <code>UserProfileAPI</code> with methods:
 * </ul>
 * <li><code><font color="#4169E1">void requestUserData(T user, AsyncReceiver callback)</font></code></li>
 * </ul>
 *
 * @see com.iwedia.fti.rjio.fti.api.AppsAPI
 * @see com.iwedia.fti.rjio.fti.api.AsyncDataReceiver
 * @see com.iwedia.fti.rjio.fti.api.AsyncReceiver
 * @see com.iwedia.fti.rjio.fti.api.DeviceAPI
 * @see com.iwedia.fti.rjio.fti.api.OverscanAPI
 * @see com.iwedia.fti.rjio.fti.api.NetworkAPI
 * @see com.iwedia.fti.rjio.fti.api.NotificationsAPI
 * @see com.iwedia.fti.rjio.fti.api.RecommendedAPI
 * @see com.iwedia.fti.rjio.fti.api.SignInAPI
 * @see com.iwedia.fti.rjio.fti.api.SignUpAPI
 * @see com.iwedia.fti.rjio.fti.api.TimerAPI
 * @see com.iwedia.fti.rjio.fti.api.UpdateAPI
 * @see com.iwedia.fti.rjio.fti.api.UserProfileAPI
 * @since 1.0
 */
package com.iwedia.fti.rjio.fti.api;