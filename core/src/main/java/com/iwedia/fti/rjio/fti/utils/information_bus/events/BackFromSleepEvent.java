package com.iwedia.fti.rjio.fti.utils.information_bus.events;

import com.iwedia.fti.rjio.fti.utils.information_bus.Event;

/**
 * Inactivity start timer event
 *
 * @author Milan Novakovic
 */

public class BackFromSleepEvent extends Event {

    /**
     * Constructor
     */
    public BackFromSleepEvent() {
        super(Events.BACK_FROM_SLEEP);
    }
}
