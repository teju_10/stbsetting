package com.iwedia.fti.rjio.fti.api;

import com.iwedia.fti.rjio.fti.entities.User;

/**
 * Sign up api
 *
 * @author Veljko Ilkic
 */
public interface SignUpAPI<T extends User> {

    /**
     * Sign up
     *
     * @param user     user
     * @param callback callback
     */
    void signUp(T user, AsyncReceiver callback);
}
