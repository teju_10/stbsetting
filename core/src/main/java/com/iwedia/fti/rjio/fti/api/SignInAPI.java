package com.iwedia.fti.rjio.fti.api;

import com.iwedia.fti.rjio.fti.entities.User;

/**
 * Sign in API
 *
 * @author Veljko Ilkic
 */
public interface SignInAPI <T extends User> {

    /**
     * Sign in
     *
     * @param user     user
     * @param callback callback
     */
    void signIn(T user, AsyncReceiver callback);

    /**
     * Sign out
     *
     * @param user     user
     * @param callback callback
     */
    void signOut(T user, AsyncReceiver callback);
}
