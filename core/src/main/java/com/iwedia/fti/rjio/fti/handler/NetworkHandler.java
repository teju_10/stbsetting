package com.iwedia.fti.rjio.fti.handler;

import com.iwedia.fti.rjio.fti.api.NetworkAPI;
import com.iwedia.fti.rjio.fti.entities.WiFiItem;

/**
 * Network handler
 *
 * @author Nikola Aleksic
 */
public abstract class NetworkHandler<T extends WiFiItem> implements NetworkAPI<T> {

}
