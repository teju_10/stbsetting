package com.iwedia.fti.rjio.fti.api;

/**
 * Async data receiver
 *
 * @author Veljko Ilkic
 */
public interface AsyncDataReceiver<T> {

    /**
     * On success
     *
     * @param data data
     */
    void onSuccess(T data);

    /**
     * On failed
     */
    void onFailed();
}
