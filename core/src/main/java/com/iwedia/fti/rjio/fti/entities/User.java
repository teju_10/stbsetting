package com.iwedia.fti.rjio.fti.entities;

/**
 * User entity
 *
 * @author Veljko Ilkic
 */
public class User {

    /**
     * Id
     */
    protected int id;

    /**
     * Username
     */
    protected String username;

    /**
     * Password
     */
    protected String password;

    /**
     * Data
     */
    protected Object data;

    /**
     * Constructor
     *
     * @param id       id
     * @param username username
     * @param password password
     */
    public User(int id, String username, String password) {
        this.id = id;
        this.username = username;
        this.password = password;
    }

    /**
     * Get id
     *
     * @return id
     */
    public int getId() {
        return id;
    }

    /**
     * Get username
     *
     * @return username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Get password
     *
     * @return password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Set data
     *
     * @param data data
     */
    public void setData(Object data) {
        this.data = data;
    }

    /**
     * Get data
     *
     * @return data
     */
    public Object getData() {
        return data;
    }
}
