package com.iwedia.fti.rjio.fti.entities;

/**
 * WiFi Item Entity
 *
 * @author Nikola Aleksic
 */
public class WiFiItem {

    /**
     * Wifi id
     */
    private int id;

    /**
     * Wifi name
     */
    private String ssid;

    /**
     * Wifi password
     */
    private String password;

    /**
     * wifi capabilities
     */
    private String capabilities;

    /**
     * Constructor
     *
     * @param id          wifi id
     * @param ssid        wifi name
     * @param capabilities wifi capabilities
     */
    public WiFiItem(int id, String ssid, String capabilities) {
        this.id = id;
        this.ssid = ssid;
        this.capabilities = capabilities;
    }

    /**
     * Get wifi id
     *
     * @return wifi id
     */
    public int getId() {
        return id;
    }

    /**
     * Get wifi ssid
     *
     * @return wifi ssid
     */
    public String getSsid() {
        return ssid;
    }

    /**
     * Set wifi password
     *
     * @param password wifi password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Get wifi password
     *
     * @return password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Get capabilities
     *
     * @return capabilities
     */
    public String getCapabilities() {
        return capabilities;
    }
}
