package com.iwedia.fti.rjio.fti.api;

/**
 * Prefs API
 *
 * @author Dragan Savic
 */
public interface PrefsAPI {

    /**
     * Store value ti prefs
     *
     * @param tag   value tag
     * @param value value
     */
    void storeValue(String tag, Object value);

    /**
     * Get value from prefs
     *
     * @param tag          value tag
     * @param defaultValue default value
     * @return stored value
     */
    Object getValue(String tag, Object defaultValue);

}
