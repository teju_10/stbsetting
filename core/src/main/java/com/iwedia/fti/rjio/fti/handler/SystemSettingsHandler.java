package com.iwedia.fti.rjio.fti.handler;

import com.iwedia.fti.rjio.fti.api.SystemSettingsAPI;

/**
 * System Settings Handler
 *
 * @author Vadzim Dambrouski
 */
public abstract class SystemSettingsHandler implements SystemSettingsAPI {
}
