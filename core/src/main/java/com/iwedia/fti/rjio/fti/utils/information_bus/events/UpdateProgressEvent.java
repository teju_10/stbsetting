package com.iwedia.fti.rjio.fti.utils.information_bus.events;

import com.iwedia.fti.rjio.fti.utils.information_bus.Event;

/**
 * Update Progress Event
 *
 * @author Dragan Savic
 */
public class UpdateProgressEvent extends Event {

    /**
     * Update Progress Event
     *
     * @param progress progress
     */
    public UpdateProgressEvent(int progress) {
        super(Events.UPDATE_PROGRESS, progress);
    }
}
