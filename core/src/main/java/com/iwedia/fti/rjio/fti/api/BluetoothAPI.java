package com.iwedia.fti.rjio.fti.api;

import com.iwedia.fti.rjio.fti.entities.BluetoothDevice;

/**
 * Bluetooth API
 *
 * @author Dejan Nadj
 */
public interface BluetoothAPI<T extends BluetoothDevice> {

    /**
     * Unpair bluetooth device
     *
     * @param bluetoothDevice   bluetooth device
     * @param callback
     */
    void unpairDevice(T bluetoothDevice, AsyncReceiver callback);


}
