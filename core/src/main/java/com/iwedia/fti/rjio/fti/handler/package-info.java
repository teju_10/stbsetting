/**
 * Provides base handlers structure independent from Android system.
 * <p>
 * <p>
 * Handler package holds these handlers implemented from core API:
 * <p>
 * </ul>
 * <li><code>AccountHandler</code></li>
 * <li><code>AppsHandler</code></li>
 * <li><code>DeviceHandler</code></li>
 * <li><code>NetworkHandler</code></li>
 * <li><code>NotificationsHandler</code></li>
 * <li><code>OverscanHandler</code></li>
 * <li><code>RecommendedHandler</code></li>
 * <li><code>TimerHandler</code></li>
 * <li><code>UpdateHandler</code></li>
 * <li><code>UserProfileHandler</code></li>
 * </ul>
 *
 * @see com.iwedia.fti.rjio.fti.handler.AccountHandler
 * @see com.iwedia.fti.rjio.fti.handler.AppsHandler
 * @see com.iwedia.fti.rjio.fti.handler.DeviceHandler
 * @see com.iwedia.fti.rjio.fti.handler.NetworkHandler
 * @see com.iwedia.fti.rjio.fti.handler.NotificationsHandler
 * @see com.iwedia.fti.rjio.fti.handler.RecommendedHandler
 * @see com.iwedia.fti.rjio.fti.handler.OverscanHandler
 * @see com.iwedia.fti.rjio.fti.handler.TimerHandler
 * @see com.iwedia.fti.rjio.fti.handler.UpdateHandler
 * @see com.iwedia.fti.rjio.fti.handler.UserProfileHandler
 * @since 1.0
 */
package com.iwedia.fti.rjio.fti.handler;