package com.iwedia.fti.rjio.fti.utils.information_bus.events;

import com.iwedia.fti.rjio.fti.utils.information_bus.Event;

/**
 * Recommendation changed information bus event
 *
 * @author Milan Novakovic
 */
public class RecommendationChangedEvent extends Event {

    /**
     * Constructor
     *
     * @param data  new recommendation data
     */
    public RecommendationChangedEvent(Object data) {
        super(Events.RECOMMENDATION_CHANGED, data);
    }
}
