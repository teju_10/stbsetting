/**
 * Provides base entity models independent from Android system.
 * <p>
 * <p>
 * Entity package holds these models:
 * <p>
 * <code>AppItem</code> class is model entity for application item with methods:
 * <ul>
 * <li><code><font color="#4169E1">int getId()</font></code> - Returns application id</li>
 * <li><code><font color="#4169E1">String getName()</font></code> - Returns application name</li>
 * <li><code><font color="#4169E1">String getPackageName()</font></code> - Returns application package name</li>
 * <li><code><font color="#4169E1">T getIcon()</font></code> - Returns application icon</li>
 * <li><code><font color="#4169E1">int getType()</font></code> - Returns application type</li>
 * <li><code><font color="#4169E1">void setData(Object data)</font></code> - Sets application data</li>
 * <li><code><font color="#4169E1">Object getData()</font></code> - Returns application data</li>
 * <li><code><font color="#4169E1">String getBackgroundUrl()</font></code> - Returns application background url image</li>
 * </ul>
 * <p>
 * <code>DeviceEntity</code> class is model entity for usb device item with methods:
 * <ul>
 * <li><code><font color="#4169E1">int getId()</font></code> - Returns device id</li>
 * <li><code><font color="#4169E1">String getName()</font></code> - Returns device name</li>
 * <li><code><font color="#4169E1">String getProduct()</font></code> - Returns device product name</li>
 * <li><code><font color="#4169E1">String getVersion()</font></code> - Returns device version</li>
 * <li><code><font color="#4169E1">String getSerialNumber()</font></code> - Returns device serial number</li>
 * <li><code><font color="#4169E1">void setData(Object data)</font></code> - Sets device data</li>
 * <li><code><font color="#4169E1">Object getData()</font></code> - Returns device data</li>
 * </ul>
 * <p>
 * <code>User</code> class is model entity for User account with methods:
 * <ul>
 * <li><code><font color="#4169E1">int getId()</font></code> - Returns user's id</li>
 * <li><code><font color="#4169E1">String getUsername()</font></code> - Returns user's username</li>
 * <li><code><font color="#4169E1">String getPassword()</font></code> - Returns user's password</li>
 * <li><code><font color="#4169E1">void setData(Object data)</font></code> - Sets user's data</li>
 * <li><code><font color="#4169E1">Object getData()</font></code> - Returns user's data</li>
 * </ul>
 * <p>
 * <code>Notification</code> class is model entity for notifications with methods:
 * <ul>
 * <li><code><font color="#4169E1">int getId()</font></code> - Returns notification id</li>
 * <li><code><font color="#4169E1">String getText()</font></code> - Returns notification text</li>
 * <li><code><font color="#4169E1">T getIcon()</font></code> - Returns notification icon</li>
 * <li><code><font color="#4169E1">void setData(Object data)</font></code> - Sets notification data</li>
 * <li><code><font color="#4169E1">Object getData()</font></code> - Returns notification data</li>
 * </ul>
 * <p>
 * <code>RecommendedItem</code> class is model entity for recommended items with methods:
 * <ul>
 * <li><code><font color="#4169E1">int getId()</font></code> - Returns recommended item id</li>
 * <li><code><font color="#4169E1">String getName()</font></code> - Returns recommended item name</li>
 * <li><code><font color="#4169E1">T getIcon()</font></code> - Returns recommended item icon</li>
 * <li><code><font color="#4169E1">String getDescription()</font></code> - Returns recommended item description</li>
 * <li><code><font color="#4169E1">T getSourceIcon()</font></code> - Returns recommended item source icon</li>
 * <li><code><font color="#4169E1">RecommendedType getType()</font></code> - Returns recommended item type</li>
 * <li><code><font color="#4169E1">void setData(Object data)</font></code> - Sets recommended item data</li>
 * <li><code><font color="#4169E1">Object getData()</font></code> - Returns recommended item data</li>
 * </ul>
 * <p>
 * <code>WifiItem</code> class is model entity for wifi network with methods:
 * <ul>
 * <li><code><font color="#4169E1">int getId()</font></code> - Returns wifi id</li>
 * <li><code><font color="#4169E1">String getName()</font></code> - Returns wifi name</li>
 * <li><code><font color="#4169E1">boolean isConnected()</font></code> - Checks wifi connection</li>
 * <li><code><font color="#4169E1">void setConnected(boolean isConnected)</font></code> - Sets wifi connection</li>
 * <li><code><font color="#4169E1">void setData(Object data)</font></code> - Sets wifi data</li>
 * <li><code><font color="#4169E1">Object getData()</font></code> - Returns wifi data</li>
 * <li><code><font color="#4169E1">void setPassword(String password)</font></code> - Sets wifi password</li>
 * <li><code><font color="#4169E1">String getPassword()</font></code> - Returns wifi password</li>
 * </ul>
 *
 * @see com.iwedia.fti.rjio.fti.entities.AppItem
 * @see com.iwedia.fti.rjio.fti.entities.DeviceEntity
 * @see com.iwedia.fti.rjio.fti.entities.Notification
 * @see com.iwedia.fti.rjio.fti.entities.RecommendedItem
 * @see com.iwedia.fti.rjio.fti.entities.User
 * @see com.iwedia.fti.rjio.fti.entities.WiFiItem
 * @since 1.0
 */
package com.iwedia.fti.rjio.fti.entities;