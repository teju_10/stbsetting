package com.iwedia.fti.rjio.fti.utils.information_bus.events;

import com.iwedia.fti.rjio.fti.utils.information_bus.Event;

/**
 * Promotion changed information bus event
 *
 * @author Milan Novakovic
 */

public class PromotionChangedEvent extends Event {

    public PromotionChangedEvent(Object data) {
        super(Events.PROMOTION_CHANGED,data);
    }
}
