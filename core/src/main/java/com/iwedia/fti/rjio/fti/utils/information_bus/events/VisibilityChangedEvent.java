package com.iwedia.fti.rjio.fti.utils.information_bus.events;

import com.iwedia.fti.rjio.fti.utils.information_bus.Event;

/**
 * Recommendation changed information bus event
 *
 * @author Milan Novakovic
 */
public class VisibilityChangedEvent extends Event {

    /**
     * Constructor
     */
    public VisibilityChangedEvent() {
        super(Events.VISIBILITY_CHANGED);
    }
}