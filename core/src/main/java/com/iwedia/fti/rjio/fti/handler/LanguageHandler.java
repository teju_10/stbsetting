package com.iwedia.fti.rjio.fti.handler;

import com.iwedia.fti.rjio.fti.api.AsyncDataReceiver;
import com.iwedia.fti.rjio.fti.api.AsyncReceiver;
import com.iwedia.fti.rjio.fti.api.LanguageAPI;
import com.iwedia.fti.rjio.fti.entities.Language;
import com.iwedia.fti.rjio.fti.utils.information_bus.InformationBus;
import com.iwedia.fti.rjio.fti.utils.information_bus.events.LanguageChangedEvent;

import java.util.ArrayList;

/**
 * Language handler
 *
 * @author Dejan Nadj
 */
public abstract class LanguageHandler<T extends Language> implements LanguageAPI<T> {

    /**
     * Available languages
     */
    protected ArrayList<T> languages = new ArrayList<>();

    /**
     * Current language
     */
    protected T currentLanguage;


    @Override
    public void getLanguageCount(AsyncDataReceiver<Integer> receiver) {
        if (receiver != null) {
            receiver.onSuccess(languages.size());
        }
    }

    @Override
    public void getLanguage(String id, AsyncDataReceiver<T> receiver) {
        for (T language : languages) {
            if (language.getId().equals(id)) {
                if (receiver != null) {
                    receiver.onSuccess(language);
                }
            }
        }
    }

    @Override
    public void getLanguage(int index, AsyncDataReceiver<T> receiver) {
        if (receiver != null) {
            receiver.onSuccess(languages.get(index));
        }
    }

    @Override
    public void getLanguageList(AsyncDataReceiver<ArrayList<T>> receiver) {
        if (receiver != null) {
            receiver.onSuccess(languages);
        }
    }

    /**
     * Get language list for support tags
     *
     * @param langTags
     * @param receiver
     */
    public void getLanguageList(String[] langTags, AsyncDataReceiver<ArrayList<T>> receiver) {
        if (receiver != null) {
            ArrayList<T> supportLanguages = new ArrayList<>();

            for (Language lang : languages){
                for (int i = 0; i < langTags.length; i++){
                    if (lang.getId().equals(langTags[i]))
                        supportLanguages.add((T)lang);
                }
            }
            receiver.onSuccess(supportLanguages);
        }
    }

    @Override
    public void getCurrentLanguage(AsyncDataReceiver<T> receiver) {
        if (receiver != null) {
            receiver.onSuccess(currentLanguage);
        }
    }

    @Override
    public void setLanguage(T language) {
        this.currentLanguage = language;
        //Trigger information bus
        InformationBus.getInstance().submitEvent(new LanguageChangedEvent(currentLanguage));
    }
}
