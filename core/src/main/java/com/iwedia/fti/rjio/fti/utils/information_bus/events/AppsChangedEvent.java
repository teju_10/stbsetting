package com.iwedia.fti.rjio.fti.utils.information_bus.events;

import com.iwedia.fti.rjio.fti.utils.information_bus.Event;

/**
 * Apps list changed event
 *
 * @author Dejan Nadj
 */
public class AppsChangedEvent extends Event {

    /**
     * Apps changed event
     *
     * @param type  event type
     * @param data  new apps list
     */
    public AppsChangedEvent(int type, Object data) {
        super(type, data);
    }
}
