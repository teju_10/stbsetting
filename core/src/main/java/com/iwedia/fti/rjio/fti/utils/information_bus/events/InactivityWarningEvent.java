package com.iwedia.fti.rjio.fti.utils.information_bus.events;

import com.iwedia.fti.rjio.fti.utils.information_bus.Event;

/**
 * Inactivity Detected Event
 *
 * @author Nikola Aleksic
 */
public class InactivityWarningEvent extends Event {

    /**
     * Constructor
     */
    public InactivityWarningEvent() {
        super(Events.INACTIVITY_WARNING);
    }
}
