package com.iwedia.fti.rjio.fti.utils.information_bus.events;

import com.iwedia.fti.rjio.fti.utils.information_bus.Event;

/**
 * Time changed event
 *
 * @author Dejan Nadj
 */
public class TimeChangedEvent extends Event {

    /**
     * Constructor
     */
    public TimeChangedEvent() {
        super(Events.TIME_CHANGED);
    }
}
