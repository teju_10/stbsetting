package com.iwedia.fti.rjio.fti.utils.information_bus;

import java.util.HashSet;
import java.util.Set;

/**
 * Event listener on information bus
 *
 * @author Dragan Krnjaic
 */
public abstract class EventListener {

    /**
     * Event types
     */
    private Set<Integer> eventTypes = new HashSet<Integer>();

    /**
     * Constructor
     */
    public EventListener() {
    }

    /**
     * Add event type
     * <p>
     * Add event that should be monitored and received
     *
     * @param type event type
     */
    public void addType(int type) {
        this.eventTypes.add(type);
    }

    /**
     * Is contains type
     *
     * @param type event type
     * @return is event type contained
     */
    public boolean isContainsType(int type) {
        return this.eventTypes.contains(type);
    }

    /**
     * Has types
     *
     * @return has types defined
     */
    public boolean hasTypes() {
        return eventTypes.size() > 0;
    }

    /**
     * Callback function
     *
     * @param event received event
     */
    abstract public void callback(Event event);
}
