package com.iwedia.fti.rjio.fti.handler;

import com.iwedia.fti.rjio.fti.api.UserProfileAPI;
import com.iwedia.fti.rjio.fti.entities.User;

/**
 * User profile handler
 *
 * @author Dragan Krnjaic
 */
public abstract class UserProfileHandler<T extends User> implements UserProfileAPI<T> {

    /**
     * Current user
     */
    protected T user;

    /**
     * Get user
     *
     * @return user
     */
    public T getUser() {
        return user;
    }
}
