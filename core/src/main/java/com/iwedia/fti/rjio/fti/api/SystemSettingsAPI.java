package com.iwedia.fti.rjio.fti.api;

/**
 * API for access to System Settings
 *
 * @author Vadzim Dambrouski
 */
public interface SystemSettingsAPI {

    /**
     * Set the first time setup completed flag. When set to true will
     * allow the user to press home button.
     *
     * @param complete FTI state
     * @param callback result callback
     */
    void setUserSetupComplete(boolean complete, AsyncReceiver callback);

    /**
     * Set device name on first time installation.
     *
     * @param deviceName new device name
     */
    void setDeviceName(String deviceName, AsyncReceiver callback);

    /**
     * Set default timezone
     */
    void setDefaultTimeZone();

    /**
     * Disable HDMI CEC
     */
    void disableHdmiCec();
}
