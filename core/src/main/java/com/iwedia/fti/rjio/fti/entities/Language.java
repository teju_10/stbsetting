package com.iwedia.fti.rjio.fti.entities;

/**
 * Language entity
 *
 * @author Dejan Nadj
 */
public class Language {

    /**
     * Id
     */
    private String id;

    /**
     * Name
     */
    private String name;

    /**
     * Constructor
     *
     * @param id   id
     * @param name name
     */
    public Language(String id, String name) {
        this.id = id;
        this.name = name;
    }

    /**
     * Get id
     *
     * @return id
     */
    public String getId() {
        return id;
    }

    /**
     * Get name
     *
     * @return name
     */
    public String getName() {
        return name;
    }
}
