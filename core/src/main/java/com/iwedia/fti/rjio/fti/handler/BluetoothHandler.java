package com.iwedia.fti.rjio.fti.handler;

import com.iwedia.fti.rjio.fti.api.BluetoothAPI;
import com.iwedia.fti.rjio.fti.entities.BluetoothDevice;

/**
 * Bluetooth handler
 *
 * @author Dejan Nadj
 */
public abstract class BluetoothHandler<T extends BluetoothDevice> implements BluetoothAPI<T> {
}
