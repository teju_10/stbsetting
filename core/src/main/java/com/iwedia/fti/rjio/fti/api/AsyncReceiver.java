package com.iwedia.fti.rjio.fti.api;

/**
 * Async receiver
 *
 * @author Veljko Ilkic
 */
public interface AsyncReceiver {

    /**
     * On success
     */
    void onSuccess();

    /**
     * On failed
     */
    void onFailed();
}
