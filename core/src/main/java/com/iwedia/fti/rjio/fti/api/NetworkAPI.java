package com.iwedia.fti.rjio.fti.api;

import com.iwedia.fti.rjio.fti.entities.WiFiItem;

import java.util.List;

/**
 * Network API
 *
 * @author Nikola Aleksic
 */
public interface NetworkAPI<T extends WiFiItem> {

    /**
     * Request list of wifi networks
     *
     * @param callback callback
     */
    void requestNetworks(AsyncDataReceiver<List<T>> callback);

    /**
     * Connect to wifi network
     *
     * @param wifiName     wifi name
     * @param wifiPassword wifi password
     * @param callback     callback
     * @param capabilities wifi capabilities
     */
    void connectToWiFi(String wifiName, String wifiPassword, String capabilities, AsyncReceiver callback);

    /**
     * Set IP Ethernet
     *
     * @param ipAddress ipAddress
     */
    void setIPEthernet(String ipAddress, AsyncReceiver callback);

    /**
     * Set Mask Ethernet
     *
     * @param maskAddress maskAddress
     */
    void setMaskEthernet(String maskAddress, AsyncReceiver callback);

    /**
     * Set Gateway Ethernet
     *
     * @param gatewayAddress gatewayAddress
     */
    void setGatewayEthernet(String gatewayAddress, AsyncReceiver callback);

    /**
     * Set DNS Ethernet
     *
     * @param dnsAddress dnsAddress
     */
    void setDNSEthernet(String dnsAddress, AsyncReceiver callback);
}
