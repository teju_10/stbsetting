package com.rjil.jiostbsetting

import androidx.appcompat.app.AppCompatActivity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.text.Html
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.widget.Button
import android.widget.CompoundButton
import android.widget.RadioGroup
import android.widget.Switch
import android.widget.TextView
import android.widget.Toast

import com.rjil.jiostbsetting.utils.Constant

import java.io.ByteArrayOutputStream
import java.io.IOException
import java.io.InputStream

class PrivacyPolicyActivity : AppCompatActivity() {

    private var desc: TextView? = null
    lateinit var privacy_switch_item: Switch
    lateinit var mContext: Context
    //  private Button accept;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_privacy_policy)
        mContext = this
        desc = findViewById(R.id.desc)
        privacy_switch_item = findViewById(R.id.privacy_switch_item)
        // accept = findViewById(R.id.accept);
        desc!!.text = Html.fromHtml(setText())
        privacy_switch_item.isChecked = Constant.readPrivacyOption(mContext, "jio_data_opted_out")

        checkSwitch()

        //        accept.setOnClickListener(new View.OnClickListener() {
        //            @Override
        //            public void onClick(View v) {
        ////                Intent intent=new Intent(PrivacyPolicyActivity.this, SetupCompleteActivity.class);
        ////                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        ////                startActivity(intent);
        ////                finish();
        //            }
        //        });
    }

    private fun checkSwitch() {

        privacy_switch_item!!.setOnCheckedChangeListener { _, b ->
            if (b) {
                Constant.writePrivacyOption(mContext.applicationContext!!, "jio_data_opted_out", true)
            } else {
                Constant.writePrivacyOption(mContext.applicationContext!!, "jio_data_opted_out", false)
            }
        }
    }

    private fun setText(): String {

        val inputStream = resources.openRawResource(R.raw.privacy)
        val byteArrayOutputStream = ByteArrayOutputStream()
        var i: Int
        try {
            i = inputStream.read()
            while (i != -1) {
                byteArrayOutputStream.write(i)
                i = inputStream.read()
            }
            inputStream.close()
        } catch (e: IOException) {
            // TODO Auto-generated catch block
            e.printStackTrace()
        }

        return byteArrayOutputStream.toString()
    }


    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        super.onKeyDown(keyCode, event)
        //        if (event.getAction() == KeyEvent.ACTION_DOWN) {
        //            if (keyCode == KeyEvent.KEYCODE_DPAD_CENTER || keyCode == KeyEvent.KEYCODE_ENTER) {
        //                Handler handler = new Handler();
        //                if ((event.getScanCode() != 28) && (event.getScanCode() != 76)) {    // Check for Enter on USB keyboard
        //                    handler.postDelayed(new Runnable() {
        //                        @Override
        //                        public void run() {
        ////                            Intent intent = new Intent(PrivacyPolicyActivity.this, SetupCompleteActivity.class);
        ////                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        ////                            startActivity(intent);
        ////                            finish();
        //                        }
        //                    }, 2000);
        //                } else {
        //                    handler.post(new Runnable() {
        //                        @Override
        //                        public void run() {
        ////                            Intent intent = new Intent(PrivacyPolicyActivity.this, SetupCompleteActivity.class);
        ////                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        ////                            startActivity(intent);
        ////                            Toast.makeText(RjioApplication.get(), R.string.information_skipped_pairing, Toast.LENGTH_SHORT).show();
        ////                            finish();
        //                        }
        //                    });
        //                }
        //                return true;
        //            }
        //        } else

        return true
    }
}
