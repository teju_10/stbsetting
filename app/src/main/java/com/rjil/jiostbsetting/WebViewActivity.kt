package com.rjil.jiostbsetting

import android.annotation.SuppressLint
import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.text.TextUtils
import android.util.Log
import android.view.KeyEvent
import android.webkit.URLUtil
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_web.*
import java.io.*
import java.lang.reflect.Method
import java.util.zip.GZIPInputStream


class WebViewActivity : AppCompatActivity() {

    val DEFAULT_LICENSE_PATH = "/system/etc/NOTICE.html.gz"
    val PROPERTY_LICENSE_PATH = "ro.config.license_path"
    val STATUS_OK = 0
    val STATUS_NOT_FOUND = 1
    val STATUS_READ_ERROR = 2
    val STATUS_EMPTY_FILE = 3
    lateinit var strTitle: String
    var mHandler: Handler? = null
    var webview: WebView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        hookWebView()
        setContentView(R.layout.activity_web_view)
        strTitle = intent.getStringExtra("title")
        val header = findViewById<TextView>(R.id.headerText)
        header.setText(strTitle)

        webview = findViewById<WebView>(R.id.webview)
        webview?.webViewClient = MyWebViewClient()
        webview?.settings?.javaScriptEnabled = true
        webview?.setBackgroundColor(0x00000000)
        val fileName = System.getenv(PROPERTY_LICENSE_PATH) ?: DEFAULT_LICENSE_PATH
        Log.e("Setting", "" + fileName)
        val file = File(fileName)
        Log.e("Setting File", "" + file)

        mHandler = object : Handler() {
            override fun handleMessage(msg: Message) {
                super.handleMessage(msg)
                if (msg.what === STATUS_OK) {
                    val text = msg.obj as String
                    Log.e("Urlllllll", text)
                    showPageOfText(text)
                } else {
                    showErrorAndFinish()
                }
            }
        }

        // Start separate thread to do the actual loading.
        val thread = Thread(LicenseFileLoader(fileName, mHandler as Handler))
        thread.start()

    }

    private fun showPageOfText(text: String) {
        webview?.loadDataWithBaseURL(null, text, "text/html", "utf-8", null)

    }

    fun showErrorAndFinish() {
        Toast.makeText(this, "settings_license_activity_unavailable", Toast.LENGTH_LONG)
                .show()
        finish()
    }


    override fun onKeyUp(keyCode: Int, event: KeyEvent?): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (webview != null && webview!!.canGoBack()) {
                webview!!.goBack()
            } else {
                finish()
            }
            return true
        }
        return super.onKeyUp(keyCode, event)
    }

    inner class LicenseFileLoader(private val mFileName: String, private val mHandler: Handler) : Runnable {
        val INNER_TAG = "WebActivity.LicenseFileLoader"
        override fun run() {
            var status = STATUS_OK
            var inputReader: InputStreamReader? = null
            val data = StringBuilder(2048)
            try {
                val tmp = CharArray(2048)
                var numRead: Int = -1
                if (mFileName.endsWith(".gz")) {
                    inputReader = InputStreamReader(
                            GZIPInputStream(FileInputStream(mFileName)))
                } else {
                    inputReader = FileReader(mFileName)
                }

                while (inputReader.read(tmp) >= 0) {
                    if (numRead != -1)
                        data.append(tmp, 0, numRead)
                    numRead = inputReader.read(tmp)
                }
            } catch (e: FileNotFoundException) {
                Log.e(INNER_TAG, "License HTML file not found at $mFileName", e)
                status = STATUS_NOT_FOUND
            } catch (e: IOException) {
                Log.e(INNER_TAG, "Error reading license HTML file at $mFileName", e)
                status = STATUS_READ_ERROR
            } finally {
                try {
                    if (inputReader != null) {
                        inputReader!!.close()
                    }
                } catch (e: IOException) {
                }

            }
            if (status == STATUS_OK && TextUtils.isEmpty(data)) {
                Log.e(INNER_TAG, "License HTML is empty (from $mFileName)")
                status = STATUS_EMPTY_FILE
            }
            // Tell the UI thread that we are finished.
            val msg = mHandler.obtainMessage(status, null)
            if (status == STATUS_OK) {
                msg.obj = data.toString()
            }
            mHandler.sendMessage(msg)
        }
    }

    inner class MyWebViewClient : WebViewClient() {
        override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
            if (URLUtil.isNetworkUrl(url)) {
                return false
            }
            try {
                val shareIntent = Intent()
                shareIntent.action = Intent.ACTION_VIEW
                shareIntent.data = Uri.parse(url)
                startActivity(shareIntent)
            } catch (e: ActivityNotFoundException) {
                Toast.makeText(this@WebViewActivity, "Appropriate app not found", Toast.LENGTH_LONG).show()
                Log.e("AndroidRide", e.toString())
            }
            return true
        }

        @RequiresApi(Build.VERSION_CODES.N)
        override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
            val url = request?.url.toString()
            if (URLUtil.isNetworkUrl(url)) {
                return false
            }
            try {
                val shareIntent = Intent()
                shareIntent.action = Intent.ACTION_VIEW
                shareIntent.data = Uri.parse(url)
                startActivity(shareIntent)
            } catch (e: ActivityNotFoundException) {
                Toast.makeText(this@WebViewActivity, "Appropriate app not found", Toast.LENGTH_LONG).show()
                Log.e("AndroidRide", e.toString())
            }
            return true

        }

    }

    @SuppressLint("DiscouragedPrivateApi", "PrivateApi")
    fun hookWebView() {
        val sdkInt = Build.VERSION.SDK_INT
        try {
            val factoryClass = Class.forName("android.webkit.WebViewFactory")
            val field = factoryClass.getDeclaredField("sProviderInstance")
            field.isAccessible = true
            var sProviderInstance = field.get(null)
            if (sProviderInstance != null) {
                Log.e("Webview", "sProviderInstance isn't null")
                return
            }
            val getProviderClassMethod: Method
            if (sdkInt > 22) { // above 22
                getProviderClassMethod = factoryClass.getDeclaredMethod("getProviderClass")
            } else if (sdkInt == 22) { // method name is a little different
                getProviderClassMethod = factoryClass.getDeclaredMethod("getFactoryClass")
            } else { // no security check below 22
                Log.e("WebView", "Don't need to Hook WebView")
                return
            }
            getProviderClassMethod.setAccessible(true)
            val providerClass = getProviderClassMethod.invoke(factoryClass) as Class<*>
            val delegateClass = Class.forName("android.webkit.WebViewDelegate")
            val providerConstructor = providerClass.getConstructor(delegateClass)
            if (providerConstructor != null) {
                providerConstructor.isAccessible = true
                val declaredConstructor = delegateClass.getDeclaredConstructor()
                declaredConstructor.isAccessible = true
                sProviderInstance = providerConstructor.newInstance(declaredConstructor.newInstance())
                Log.e("sProviderInstance:{}", "" + sProviderInstance)
                field.set("sProviderInstance", sProviderInstance)
            }
            Log.e("Webview", "Hook done!")
        } catch (e: Throwable) {
            Log.e("Webview", "" + e)
        }

    }
}
