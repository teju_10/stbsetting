package com.rjil.jiostbsetting.listener

import android.util.Log
import android.view.View
import androidx.recyclerview.widget.RecyclerView

interface OnItemClickListener {
    fun onItemViewClick(position: Int, view: View)
    fun onItemViewFocusChanged(gainFocus: Boolean, view: View, position: Int)

//    fun onLayoutChange(lastSelectedPosition: Int, holder: RecyclerView.ViewHolder?) {
//        var recyclerView: RecyclerView? = null
//
//        recyclerView!!.addOnLayoutChangeListener(View.OnLayoutChangeListener { view, i, i2, i3, i4, i5, i6, i7, i8 ->
//            try {
//                if (view != null && lastSelectedPosition >= 0) {
////                vPos = recyclerView.findViewHolderForAdapterPosition(lastSelectedPosition)
////                vPos!!.itemView.requestFocus()
//                    holder!!.itemView.requestFocus()
//                    Log.d("CurrentFocusedView", "Focused")
//                } else {
//                    Log.d("Change Lay Selected Pos", lastSelectedPosition.toString())
//                }
//            } catch (e: java.lang.Exception) {
//                e.printStackTrace()
//            }
//        })
//
//    }

}
