package com.rjil.jiostbsetting;

import android.app.Application;
import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;

import com.iwedia.fti.rjio.fti_sdk.RjioSdk;

public class SettingApplication extends Application {

     static SettingApplication instance;
     WifiManager wifiManager;
     static String ssid;


    public static SettingApplication get() {
        return instance;
    }
    @Override
    public void onCreate() {
        super.onCreate();
        instance=this;
        RjioSdk.get().setup(this);
       /* wifiManager= (WifiManager) this.getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInfo=wifiManager.getConnectionInfo();
        SettingApplication.setSSID(wifiInfo.getSSID().replace("\"", ""));*/
    }

    public static String getSSID() {
        return ssid;
    }

    public static void setSSID(String ssid) {
        SettingApplication.ssid = ssid;
    }
}
