package com.rjil.jiostbsetting.adapters.Language_Keyboard

import android.content.Context
import android.graphics.Color
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.widget.TextView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.RadioButton
import com.rjil.jiostbsetting.R
import android.os.Build
import android.renderscript.Sampler
import android.util.DisplayMetrics
import android.util.Log
import android.view.animation.AnimationUtils
import android.widget.RelativeLayout
import com.rjil.jiostbsetting.holder.RecyclerViewHolder
import com.rjil.jiostbsetting.utils.SharedPreferance
import java.util.*
import kotlin.collections.ArrayList


class LangChangeAdapter(val mContext: Context, val value: ArrayList<String>) : RecyclerView.Adapter<LangChangeAdapter.ViewHolder>() {

    var lastSelectedPosition = -1
     var checkedItem = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LangChangeAdapter.ViewHolder {
        val view1 = LayoutInflater.from(mContext)
                .inflate(R.layout.item_changelang_list, parent, false)

        val vh = ViewHolder(view1)

        vh.itemView.setOnFocusChangeListener(View.OnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                vh.relativeLyout!!.background = mContext!!.resources.getDrawable(R.drawable.rl_shape_corner)
                startFocusAnimation(vh, true)
            } else {
                vh.relativeLyout!!.background = mContext!!.resources.getDrawable(R.drawable.rv_focused_bg)
                startFocusAnimation(vh, false)
            }
        })

        return ViewHolder(view1)
    }

//    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
//        super.onAttachedToRecyclerView(recyclerView)
//        Log.d("ItemListAdapter", "onAttachedToRecyclerView ")
////        this.recyclerView = recyclerView
//    }

    override fun onBindViewHolder(Vholder: ViewHolder, position: Int) {
        Vholder?.textView!!.text = value[position]
        Vholder?.textView.setTextColor(Color.BLACK)
        Vholder.rb_lang_select!!.isChecked = lastSelectedPosition == position

        Vholder.rb_lang_select.isChecked = position == checkedItem


        //Vholder.rb_lang_select.isChecked = value[position].equals(SharedPreferance.getLang(mContext))
//        if (!Vholder.rb_lang_select.isChecked) {
//            Vholder.rb_lang_select.isChecked = true
//        } else {
//            Vholder.rb_lang_select.isChecked = true
//        }


        if (position == 0) {
            Vholder.itemView.requestFocus()
            Vholder.relativeLyout!!.background = mContext!!.resources.getDrawable(R.drawable.rl_shape_corner)
            val anim = AnimationUtils.loadAnimation(mContext, R.anim.anim_scale_big)
            Vholder.itemView.startAnimation(anim)
            anim.fillAfter = true
        }

//        Vholder.itemView.setOnFocusChangeListener { view, b ->
//            if (b) {
//
//                    Vholder.relativeLyout!!.background = mContext!!.resources.getDrawable(R.drawable.rl_shape_corner)
//                    val anim = AnimationUtils.loadAnimation(mContext, R.anim.anim_scale_big)
//                    Vholder.itemView.startAnimation(anim)
//                    anim.fillAfter = true
//            } else {
//                Vholder.relativeLyout!!.background = mContext!!.resources.getDrawable(R.drawable.rv_focused_bg)
//                val anim = AnimationUtils.loadAnimation(mContext, R.anim.anim_scale_small)
//                Vholder.itemView.startAnimation(anim)
//                anim.fillAfter = true
//            }
//        }

    }

    private fun startFocusAnimation(holder: ViewHolder, b: Boolean) {
        if (b) {
            val anim = AnimationUtils.loadAnimation(mContext, R.anim.anim_scale_big)
            holder.itemView.startAnimation(anim)
            anim.fillAfter = true
        } else {
            val anim = AnimationUtils.loadAnimation(mContext, R.anim.anim_scale_small)
            holder.itemView.startAnimation(anim)
            anim.fillAfter = true
        }
    }

    fun checkItem(position: Int) {
        val previousCheckedItem = checkedItem
        checkedItem = position
        notifyItemChanged(previousCheckedItem)
        notifyItemChanged(checkedItem)
    }

    fun refresh(items: java.util.ArrayList<String>) {
        checkedItem = -1
        this.value.clear()
        this.value.addAll(items)
        notifyDataSetChanged()
    }

    class ViewHolder(v: View) : RecyclerView.ViewHolder(v) {

        var rb_lang_select: RadioButton = v.findViewById(R.id.lang_select)
        var textView: TextView = v.findViewById(R.id.tvLang)
        var relativeLyout: RelativeLayout = v.findViewById(R.id.relativeLyout)
    }

    override fun getItemCount(): Int {
        return value.size
    }

}

