package com.rjil.jiostbsetting.adapters.apps

import android.content.Context
import android.graphics.drawable.GradientDrawable
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import com.rjil.jiostbsetting.R
import com.rjil.jiostbsetting.holder.DetailsRecyclerViewHolder
import com.rjil.jiostbsetting.models.PermissionInfos

class AppPermissionAdapters(val context: Context, var items: ArrayList<PermissionInfos>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var previousFocusedViewHolder: DetailsRecyclerViewHolder? = null
    private var previouslyFocusedPos = 0
    private var currentlyFocusedPos = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return DetailsRecyclerViewHolder(View.inflate(context, R.layout.item_details_recyclerview, null))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        val viewHolder = holder as DetailsRecyclerViewHolder
        viewHolder.mName?.text = items[position].groupName;
        viewHolder.mValue?.visibility = View.GONE
        viewHolder.mArrowIcon?.visibility = View.GONE
        viewHolder.mSwitchItem?.visibility = View.VISIBLE
        viewHolder.mFrameLayout?.tag = items[position]
        viewHolder.mSwitchItem?.isChecked = items.get(position).pGranted

        if (position == 0) {
            holder.itemView.requestFocus()
            holder.relativeLyout!!.background = context!!.resources.getDrawable(R.drawable.rl_shape_corner)
            val anim = AnimationUtils.loadAnimation(context, R.anim.anim_scale_big)
            holder.itemView.startAnimation(anim)
            anim.fillAfter = true
        }

        holder.itemView.setOnFocusChangeListener { view, b ->
            if (b) {
                holder.relativeLyout!!.background = context!!.resources.getDrawable(R.drawable.rl_shape_corner)
                val anim = AnimationUtils.loadAnimation(context, R.anim.anim_scale_big)
                holder.itemView.startAnimation(anim)
                anim.fillAfter = true
            } else {
                holder.relativeLyout!!.background = context!!.resources.getDrawable(R.drawable.rv_focused_bg)
                val anim = AnimationUtils.loadAnimation(context, R.anim.anim_scale_small)
                holder.itemView.startAnimation(anim)
                anim.fillAfter = true
            }
        }

    }

    private fun startFocusAnimation(holder: DetailsRecyclerViewHolder, hasFocus: Boolean) {
        if (hasFocus) {
            previousFocusedViewHolder?.let {
                holder.relativeLyout!!.background = context!!.resources.getDrawable(R.drawable.rl_shape_corner)
                val anim = AnimationUtils.loadAnimation(context, R.anim.anim_scale_big)
                holder.itemView.startAnimation(anim)
                anim.fillAfter = true

            }
        } else {
            holder.relativeLyout!!.background = context!!.resources.getDrawable(R.drawable.rv_focused_bg)
            val anim = AnimationUtils.loadAnimation(context, R.anim.anim_scale_small)
            holder.itemView.startAnimation(anim)
            anim.fillAfter = true
        }
    }

    private fun updateFocusPositions(viewHolder: DetailsRecyclerViewHolder, hasFocus: Boolean, position: Int) {
        if (hasFocus) {
            previouslyFocusedPos = currentlyFocusedPos
            currentlyFocusedPos = position

        } else {
            previousFocusedViewHolder = viewHolder
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

}


