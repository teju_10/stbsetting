package com.rjil.jiostbsetting.adapters.network

import android.content.Context
import android.content.Context.WIFI_SERVICE
import android.graphics.drawable.GradientDrawable
import android.net.wifi.ScanResult
import android.net.wifi.WifiInfo
import android.net.wifi.WifiManager
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import android.text.format.Formatter
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import com.rjil.jiostbsetting.R
import com.rjil.jiostbsetting.holder.RecyclerViewHolder
import com.rjil.jiostbsetting.holder.WifiSettingRecyclerViewHolder

class ConnectedWifiAdapter(val context: Context, var items: ArrayList<String>, var scanResult: ScanResult) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    var wifiManager: WifiManager? = null
    var info: WifiInfo? = null

    private var previousFocusedViewHolder: WifiSettingRecyclerViewHolder? = null
    private var previouslyFocusedPos = 0
    private var currentlyFocusedPos = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        wifiManager = context.getSystemService(WIFI_SERVICE) as WifiManager?
        info = wifiManager!!.connectionInfo
        return WifiSettingRecyclerViewHolder(View.inflate(context, R.layout.item_wifi_details_recyclerview, null))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = holder as WifiSettingRecyclerViewHolder
        viewHolder.mName?.text = items[position]
        viewHolder.mFrameLayout?.tag = items[position]
        viewHolder.mValue?.visibility = View.GONE
        viewHolder.mArrowIcon?.visibility = View.VISIBLE
        //check is connected or not
        if (position == 0) {
            viewHolder.mFrameLayout?.isFocusable = true
            viewHolder.mValue?.visibility = View.VISIBLE
            viewHolder.mValue?.text = scanResult.SSID
            viewHolder.mArrowIcon?.visibility = View.INVISIBLE

        } else if (position == 2) {
            viewHolder.mFrameLayout?.isFocusable = false
            viewHolder.mFrameLayout?.isEnabled = false
            viewHolder.mValue?.visibility = View.VISIBLE
            viewHolder.mArrowIcon?.visibility = View.INVISIBLE
            viewHolder.mValue?.text = getIpAddress(this!!.info!!)
        } else if (position == 3) {
            viewHolder.mFrameLayout?.isFocusable = false
            viewHolder.mFrameLayout?.isEnabled = false
            viewHolder.mValue?.visibility = View.VISIBLE
            viewHolder.mValue?.text = getSignalStrength()
            viewHolder.mArrowIcon?.visibility = View.INVISIBLE
        }

//        else if( position==4){
//            viewHolder.mFrameLayout?.isFocusable=false
//            viewHolder.mValue?.visibility = View.VISIBLE
//            viewHolder.mArrowIcon?.visibility = View.INVISIBLE
//        }
//        else if(position==6){
//            viewHolder.mFrameLayout?.isFocusable=false
//            viewHolder.mValue?.visibility = View.VISIBLE
//            viewHolder.mArrowIcon?.visibility = View.INVISIBLE
//        }
//
//        if(position==0){
//            viewHolder.mValue?.visibility = View.VISIBLE
//            viewHolder.mValue?.text=scanResult.SSID
//            viewHolder.mArrowIcon?.visibility = View.INVISIBLE
//        }else if(position==2){
//            viewHolder.mFrameLayout?.isFocusable=false
//            viewHolder.mFrameLayout?.isEnabled=false
//            viewHolder.mValue?.visibility = View.VISIBLE
//            viewHolder.mArrowIcon?.visibility = View.INVISIBLE
//            viewHolder.mValue?.text=getIpAddress(this!!.info!!)
//        }
//        else if(position==3){
//            viewHolder.mFrameLayout?.isFocusable=false
//            viewHolder.mFrameLayout?.isEnabled=false
//            viewHolder.mValue?.visibility = View.VISIBLE
//            viewHolder.mArrowIcon?.visibility = View.INVISIBLE
//        }
//        else if( position==4){
//            viewHolder.mFrameLayout?.isFocusable=false
//            viewHolder.mValue?.visibility = View.VISIBLE
//            viewHolder.mArrowIcon?.visibility = View.INVISIBLE
//        }
//        else if(position==6){
//            viewHolder.mFrameLayout?.isFocusable=false
//            viewHolder.mValue?.visibility = View.VISIBLE
//            viewHolder.mArrowIcon?.visibility = View.INVISIBLE
//        }


//        val drawable = viewHolder.mFrameLayout?.background as GradientDrawable
//        drawable.setColor(ContextCompat.getColor(context, R.color.list_item_def_color))
        if (position == 0) {
            holder.itemView.requestFocus()
            holder.relative_wifi_detail!!.background = context!!.resources.getDrawable(R.drawable.rl_shape_corner)
            val anim = AnimationUtils.loadAnimation(context, R.anim.anim_scale_big)
            holder.itemView.startAnimation(anim)
            anim.fillAfter = true
        }
        holder.itemView.setOnFocusChangeListener { view, b ->
            if (b) {
                holder.relative_wifi_detail!!.background = context!!.resources.getDrawable(R.drawable.rl_shape_corner)
                val anim = AnimationUtils.loadAnimation(context, R.anim.anim_scale_big)
                holder.itemView.startAnimation(anim)
                anim.fillAfter = true

            } else {
                holder.relative_wifi_detail!!.background = context!!.resources.getDrawable(R.drawable.rv_focused_bg)
                val anim = AnimationUtils.loadAnimation(context, R.anim.anim_scale_small)
                holder.itemView.startAnimation(anim)
                anim.fillAfter = true
            }
        }

    }

    fun getIpAddress(wifiInfo: WifiInfo): String? {
        var ip: Int
        ip = wifiInfo.ipAddress
        var ipaddress: String = Formatter.formatIpAddress(ip);
        return ipaddress
    }

    override fun getItemCount(): Int {
        return items.size
    }

    private fun getSignalStrength(): String {
        val signalLevels = context?.resources?.getStringArray(R.array.wifi_signal_strength)
        //    val wifiManager = context.getSystemService(Context.WIFI_SERVICE) as WifiManager
        val numberOfLevels = 4
        val wifiInfo = wifiManager?.connectionInfo
        val level = WifiManager.calculateSignalLevel(wifiInfo!!.rssi, numberOfLevels)
        return signalLevels?.get(level)!!
    }
}