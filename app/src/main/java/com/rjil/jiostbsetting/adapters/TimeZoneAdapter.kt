package com.rjil.jiostbsetting.adapters

import android.content.Context
import android.graphics.drawable.GradientDrawable
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.rjil.jiostbsetting.R
import com.rjil.jiostbsetting.holder.TimeZoneRecyclerViewHolder
import com.rjil.jiostbsetting.models.TimeZoneInfo
import java.util.*

class TimeZoneAdapter(val context: Context, var items: ArrayList<TimeZoneInfo>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val HOURS_12 = "12"
    private val HOURS_24 = "24"
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return TimeZoneRecyclerViewHolder(View.inflate(context, R.layout.timezone_recyclerview, null))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = holder as TimeZoneRecyclerViewHolder
        viewHolder.mName?.text = items[position].tzName
        viewHolder.mValue?.text =items[position].tzOffset.toString()
        viewHolder.itemView.tag = items[position].tzId
        viewHolder.mFrameLayout?.tag=items[position]


            viewHolder.mValue?.visibility = View.VISIBLE
            viewHolder.mArrowIcon?.visibility = View.GONE
            viewHolder.mSwitchItem?.visibility = View.GONE

        var timeZId:String=TimeZone.getDefault().id
        if(items[position].tzId.equals(timeZId)){
            items[position].setSelected(true);
            viewHolder.mRadioButton?.setChecked(true)
        }else{
            items[position].setSelected(false);
            viewHolder.mRadioButton?.setChecked(false);
        }

        val drawable = viewHolder.mFrameLayout?.background as GradientDrawable
        //drawable.setColor(ContextCompat.getColor(context, R.color.list_item_def_color))
    }

    override fun getItemCount(): Int {
        return items.size
    }



}