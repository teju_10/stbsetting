package com.rjil.jiostbsetting.adapters

import android.content.Context
import android.graphics.drawable.GradientDrawable
import android.util.Log
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import com.rjil.jiostbsetting.R
import com.rjil.jiostbsetting.holder.DetailsRecyclerViewHolder
import com.rjil.jiostbsetting.holder.RecyclerViewHolder
import com.rjil.jiostbsetting.models.PermissionInfos

class Apps_DetailsAdapter(val context: Context, var items: ArrayList<String>,
                          var alPackageDetails: ArrayList<String>,var permissionList1: ArrayList<PermissionInfos>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return RecyclerViewHolder(View.inflate(context, R.layout.item_recyclerview, null))
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        Log.d("ItemListAdapter", "onAttachedToRecyclerView ")
//        this.recyclerView = recyclerView
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = holder as RecyclerViewHolder

        if (position == items.size - 1)
            if (permissionList1 != null && permissionList1.size > 0)
                viewHolder.mIcon.visibility = View.VISIBLE
            else {
                viewHolder.mIcon.visibility = View.GONE
                viewHolder.itemView.isFocusable = false
            }


        viewHolder.mName.text = items[position]
        viewHolder.itemView.tag = items[position]

        Log.e("AppDetailsAdapter@@@: ", alPackageDetails[position])
        viewHolder.tv_item_size?.visibility = View.VISIBLE
        viewHolder.tv_item_size.text = alPackageDetails[position]

        if (position == 0) {
            holder.itemView.requestFocus()
            holder.relativeLyout!!.background = context!!.resources.getDrawable(R.drawable.rl_shape_corner)
            val anim = AnimationUtils.loadAnimation(context, R.anim.anim_scale_big)
            holder.itemView.startAnimation(anim)
            anim.fillAfter = true
        }

        holder.itemView.setOnFocusChangeListener { view, b ->
            if (b) {
                holder.relativeLyout!!.background = context!!.resources.getDrawable(R.drawable.rl_shape_corner)
                val anim = AnimationUtils.loadAnimation(context, R.anim.anim_scale_big)
                holder.itemView.startAnimation(anim)
                anim.fillAfter = true
            } else {
                holder.relativeLyout!!.background = context!!.resources.getDrawable(R.drawable.rv_focused_bg)
                val anim = AnimationUtils.loadAnimation(context, R.anim.anim_scale_small)
                holder.itemView.startAnimation(anim)
                anim.fillAfter = true
            }
        }

    }

    override fun getItemCount(): Int {
        return items.size
    }

}