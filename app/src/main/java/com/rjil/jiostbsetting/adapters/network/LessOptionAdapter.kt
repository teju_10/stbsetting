package com.rjil.jiostbsetting.adapters.network

import android.content.Context
import android.content.Context.WIFI_SERVICE
import android.net.wifi.WifiInfo
import android.net.wifi.WifiManager
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.recyclerview.widget.RecyclerView
import com.rjil.jiostbsetting.R
import com.rjil.jiostbsetting.holder.WifiSettingRecyclerViewHolder
import com.rjil.jiostbsetting.utils.Constant.Companion.TAG


class LessOptionAdapter(val context: Context, var items: ArrayList<String>, var values: ArrayList<String>, listType: String) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        var isConnected = ""
    }

    var wifiManager: WifiManager? = null
    var info: WifiInfo? = null
    var listType: String? = null
    private lateinit var recyclerView: RecyclerView


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        wifiManager = context.getSystemService(WIFI_SERVICE) as WifiManager?
        info = wifiManager!!.connectionInfo
        this@LessOptionAdapter.listType = listType
        return WifiSettingRecyclerViewHolder(View.inflate(context, R.layout.item_wifi_details_recyclerview, null))
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        this.recyclerView = recyclerView
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = holder as WifiSettingRecyclerViewHolder
        viewHolder.mName?.text = items[position]
        viewHolder.mValue?.text = values[position]
        viewHolder.mFrameLayout?.tag = items[position]
        viewHolder.mValue?.visibility = View.GONE
        viewHolder.mArrowIcon?.visibility = View.VISIBLE
        if (position == 0) {
            viewHolder.mValue?.visibility = View.VISIBLE
        }
        if (items[0] == "Ethernet") {
            if (position == 0) {
                holder.itemView.isFocusable = true
                holder.itemView.requestFocus()
                holder.relative_wifi_detail.background = context.resources.getDrawable(R.drawable.rl_shape_corner)
                val anim = AnimationUtils.loadAnimation(context, R.anim.anim_scale_big)
                holder.itemView.startAnimation(anim)
                anim.fillAfter = true

            } else if (position == 1) {
                holder.mArrowIcon!!.visibility = View.VISIBLE
            }
        }else if (items[0] == "Wi-Fi") {
            if (position == 0) {
                Log.d(TAG, "First Position" + items[0])
                holder.itemView.isFocusable = true
                holder.itemView.requestFocus()
                holder.relative_wifi_detail.background = context.resources.getDrawable(R.drawable.rl_shape_corner)
                val anim = AnimationUtils.loadAnimation(context, R.anim.anim_scale_big)
                holder.itemView.startAnimation(anim)
                anim.fillAfter = true
            } else if (position == 1) {

                viewHolder.mArrowIcon?.visibility = View.GONE
                // viewHolder.mValue?.visibility = View.VISIBLE
                //  viewHolder.mValue?.text = "Not connected"
            }
        }


        holder.itemView.setOnFocusChangeListener { view, b ->
            if (b) {
                holder.relative_wifi_detail.background = context.resources.getDrawable(R.drawable.rl_shape_corner)
                val anim = AnimationUtils.loadAnimation(context, R.anim.anim_scale_big)
                holder.itemView.startAnimation(anim)
                anim.fillAfter = true
            } else {
                holder.relative_wifi_detail.background = context.resources.getDrawable(R.drawable.rv_focused_bg)
                val anim = AnimationUtils.loadAnimation(context, R.anim.anim_scale_small)
                holder.itemView.startAnimation(anim)
                anim.fillAfter = true
            }
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun getItemId(position: Int): Long {
        return items.get(position).hashCode().toLong()
    }

    fun updateMenu(valueList: ArrayList<String>, connType: String) {
        if (connType == "WIFI") {
            listType = "ConnectedWifiDetails"
            this.items.removeAt(0)
            this.items.add(0, "Wi-Fi")
            this.items.removeAt(1)
            this.items.add(1, "Ethernet")
            if (valueList[0] == "<unknown ssid>") {
                this.values.add(0, "Scanning..")
            } else {
                this.values = valueList
            }
        } else {
            listType = "ConnectedWifiDetails"
            this.items.removeAt(0)
            this.items.add(0, "Ethernet")
            this.items.removeAt(1)
            this.items.add(1, "Wi-Fi")
            this.values.removeAt(0)
            this.values.add(0, isConnected)
        }
        notifyDataSetChanged()
    }

}