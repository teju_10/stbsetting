package com.rjil.jiostbsetting.adapters

import android.app.ActivityManager
import android.content.Context
import android.content.pm.PackageInfo
import android.graphics.drawable.GradientDrawable
import android.os.Handler
import android.util.Log
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.fragment.app.FragmentActivity
import com.rjil.jiostbsetting.R
import com.rjil.jiostbsetting.fragments.AppListFragment
import com.rjil.jiostbsetting.holder.AppListRecyclerViewHolder
import com.rjil.jiostbsetting.holder.DetailsRecyclerViewHolder
import com.rjil.jiostbsetting.holder.DetailsSubRecyclerViewHolder
import com.rjil.jiostbsetting.models.AppList
import kotlinx.android.synthetic.main.activity_details.*


class SubDetailsAdapter(val context: Context, val app: ArrayList<PackageInfo>, var activity: FragmentActivity) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): RecyclerView.ViewHolder {
        return AppListRecyclerViewHolder(View.inflate(context, R.layout.app_list_recyclerview, null))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = holder as AppListRecyclerViewHolder

      //  viewHolder.appName?.text = app[position].name
       viewHolder.appName?.text = app[position].applicationInfo?.loadLabel(context.getPackageManager()).toString()

        /*if (position == 0) {
            holder.itemView.requestFocus()
            holder.relative_applist_adpater!!.background = context!!.resources.getDrawable(R.drawable.rl_shape_corner)
            val anim = AnimationUtils.loadAnimation(context, R.anim.anim_scale_big)
            holder.itemView.startAnimation(anim)
            anim.fillAfter = true
            try {

                AppListFragment.subrecyclerView.smoothScrollToPosition(0)
            }
            catch (e: Exception)
            {
                e.printStackTrace()
            }
        }*/

        holder.itemView.setOnFocusChangeListener { view, b ->
            if (b) {
                changeAppLogo(position)
                holder.relative_applist_adpater!!.background = context!!.resources.getDrawable(R.drawable.rl_shape_corner)
                val anim = AnimationUtils.loadAnimation(context, R.anim.anim_scale_big)
                holder.itemView.startAnimation(anim)
                anim.fillAfter = true

            } else {
                holder.relative_applist_adpater!!.background = context!!.resources.getDrawable(R.drawable.rv_focused_bg)
                val anim = AnimationUtils.loadAnimation(context, R.anim.anim_scale_small)
                holder.itemView.startAnimation(anim)
                anim.fillAfter = true
            }
        }
    }

    override fun getItemCount(): Int {
        return app.size
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    fun changeAppLogo(pos:Int) {
        try {
            Handler().post() {
                val packageInfo: PackageInfo = app.get(pos)
                Log.e("Info", packageInfo.packageName)
                val icon = context!!.packageManager.getApplicationIcon(packageInfo.packageName)
                activity.jiologo!!.visibility = View.GONE
                activity.applogo!!.visibility = View.VISIBLE
                activity.applogo?.setImageDrawable(icon)
            }

            //Get Permissions

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}

