package com.rjil.jiostbsetting.adapters.apps

import android.content.Context
import android.content.Context.WIFI_SERVICE
import android.graphics.drawable.GradientDrawable
import android.net.wifi.WifiInfo
import android.net.wifi.WifiManager
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.rjil.jiostbsetting.R
import com.rjil.jiostbsetting.holder.WifiSettingRecyclerViewHolder
import java.lang.Exception

class StorageAdapter(val context: Context, var items: ArrayList<String>, var values: ArrayList<String>, listType:String) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    var wifiManager: WifiManager? = null
    var info: WifiInfo? = null
    var listType:String?=null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        wifiManager = context.getSystemService(WIFI_SERVICE) as WifiManager?
        info = wifiManager!!.connectionInfo
        this@StorageAdapter.listType=listType
        return WifiSettingRecyclerViewHolder(View.inflate(context, R.layout.item_wifi_details_recyclerview, null))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = holder as WifiSettingRecyclerViewHolder
        viewHolder.mName?.text = items[position]
        viewHolder.mValue?.text=values[position]
        viewHolder.mFrameLayout?.tag = items[position]
        viewHolder.mValue?.visibility = View.GONE
        viewHolder.mArrowIcon?.visibility = View.VISIBLE
        if(listType.equals("ConnectedWifiDetails")) {
            if (position == 0) {
                viewHolder.mValue?.visibility = View.VISIBLE
            }
        }
      try {
//          val drawable = viewHolder.mFrameLayout?.background as GradientDrawable
//          drawable.setColor(ContextCompat.getColor(context, R.color.list_item_def_color))
      }catch (e:Exception){
          e.printStackTrace()
      }
    }
    override fun getItemCount(): Int {
        return items.size
    }

    public fun updateMenu(valueList: ArrayList<String>,connType:String){
        if(connType.equals("WIFI")) {
            listType="ConnectedWifiDetails"
            this.items.removeAt(0)
            this.items.add(0,"Wi-Fi")
            this.values = valueList
        }else{
            this.items.removeAt(0)
            this.items.add(0,"Ethernet")
            this.values.removeAt(0)
            this.values.add(0,"Connected")
        }
        notifyDataSetChanged()
    }

}