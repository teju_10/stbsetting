package com.rjil.jiostbsetting.adapters

import android.content.Context

class SubRadioadapter(context: Context, items: List<String>) : RadioAdapter<String>(context, items) {

    override fun onBindViewHolder(viewHolder: RadioAdapter<String>.ViewHolder, i: Int) {
        super.onBindViewHolder(viewHolder, i)
        viewHolder.mText.setText(mItems[i])
    }
}