package com.rjil.jiostbsetting.adapters

import android.content.Context
import android.graphics.drawable.GradientDrawable
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import android.view.View
import android.view.ViewGroup
import com.rjil.jiostbsetting.R
import com.rjil.jiostbsetting.fragments.DetailsFragment
import com.rjil.jiostbsetting.holder.DetailsRecyclerViewHolder
import com.rjil.jiostbsetting.models.Bluetooth
import com.rjil.jiostbsetting.utils.Constant
import java.lang.Exception

class DetailsSubListAdapter(val context: Context, var items: ArrayList<String>, var values: ArrayList<Bluetooth>, var title: String) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return DetailsRecyclerViewHolder(View.inflate(context, R.layout.item_details_remote_recyclerview, null))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = holder as DetailsRecyclerViewHolder
        viewHolder.mFrameLayout!!.isFocusable = false
        Log.d("DetailsubAdap", "sub_itemValue.size.toString() "+ values.size.toString())

        try {
            if(items.get(position)!=null){
                viewHolder.mName?.text = items.get(position)
            }else{
                viewHolder.mName?.text = "JIO BLE Remote"
            }
            if(values.get(position).isConnected){
                viewHolder.mValue?.text = "Connected"
            }else{
                viewHolder.mValue?.text = "Disconnected"
            }
            viewHolder.itemView.tag = values.get(position)

        if(values.get(position).equals("") || values.get(position).equals(">")) {
            viewHolder.mValue?.visibility = View.GONE
            viewHolder.mArrowIcon?.visibility = View.VISIBLE
            viewHolder.mSwitchItem?.visibility = View.GONE
        }else if(values.get(position).equals("SWITCH")) {
            viewHolder.mValue?.visibility = View.GONE
            viewHolder.mArrowIcon?.visibility = View.GONE
            viewHolder.mSwitchItem?.visibility = View.VISIBLE
            if(title.equals("Others")){
                Log.d("TAG","CEC:"+Constant.readCecOption(context,"hdmi_control_enabled"))
                viewHolder.mSwitchItem?.isChecked = Constant.readCecOption(context,"hdmi_control_enabled")
            }
        }else {
            viewHolder.mFrameLayout!!.isFocusable = false
            viewHolder.mValue?.visibility = View.VISIBLE
            viewHolder.mArrowIcon?.visibility = View.GONE
            viewHolder.mSwitchItem?.visibility = View.GONE
        }
        }catch (e:Exception){
            e.printStackTrace()
        }

    }

    override fun getItemCount(): Int {
        return items.size
    }

}