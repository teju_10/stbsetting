package com.rjil.jiostbsetting.adapters

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.RadioButton
import com.rjil.jiostbsetting.R
import com.rjil.jiostbsetting.fragments.DaydreamFragment
import com.rjil.jiostbsetting.holder.DetailsSubRecyclerViewHolder
import com.rjil.jiostbsetting.utils.Constant


class DetailsSubAdapter(val context: Context, var items: Array<String>, var values: Array<String>, var type: Int, var mainTitle: String) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    var lastSelectedPosition = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return DetailsSubRecyclerViewHolder(View.inflate(context, R.layout.item_details_sub_recyclerview, null))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = holder as DetailsSubRecyclerViewHolder
        //adapPos = position
        if (type == Constant.RADIO_ITEM) {
            viewHolder.radio?.visibility = View.VISIBLE
            viewHolder.mArrowIcon?.visibility = View.GONE
            viewHolder.mSwitchItem?.visibility = View.GONE
            viewHolder.mValue?.visibility = View.GONE
            viewHolder.mName?.visibility = View.VISIBLE
            viewHolder.mName?.text = items[position]
            if (mainTitle.equals("Inactivity Timer & HDMI CEC")) {
                if (DaydreamFragment.title.equals("Screensaver")) {
                    viewHolder.radio!!.isChecked = values[position] != "" &&
                            values[position] == Constant.getActiveDreamComponent()?.packageName
                } else if (DaydreamFragment.title == "When to Start Screensaver") {
                    viewHolder.radio!!.isChecked = lastSelectedPosition == position
                    viewHolder.radio!!.isChecked = (values[position].toLong()).toInt() == Constant.getDreamTime(context)
                } else if (DaydreamFragment.title.equals("Put Device to Sleep")) {
                    viewHolder.radio!!.setChecked(lastSelectedPosition == position)
                    viewHolder.radio!!.isChecked = (values[position].toLong()).toInt() == Constant.getSleepTime(context)
                }
                //  viewHolder.radio!!.setOnClickListener(RadioClick(position))

            }
        } else {
            viewHolder.mName?.text = items[position]
            viewHolder.mValue?.text = values[position]
            viewHolder.itemView.tag = values[position]
            viewHolder.radio?.visibility = RadioButton.GONE
            if (values[position].equals("") || values[position].equals(">")) {
                viewHolder.mValue?.visibility = View.GONE
                viewHolder.mArrowIcon?.visibility = View.VISIBLE
                viewHolder.mSwitchItem?.visibility = View.GONE
            } else if (values[position].equals("SWITCH")) {
                viewHolder.mValue?.visibility = View.GONE
                viewHolder.mArrowIcon?.visibility = View.GONE
                viewHolder.mSwitchItem?.visibility = View.VISIBLE
                if (mainTitle.equals("Developer Options")) {

                  /*  if (position == 0) {
                        holder.itemView.requestFocus()
                        holder.relative_subdetailadapter!!.background = context!!.resources.getDrawable(R.drawable.rl_shape_corner)
                        val anim = AnimationUtils.loadAnimation(context, R.anim.anim_scale_big)
                        holder.itemView.startAnimation(anim)
                        anim.fillAfter = true
                    }*/

                    when (position) {
                        0 -> viewHolder.mSwitchItem?.isChecked = Constant.getDevMode(context)
                        1 -> viewHolder.mSwitchItem?.isChecked = Constant.getStayAwake(context)
                        2 -> viewHolder.mSwitchItem?.isChecked = Constant.getBLEHCILog(context)
                        3 -> viewHolder.mSwitchItem?.isChecked = Constant.getAdbDebug(context)
                    }

                }
            } else {
                viewHolder.mValue?.visibility = View.VISIBLE
                viewHolder.mArrowIcon?.visibility = View.GONE
                viewHolder.mSwitchItem?.visibility = View.GONE
                viewHolder.mFrameLayout?.isFocusable = false
            }

        }


        if (position == 0 && values[position].equals(">") || position == 0 && viewHolder.mSwitchItem!!.visibility == View.VISIBLE) {
            holder.itemView.requestFocus()
            holder.relative_subdetailadapter!!.background = context!!.resources.getDrawable(R.drawable.rl_shape_corner)
            val anim = AnimationUtils.loadAnimation(context, R.anim.anim_scale_big)
            holder.itemView.startAnimation(anim)
            anim.fillAfter = true
        }
        holder.itemView.setOnFocusChangeListener { view, b ->
            if (b) {
                holder.relative_subdetailadapter!!.background = context!!.resources.getDrawable(R.drawable.rl_shape_corner)
                val anim = AnimationUtils.loadAnimation(context, R.anim.anim_scale_big)
               // holder.itemView.startAnimation(anim)
                holder.relative_subdetailadapter.startAnimation(anim)

                anim.fillAfter = true

            } else {
                holder.relative_subdetailadapter!!.background = context!!.resources.getDrawable(R.drawable.rv_focused_bg)
                val anim = AnimationUtils.loadAnimation(context, R.anim.anim_scale_small)
                holder.relative_subdetailadapter.startAnimation(anim)
                anim.fillAfter = true
            }
//            updateFocusPositions(holder, b, position)
//            startFocusAnimation(holder, b)
        }

    }


    override fun getItemCount(): Int {
        return items.size
    }

}