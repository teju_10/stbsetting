package com.rjil.jiostbsetting.adapters

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.opengl.Visibility
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import com.rjil.jiostbsetting.R
import com.rjil.jiostbsetting.adapters.Language_Keyboard.LangChangeAdapter
import com.rjil.jiostbsetting.holder.DetailsRecyclerViewHolder
import com.rjil.jiostbsetting.utils.Constant
import com.rjil.jiostbsetting.utils.SharedPreferance


class DetailsAdapter(val context: Context, var items: Array<String>, var values: Array<String>, var title: String) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var getPosition: Int = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return DetailsRecyclerViewHolder(View.inflate(context, R.layout.item_details_recyclerview, null))


    }

//    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
//        super.onAttachedToRecyclerView(recyclerView)
//        Log.d("ItemListAdapter", "onAttachedToRecyclerView ")
////        this.recyclerView = recyclerView
//    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = holder as DetailsRecyclerViewHolder
        getPosition = position
        viewHolder.mName?.text = items[position]
        viewHolder.mValue?.text = values[position]
        viewHolder.itemView.tag = values[position]
        if (values[position].equals("") || values[position].equals(">")) {
            viewHolder.mValue?.visibility = View.GONE
            viewHolder.mArrowIcon?.visibility = View.VISIBLE
            viewHolder.mSwitchItem?.visibility = View.GONE
        } else if (values[position].equals("SWITCH")) {
            viewHolder.mValue?.visibility = View.GONE
            viewHolder.mArrowIcon?.visibility = View.GONE
            viewHolder.mSwitchItem?.visibility = View.VISIBLE
            Log.d("TAG", "title:" + title + " ##:" + Constant.getLocationMode(context))
            if (title.equals("Inactivity Timer & HDMI CEC")) {
                Log.d("TAG", "CEC:" + Constant.readCecOption(context, "hdmi_control_enabled"))
                viewHolder.mSwitchItem?.isChecked = Constant.readCecOption(context, "hdmi_control_enabled")
                viewHolder.mSwitchItem?.isChecked = Constant.isDaydreamEnabled(context) == 1
            } else if (title.equals("Developers & Location")) {
                viewHolder.mSwitchItem?.isChecked = SharedPreferance.getLocationValue(context) == 1
            }
        } else if(title.equals("Storage & Reset")&&!values[position].equals("0 KB")){
            viewHolder.mValue!!.visibility = View.VISIBLE
            viewHolder.mArrowIcon!!.visibility = View.VISIBLE
        } else {
            viewHolder.mValue?.visibility = View.VISIBLE
            viewHolder.mArrowIcon?.visibility = View.GONE
            viewHolder.mSwitchItem?.visibility = View.GONE
            viewHolder.mFrameLayout?.isFocusable = false
        }
        if (position == 0) {
            holder.itemView.requestFocus()
            holder.relativeLyout!!.background = context!!.resources.getDrawable(R.drawable.rl_shape_corner)
            val anim = AnimationUtils.loadAnimation(context, R.anim.anim_scale_big)
            holder.itemView.startAnimation(anim)
            anim.fillAfter = true
        }
        holder.itemView.setOnFocusChangeListener { view, b ->
            //   updateFocusPositions(holder, b, position)
            // startFocusAnimation(holder, b)
            if (b) {
                holder.relativeLyout!!.background = context!!.resources.getDrawable(R.drawable.rl_shape_corner)
                val anim = AnimationUtils.loadAnimation(context, R.anim.anim_scale_big)
                holder.itemView.startAnimation(anim)
                anim.fillAfter = true

            } else {
                holder.relativeLyout!!.background = context!!.resources.getDrawable(R.drawable.rv_focused_bg)
                val anim = AnimationUtils.loadAnimation(context, R.anim.anim_scale_small)
                holder.itemView.startAnimation(anim)
                anim.fillAfter = true
            }
        }

        holder.itemView.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
            val position = holder.getAdapterPosition()

            if (event.action == KeyEvent.ACTION_DOWN) {
                if (keyCode == KeyEvent.KEYCODE_DPAD_DOWN) {
                    return@OnKeyListener position == itemCount - 1
                }
                //  return@OnKeyListener keyCode == KeyEvent.KEYCODE_DPAD_RIGHT || keyCode == KeyEvent.KEYCODE_DPAD_LEFT
            }
            false
        })
    }


    override fun getItemCount(): Int {
        return items.size
    }

    fun updateMenu(position: Int,holder: RecyclerView.ViewHolder) {
        if (position == 1||Constant.isStorageUSBConnected(context)) {
            holder.itemView.visibility = View.VISIBLE

        }else{
            holder.itemView.visibility = View.GONE
        }
    }

//    fun hideItem(position: Int) {
//        items.add(position, items[position])
//        items.remove(position)
//        notifyItemRemoved(position)
//    }

}