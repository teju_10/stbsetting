package com.rjil.jiostbsetting.adapters

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.FrameLayout
import android.widget.RadioButton
import android.widget.RelativeLayout
import android.widget.TextView
import com.rjil.jiostbsetting.R
import com.rjil.jiostbsetting.utils.Constant

class DeviceSleepAdapter(val context: Context, var items: Array<String>, var values: Array<String>, var type: Int) : RecyclerView.Adapter<DeviceSleepAdapter.ViewHolder>() {

    var lastSelectedPosition = -1
    var mTrackPlaying = -1

    fun setTrackPlaying(position: Int) {
        mTrackPlaying = position
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DeviceSleepAdapter.ViewHolder {
        val view1 = LayoutInflater.from(context)
                .inflate(R.layout.item_devicesleep_recyclerview, parent, false)
        return ViewHolder(view1)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
            viewHolder.ds_tv_item_tip?.text = items[position]
                viewHolder.dsradio!!.isChecked = lastSelectedPosition == position
                viewHolder.dsradio!!.isChecked = (values[position].toLong()).toInt() == Constant.getSleepTime(context)

        viewHolder.itemView.setOnFocusChangeListener { view, b ->
            Log.d("Selected Position and Boolean Value", position.toString() + ", " + b)
            if (b) {
                viewHolder.relativeLyout!!.background = context!!.resources.getDrawable(R.drawable.white_shape_corner)
                val anim = AnimationUtils.loadAnimation(context, R.anim.anim_scale_big)
                viewHolder.relativeLyout!!.startAnimation(anim)
                anim.fillAfter = true
            } else {
                viewHolder.relativeLyout!!.background = context!!.resources.getDrawable(R.drawable.rv_focused_bg)
                val anim = AnimationUtils.loadAnimation(context, R.anim.anim_scale_small)
                viewHolder.relativeLyout!!.startAnimation(anim)
                anim.fillAfter = true
            }

        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    class ViewHolder(v: View) : RecyclerView.ViewHolder(v) {

        var dsradio: RadioButton = v.findViewById(R.id.dsradio)
        var ds_tv_item_tip: TextView = v.findViewById(R.id.ds_tv_item_tip)
        var relativeLyout: RelativeLayout = v.findViewById(R.id.relative_dsadapter)
        var fl_main_layout: FrameLayout = v.findViewById(R.id.fl_main_layout)

    }


}