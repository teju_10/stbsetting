package com.rjil.jiostbsetting.adapters

import android.content.Context
import android.graphics.drawable.GradientDrawable
import android.util.Log
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import com.rjil.jiostbsetting.R
import com.rjil.jiostbsetting.holder.DetailsRecyclerViewHolder
import com.rjil.jiostbsetting.holder.RecyclerViewHolder

class MenuAdapter(val context: Context, var items: Array<String>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var previousFocusedViewHolder: RecyclerViewHolder? = null
    private var previouslyFocusedPos = 0
    private var currentlyFocusedPos = 0
    //    private lateinit var recyclerView: RecyclerView

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return RecyclerViewHolder(View.inflate(context, R.layout.item_recyclerview, null))
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        Log.d("ItemListAdapter", "onAttachedToRecyclerView ")
//        this.recyclerView = recyclerView
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = holder as RecyclerViewHolder
        viewHolder.mName.text = items[position]
        viewHolder.itemView.tag = items[position]


        if (position == 0) {
            holder.itemView.requestFocus()
            holder.relativeLyout!!.background = context!!.resources.getDrawable(R.drawable.rl_shape_corner)
            val anim = AnimationUtils.loadAnimation(context, R.anim.anim_scale_big)
            holder.itemView.startAnimation(anim)
            anim.fillAfter = true
        }

        holder.itemView.setOnFocusChangeListener { view, b ->
            updateFocusPositions(holder, b, position)
            startFocusAnimation(holder, b)
        }
        // val drawable = viewHolder.mFrameLayout.background as GradientDrawable
        //drawable.setColor(ContextCompat.getColor(context, R.color.list_item_def_color))
    }

    override fun getItemCount(): Int {
        return items.size
    }


    private fun startFocusAnimation(holder: RecyclerViewHolder, hasFocus: Boolean) {
        if (hasFocus) {
            previousFocusedViewHolder?.let {
                holder.relativeLyout!!.background = context!!.resources.getDrawable(R.drawable.rl_shape_corner)
                val anim = AnimationUtils.loadAnimation(context, R.anim.anim_scale_big)
                holder.itemView.startAnimation(anim)
                anim.fillAfter = true

            }
        } else {
            holder.relativeLyout!!.background = context!!.resources.getDrawable(R.drawable.rv_focused_bg)
            val anim = AnimationUtils.loadAnimation(context, R.anim.anim_scale_small)
            holder.itemView.startAnimation(anim)
            anim.fillAfter = true
        }
    }

    private fun updateFocusPositions(viewHolder: RecyclerViewHolder, hasFocus: Boolean, position: Int) {
        if (hasFocus) {
            previouslyFocusedPos = currentlyFocusedPos
            currentlyFocusedPos = position

        } else {
            previousFocusedViewHolder = viewHolder
        }
    }
}