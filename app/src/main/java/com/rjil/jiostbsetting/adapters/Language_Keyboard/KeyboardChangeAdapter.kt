package com.rjil.jiostbsetting.adapters.Language_Keyboard

import android.content.Context
import android.util.Log
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.RadioButton
import android.widget.RelativeLayout
import android.widget.TextView
import com.rjil.jiostbsetting.R


class KeyboardChangeAdapter(val mContext: Context, val value: ArrayList<String>) : RecyclerView.Adapter<KeyboardChangeAdapter.ViewHolder>() {

    override fun onBindViewHolder(Vholder: ViewHolder, position: Int) {
        Vholder?.tv_kb_name!!.text = value[position]

        if (!Vholder.select_keyboard_type.isChecked) {
            Vholder.select_keyboard_type.isChecked = true
        } else {
            Vholder.select_keyboard_type.isChecked = true

        }


        if (position == 0) {
            Vholder.itemView.requestFocus()
            Vholder.relativeLyout!!.background = mContext!!.resources.getDrawable(R.drawable.rl_shape_corner)
            val anim = AnimationUtils.loadAnimation(mContext, R.anim.anim_scale_big)
            Vholder.itemView.startAnimation(anim)
            anim.fillAfter = true
        }

        Vholder.itemView.setOnFocusChangeListener { view, b ->
            if (b) {

                    Vholder.relativeLyout!!.background = mContext!!.resources.getDrawable(R.drawable.rl_shape_corner)
                    val anim = AnimationUtils.loadAnimation(mContext, R.anim.anim_scale_big)
                    Vholder.itemView.startAnimation(anim)
                    anim.fillAfter = true
            } else {
                Vholder.relativeLyout!!.background = mContext!!.resources.getDrawable(R.drawable.rv_focused_bg)
                val anim = AnimationUtils.loadAnimation(mContext, R.anim.anim_scale_small)
                Vholder.itemView.startAnimation(anim)
                anim.fillAfter = true
            }
        }

    }

    class ViewHolder(v: View) : RecyclerView.ViewHolder(v) {

        var select_keyboard_type: RadioButton = v.findViewById(R.id.select_keyboard_type)
        var tv_kb_name: TextView = v.findViewById(R.id.tv_kb_name)
        var relativeLyout: RelativeLayout = v.findViewById(R.id.relativeLyout)

    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        Log.d("ItemListAdapter", "onAttachedToRecyclerView ")
//        this.recyclerView = recyclerView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view1 = LayoutInflater.from(mContext)
                .inflate(R.layout.item_changekeyboard_list, parent, false)
        return ViewHolder(view1)
    }


    override fun getItemCount(): Int {
        return value.size
    }


//    inner class RadioClick internal constructor(var pos: Int) : View.OnClickListener {
//        override fun onClick(view: View) {
//            try {
//                lastSelectedPosition = pos
//                notifyDataSetChanged()
//                try {
//                    val imm = mContext.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
//                    val token = (getContext() as Activity).getWindow().getAttributes().token
//                    //imm.setInputMethod(token, LATIN);
//                    imm.switchToLastInputMethod(token)
//                } catch (t: Throwable) { // java.lang.NoSuchMethodError if API_level<11
//                    Log.d("", "cannot set the previous input method:")
//                    t.printStackTrace()
//                }
//
//            } catch (e: Exception) {
//                e.printStackTrace()
//            }
//
//        }
//    }


}