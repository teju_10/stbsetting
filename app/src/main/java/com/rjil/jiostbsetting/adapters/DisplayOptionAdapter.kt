package com.rjil.jiostbsetting.adapters

import android.content.Context
import android.graphics.drawable.GradientDrawable
import android.provider.Settings
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.CompoundButton
import com.rjil.jiostbsetting.R
import com.rjil.jiostbsetting.holder.AppListRecyclerViewHolder
import com.rjil.jiostbsetting.holder.DetailsRecyclerViewHolder
import com.rjil.jiostbsetting.utils.SharedPreferance
import java.lang.IllegalStateException
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class DisplayOptionAdapter(val context: Context, var items: Array<String>, var values: Array<String?>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var previousFocusedViewHolder: DetailsRecyclerViewHolder? = null
    private var previouslyFocusedPos = 0
    private var currentlyFocusedPos = 0

    private val HOURS_12 = "12"
    private val HOURS_24 = "24"

    lateinit var mNowTime : String

    companion object{
        val TAG = DisplayOptionAdapter.javaClass.simpleName
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return DetailsRecyclerViewHolder(View.inflate(context, R.layout.item_details_recyclerview, null))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = holder as DetailsRecyclerViewHolder
        viewHolder.mName?.text = items[position]
        viewHolder.mValue?.text = values[position]
        viewHolder.itemView.tag = values[position]
        viewHolder.mFrameLayout?.tag=items[position]
        if(values[position].equals("") || values[position].equals(">")) {
            viewHolder.mValue?.visibility = View.GONE
            viewHolder.mArrowIcon?.visibility = View.VISIBLE
            viewHolder.mSwitchItem?.visibility = View.GONE
//            viewHolder.mFrameLayout?.isFocusable=false
        }else if(values[position].equals("SWITCH")) {
            viewHolder.mValue?.visibility = View.GONE
            viewHolder.mArrowIcon?.visibility = View.GONE
            viewHolder.mSwitchItem?.visibility = View.VISIBLE
//            if(context is DisplayOptionsActivity){
//                val  activity: DisplayOptionsActivity = context as DisplayOptionsActivity
               /* if (getTime24Hour().equals(HOURS_24)) {
                    viewHolder.mSwitchItem?.setChecked(true)
                } else {
                    viewHolder.mSwitchItem?.setChecked(false)
                }*/



            viewHolder.mSwitchItem?.isChecked = SharedPreferance.getDateAndTimeFormat(context)

            viewHolder.mSwitchItem?.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { buttonView, isChecked ->
                if (isChecked) {
                    setTime24Hour(true)
                    values[3] = updateTime()
                    viewHolder.mValue?.text = values[3]
                    Log.d(TAG,"checkedValue"+values[3])

                } else {
                    setTime24Hour(false)
                    values[3] = updateTimeWith12()
                    viewHolder.mValue?.text = values[3]
                    Log.d(TAG,"UncheckedValue"+values[3])

                }
            })


            //}
        }else {
            viewHolder.mValue?.visibility = View.VISIBLE
            viewHolder.mArrowIcon?.visibility = View.GONE
            viewHolder.mSwitchItem?.visibility = View.GONE
            viewHolder.mFrameLayout?.isFocusable=false
        }

        if (position == 0) {
            holder.itemView.requestFocus()
            holder.relativeLyout!!.background = context!!.resources.getDrawable(R.drawable.rl_shape_corner)
            val anim = AnimationUtils.loadAnimation(context, R.anim.anim_scale_big)
            holder.itemView.startAnimation(anim)
            anim.fillAfter = true
        }

        holder.itemView.setOnFocusChangeListener { view, b ->
            if (b) {
                    holder.relativeLyout!!.background = context!!.resources.getDrawable(R.drawable.rl_shape_corner)
                    val anim = AnimationUtils.loadAnimation(context, R.anim.anim_scale_big)
                    holder.itemView.startAnimation(anim)
                    anim.fillAfter = true

            } else {
                holder.relativeLyout!!.background = context!!.resources.getDrawable(R.drawable.rv_focused_bg)
                val anim = AnimationUtils.loadAnimation(context, R.anim.anim_scale_small)
                holder.itemView.startAnimation(anim)
                anim.fillAfter = true
            }
        }
    }

    private fun setTime24Hour(is24Hour: Boolean) {
        try {
            Settings.System.putString(context.getContentResolver(),
                    Settings.System.TIME_12_24,
                    if (is24Hour) HOURS_24 else HOURS_12)
        } catch (e: SecurityException) {
            e.printStackTrace()
        }

    }



    override fun getItemCount(): Int {
        return items.size
    }

    fun updateTime () : String
    {
        val now = Calendar.getInstance()
        val timeFormat = android.text.format.DateFormat.getTimeFormat(context)
        mNowTime = timeFormat.format(now.time)
        return mNowTime
    }


    fun updateTimeWith12 () : String
    {
        val timeFormat = SimpleDateFormat("hh:mm a")
        mNowTime = timeFormat.format(Date())
        return mNowTime
    }



    fun getTime24Hour(): String {
        try {
            return Settings.System.getString(context.getContentResolver(),
                    Settings.System.TIME_12_24)
        } catch (e: IllegalStateException) {
            e.printStackTrace()
            return "time_12_24"
        }
    }

    private fun getAutoDateTime(): String {
        try {
            val value = Settings.Global.getInt(context.getContentResolver(), Settings.Global.AUTO_TIME)
            return if (value == 1) {
                context.getString(R.string.use_network_prov_time)
            } else {
                context.getString(R.string.off)
            }
        } catch (e: Settings.SettingNotFoundException) {
            e.printStackTrace()
            return ""
        }

    }


    private fun startFocusAnimation(holder: DetailsRecyclerViewHolder, hasFocus: Boolean) {
        if (hasFocus) {
            previousFocusedViewHolder?.let {
                holder.relativeLyout!!.background = context!!.resources.getDrawable(R.drawable.rl_shape_corner)
                val anim = AnimationUtils.loadAnimation(context, R.anim.anim_scale_big)
                holder.itemView.startAnimation(anim)
                anim.fillAfter = true

            }
        } else {
            holder.relativeLyout!!.background = context!!.resources.getDrawable(R.drawable.rv_focused_bg)
            val anim = AnimationUtils.loadAnimation(context, R.anim.anim_scale_small)
            holder.itemView.startAnimation(anim)
            anim.fillAfter = true
        }
    }

    private fun updateFocusPositions(viewHolder: DetailsRecyclerViewHolder, hasFocus: Boolean, position: Int) {
        if (hasFocus) {
            previouslyFocusedPos = currentlyFocusedPos
            currentlyFocusedPos = position

        } else {
            previousFocusedViewHolder = viewHolder
        }
    }

}