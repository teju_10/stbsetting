package com.rjil.jiostbsetting.adapters.network

import android.content.Context
import android.content.Context.WIFI_SERVICE
import android.net.wifi.ScanResult
import android.net.wifi.WifiConfiguration
import android.net.wifi.WifiInfo
import android.net.wifi.WifiManager
import android.util.Log
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.Toast
import com.iwedia.fti.rjio.fti_sdk.RjioSdk
import com.rjil.jiostbsetting.R
import com.rjil.jiostbsetting.SettingApplication
import com.rjil.jiostbsetting.fragments.WifiFragment
import com.rjil.jiostbsetting.holder.RecyclerViewHolder
import com.rjil.jiostbsetting.holder.WifiSettingRecyclerViewHolder
import com.rjil.stbwifilibrary.WiFiManager
import java.util.*


class WifiDetailsAdapter(val context: Context, var items: ArrayList<ScanResult>, var configuredNetworks: MutableList<WifiConfiguration>, var currentssid: String) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    var wifiManager: WifiManager? = null
    var customwifiManager: WiFiManager? = null
    var info: WifiInfo? = null
    var lastPos = -1
//    var ssid : String?=null
//    lateinit var scanningWifiConnected : ScanningWifiConnected

   /* interface ScanningWifiConnected
    {
        fun isScanning(flag: Boolean =false)
    }*/

    companion object {

    }
    init {

//        scanningWifiConnected=(ScanningWifiConnected)context
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        wifiManager = context.getSystemService(WIFI_SERVICE) as WifiManager?
        init()
        return WifiSettingRecyclerViewHolder(View.inflate(context, R.layout.item_wifi_details_recyclerview, null))
    }

    private fun init() {
        customwifiManager= WiFiManager(context)
        wifiManager = context.getSystemService(Context.WIFI_SERVICE) as WifiManager
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = holder as WifiSettingRecyclerViewHolder
        viewHolder.mName?.text = items[position].SSID
        viewHolder.mFrameLayout?.tag = items[position]
        viewHolder.mValue?.visibility = View.GONE
        viewHolder.mArrowIcon?.visibility = View.VISIBLE
        viewHolder.wifi_icon?.visibility = View.VISIBLE

//        info= wifiManager!!.connectionInfo
//        ssid = info!!.ssid.replace("\"","")



        if (position == 0) {
            holder.itemView.requestFocus()
            holder.relative_wifi_detail!!.background = context!!.resources.getDrawable(R.drawable.rl_shape_corner)
            val anim = AnimationUtils.loadAnimation(context, R.anim.anim_scale_big)
            holder.itemView.startAnimation(anim)
            anim.fillAfter = true
            WifiFragment.recyclerView.smoothScrollToPosition(0)

//            Log.d("TAGCurrentSSID",ssid)

        }


       /* //check is connected or not
        if(!RjioSdk.get().networkHandler.isEthernetConnected)
        {
            info = wifiManager!!.connectionInfo
            val connSSID = info?.ssid?.replace("\"", "")

            if (items.get(position).SSID.equals(connSSID)) {
                viewHolder.mValue?.visibility = View.VISIBLE
                viewHolder.mValue?.text = "Connected"
            }
        }*/


        if(items[position].capabilities.contains("WEP") || items[position].capabilities.contains("WPA") || items[position].capabilities.contains("WPA2") || items[position].capabilities.contains("802.1x EAP"))
        {
            holder.wifi_icon!!.setImageResource(R.drawable.ic_wifi_lock)
        }
        else
        {
            holder.wifi_icon!!.setImageResource(R.drawable.ic_wifi)
        }


        if(customwifiManager!!.getConfigFromConfiguredNetworksBySsids(items[position].SSID,configuredNetworks))
        {
            viewHolder.mValue?.visibility = View.VISIBLE
            if(position!=0)
            {
                viewHolder.mValue?.text = "Saved"
            }
            else
            {
                if(!RjioSdk.get().networkHandler.isEthernetConnected)
                {
                    for(i in items.indices)
                    {
                        if(SettingApplication.getSSID()!=null)
                        {
                            if(items[i].SSID==SettingApplication.getSSID())
                            {
                                viewHolder.mValue?.visibility = View.VISIBLE
                                viewHolder.mValue?.text = "Connected"
                                break
                            }
                            else
                            {
                                viewHolder.mValue?.text = "Saved"
                            }
                        }
                        else
                        {
                            viewHolder.mValue?.text = "Saved"
                        }
                    }
                }
                else
                {
                    viewHolder.mValue?.text = "Saved"
                }
            }
        }
        else
        {
            if(position==0)
            {
                if(!RjioSdk.get().networkHandler.isEthernetConnected)
                {
                    for(i in items)
                    {
                        if(SettingApplication.getSSID()!=null)
                        {
                            if(i.SSID==SettingApplication.getSSID())
                            {
                                viewHolder.mValue?.visibility = View.VISIBLE
                                viewHolder.mValue?.text = "Connected"
                                break
                            }
                        }

                    }
                }
            }
        }

      /* if(configuredNetworks.)
       {

       }*/


        holder.itemView.setOnFocusChangeListener { view, b ->
            if (b) {
                holder.relative_wifi_detail!!.background = context!!.resources.getDrawable(R.drawable.rl_shape_corner)
                val anim = AnimationUtils.loadAnimation(context, R.anim.anim_scale_big)
                holder.itemView.startAnimation(anim)
                anim.fillAfter = true

            } else {
                holder.relative_wifi_detail!!.background = context!!.resources.getDrawable(R.drawable.rv_focused_bg)
                val anim = AnimationUtils.loadAnimation(context, R.anim.anim_scale_small)
                holder.itemView.startAnimation(anim)
                anim.fillAfter = true
            }
        }
    }

    fun isNullOrEmpty(str: String?): Boolean {
        if (str != null && !str.trim().isEmpty())
            return false
        return true
    }


    override fun getItemId(position: Int): Long {
        return position.toLong()
    }


    override fun getItemViewType(position: Int): Int {
        return position
    }



    override fun getItemCount(): Int {
        return items.size
    }



}