package com.rjil.jiostbsetting.adapters

import android.content.Context
import android.widget.RadioButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.rjil.jiostbsetting.R

abstract class RadioAdapter<T>(private val mContext: Context, var mItems: List<T>) :
        RecyclerView.Adapter<RadioAdapter<T>.ViewHolder>() {
    var mSelectedItem = -1

    override fun onBindViewHolder(viewHolder: RadioAdapter<T>.ViewHolder, i: Int) {
        viewHolder.mRadio.setChecked(i == mSelectedItem)
    }

    override fun getItemCount(): Int {
        return mItems.size
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ViewHolder {
        val inflater = LayoutInflater.from(mContext)
        val view = inflater.inflate(R.layout.radio_recyclerview_item, viewGroup, false)
        return ViewHolder(view)
    }

    inner class ViewHolder(inflate: View) : RecyclerView.ViewHolder(inflate) {

        var mRadio: RadioButton
        var mText: TextView

        init {
            mText = inflate.findViewById(R.id.text)
            mRadio = inflate.findViewById(R.id.radio)
            val clickListener = object : View.OnClickListener {
                override fun onClick(v: View) {
                    mSelectedItem = adapterPosition
                    notifyDataSetChanged()
                }
            }
            itemView.setOnClickListener(clickListener)
            mRadio.setOnClickListener(clickListener)
        }
    }
}
