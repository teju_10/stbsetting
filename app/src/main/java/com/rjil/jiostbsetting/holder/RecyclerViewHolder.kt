package com.rjil.jiostbsetting.holder

import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.widget.*
import com.rjil.jiostbsetting.R

class RecyclerViewHolder internal constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {

    var mFrameLayout: FrameLayout
    var mName: TextView
    var mIcon: ImageView
    var relativeLyout: RelativeLayout
    var tv_item_size: TextView

    init {
        mName = itemView.findViewById<View>(R.id.tv_item_tip) as TextView
        mFrameLayout = itemView.findViewById<View>(R.id.fl_main_layout) as FrameLayout
        mIcon = itemView.findViewById<View>(R.id.tv_item_arrow) as ImageView
        relativeLyout = itemView.findViewById<View>(R.id.relativeLyout) as RelativeLayout
        tv_item_size = itemView.findViewById(R.id.tv_item_size) as TextView
    }
}