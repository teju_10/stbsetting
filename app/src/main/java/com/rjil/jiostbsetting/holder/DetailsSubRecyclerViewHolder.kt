package com.rjil.jiostbsetting.holder

import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.widget.*
import com.rjil.jiostbsetting.R

class DetailsSubRecyclerViewHolder internal constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {

    var mFrameLayout: FrameLayout? = null
    var mName: TextView? = null
    var mValue: TextView? = null
    var mArrowIcon: ImageView? = null
    var mSwitchItem: Switch? = null
    var radio: RadioButton? = null
    var relative_subdetailadapter: RelativeLayout

    companion object {

    }

        init {
            mName = itemView.findViewById<View>(R.id.tv_item_tip) as TextView
            mValue = itemView.findViewById<View>(R.id.tv_item_value) as TextView
            mFrameLayout = itemView.findViewById<View>(R.id.fl_main_layout) as FrameLayout
            mArrowIcon = itemView.findViewById<View>(R.id.arrow_icon) as ImageView
            mSwitchItem = itemView.findViewById<View>(R.id.switch_item) as Switch
            radio = itemView.findViewById<View>(R.id.radio) as RadioButton
            relative_subdetailadapter = itemView.findViewById<View>(R.id.relative_subdetailadapter) as RelativeLayout
        }
}