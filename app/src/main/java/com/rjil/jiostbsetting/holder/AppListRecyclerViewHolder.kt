package com.rjil.jiostbsetting.holder

import android.os.Build
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.widget.FrameLayout
import android.widget.RelativeLayout
import android.widget.TextView
import com.rjil.jiostbsetting.R

class AppListRecyclerViewHolder internal constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {

   // var mFrameLayout: FrameLayout
    var appName: TextView
    var relative_applist_adpater: RelativeLayout

    init {
        appName = itemView.findViewById<View>(R.id.appname) as TextView
        relative_applist_adpater = itemView.findViewById<View>(R.id.relative_applist_adpater) as RelativeLayout
    //    mFrameLayout = itemView.findViewById<View>(R.id.fl_main_layout) as FrameLayout
    }
}
