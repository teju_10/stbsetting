package com.rjil.jiostbsetting

import android.app.Notification
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.hardware.usb.UsbDevice
import android.net.NetworkInfo
import android.os.Bundle
import android.os.Handler
import android.os.IBinder
import android.os.RemoteException
import android.util.Log
import android.view.KeyEvent
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentManager
import com.iwedia.notificationservice.INotificationServiceAPI
import com.iwedia.notificationservice.INotificationServiceCallbackAPI
import com.iwedia.notificationservice.lib.RecommendedContentItem
import com.rjil.jiostbsetting.RJioSdk.information_bus.EventListener
import com.rjil.jiostbsetting.RJioSdk.information_bus.InformationBus
import com.rjil.jiostbsetting.fragments.DetailsFragment
import com.rjil.jiostbsetting.fragments.DetailsFragment.Companion.isStartNowClicked
import com.rjil.jiostbsetting.fragments.DisplayConfrugationFragment
import com.rjil.jiostbsetting.fragments.NetworkDetailsFragment
import kotlinx.android.synthetic.main.activity_details.*


class DetailsActivity : AppCompatActivity() {

    var header: TextView? = null
    private var mIsAutoProcessFocus: Boolean = false
    lateinit var mContext: Context
    internal var mService: INotificationServiceAPI? = null
    internal var mBound = false

    companion object{
        lateinit var activity: DetailsActivity
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)
        mContext = this
        activity = this
        val image = findViewById<ImageView>(R.id.jiologo)

        var strTitle: String = intent.getStringExtra("title")
        header = findViewById(R.id.headerText)
        header?.text = strTitle

        doBindService()

        if (strTitle == "Network") {
            addNetworkFragment("title")
        } else {
            addDetailsFragment()
        }

//        InformationBus.getInstance().register(EventListener)

    }

    private fun doBindService() {

        val serviceIntent: Intent = Intent()
                .setComponent(ComponentName(
                        "com.iwedia.notificationservice",
                        "com.iwedia.notificationservice.lib.NotificationService"))
        serviceIntent.putExtra("start_foreground_service", true)
        startService(serviceIntent)
        Handler().postDelayed({
            val serviceIntent: Intent = Intent()
                    .setComponent(ComponentName(
                            "com.iwedia.notificationservice",
                            "com.iwedia.notificationservice.lib.NotificationService"))
            if (!bindService(serviceIntent, mConnection, Context.BIND_AUTO_CREATE)) {
                Log.e("DetailActivity", "Error: The requested service doesn't " +
                        "exist, or this client isn't allowed access to it.")
            }
        }, 200)
    }

    private val mConnection: ServiceConnection = object : ServiceConnection {

        override fun onServiceConnected(className: ComponentName?, service: IBinder?) {
            // We've bound to the Service,
            mService = INotificationServiceAPI.Stub.asInterface(service)
            mBound = true
            try {
                mService!!.remoteServiceSetCallBacks(mCallBacks)
            } catch (e: RemoteException) {
                e.printStackTrace()
            }
        }

        override fun onServiceDisconnected(arg0: ComponentName?) {
            mService = null
            mBound = false
        }
    }

    private val mCallBacks = CallBacks()

    class CallBacks : INotificationServiceCallbackAPI.Stub() {


        override fun notificationServiceWifiNetwConnectedEvent(netwInfo: NetworkInfo?, wifiSSID: String) {
            activity.runOnUiThread(Runnable {
                /* Toast.makeText(activity, "Notification client test activity:" +
                         "\nNetwork event:" +
                         "\nWiFi connected: " + wifiSSID,
                         Toast.LENGTH_LONG
                 ).show()*/
                SettingApplication.setSSID(wifiSSID.replace("\"",""))
            })
        }

        override fun notificationServiceEthernetNetwConnectedEvent(netwInfo: NetworkInfo?) {
            activity.runOnUiThread(Runnable {
                /* Toast.makeText(activity, "Notification client test activity:" +
                         "\nNetwork event:" +
                         "\nEthernet connected",
                         Toast.LENGTH_LONG
                 ).show()*/

            })
        }

        override fun notificationServiceNetwDisconnected() {
            activity.runOnUiThread(Runnable {
                 /*Toast.makeText(activity, "Notification client test activity:" +
                         "\nNetwork event:" +
                         "\nNetwork disonnected",
                         Toast.LENGTH_LONG
                 ).show()*/
                SettingApplication.setSSID("")
            })
        }

        override fun notificationServiceUsbDevAttachedEvent(usbDevice: UsbDevice) {
            activity.runOnUiThread(Runnable {
                /* Toast.makeText(activity, "Notification client test activity:" +
                         "\nUsb event: " +
                         "\nNEW usb device ATTACHED:" +
                         "\nProduct name: " + usbDevice.productName +
                         "\nManufacturer name: " + usbDevice.manufacturerName +
                         "\nDevice name: " + usbDevice.deviceName,
                         Toast.LENGTH_LONG
                 ).show()*/
            })
        }

        override fun notificationServiceUsbDevDetachedEvent(usbDevice: UsbDevice) {
            activity.runOnUiThread(Runnable {
                /* Toast.makeText(activity, "Notification client test activity:" +
                         "\nUsb event: " +
                         "\nUsb device DETACHED:" +
                         "\nProduct name: " + usbDevice.productName +
                         "\nManufacturer name: " + usbDevice.manufacturerName +
                         "\nDevice name: " + usbDevice.deviceName,
                         Toast.LENGTH_LONG
                 ).show()*/
            })
        }

        override fun notificationServiceNotificationPosted(notification: Notification, uniqueSbnKey: String) {
            activity.runOnUiThread(Runnable {
                val bundle: Bundle? = notification.extras
                var title : String? = ""
                var text  : String? = ""
                if (bundle != null) {
                    title = bundle.getString("android.title")
                    text = bundle.getString("android.text")
                }
                /* Toast.makeText(activity, "Notification client test activity:" +
                         "\nNotification event: \nTitle: " +
                         "\nNew Notification RECEIVED: \nTitle: " + title +
                         "\nText: " + text +
                         "\nUniqueKey: " + uniqueSbnKey,
                         Toast.LENGTH_LONG

                 ).show()*/
                if (activity.mService != null) {
                    try {
                        activity.mService!!.remoteServiceCancelNotification(uniqueSbnKey)
                    } catch (e: RemoteException) {
                        e.printStackTrace()
                    }
                }
            })
        }

        override fun notificationServiceNotificationDeleted(notification: Notification, uniqueSbnKey: String) {
            activity.runOnUiThread {
                val bundle: Bundle? = notification.extras
                var title: String? = ""
                var text : String?= ""
                if (bundle != null) {
                    title = bundle.getString("android.title")
                    text = bundle.getString("android.text")
                }
                /*  Toast.makeText(activity, "Notification client test activity:" +
                              "\nNotification event: " +
                              "\nNotification DELETED: \nTitle: " + title +
                              "\nText: " + text +
                              "\nUniqueKey: " + uniqueSbnKey,
                              Toast.LENGTH_LONG

                      ).show()*/
            }
        }

        override fun newRecommendedContentAvailable() {
            if (activity.mService != null) {
                try {
                    activity.mService!!.remoteServiceGetRecommendedContent()
                } catch (e: RemoteException) {
                    e.printStackTrace()
                }
            }
        }

        override fun recommendedContentData(p0: MutableList<RecommendedContentItem>?)
        {

        }

        /* override fun recommendedContentData(recommendedContentItemList: List<RecommendedContentItem>) {
             val recommendedContentItemArrayList = recommendedContentItemList as ArrayList<RecommendedContentItem>
             val adapter = RecommendedContentAdapter(activity, recommendedContentItemArrayList)
             activity.runOnUiThread(Runnable { rec.setAdapter(adapter) })
         }*/


        override fun notificationRCUBattery(percentage: Int) {
            activity.runOnUiThread(Runnable {
                /* Toast.makeText(activity, "Battery level" +
                         "\nPercentage: " + percentage.toString() + " %",
                         Toast.LENGTH_LONG

                 ).show()*/
            })
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }

    fun hideImageHolder() {
        image_holder.visibility = GONE
    }

    fun showImageHolder() {
        image_holder.visibility = VISIBLE
    }

    override fun onKeyUp(keyCode: Int, event: KeyEvent?): Boolean {

        val f = this.supportFragmentManager.findFragmentById(R.id.sub_details_container)
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (supportFragmentManager.backStackEntryCount == 1) {
                if (!isStartNowClicked)
                    finish()

                isStartNowClicked = false
            } else {
                supportFragmentManager.popBackStackImmediate()
            }
            return true
        } else if (keyCode == KeyEvent.KEYCODE_DPAD_CENTER && f is DisplayConfrugationFragment && (f.mParam1.equals(R.string.set_date) || f.mParam1.equals(R.string.set_time))) {
            f.onEnterKeyPressed()
        }
        return super.onKeyUp(keyCode, event)
    }

    fun addDetailsFragment() {

        val fm = supportFragmentManager
        val ft = fm.beginTransaction()

        if (fm.findFragmentByTag("Lang_Change_Frag") != null) {
            fm.popBackStackImmediate()
            ft.addToBackStack(null)
            supportFragmentManager.popBackStackImmediate()
        }

//        if(fm.findFragmentByTag("details_frag")!=null){
//            fm.popBackStackImmediate()
//            ft.addToBackStack(null)
//
//        }
        val llf = DetailsFragment.newInstance()
        ft.replace(R.id.sub_details_container, llf, "details_frag")
        ft.addToBackStack("details_frag")
        ft.commit()
    }

    fun addNetworkFragment(title: String) {

        val fm = supportFragmentManager
        val ft = fm.beginTransaction()
        val llf = NetworkDetailsFragment.newInstance(title)
        ft.add(R.id.sub_details_container, llf, "NetWork_Frag")
        ft.addToBackStack("NetWork_Frag")
        ft.commit()
    }

    fun clearBackStackInclusive(tag: String) {
        supportFragmentManager.popBackStack(tag, FragmentManager.POP_BACK_STACK_INCLUSIVE)
    }

    private fun doUnbindService() {
        val serviceIntent: Intent = Intent()
                .setComponent(ComponentName(
                        "com.iwedia.notificationservice",
                        "com.iwedia.notificationservice.lib.NotificationService"))
        serviceIntent.putExtra("stop_foreground_service", true)
        startService(serviceIntent)
        Handler().postDelayed({
            Log.e("IwediaNotificationService", "unbinding")
            unbindService(mConnection)
            mService = null
            mBound = false
        }, 200)
    }

    override fun onStop() {
        super.onStop()
        //doUnbindService()
    }

    override fun onDestroy() {
        super.onDestroy()
    }


}