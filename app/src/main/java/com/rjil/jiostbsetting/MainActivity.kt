package com.rjil.jiostbsetting

import android.app.Notification
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.hardware.usb.UsbDevice
import android.net.NetworkInfo
import android.os.Bundle
import android.os.Handler
import android.os.IBinder
import android.os.RemoteException
import androidx.appcompat.app.AppCompatActivity
import android.util.Log
import android.view.KeyEvent
import android.widget.Toast
import com.iwedia.fti.rjio.fti_sdk.handler.RjioUtilityServiceHandler
import com.iwedia.notificationservice.INotificationServiceAPI
import com.iwedia.notificationservice.INotificationServiceCallbackAPI
import com.iwedia.notificationservice.lib.RecommendedContentItem
import android.app.admin.DevicePolicyManager as DevicePolicyManager1


class MainActivity : AppCompatActivity() {
    lateinit var mContext: Context

    var adminComponent: ComponentName? = null
    companion object {
        var devicePolicyManager: DevicePolicyManager1? = null
        lateinit var activity: MainActivity
    }

    internal var mService: INotificationServiceAPI? = null
    internal var mBound = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        devicePolicyManager = getSystemService(Context.DEVICE_POLICY_SERVICE) as android.app.admin.DevicePolicyManager
//        adminComponent = ComponentName(packageName, "$packageName.DeviceAdministrator")
//        // Request device admin activation if not enabled.
//        if (!devicePolicyManager!!.isAdminActive(adminComponent)) {
//            val activateDeviceAdmin = Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN)
//            activateDeviceAdmin.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, adminComponent)
//            startActivityForResult(activateDeviceAdmin, DPM_ACTIVATION_REQUEST_CODE)
//        }
        setContentView(R.layout.activity_main)
        mContext = this
        activity = this
        try {
            RjioUtilityServiceHandler.getInstance().connect(mContext)
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    private fun doBindService() {
        val serviceIntent: Intent = Intent()
                .setComponent(ComponentName(
                        "com.iwedia.notificationservice",
                        "com.iwedia.notificationservice.lib.NotificationService"))
        serviceIntent.putExtra("start_foreground_service", true)
        startService(serviceIntent)
        Handler().postDelayed({
            val serviceIntent: Intent = Intent()
                    .setComponent(ComponentName(
                            "com.iwedia.notificationservice",
                            "com.iwedia.notificationservice.lib.NotificationService"))
            if (!bindService(serviceIntent, mConnection, Context.BIND_AUTO_CREATE)) {
                Log.e("MainActivity", "Error: The requested service doesn't " +
                        "exist, or this client isn't allowed access to it.")
            }
        }, 200)
    }

    private val mConnection: ServiceConnection = object : ServiceConnection {
        override fun onServiceConnected(className: ComponentName?, service: IBinder?) {
            // We've bound to the Service,
            mService = INotificationServiceAPI.Stub.asInterface(service)
            mBound = true
            try {
                mService!!.remoteServiceSetCallBacks(mCallBacks)
            } catch (e: RemoteException) {
                e.printStackTrace()
            }
        }

        override fun onServiceDisconnected(arg0: ComponentName?) {
            mService = null
            mBound = false
        }
    }


    private val mCallBacks = CallBacks()

    class CallBacks : INotificationServiceCallbackAPI.Stub() {


        override fun notificationServiceWifiNetwConnectedEvent(netwInfo: NetworkInfo?, wifiSSID: String) {
            activity.runOnUiThread(Runnable {
               /* Toast.makeText(activity, "Notification client test activity:" +
                        "\nNetwork event:" +
                        "\nWiFi connected: " + wifiSSID,
                        Toast.LENGTH_LONG
                ).show()*/
            })
        }

        override fun notificationServiceEthernetNetwConnectedEvent(netwInfo: NetworkInfo?) {
            activity.runOnUiThread(Runnable {
               /* Toast.makeText(activity, "Notification client test activity:" +
                        "\nNetwork event:" +
                        "\nEthernet connected",
                        Toast.LENGTH_LONG
                ).show()*/
            })
        }

        override fun notificationServiceNetwDisconnected() {
            activity.runOnUiThread(Runnable {
               /* Toast.makeText(activity, "Notification client test activity:" +
                        "\nNetwork event:" +
                        "\nNetwork disonnected",
                        Toast.LENGTH_LONG
                ).show()*/
            })
        }

        override fun notificationServiceUsbDevAttachedEvent(usbDevice: UsbDevice) {
            activity.runOnUiThread(Runnable {
               /* Toast.makeText(activity, "Notification client test activity:" +
                        "\nUsb event: " +
                        "\nNEW usb device ATTACHED:" +
                        "\nProduct name: " + usbDevice.productName +
                        "\nManufacturer name: " + usbDevice.manufacturerName +
                        "\nDevice name: " + usbDevice.deviceName,
                        Toast.LENGTH_LONG
                ).show()*/
            })
        }

        override fun notificationServiceUsbDevDetachedEvent(usbDevice: UsbDevice) {
            activity.runOnUiThread(Runnable {
               /* Toast.makeText(activity, "Notification client test activity:" +
                        "\nUsb event: " +
                        "\nUsb device DETACHED:" +
                        "\nProduct name: " + usbDevice.productName +
                        "\nManufacturer name: " + usbDevice.manufacturerName +
                        "\nDevice name: " + usbDevice.deviceName,
                        Toast.LENGTH_LONG
                ).show()*/
            })
        }

        override fun notificationServiceNotificationPosted(notification: Notification, uniqueSbnKey: String) {
            activity.runOnUiThread(Runnable {
                val bundle: Bundle? = notification.extras
                var title : String? = ""
                var text  : String? = ""
                if (bundle != null) {
                    title = bundle.getString("android.title")
                    text = bundle.getString("android.text")
                }
               /* Toast.makeText(activity, "Notification client test activity:" +
                        "\nNotification event: \nTitle: " +
                        "\nNew Notification RECEIVED: \nTitle: " + title +
                        "\nText: " + text +
                        "\nUniqueKey: " + uniqueSbnKey,
                        Toast.LENGTH_LONG

                ).show()*/
                if (activity.mService != null) {
                    try {
                        activity.mService!!.remoteServiceCancelNotification(uniqueSbnKey)
                    } catch (e: RemoteException) {
                        e.printStackTrace()
                    }
                }
            })
        }

        override fun notificationServiceNotificationDeleted(notification: Notification, uniqueSbnKey: String) {
            activity.runOnUiThread(Runnable {
                val bundle: Bundle? = notification.extras
                var title: String? = ""
                var text : String?= ""
                if (bundle != null) {
                    title = bundle.getString("android.title")
                    text = bundle.getString("android.text")
                }
                /*  Toast.makeText(activity, "Notification client test activity:" +
                          "\nNotification event: " +
                          "\nNotification DELETED: \nTitle: " + title +
                          "\nText: " + text +
                          "\nUniqueKey: " + uniqueSbnKey,
                          Toast.LENGTH_LONG

                  ).show()*/
            })
        }

        override fun newRecommendedContentAvailable() {
            if (activity.mService != null) {
                try {
                    activity.mService!!.remoteServiceGetRecommendedContent()
                } catch (e: RemoteException) {
                    e.printStackTrace()
                }
            }
        }

        override fun recommendedContentData(p0: MutableList<RecommendedContentItem>?)
        {

        }

        /* override fun recommendedContentData(recommendedContentItemList: List<RecommendedContentItem>) {
             val recommendedContentItemArrayList = recommendedContentItemList as ArrayList<RecommendedContentItem>
             val adapter = RecommendedContentAdapter(activity, recommendedContentItemArrayList)
             activity.runOnUiThread(Runnable { rec.setAdapter(adapter) })
         }*/


        override fun notificationRCUBattery(percentage: Int) {
            activity.runOnUiThread(Runnable {
               /* Toast.makeText(activity, "Battery level" +
                        "\nPercentage: " + percentage.toString() + " %",
                        Toast.LENGTH_LONG

                ).show()*/
            })
        }
    }


    override fun onKeyUp(keyCode: Int, event: KeyEvent?): Boolean {
        Log.d("TAG", "KeyCode:" + keyCode)
        return super.onKeyUp(keyCode, event)
    }
    override fun onDestroy() {
        super.onDestroy()

        try {
            RjioUtilityServiceHandler.getInstance().unBindService(mContext)

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

}