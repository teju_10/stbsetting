package com.rjil.jiostbsetting.network

import android.content.Context
import android.os.AsyncTask
import android.util.Log
import com.iwedia.ui.rjio.launcher.reliance.internate.NetWorkInfoUtility
import com.iwedia.ui.rjio.launcher.reliance.internate.OnConnectionCallback


class CheckNetworkConnection(private val context: Context?,
                             private val onConnectionCallback: OnConnectionCallback) : AsyncTask<Void, Void, Boolean>() {

    override fun onPreExecute() {
        super.onPreExecute()
    }

    override fun doInBackground(vararg params: Void): Boolean? {
        return if (context == null) false else NetWorkInfoUtility().isNetWorkAvailableNow(context!!)

    }

    override fun onPostExecute(b: Boolean?) {
        super.onPostExecute(b)

        if (b!!) {
            Log.e("connected", "Connected")
            onConnectionCallback.onConnectionSuccess()
        } else {
            var msg = "No Internet Connection"
            Log.e("connected", "Not Connected")

            if (context == null)
                msg = "Context is null"
            onConnectionCallback.onConnectionFail(msg)
        }

    }

}