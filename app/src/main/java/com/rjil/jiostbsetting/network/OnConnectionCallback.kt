package com.iwedia.ui.rjio.launcher.reliance.internate

interface OnConnectionCallback {
    fun onConnectionSuccess()

    fun onConnectionFail(errorMsg: String)
}