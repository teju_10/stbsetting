package com.rjil.jiostbsetting

import android.content.Intent
import android.content.pm.IPackageDataObserver
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.preference.PreferenceManager
import android.util.Log
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.rjil.jiostbsetting.utils.Constant
import com.rjil.jiostbsetting.utils.SharedPreferance
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import java.io.File
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import java.lang.reflect.InvocationTargetException
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T


class AlertActivity : AppCompatActivity() {

    private val REQUEST_UNINSTALL: Int = 0
    var btnReset: Button? = null
    var btnCancel: Button? = null
    var alertHeader: TextView? = null
    var tv_lbl: TextView? = null

    var pName = ArrayList<String>()

//    companion object {
//        fun newInstance(): AlertActivity {
//            return AlertActivity()
//        }
//    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.alert_fragment)
//        val activity = activity
//        if (activity is DetailsActivity) {
//            activity.hideImageHolder()
//        }

        btnReset = findViewById(R.id.btnReset) as Button?
        btnCancel = findViewById(R.id.btnCancel) as Button?
        tv_lbl = findViewById(R.id.tv_lbl) as TextView?

        alertHeader = findViewById(R.id.alertHeader) as TextView
        var strTitle: String = intent.getStringExtra("title")
        alertHeader?.setText("" + strTitle)
        btnReset?.setText("" + strTitle)

        if (strTitle.equals("Reset")) {
            tv_lbl?.text = "Reset to restore your JioGigaTV to factory settings and erase all settings and information."

        } else if (strTitle.equals("Restart")) {
            tv_lbl?.text = "Restart your JioGigaTV"

        } else if (strTitle.equals("Uninstall")) {
            tv_lbl?.text = "Select uninstall to remove this application from the JioGigaTv."

        } else if (strTitle.equals("Force stop")) {
            tv_lbl?.text = "Select forced stop to stop this application from the JioGigaTv."

        } else if (strTitle.equals("Clear Data")) {
            tv_lbl?.text = "Select clear data to clear all the history from this application."

        } else if (strTitle.equals("Clear Cache")) {
            tv_lbl?.text = "Select clear cache to clear cache from this application."

        } else if (strTitle.equals("Uninstall Updates")) {
            tv_lbl?.text = "Select uninstall updates to move the application in the previous version from the JioGigaTv."

        } else if (strTitle.equals("Disable Application")) {
            tv_lbl?.text = "Do you want to disable this app?"

        } else if (strTitle.equals("Enable Application")) {
            tv_lbl?.text = "Do you want to enable this app?"

        }


        btnCancel?.setOnFocusChangeListener { view: View, b: Boolean ->
            if (b) {
                btnCancel!!.background = resources.getDrawable(R.drawable.cancel_shape_select)
                val anim = AnimationUtils.loadAnimation(this, R.anim.anim_scale_big)
                btnCancel?.startAnimation(anim)
                anim.fillAfter = true

            } else {
                btnCancel?.background = resources.getDrawable(R.drawable.rv_focused_bg)
                val anim = AnimationUtils.loadAnimation(this, R.anim.anim_scale_small)
                btnCancel?.startAnimation(anim)
                anim.fillAfter = true
            }
        }

        btnReset?.setOnFocusChangeListener { view: View, b: Boolean ->
            if (b) {
                btnReset!!.background = resources.getDrawable(R.drawable.reset_shape)
                val anim = AnimationUtils.loadAnimation(this, R.anim.anim_scale_big)
                btnReset?.startAnimation(anim)
                anim.fillAfter = true

            } else {
                btnReset?.background = resources.getDrawable(R.drawable.reset_shape_unselect)
                val anim = AnimationUtils.loadAnimation(this, R.anim.anim_scale_small)
                btnReset?.startAnimation(anim)
                anim.fillAfter = true
            }
        }

        btnCancel?.setOnClickListener {
            Log.e("Action", "Cancel")
//            fragmentManager!!.popBackStack()
            finish()
        }

        btnReset?.setOnClickListener {
            Log.e("Action", "Reset")

            if (strTitle.equals("Reset")) {
                Constant.factoryReset(applicationContext!!)

            } else if (strTitle.equals("Restart")) {
                Constant.rebootBOX(applicationContext!!)

            } else if (strTitle.equals("Uninstall")) {
                var packagename: String = intent.getStringExtra("packagename")
                val intent = Intent(Intent.ACTION_DELETE);
                intent.setData(Uri.parse("package:$packagename"));
                startActivity(intent)

                SharedPreferance.setUninstall(applicationContext!!, true)
                finish()

            } else if (strTitle == "Forced Stop") {
                var packagename: String = intent.getStringExtra("packagename")
                Constant.forceStopPackage(applicationContext!!, packagename)

//                val appSharedPrefs = PreferenceManager.getDefaultSharedPreferences(applicationContext!!)
//                val gson = Gson()
//                val json = appSharedPrefs.getString("packageName", "")
//                val type = object : TypeToken<List<String>>() {
//                }.type
//                if (json != null && !json.equals(""))
//                    pName = gson.fromJson(json, type)
//
//
//                pName.add(packagename);
//                val prefsEditor = appSharedPrefs.edit()
//                var pList = gson.toJson(pName)
//                prefsEditor.putString("packageName", pList)
//                prefsEditor.commit()

//                SharedPreferance.setForcePackage(applicationContext, packagename)
                SharedPreferance.setForceFlag(applicationContext!!, true)
                finish()

            } else if (strTitle.equals("Clear Data")) {

                var packagename: String = intent.getStringExtra("packagename")
                Log.e("@@ PAckage Name: ", packagename)
                var comnd = "pm clear $packagename"
                val proc: Process? = Runtime.getRuntime().exec(comnd)
                proc!!.waitFor()
                SharedPreferance.setUninstall(applicationContext!!, true)
                finish()
            } else if (strTitle == "Clear Cache") {
                try {
                    val m = packageManager
                    var packagename: String = intent.getStringExtra("packagename")
                    val p = m.getPackageInfo(packagename, 0)
                    var s: String = p.applicationInfo.dataDir
                    val file = File("$s/cache/")
                    println("SYSTEM PATH $file")
                   clearCache(m)
                } catch (e: Exception) {
                    e.printStackTrace()
                }

                SharedPreferance.setUninstall(applicationContext!!, true)
                finish()
            } else if (strTitle.equals("Uninstall Updates")) {

                Constant.uninstallUpdates(applicationContext!!, packageName)
                SharedPreferance.setUninstall(applicationContext!!, true)
                finish()

            } else if (strTitle.equals("Enable Application")) {
                var packagename: String = intent.getStringExtra("packagename")
                Constant.EnableApp(this, packagename)
                SharedPreferance.setUninstall(applicationContext!!, true)
                finish()

            } else if (strTitle.equals("Disable Application")) {
                var packagename: String = intent.getStringExtra("packagename")
                Constant.DisableApp(this, packagename)
                SharedPreferance.setUninstall(applicationContext!!, true)
                finish()
            }


        }
    }

    private val CACHE_APP = java.lang.Long.MAX_VALUE
    private var mClearCacheObserver: CachePackageDataObserver? = null

    fun clearCache(mPM:PackageManager) {

        if (mClearCacheObserver == null) {
            mClearCacheObserver = CachePackageDataObserver()
        }

        val classes = arrayOf(java.lang.Long.TYPE, IPackageDataObserver::class.java)
        val localLong = java.lang.Long.valueOf(CACHE_APP)
        try {
            val localMethod = mPM.javaClass.getMethod("freeStorageAndNotify", *classes)
            try {
                localMethod.invoke(mPM, localLong, mClearCacheObserver)
            } catch (e: IllegalArgumentException) {
                e.printStackTrace()
            } catch (e: IllegalAccessException) {
                e.printStackTrace()
            } catch (e: InvocationTargetException) {
                e.printStackTrace()
            }

        } catch (e1: NoSuchMethodException) {
            e1.printStackTrace()
        }

    }

    private inner class CachePackageDataObserver : IPackageDataObserver.Stub() {
        override fun onRemoveCompleted(packageName: String, succeeded: Boolean) {

        }
    }

}
