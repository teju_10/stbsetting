package com.rjil.jiostbsetting.fragments.apps

import android.content.Context
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.content.pm.PermissionInfo
import android.graphics.Rect
import android.os.Bundle
import android.os.UserHandle
import androidx.annotation.Nullable
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Switch
import com.rjil.jiostbsetting.R
import com.rjil.jiostbsetting.adapters.apps.AppPermissionAdapters
import com.rjil.jiostbsetting.listener.OnItemClickListener
import com.rjil.jiostbsetting.listener.addOnItemClickListener
import com.rjil.jiostbsetting.models.PermissionInfos
import com.rjil.jiostbsetting.utils.Constant
import java.lang.reflect.InvocationTargetException
import java.util.*
import kotlin.collections.ArrayList


class AppPermissionsFragment : Fragment() {

    private val mPackageManager: PackageManager? = null

    companion object {
        var itemValue = arrayOf("")
        var itemCount: Int = 0
        var itemArray = arrayOf("")
        fun newInstance(): AppPermissionsFragment {
            return AppPermissionsFragment()
        }
    }

    var packagename: String = ""
    var permissionList: ArrayList<PermissionInfos>? = null
    var permissionList1: ArrayList<PermissionInfos>? = null
    private lateinit var permissionAdpter: AppPermissionAdapters

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.fragment_app_permissions, container, false)
        AppPermissionsFragment.itemValue = Constant.PERMISSION_VALUE
        AppPermissionsFragment.itemArray = Constant.APP_PERMISSION
        val activity = activity
        val context = activity?.applicationContext
        permissionList = ArrayList()
        permissionList1 = ArrayList()
        packagename = this.arguments!!.getString("pkgname").toString()
        getPermissionsByPackageName(packagename)

        val recyclerView = view.findViewById(R.id.app_permissions_fragment) as RecyclerView
        val manager = GridLayoutManager(context, 1)
        manager.setOrientation(LinearLayoutManager.VERTICAL)
        manager.supportsPredictiveItemAnimations()
        recyclerView!!.setLayoutManager(manager)
        val animator = DefaultItemAnimator()
        recyclerView!!.setItemAnimator(animator)
        val itemSpace = resources.getDimensionPixelSize(R.dimen.recyclerView_item_space)
        recyclerView!!.addItemDecoration(SpaceItemDecoration(itemSpace))
        //  recyclerView!!.setSelectPadding(35, 34, 35, 34)
        permissionAdpter = AppPermissionAdapters(context!!, permissionList1!!)
        recyclerView.adapter = permissionAdpter

        recyclerView.addOnItemClickListener(object : OnItemClickListener {
            override fun onItemViewClick(position: Int, view: View) {
                var switch1: Switch? = view?.findViewById(R.id.switch_item)!!
                if (switch1!!.isChecked) {
                    if (removePermission(context, packagename, permissionList1?.get(position)!!.getPermissionName())) {
                        switch1.isChecked = !switch1.isChecked
                        permissionList1?.get(position)!!.pGranted = false
                    }

                } else {
                    if (grantPermission(context, packagename, permissionList1?.get(position)!!.getPermissionName())) {
                        switch1.isChecked = true
                        permissionList1?.get(position)!!.pGranted = true
                    }

                }
            }

            override fun onItemViewFocusChanged(gainFocus: Boolean, view: View, position: Int) {
            }


        })
        return view

    }

    private fun getPermissionsForGroup(groupName: String): ArrayList<String> {
        val pm = context?.getPackageManager()
        val permissionNameList = ArrayList<String>()

        try {
            val permissionInfoList = pm?.queryPermissionsByGroup(groupName, PackageManager.GET_META_DATA)
            if (permissionInfoList != null) {
                for (permInfo in permissionInfoList!!) {
                    var permName: String? = permInfo.name
                    if (permName == null) {
                        permName = "null"
                    } else if (permName.isEmpty()) {
                        permName = "empty"
                    }
                    permissionNameList.add(permName)
                }
            }
        } catch (e: PackageManager.NameNotFoundException) {
            // e.printStackTrace();
            Log.d("TAG", "permissions not found for group = $groupName")
        }

        Collections.sort(permissionNameList)

        return permissionNameList
    }

    internal fun grantPermission(context: Context, packageName: String, permission: String): Boolean {
        try {
            val packageManager = context.packageManager
            val method = packageManager.javaClass
                    .getMethod("grantRuntimePermission", String::class.java, String::class.java, UserHandle::class.java)

            method.invoke(packageManager, packageName, permission, android.os.Process.myUserHandle())
        } catch (e: NoSuchMethodException) {
            Log.e("TAG", "NoSuchMethodException while granting permission", e)
            return false
        } catch (e: InvocationTargetException) {
            Log.e("TAG", "InvocationTargetException while granting permission", e)
            return false
        } catch (e: IllegalAccessException) {
            Log.e("TAG", "IllegalAccessException while granting permission", e)
            return false
        }

        return true
    }

    internal fun removePermission(context: Context, packageName: String, permission: String): Boolean {
        try {
            val packageManager = context.packageManager
            val method = packageManager.javaClass
                    .getMethod("revokeRuntimePermission", String::class.java, String::class.java, UserHandle::class.java)

            method.invoke(packageManager, packageName, permission, android.os.Process.myUserHandle())
        } catch (e: NoSuchMethodException) {
            Log.e("TAG", "NoSuchMethodException while granting permission", e)
            return false
        } catch (e: InvocationTargetException) {
            Log.e("TAG", "InvocationTargetException while granting permission", e)
            return false
        } catch (e: IllegalAccessException) {
            Log.e("TAG", "IllegalAccessException while granting permission", e)
            return false
        }

        return true
    }


    // Custom method to get app requested and granted permissions from package name
    protected fun getPermissionsByPackageName(packageName: String): String {
        // Initialize a new string builder instance
        val builder = StringBuilder()

        try {
            // Get the package info
            val packageInfo = activity?.getPackageManager()?.getPackageInfo(packageName, PackageManager.GET_PERMISSIONS)


            // Loop through the package info requested permissions
            for (i in packageInfo!!.requestedPermissions.indices) {
                val permission = packageInfo.requestedPermissions[i]
                val permissionInfo = context?.let { getPermissionInfo(it, permission) }
                Log.e("PermissionName", "" + permissionInfo?.group)
                val permissionInfos = PermissionInfos();

                if (permissionInfo?.group != null) {
                    if (packageInfo!!.requestedPermissionsFlags[i] and PackageInfo.REQUESTED_PERMISSION_GRANTED != 0) {
                        permissionInfos.setGroupName(permissionInfo.group.substring(permissionInfo.group.lastIndexOf(".") + 1))
                        permissionInfos.setPermissionName(permissionInfo.name)
                        permissionInfos.setPGranted(true)
                        Log.e("Permission", "" + permissionInfo?.name)

                    } else {
                        permissionInfos.setGroupName(permissionInfo.group.substring(permissionInfo.group.lastIndexOf(".") + 1))
                        permissionInfos.setPermissionName(permissionInfo.name)
                        permissionInfos.setPGranted(false)
                    }
                    permissionList?.add(permissionInfos)

                }

            }
            permissionList?.distinctBy { it -> it.groupName }?.let { it1 -> permissionList1?.addAll(it1) }
            Log.e("PermissionSize", "" + permissionList?.distinctBy { it -> it.groupName }?.size)

        } catch (e: Exception) {
            e.printStackTrace()
        }

        return builder.toString()
    }

    @Nullable
    fun getPermissionInfo(context: Context, permission: String): PermissionInfo? {
        try {
            return context.packageManager.getPermissionInfo(permission, PackageManager.GET_META_DATA)
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }

        return null
    }

    inner class SpaceItemDecoration internal constructor(private val space: Int) : RecyclerView.ItemDecoration() {

        override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView,
                                    state: RecyclerView.State) {
            outRect.top = space
        }
    }

    override fun onDestroy() {
        super.onDestroy()
    }
}


