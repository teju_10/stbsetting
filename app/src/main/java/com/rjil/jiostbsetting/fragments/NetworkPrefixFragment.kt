package com.rjil.jiostbsetting.fragments


import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import com.rjil.jiostbsetting.DetailsActivity
import com.rjil.jiostbsetting.R
import kotlinx.android.synthetic.main.fragment_network_prefix.view.*

class NetworkPrefixFragment : Fragment() {
    companion object {
        fun newInstance(): NetworkPrefixFragment {
            return NetworkPrefixFragment()
        }
    }

    var prefixLength: EditText? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.fragment_network_prefix, container, false)
        prefixLength = view.prefixLength
        val activity = this.activity
        if (activity is DetailsActivity) {
            activity.hideImageHolder()
        }

        prefixLength!!.setOnKeyListener { _, keyCode, keyEvent ->
            if (keyEvent.action == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                addDNS1Fragment("IP SETTINGS", prefixLength?.text.toString())

                return@setOnKeyListener true
            }
            return@setOnKeyListener false
        }
        return view
    }

    fun addDNS1Fragment(title: String, prefixLength: String) {
        val header = activity?.findViewById<TextView>(R.id.headerText)
        header?.setText(title)
        val fm = activity?.supportFragmentManager
        val ft = fm?.beginTransaction()
        val llf = DNS1Fragment.newInstance();
        val bundle: Bundle = Bundle()
        bundle.putString("title", title)
        bundle.putString("ip_address", arguments?.getString("ip_address"))
        bundle.putString("getway", arguments?.getString("getway"))
        bundle.putString("prefixLength", prefixLength)
        llf.arguments = bundle
        ft?.replace(R.id.sub_details_container, llf, "DNS1Fragment")
        ft?.addToBackStack("DNS1Fragment")
        ft?.commit()

        val newBackStackLength: Int = fm?.getBackStackEntryCount()!!.plus(1);

        fm?.addOnBackStackChangedListener(object : FragmentManager.OnBackStackChangedListener {
            override fun onBackStackChanged() {
                val nowCount = fm.getBackStackEntryCount()
                if (newBackStackLength !== nowCount) {
                    if (!DetailsFragment.isStartNowClicked) {
                        // we don't really care if going back or forward. we already performed the logic here.
                        fm.removeOnBackStackChangedListener(this)

                        if (newBackStackLength > nowCount) { // user pressed back
                            //  fm.popBackStackImmediate()
                            header?.setText(arguments?.getString("title"))
                        }
                    }
                }
            }
        })
    }

}
