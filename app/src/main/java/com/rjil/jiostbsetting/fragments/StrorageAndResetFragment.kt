package com.rjil.jiostbsetting.fragments

import android.Manifest.permission.READ_EXTERNAL_STORAGE
import android.Manifest.permission.WRITE_EXTERNAL_STORAGE
import android.app.AlertDialog
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.graphics.Rect
import android.hardware.usb.UsbDevice
import android.hardware.usb.UsbManager
import android.os.Build
import android.os.Bundle
import android.os.Environment
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat.getSystemService
import com.github.mjdev.libaums.UsbMassStorageDevice
import com.rjil.jiostbsetting.R
import com.rjil.jiostbsetting.adapters.DetailsSubAdapter
import com.rjil.jiostbsetting.fragments.DetailsFragment.Companion.ACTION_USB_PERMISSION
import com.rjil.jiostbsetting.listener.OnItemClickListener
import com.rjil.jiostbsetting.listener.addOnItemClickListener
import com.rjil.jiostbsetting.tvrecyclerview.TvRecyclerView
import com.rjil.jiostbsetting.utils.Const
import com.rjil.jiostbsetting.utils.Constant
import com.rjil.jiostbsetting.utils.DreamInfo
import com.rjil.jiostbsetting.utils.StorageInformation
import java.io.*
import java.text.DecimalFormat


class StrorageAndResetFragment : Fragment() {
    lateinit var adapter: DetailsSubAdapter
    lateinit var devices: Array<UsbMassStorageDevice>
    lateinit var device: UsbMassStorageDevice
    var availabesize: String = ""
    var filter = IntentFilter(ACTION_USB_PERMISSION)

    companion  object {
        var itemCount: Int = 0
        var itemArray = arrayOf("")
        var itemValue = arrayOf("", "", "", "", "", "", "")
        var dreamInfo: List<DreamInfo> = emptyList()
        var title: String = ""
        val PERMISSION_REQUEST_CODE = 101

        fun newInstance(): StrorageAndResetFragment {
            return StrorageAndResetFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.fragment_sub_details_menu, container,
                false)

        val activity = activity
        val recyclerView = view.findViewById<RecyclerView>(R.id.sub_details_recycler_view) as RecyclerView
        recyclerView.layoutManager = GridLayoutManager(activity, 1) as RecyclerView.LayoutManager?
        val bundle = this.arguments
        title = bundle?.getString("title")!!

        filter.addAction(UsbManager.ACTION_USB_DEVICE_ATTACHED)
        filter.addAction(UsbManager.ACTION_USB_DEVICE_DETACHED)

        //Constant.getCurrentUser(activity!!)
        if (title.equals("Device Storage")) {
            itemValue[0] = Constant.InternalStorage(activity?.applicationContext!!)
            itemValue[1] = bundle?.getString("cachedData")!!
            itemValue[2] = bundle?.getString("appSize")!!
//            itemValue[1] = Constant.Cache(activity?.applicationContext!!)
//            itemValue[2] = Constant.App(activity?.applicationContext!!)
            itemValue[3] = Constant.getDownloadStorage()

            //  itemValue[3] = Constant.Downloads(context!!)
            itemValue[4] = Const.getAvailableInternalMemorySize()
            itemArray = Constant.DEVICE_STORAGE
            itemCount = Constant.DEVICE_STORAGE.size

        } else if (title.equals("External Storage")) {
            devices = UsbMassStorageDevice.getMassStorageDevices(context!!)
            setupDevice()

        }

        adapter = DetailsSubAdapter(context!!, itemArray, itemValue, Constant.NORMAL_ITEM, title)

        if (itemCount > 0) {
            val manager = GridLayoutManager(context, itemCount)
            manager.orientation = LinearLayoutManager.HORIZONTAL
            recyclerView.layoutManager = manager
            val itemSpace = resources.getDimensionPixelSize(R.dimen.recyclerView_item_space)
            recyclerView.addItemDecoration(SpaceItemDecoration(itemSpace))
            //recyclerView.setSelectPadding(35, 34, 35, 34)
            recyclerView.adapter = adapter
        }


        recyclerView.addOnItemClickListener(object : OnItemClickListener {
            override fun onItemViewClick(position: Int, view: View) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onItemViewFocusChanged(gainFocus: Boolean, view: View, position: Int) {
                println("DetailsSubFragment  $gainFocus")
                if (title == "Device Storage")
                    recyclerView.isFocusable = false
            }

        })


// && usbManager.hasPermission(usbDevice)
        return view
    }

    override fun onResume() {
        super.onResume()
        context!!.registerReceiver(usbReceiver,filter)
    }

    override fun onPause() {
        super.onPause()
        context!!.unregisterReceiver(usbReceiver)
    }

    inner class SpaceItemDecoration internal constructor(private val space: Int) : RecyclerView.ItemDecoration() {

        override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView,
                                    state: RecyclerView.State) {
            outRect.top = space
        }
    }

    fun setupDevice() {

        for (device in devices) {
            device.init()

            // Only uses the first partition on the device
            val currentFs = device.partitions[0].fileSystem
            Log.d("StorageFrag", "Capacity: " + currentFs.capacity)
            Log.d("StorageFrag", "Occupied Space: " + currentFs.occupiedSpace)
            Log.d("StorageFrag", "Free Space: " + currentFs.freeSpace)
            Log.d("StorageFrag", "Chunk size: " + currentFs.chunkSize)
            availabesize = getFileSize(currentFs.capacity)
            println("Get File Size " + getFileSize(currentFs.capacity))
            itemValue[0] = getFileSize(currentFs.occupiedSpace)
            itemValue[1] = getFileSize(currentFs.freeSpace)
            itemArray = Constant.EXTERNAL_STORAGE
            itemCount = Constant.EXTERNAL_STORAGE.size
        }
    }

    fun getFileSize(size: Long): String {
        if (size <= 0)
            return "0"

        val units = arrayOf("B", "KB", "MB", "GB", "TB")
        val digitGroups = (Math.log10(size.toDouble()) / Math.log10(1024.0)).toInt()

        return DecimalFormat("#,##0.#").format(size / Math.pow(1024.0, digitGroups.toDouble())) + " " + units[digitGroups]
    }


    private val usbReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {

            var action = intent.action
            Log.d(Constant.TAG, "Received Broadcast: $action")

            if (UsbManager.ACTION_USB_DEVICE_ATTACHED == intent.action) {
                synchronized(this) {

                }
            } else if (UsbManager.ACTION_USB_DEVICE_DETACHED == intent.action) {
                fragmentManager!!.popBackStack()
            }
        }

    }

}