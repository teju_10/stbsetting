package com.rjil.jiostbsetting.fragments

import android.graphics.Rect
import android.os.Bundle
import android.provider.Settings
import android.provider.Settings.Global.DEVELOPMENT_SETTINGS_ENABLED
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Switch
import com.rjil.jiostbsetting.R
import com.rjil.jiostbsetting.adapters.DetailsSubAdapter
import com.rjil.jiostbsetting.listener.OnItemClickListener
import com.rjil.jiostbsetting.listener.addOnItemClickListener
import com.rjil.jiostbsetting.tvrecyclerview.TvRecyclerView
import com.rjil.jiostbsetting.utils.Constant
import com.rjil.jiostbsetting.utils.DreamInfo

class DeveloperFragment : Fragment() {

    companion object {
        var itemCount: Int = 0
        var itemArray = arrayOf("")
        var itemValue = arrayOf("")
        var dreamInfo: List<DreamInfo> = emptyList()
        var title: String = ""

        fun newInstance(): DeveloperFragment {
            return DeveloperFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.fragment_sub_details_menu, container,
                false)
        val activity = activity
        val recyclerView = view.findViewById<RecyclerView>(R.id.sub_details_recycler_view) as RecyclerView
        recyclerView.layoutManager = GridLayoutManager(activity, 1) as RecyclerView.LayoutManager?
        val bundle = this.arguments
        title = bundle?.getString("title")!!
        var adapter: DetailsSubAdapter
        itemCount = Constant.DEVELOPER_SUB_OPTION.size
        itemArray = Constant.DEVELOPER_SUB_OPTION
        itemValue = Constant.DEVELOPER_SUB_OPTION_VALUE
        adapter = DetailsSubAdapter(context!!, itemArray, itemValue, Constant.NORMAL_ITEM, title)
        if (itemCount > 0) {
            val manager = GridLayoutManager(context, itemCount)
            manager.orientation = LinearLayoutManager.HORIZONTAL
            recyclerView.layoutManager = manager
            val itemSpace = resources.getDimensionPixelSize(R.dimen.recyclerView_item_space)
            recyclerView.addItemDecoration(SpaceItemDecoration(itemSpace))
            recyclerView.adapter = adapter
        }
        recyclerView.addOnItemClickListener(object : OnItemClickListener {
            override fun onItemViewClick(position: Int, view: View) {
                when (position) {
                    0 -> if (view?.tag.toString().equals("SWITCH")) {
                        var switch: Switch? = view?.findViewById(R.id.switch_item)!!
                        if (switch != null) {
                            Constant.enableDevMode(activity?.applicationContext!!, !switch.isChecked)
                            switch.isChecked = Constant.getDevMode(activity?.applicationContext!!)
                            if (!switch.isChecked) {
                                Constant.setStayAwake(activity?.applicationContext!!, false)
                                Constant.setBLEHCILog(activity?.applicationContext!!, false)
                                Constant.setAdbDebug(activity?.applicationContext!!, false)
                                adapter.notifyItemChanged(1)
                                adapter.notifyItemChanged(2)
                                adapter.notifyItemChanged(3)
                            }
                        }
                    }
                    1 ->

                        if (isEnabled() == 1) {
                            if (view?.tag.toString().equals("SWITCH")) {
                                var switch: Switch? = view?.findViewById(R.id.switch_item)!!
                                if (switch != null) {
                                    Constant.setStayAwake(activity?.applicationContext!!, !switch.isChecked)
                                    switch.isChecked = Constant.getStayAwake(activity?.applicationContext!!)
                                }
                            }
                        } else {
                            var switch: Switch? = view?.findViewById(R.id.switch_item)!!
                            switch?.isChecked = false

                    }
                    2 ->

                        if (isEnabled() == 1) {
                            if (view?.tag.toString().equals("SWITCH")) {
                                var switch: Switch? = view?.findViewById(R.id.switch_item)!!
                                if (switch != null) {
                                    Constant.setBLEHCILog(activity?.applicationContext!!, !switch.isChecked)
                                    switch.isChecked = Constant.getBLEHCILog(activity?.applicationContext!!)
                                }
                            }
                        } else {
                            var switch: Switch? = view?.findViewById(R.id.switch_item)!!
                            switch?.isChecked = false

                    }
                    3 -> /*if(view?.tag.toString().equals("SWITCH")) {
                        var switch: Switch? = view?.findViewById(R.id.switch_item)!!
                        if (switch != null) {
                            Constant.setAdbDebug(activity?.applicationContext!!, !switch.isChecked)
                            switch.isChecked = Constant.getAdbDebug(activity?.applicationContext!!)
                        }*/

                        if (isEnabled() == 1) {
                            if (view?.tag.toString().equals("SWITCH")) {
                                var switch: Switch? = view?.findViewById(R.id.switch_item)!!
                                if (switch != null) {
                                    Constant.setAdbDebug(activity?.applicationContext!!, !switch.isChecked)
                                    switch.isChecked = Constant.getAdbDebug(activity?.applicationContext!!)
                                }
                            }
                        } else {
                            var switch: Switch? = view?.findViewById(R.id.switch_item)!!
                            switch?.isChecked = false
                        }

                    4 -> {
                        // revoke USB debugging
                    }
                    5 -> {
                        // logger buffer sizes
                    }
                }
            }

            override fun onItemViewFocusChanged(gainFocus: Boolean, view: View, position: Int) {
//                if (gainFocus) {
//                    view!!.background = context!!.resources.getDrawable(R.drawable.rl_shape_corner)
//                } else {
//                    view!!.background = context!!.resources.getDrawable(R.drawable.rv_focused_bg)
//                }
            }


        })
        return view
    }

    private fun isEnabled(): Int {
        return Settings.Secure.getInt(activity?.contentResolver, DEVELOPMENT_SETTINGS_ENABLED, 0)
    }

    inner class SpaceItemDecoration internal constructor(private val space: Int) : RecyclerView.ItemDecoration() {

        override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView,
                                    state: RecyclerView.State) {
            outRect.top = space
        }
    }
}