package com.rjil.jiostbsetting.fragments

import android.app.Notification
import android.content.*
import android.graphics.Rect
import android.hardware.usb.UsbDevice
import android.net.NetworkInfo
import android.net.wifi.ScanResult
import android.net.wifi.WifiInfo
import android.net.wifi.WifiManager
import android.os.*
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.iwedia.fti.rjio.fti_sdk.RjioSdk
import com.iwedia.notificationservice.INotificationServiceAPI
import com.iwedia.notificationservice.INotificationServiceCallbackAPI
import com.iwedia.notificationservice.lib.RecommendedContentItem
import com.rjil.jiostbsetting.DetailsActivity
import com.rjil.jiostbsetting.R
import com.rjil.jiostbsetting.SettingApplication
import com.rjil.jiostbsetting.WpsActivity
import com.rjil.jiostbsetting.adapters.network.WifiDetailsAdapter
import com.rjil.jiostbsetting.custom.CustomGridLayout
import com.rjil.jiostbsetting.listener.OnItemClickListener
import com.rjil.jiostbsetting.listener.addOnItemClickListener
import com.rjil.jiostbsetting.main.ProgressDialog
import com.rjil.jiostbsetting.utils.Const
import com.rjil.jiostbsetting.utils.Constant
import com.rjil.jiostbsetting.utils.Utils
import com.rjil.stbwifilibrary.WiFiManager
import java.util.*
import kotlin.collections.ArrayList

class WifiFragment : Fragment() /*OnWifiConnectListener*/ {


    companion object {

        var itemCount: Int = 0
        var itemArray = arrayOf("")
        var itemValue = arrayOf("")
        private const val PERMISSIONS_REQUEST_CODE_ACCESS_COARSE_LOCATION = 120
        fun newInstance(): WifiFragment {
            return WifiFragment()
        }
        const val WIFI_CAPABILITIES_KEY = "capabilities"
        const val WIFI_KEY = "wifi"
        lateinit var recyclerView: RecyclerView

        var itemWifiScaned: ArrayList<ScanResult>? = null

    }

    var lastSelectedPosition = 0
    var scanResult: ScanResult? = null
    var pos : Int = 1
    var progress: RelativeLayout? = null
    var progress_dialog : ProgressDialog?=null
    var header: TextView? = null
    var itemWifiScaned1: ArrayList<ScanResult>? = null
    public lateinit var wifiDetailAdpter: WifiDetailsAdapter
    private var mWiFiManager: WiFiManager? = null
    private val wifiManager: WifiManager
        get() = activity?.getSystemService(Context.WIFI_SERVICE) as WifiManager
    var info: WifiInfo? = null
    var currentssid : String?=null

    internal var mService: INotificationServiceAPI? = null
    internal var mBound = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View = inflater?.inflate(R.layout.fragment_wifi_list, container, false)
        val activity = activity

        val args = arguments
        if (args != null)
            itemWifiScaned = args.getParcelableArrayList("list")
        mWiFiManager = WiFiManager(activity)

        // Refresh Lists New List
       /* if (NetworkDetailsFragment.isRefreshList!!)
            itemWifiScaned=ArrayList<ScanResult>(itemWifiScaned1)
        else
        {
            itemWifiScaned=itemWifiScaned1
        }*/
//        itemWifiScaned=itemWifiScaned1
//        Log.e("rrrr", "" + itemWifiScaned!!.size)
        activity?.registerReceiver(wifiReceiver, IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION))

        doBindService()


        header = activity?.findViewById(R.id.headerText) as TextView
//        wifilist_progress = view.findViewById(R.id.wifilist_progress)
        progress_dialog = view.findViewById(R.id.progress_dialog)
//        progressDialog.setDialogMessage(java.lang.String.format("%s\n%s", getString(R.string.press_wps_button), getString(R.string.press_wps_button_hint)))
        header?.setText(args!!.getString("title"))
        recyclerView = view.findViewById(R.id.details_recycler_view) as RecyclerView
        progress = view.findViewById<RelativeLayout>(R.id.progress) as RelativeLayout
        val manager = CustomGridLayout(context, 1)
        manager.setOrientation(LinearLayoutManager.VERTICAL)
        manager.supportsPredictiveItemAnimations()
        recyclerView.setLayoutManager(manager)
        Utils.hideKeyboardDefault(activity,view)
        val animator = DefaultItemAnimator()
        recyclerView.setItemAnimator(null)
        val itemSpace = resources.getDimensionPixelSize(R.dimen.recyclerView_item_space)
        recyclerView.addItemDecoration(SpaceItemDecoration(itemSpace))
//        recyclerView!!.setSelectPadding(35, 34, 35, 34)
        info = wifiManager.connectionInfo
//        recyclerView.recycledViewPool.clear()
        currentssid=info!!.ssid.replace("\"", "")
       /* val comparator = object: Comparator<ScanResult> {
            override fun compare(lhs:ScanResult, rhs:ScanResult):Int {
                return (if (lhs.level < rhs.level) -1 else (if (lhs.level === rhs.level) 0 else 1))
            }
        }
        val results = wifiManager.scanResults
        Collections.sort(results, comparator)*/




        wifiDetailAdpter = WifiDetailsAdapter(context!!, itemWifiScaned!!, mWiFiManager!!.configuredNetworks, currentssid!!)
        recyclerView.adapter = wifiDetailAdpter

        recyclerView.addOnItemClickListener(object : OnItemClickListener {

            override fun onItemViewClick(position: Int, view: View) {
                try {
                    lastSelectedPosition = position
                    scanResult = itemWifiScaned!![position]

                    if(RjioSdk.get().networkHandler.isEthernetConnected)
                    {
                        Toast.makeText(activity, R.string.no_internt_access, Toast.LENGTH_LONG).show()
                    }
                    else
                    {
                        // TODO to check weather network is configured n/w or not
                        if(mWiFiManager!!.getConfigFromConfiguredNetworksBySsids(scanResult!!.SSID,mWiFiManager!!.configuredNetworks))
                        {
                            if(position==0)
                            {
                                val handler = Handler(Looper.getMainLooper())
                                handler.postDelayed(Runnable {
                                    if (isWifiConncted(scanResult!!)) {
                                        addWifiConfigFragment("Wi-Fi", true,lastSelectedPosition)
                                    } else {
                                        /*if(WpsActivity.isWpsCancelled)
                                        {

                                        }*/
                                        val intent = Intent(activity,WpsActivity::class.java)
                                        intent.putExtra("loader","SavedNetwork")
                                        intent.putExtra("SSID",scanResult!!.SSID)
                                        intent.putExtra("position",lastSelectedPosition)
                                        startActivity(intent)
//                                        addWifiConfigFragment("Wi-Fi", false, lastSelectedPosition)
                                    }
                                },300)
                            }
                            else
                            {
                                if(!mWiFiManager!!.connectToSavedNetwork(scanResult!!.SSID))
                                {
//                                mWiFiManager!!.removeNetwork(scanResult!!.SSID)

                                    val handler = Handler(Looper.getMainLooper())
                                    handler.postDelayed(Runnable {
                                        if (isWifiConncted(scanResult!!)) {
                                            addWifiConfigFragment("Wi-Fi", true, lastSelectedPosition)
                                        } else {
                                            addWifiConfigFragment("Wi-Fi", false, lastSelectedPosition)
                                        }
                                    },300)
                                }
                                else
                                {
                                    // TODO saved Network
                                        val intent = Intent(activity,WpsActivity::class.java)
                                        intent.putExtra("loader","SavedNetwork")
                                        intent.putExtra("SSID",scanResult!!.SSID)
                                        intent.putExtra("position",lastSelectedPosition)
                                        startActivity(intent)
                                }
                            }
                        }
                        else
                        {
                            // TODO if ssid is not configured then we need to enter password
                            if(scanResult!!.capabilities.contains("WEP") || scanResult!!.capabilities.contains("WPA") || scanResult!!.capabilities.contains("WPA2") || scanResult!!.capabilities.contains("802.1x EAP"))
                            {
                                Handler().postDelayed({
                                    if (isWifiConncted(scanResult!!)) {
                                        addWifiConfigFragment("Wi-Fi", true, lastSelectedPosition)
                                    } else {
                                        addWifiConfigFragment("Wi-Fi", false, lastSelectedPosition)
                                    }
                                },200)

                            }
                            else
                            {
                                // TODO open network
                                    val intent = Intent(activity,WpsActivity::class.java)
                                    intent.putExtra("loader","OpenNetwork")
                                    intent.putExtra("SSID",scanResult!!.SSID)
                                    intent.putExtra("position",lastSelectedPosition)
                                    startActivity(intent)
                            }
                        }
                    }

                } catch (ex: Exception) {
                    ex.printStackTrace()
                }


            }

            override fun onItemViewFocusChanged(gainFocus: Boolean, view: View, position: Int) {

            }
        })

        recyclerView.addOnLayoutChangeListener(View.OnLayoutChangeListener { view, i, i2, i3, i4, i5, i6, i7, i8 ->
            try {
                var vPos: RecyclerView.ViewHolder?
                try {
                    if(view!=null && lastSelectedPosition>0)
                    {
                        if(WpsActivity.isnotify)
                        {
                            vPos = recyclerView.findViewHolderForAdapterPosition(0)
                            vPos!!.itemView.requestFocus()
                        }
                        else
                        {
                            vPos = recyclerView.findViewHolderForAdapterPosition(lastSelectedPosition)
                            vPos!!.itemView.requestFocus()
                        }
                    }

                }catch (e: java.lang.Exception)
                {
                    e.printStackTrace()
                }

            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }

        })

//            recyclerView.recycledViewPool.clear()
        wifiDetailAdpter.notifyDataSetChanged()


        return view
    }


    private fun doBindService() {
        val serviceIntent: Intent = Intent()
                .setComponent(ComponentName(
                        "com.iwedia.notificationservice",
                        "com.iwedia.notificationservice.lib.NotificationService"))
        serviceIntent.putExtra("start_foreground_service", true)
        context!!.startService(serviceIntent)
        Handler().postDelayed({
            val serviceIntent: Intent = Intent()
                    .setComponent(ComponentName(
                            "com.iwedia.notificationservice",
                            "com.iwedia.notificationservice.lib.NotificationService"))
            if (!context!!.bindService(serviceIntent, mConnection, Context.BIND_AUTO_CREATE)) {
                Log.e("DetailActivity", "Error: The requested service doesn't " +
                        "exist, or this client isn't allowed access to it.")
            }
        }, 200)
    }

    private val mConnection: ServiceConnection = object : ServiceConnection {
        override fun onServiceConnected(className: ComponentName?, service: IBinder?) {
            // We've bound to the Service,
            mService = INotificationServiceAPI.Stub.asInterface(service)
            mBound = true
            try {
                mService!!.remoteServiceSetCallBacks(mCallBacks)
            } catch (e: RemoteException) {
                e.printStackTrace()
            }
        }

        override fun onServiceDisconnected(arg0: ComponentName?) {
            mService = null
            mBound = false
        }
    }


    private val mCallBacks = CallBacks()

    class CallBacks : INotificationServiceCallbackAPI.Stub() {


        override fun notificationServiceWifiNetwConnectedEvent(netwInfo: NetworkInfo?, wifiSSID: String) {


            Handler(Looper.getMainLooper()).post(object:Runnable {
                override fun run() {
                 /*   Toast.makeText(context, "Notification client test activity:" +
                            "\nNetwork event:" +
                            "\nWiFi connected: " + wifiSSID,
                            Toast.LENGTH_LONG
                    ).show()*/
                    println("Fragment Connnnnnnnn")
                    SettingApplication.setSSID(wifiSSID.replace("\"",""))
                }
            })

            if(WpsActivity.isWpsCancelled)
            {
                Handler(Looper.getMainLooper()).postDelayed({
//                    Collections.swap(itemWifiScaned,Constant.lastselectedposition!!,0)
//                    recyclerView.adapter!!.notifyItemMoved(Constant.lastselectedposition!!,0)
                recyclerView.adapter!!.notifyDataSetChanged()
                },1000)
            }
            WpsActivity.isWpsCancelled=false

        }

        override fun notificationServiceEthernetNetwConnectedEvent(netwInfo: NetworkInfo?) {

        }

        override fun notificationServiceNetwDisconnected() {


            Handler(Looper.getMainLooper()).post(object:Runnable {
                override fun run() {
                    /*Toast.makeText(DetailsActivity.activity, "Notification client test activity:" +
                            "\nNetwork event:" +
                            "\nNetwork disonnected",
                            Toast.LENGTH_LONG
                    ).show()*/
                    println("Fragment DisConnnnnnnnn")
                    SettingApplication.setSSID("")
//                    recyclerView.adapter!!.notifyDataSetChanged()
                }
            })

           /* Handler(Looper.getMainLooper()).postDelayed({
                recyclerView.adapter!!.notifyDataSetChanged()
            },1000)*/
        }

        override fun notificationServiceUsbDevAttachedEvent(usbDevice: UsbDevice) {

        }

        override fun notificationServiceUsbDevDetachedEvent(usbDevice: UsbDevice) {

        }

        override fun notificationServiceNotificationPosted(notification: Notification, uniqueSbnKey: String) {
        }

        override fun notificationServiceNotificationDeleted(notification: Notification, uniqueSbnKey: String) {

        }

        override fun newRecommendedContentAvailable() {

        }

        override fun recommendedContentData(p0: MutableList<RecommendedContentItem>?)
        {

        }

        /* override fun recommendedContentData(recommendedContentItemList: List<RecommendedContentItem>) {
             val recommendedContentItemArrayList = recommendedContentItemList as ArrayList<RecommendedContentItem>
             val adapter = RecommendedContentAdapter(activity, recommendedContentItemArrayList)
             activity.runOnUiThread(Runnable { rec.setAdapter(adapter) })
         }*/


        override fun notificationRCUBattery(percentage: Int) {

        }
    }


    private val wifiReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val results = wifiManager.getScanResults()
            info = wifiManager!!.connectionInfo

            val connSSID = info?.ssid?.replace("\"", "")

            if (results != null && results.size > 0) {
                itemWifiScaned?.clear()
                itemWifiScaned1?.clear()
                itemWifiScaned?.addAll(results.distinctBy { it -> it.SSID })
//                itemWifiScaned=ArrayList<ScanResult>(itemWifiScaned1)

                try {
                    for (item in itemWifiScaned?.indices!!) {
                        if (item < itemWifiScaned!!.size)
                        {
                            val str = itemWifiScaned!![item].SSID
                            Log.d("ssid+bssid", str + itemWifiScaned?.get(item)?.BSSID)
                            if (isNullOrEmpty(str)) {
                                println("ssiddrop")
                                itemWifiScaned!!.remove(itemWifiScaned?.get(item))
                            } else {
                                if (itemWifiScaned?.get(item)?.SSID.equals(connSSID)) {
                                    Collections.swap(itemWifiScaned, item, 0)
                                    Log.e("List", "" + itemWifiScaned)

                                }
                            }
                        }
                    }

                } catch (e: Exception) {
                    e.printStackTrace()
                }

                wifiDetailAdpter.notifyDataSetChanged()

            }

        }

    }


    fun isNullOrEmpty(str: String?): Boolean {
        if (str != null && !str.trim().isEmpty())
            return false
        return true
    }

    override fun onDestroy() {
        super.onDestroy()
        activity?.unregisterReceiver(wifiReceiver)
    }


    inner class SpaceItemDecoration internal constructor(private val space: Int) : RecyclerView.ItemDecoration() {

        override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView,
                                    state: RecyclerView.State) {
            outRect.top = space
        }
    }


    private fun isWifiConncted(scanResult: ScanResult): Boolean {
        info = wifiManager!!.connectionInfo
        val connSSID = info?.ssid?.replace("\"", "")
        if (scanResult.SSID.equals(connSSID)) {
            return true
        }
        return false
    }




    fun addWifiConfigFragment(title: String, isConnected: Boolean, position: Int) {
        //    val header = activity?.findViewById<TextView>(R.id.headerText)
        header?.setText(title)
        val fm = activity?.supportFragmentManager
        val ft = fm?.beginTransaction()
        val llf = WifiConfigFragment.newInstance()
        val bundle: Bundle = Bundle()
        bundle.putString("title", title)
        bundle.putInt("position",position)
        bundle.putBoolean("Connected", isConnected)
        bundle.putParcelable("scanResult", scanResult)
        llf.arguments = bundle
        ft?.replace(R.id.sub_details_container, llf, "WifiConfigFragment")
        ft?.addToBackStack("WifiConfigFragment")
        ft?.commit()

        val newBackStackLength: Int = fm?.getBackStackEntryCount()!!.plus(1)

        fm.addOnBackStackChangedListener(object : FragmentManager.OnBackStackChangedListener {
            override fun onBackStackChanged() {
                val nowCount = fm.getBackStackEntryCount()
                if (newBackStackLength !== nowCount) {
                    if (!DetailsFragment.isStartNowClicked) {
                        // we don't really care if going back or forward. we already performed the logic here.
                        fm.removeOnBackStackChangedListener(this)

                        if (newBackStackLength > nowCount) { // user pressed back
                            //  fm.popBackStackImmediate()

                            header?.setText(arguments?.getString("title"))
                           /* if (checkPermissions()) {
                                startScanning()
                                wifiDetailAdpter.notifyDataSetChanged()
                            }*/
                        }
                    }
                }
            }
        })
    }

    override fun onResume() {
        super.onResume()
        if(WpsActivity.isnotify)
        {
            Collections.swap(itemWifiScaned,Constant.lastselectedposition!!,0)
            wifiDetailAdpter.notifyItemMoved(Constant.lastselectedposition!!,0)
            wifiDetailAdpter.notifyDataSetChanged()
            recyclerView.layoutManager!!.scrollToPosition(0)
        }

    }



  /*  override fun onResume() {
        super.onResume()
       *//* if(!RjioSdk.get().networkHandler.isEthernetConnected)
            mWiFiManager!!.setOnWifiConnectListener(this)*//*
    }

    override fun onPause() {
        super.onPause()
     *//*   if(!RjioSdk.get().networkHandler.isEthernetConnected)
            mWiFiManager!!.removeOnWifiConnectListener()*//*
    }
*/
   /* override fun onWiFiConnectLog(log: String?) {
    }

    override fun onWiFiConnectSuccess(SSID: String?) {
        Toast.makeText(activity, "$SSID  Connected Successfully!!", Toast.LENGTH_SHORT).show()
        loader(false, SSID!!)

    }

    override fun onWiFiConnectFailure(SSID: String?) {
        Toast.makeText(activity, "Connection Failed", Toast.LENGTH_SHORT).show()
        loader(false, SSID!!)
    }*/

}





