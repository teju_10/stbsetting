package com.rjil.jiostbsetting.fragments;

import android.content.Context
import android.graphics.Rect
import android.os.Bundle
import android.provider.Settings
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodInfo
import android.view.inputmethod.InputMethodManager
import com.rjil.jiostbsetting.R
import com.rjil.jiostbsetting.adapters.Language_Keyboard.KeyboardChangeAdapter
import com.rjil.jiostbsetting.listener.OnItemClickListener
import com.rjil.jiostbsetting.listener.addOnItemClickListener
import java.util.*

class KeyboardChangeFragment : Fragment() {

    private val ARG_PARAM1 = "param1"
    private val ARG_PARAM2 = "param2"
    var recyclerView_Adapter: RecyclerView.Adapter<*>? = null
    var arrayLang: ArrayList<String> = ArrayList()
    lateinit var mContext: Context
    var fragment: Fragment? = null
    // TODO: Rename and change types of parameters
    private var mParam1: String? = null
    private var mParam2: String? = null

    companion object {
        public fun newInstance(param1: String): KeyboardChangeFragment {
            val fragment = KeyboardChangeFragment()
            val args = Bundle()
            args.putString("param1", param1)
            // args.putString(ARG_PARAM2, ConfriType);
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            mParam1 = arguments!!.getString(ARG_PARAM1)
            mParam2 = arguments!!.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.fragment_keyboardchange, container,
                false)
        mContext = this!!.activity!!

        val activity = activity
        val change_keyboard_recy_view = view.findViewById(R.id.change_keyboard_recy_view) as RecyclerView
        change_keyboard_recy_view!!.layoutManager = GridLayoutManager(activity, 1) as RecyclerView.LayoutManager?
        val itemSpace = resources.getDimensionPixelSize(R.dimen.recyclerView_item_space)
        change_keyboard_recy_view.addItemDecoration(SpaceItemDecoration(itemSpace))
//        change_keyboard_recy_view.setSelectPadding(35, 34, 35, 34)

        val bundle = this.arguments
        StrorageAndResetFragment.title = bundle?.getString("title")!!
        val lm = LinearLayoutManager(context)
        change_keyboard_recy_view!!.setLayoutManager(lm)
        recyclerView_Adapter = KeyboardChangeAdapter(mContext, arrayLang)
        change_keyboard_recy_view!!.setAdapter(recyclerView_Adapter);
        detechKeyboardLang()

        change_keyboard_recy_view.addOnItemClickListener(object : OnItemClickListener {
            override fun onItemViewClick(position: Int, view: View) {
                when (position) {
                    0 -> {
//                        view!!.background = context!!.resources.getDrawable(R.drawable.rl_shape_corner)
                    }
                }
            }

            override fun onItemViewFocusChanged(gainFocus: Boolean, view: View, position: Int) {
                println("LanguageChangeFragment  $gainFocus")

            }
        })

        return view
    }

    fun detechKeyboardLang() {

        val imm = activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        val ims = imm.enabledInputMethodList
        val n: Int = ims.size

        for (i in 0 until n) {
            val imi: InputMethodInfo = ims.get(i)
            if (imi.getId().equals(Settings.Secure.getString(context!!.getContentResolver(), Settings.Secure.DEFAULT_INPUT_METHOD))) {
                Log.e("LOG  detechKeyboardLang", imi.loadLabel(context!!.packageManager) as String)
                arrayLang.add(imi.loadLabel(context!!.packageManager) as String)
                break
            }
        }
    }

    inner class SpaceItemDecoration internal constructor(private val space: Int) : RecyclerView.ItemDecoration() {
        override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView,
                                    state: RecyclerView.State) {
            outRect.top = space
        }
    }
}
