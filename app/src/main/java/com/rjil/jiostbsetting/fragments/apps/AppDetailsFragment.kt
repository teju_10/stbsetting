package com.rjil.jiostbsetting.fragments.apps

import android.content.Context
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.content.pm.PermissionInfo
import android.graphics.Rect
import android.os.Bundle
import android.os.Handler
import android.util.Log
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.rjil.jiostbsetting.AlertFragment
import com.rjil.jiostbsetting.R
import com.rjil.jiostbsetting.adapters.Apps_DetailsAdapter
import com.rjil.jiostbsetting.listener.OnItemClickListener
import com.rjil.jiostbsetting.listener.addOnItemClickListener
import com.rjil.jiostbsetting.utils.Constant
import kotlinx.android.synthetic.main.activity_details.*
import android.widget.FrameLayout
import android.widget.RelativeLayout
import androidx.annotation.Nullable
import com.rjil.jiostbsetting.models.PermissionInfos
import com.rjil.jiostbsetting.utils.SharedPreferance
import com.rjil.jiostbsetting.utils.StorageInformation
import java.util.ArrayList


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER


class AppDetailsFragment : Fragment() {

    companion object {

        fun newInstance(): AppDetailsFragment {
            return AppDetailsFragment()
        }
    }

    var permissionList: ArrayList<PermissionInfos>? = null
    var permissionList1: ArrayList<PermissionInfos>? = null
    lateinit var apprecyclerView: RecyclerView

    lateinit var appversion_title: TextView
    lateinit var apppkg_title: TextView
    lateinit var appversion_tv: TextView
    lateinit var apppkg_tv: TextView


    var alOptions = ArrayList<String>()
    var alItems = ArrayList<String>()

    var lastSelectedPosition = 0
    var packagename: String = ""
    var versionName: String = ""
    lateinit var appType: String

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.fragment_applist_details, container, false)


        val activity = activity
        if (SharedPreferance.getUninstall(activity)) {
         //   SharedPreferance.setUninstall(activity, false)
            fragmentManager!!.popBackStack()
        }

        val context = activity?.applicationContext
        alOptions = ArrayList()
        alItems = ArrayList()
        permissionList = ArrayList()
        permissionList1 = ArrayList()


        apprecyclerView = view.findViewById<RecyclerView>(R.id.app_details_fragment) as RecyclerView
        appversion_tv = view.findViewById(R.id.appversion_tv) as TextView
        apppkg_tv = view.findViewById(R.id.apppkg_tv) as TextView
        appversion_title = view.findViewById(R.id.appversion_title) as TextView
        apppkg_title = view.findViewById(R.id.apppkg_title) as TextView

        setPackageNameGone()

        packagename = this.arguments!!.getString("pkgname").toString()
        versionName = this.arguments!!.getString("version").toString()
        val name = this.arguments!!.getString("name").toString()
//        val versionName = this.arguments!!.getString("version").toString()
        val className = this.arguments!!.getString("class").toString()
        val srcDir = this.arguments!!.getString("src").toString()
        val userId = this.arguments!!.getInt("uid")

        getPermissionsByPackageName(packagename)

        appType = this.arguments!!.getString("previous_title")

        apprecyclerView.layoutManager = GridLayoutManager(activity!!, 1)
        val itemSpace = resources.getDimensionPixelSize(R.dimen.recyclerView_item_space)
        apprecyclerView.addItemDecoration(SpaceItemDecoration(itemSpace))


        apprecyclerView.addOnItemClickListener(object : OnItemClickListener {
            override fun onItemViewClick(position: Int, view: View) {

                lastSelectedPosition = position

                val Fram = apprecyclerView.getChildAt(position) as FrameLayout
                val rel = Fram.getChildAt(0) as RelativeLayout
                val text = rel.getChildAt(2) as TextView
                Log.e("TextName!!!!#@@@@: ", text.text.toString())


                if (text.text == "Clear Data") {
                    openAlert("Clear Data", packagename, AlertFragment.newInstance())
                } else if (text.text.equals("Clear Cache")) {
                    openAlert("Clear Cache", packagename, AlertFragment.newInstance())
                } else if (text.text.equals("Uninstall")) {
                    openAlert("Uninstall", packagename, AlertFragment.newInstance())
                } else if (text.text.equals("Force stop")) {
                    openAlert("Force stop", packagename, AlertFragment.newInstance())
                } else if (text.text == "Uninstall Updates") {
                    openAlert("Uninstall Updates", packagename, AlertFragment.newInstance())
                } else if (text.text == "Permissions" && permissionList1 != null && permissionList1!!.size > 0) {
                    openAlert("Application Permission", packagename, AppPermissionsFragment.newInstance())
                } else if (text.text.equals("Enable")) {
                    openAlert("Enable Application", packagename, AlertFragment.newInstance())
                } else if (text.text.equals("Disable")) {
                    openAlert("Disable Application", packagename, AlertFragment.newInstance())
                }
            }

            override fun onItemViewFocusChanged(gainFocus: Boolean, view: View, position: Int) {

            }
        })

        apprecyclerView.addOnLayoutChangeListener { view, i, i2, i3, i4, i5, i6, i7, i8 ->
            try {
                var vPos: RecyclerView.ViewHolder?
                if (view != null && lastSelectedPosition >= 0) {
                    vPos = apprecyclerView.findViewHolderForAdapterPosition(lastSelectedPosition)
                    vPos!!.itemView.requestFocus()
                    Log.d("CurrentFocusedView", "Focused")
                } else {
                    Log.d("Change Lay Selected Pos", lastSelectedPosition.toString())
                }
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }


        }
        return view

    }

    fun setPackageNameGone() {
        apppkg_title.text = ""
        appversion_title.text = ""
        appversion_tv.text = ""
        apppkg_tv.text = ""
    }

    fun setPackageNameVisible() {

        appversion_title.text = "Version"
        apppkg_title.text = "Package Name"
        appversion_tv.text = ("Version $versionName")
        apppkg_tv.text = ("" + packagename)
    }

    @Nullable
    fun getPermissionInfo(context: Context, permission: String): PermissionInfo? {
        try {
            return context.packageManager.getPermissionInfo(permission, PackageManager.GET_META_DATA)
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }

        return null
    }

    fun getPermissionsByPackageName(packageName: String): String {
        // Initialize a new string builder instance
        val builder = StringBuilder()

        try {
            // Get the package info
            val packageInfo = activity?.getPackageManager()?.getPackageInfo(packageName, PackageManager.GET_PERMISSIONS)


            // Loop through the package info requested permissions
            for (i in packageInfo!!.requestedPermissions.indices) {
                val permission = packageInfo.requestedPermissions[i]
                val permissionInfo = context?.let { getPermissionInfo(it, permission) }
                Log.e("PermissionName", "" + permissionInfo?.group)
                val permissionInfos = PermissionInfos()

                if (permissionInfo?.group != null) {
                    if (packageInfo!!.requestedPermissionsFlags[i] and PackageInfo.REQUESTED_PERMISSION_GRANTED != 0) {
                        permissionInfos.setGroupName(permissionInfo.group.substring(permissionInfo.group.lastIndexOf(".") + 1))
                        permissionInfos.setPermissionName(permissionInfo.name)
                        permissionInfos.setPGranted(true)
                        Log.e("Permission", "" + permissionInfo?.name)

                    } else {
                        permissionInfos.setGroupName(permissionInfo.group.substring(permissionInfo.group.lastIndexOf(".") + 1))
                        permissionInfos.setPermissionName(permissionInfo.name)
                        permissionInfos.setPGranted(false)
                    }
                    permissionList?.add(permissionInfos)

                }

            }
            permissionList?.distinctBy { it -> it.groupName }?.let { it1 -> permissionList1?.addAll(it1) }
            Log.e("PermissionSize", "" + permissionList?.distinctBy { it -> it.groupName }?.size)

        } catch (e: Exception) {
            e.printStackTrace()
        }

        return builder.toString()
    }

    inner class SpaceItemDecoration internal constructor(private val space: Int) : RecyclerView.ItemDecoration() {

        override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView,
                                    state: RecyclerView.State) {
            outRect.top = space
        }
    }

    private fun openAlert(title: String, packagename: String, newInstance: Fragment) {
        val header = activity?.findViewById<TextView>(R.id.headerText)
        header?.setText(title)
        val fm = activity?.supportFragmentManager
        val ft = fm?.beginTransaction()
//        val llf = AlertFragment.newInstance()
        val llf = newInstance
        val args = Bundle()
        args.putString("title", title)
        args.putString("pkgname", packagename)
        llf.arguments = args
        ft?.replace(R.id.sub_details_container, llf, "alert_frag")
        ft?.addToBackStack("alert_frag")
        ft?.commit()

        val newBackStackLength: Int = fm?.getBackStackEntryCount()!!.plus(1)

        fm?.addOnBackStackChangedListener(object : FragmentManager.OnBackStackChangedListener {
            override fun onBackStackChanged() {
                val nowCount = fm.getBackStackEntryCount()
                if (newBackStackLength !== nowCount) {
                    fm.removeOnBackStackChangedListener(this)

                    if (newBackStackLength > nowCount) { // user pressed back
                        //  fm.popBackStackImmediate()
                        //header.setText(activity?.intent?.getStringExtra("title"))
                        activity!!.headerText.text = arguments!!.getString("name").toString()
                    }
                }
            }
        })
    }

    override fun onResume() {
        super.onResume()
        callForceStop()
    }

    private fun callForceStop() {

        val storageInformation = StorageInformation(context!!)
        var alSize = storageInformation.getpackageDetails(packagename)
        Log.e("PackageDetails@@@@@", alSize.size.toString())
        activity!!.headerText.text = arguments!!.getString("name").toString()
        alOptions = ArrayList()
        alItems = ArrayList()

        Handler().postDelayed({
            try {
                if (appType == "Downloaded Apps") {
                    alOptions.add("" + alSize[0].dataSize)
                    alOptions.add("")
                    alOptions.add("" + alSize[0].cacheSize)
                    alOptions.add("")

                    alItems.add("Clear Data")
                    alItems.add("Uninstall")
                    if (!Constant.getStopped(context!!, packagename)) {
                        alItems.add("Force stop")
                        alOptions.add(2, "")
                    }
                    alItems.add("Clear Cache")
                    alItems.add("Permissions")

                } else {
                    if (Constant.enableDisable(packagename)) {
                        //  alOptions = ArrayList()
                        alOptions.add("" + alSize[0].dataSize)
                        alOptions.add("" + alSize[0].cacheSize)
                        alOptions.add("")

                        //alItems = ArrayList()
                        alItems.add("Clear Data")
                        if (Constant.getSystemUninstallUpdatesStatus(context, packagename)) {
                            alItems.add("Uninstall updates")
                            alOptions.add(1, "")
                        }
                        alItems.add("Clear Cache")
                        alItems.add("Permissions")

                    } else {
                        //  alOptions = ArrayList()
                        alOptions.add("" + alSize[0].dataSize)
                        alOptions.add("")
                        alOptions.add("" + alSize[0].cacheSize)
                        alOptions.add("")

                        // alItems = ArrayList()
                        alItems.add("Clear Data")
                        if (Constant.isAppStatus(context!!, packagename)) {
                            alItems.add("Disable")
                        } else {
                            alItems.add("Enable")
                        }
                        if (Constant.getSystemUninstallUpdatesStatus(context, packagename)) {
                            alItems.add("Uninstall Updates")
                            alOptions.add(2, "")

                            if (!Constant.getStopped(context!!, packagename)) {
                                alItems.add("Force stop")
                                alOptions.add(3, "")
                            }
                        } else if (!Constant.getStopped(context!!, packagename)) {
                            alItems.add("Force stop")
                            alOptions.add(2, "")
                        }

                        alItems.add("Clear Cache")
                        alItems.add("Permissions")

                    }
                }

                if (alOptions != null && alOptions.size > 0) {
                    apprecyclerView.adapter = Apps_DetailsAdapter(context!!, alItems, alOptions, permissionList1!!)
                    setPackageNameVisible()
                }

            } catch (exc: Exception) {
                exc.printStackTrace()
            }
        }, 100)
    }
}