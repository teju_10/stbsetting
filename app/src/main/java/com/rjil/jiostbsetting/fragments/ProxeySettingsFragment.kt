package com.rjil.jiostbsetting.fragments


import android.content.Intent
import android.graphics.Rect
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import com.rjil.jiostbsetting.DetailsActivity
import com.rjil.jiostbsetting.R
import com.rjil.jiostbsetting.WpsActivity
import com.rjil.jiostbsetting.adapters.MenuAdapter
import com.rjil.jiostbsetting.fragments.WifiConfigFragment.Companion.SECURITY_TYPE
import com.rjil.jiostbsetting.fragments.WifiFragment.Companion.WIFI_CAPABILITIES_KEY
import com.rjil.jiostbsetting.fragments.WifiFragment.Companion.WIFI_KEY
import com.rjil.jiostbsetting.listener.OnItemClickListener
import com.rjil.jiostbsetting.listener.addOnItemClickListener
import com.rjil.jiostbsetting.tvrecyclerview.TvRecyclerView
import com.rjil.jiostbsetting.utils.Constant
import com.rjil.jiostbsetting.utils.SharedPreferance
import com.rjil.jiostbsetting.utils.Utils
import kotlinx.android.synthetic.main.fragment_proxey_settings.view.*

class ProxeySettingsFragment : Fragment() {

    var lastSelectedPosition = 0
    var securityType : Boolean  =false
    var proxytext: LinearLayout?=null
    var title : String?=null
    var header: TextView? = null
    var wifiName : String?=null

    companion object {
        fun newInstance(): ProxeySettingsFragment {
            return ProxeySettingsFragment()
        }
    }

    lateinit var recyclerView: RecyclerView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.fragment_proxey_settings, container, false)
        val activity = this.activity
        val context = activity?.applicationContext
        recyclerView = view.tv_recycler_view
        proxytext = view.proxytext
        recyclerView?.layoutManager = GridLayoutManager(activity, 1)

        val args = arguments
        if (args != null) {
            securityType = args.getBoolean(SECURITY_TYPE)

            header = activity?.findViewById(R.id.headerText) as TextView
            header?.setText(args!!.getString("title"))

        }

//        Utils.hideKeyboardDefault(activity,view)


        if (securityType) {
            if (activity is DetailsActivity) {
                activity.showImageHolder()
            }
            wifiName=args!!.getString(WIFI_KEY)
            proxytext!!.visibility = View.GONE

            val manager = GridLayoutManager(context, Constant.ADD_NEW_NETWORK_OPTIONS.size)
            manager.orientation = LinearLayoutManager.HORIZONTAL
            recyclerView?.layoutManager = manager
            val itemSpace = resources.getDimensionPixelSize(R.dimen.recyclerView_item_space)
            recyclerView?.addItemDecoration(SpaceItemDecoration(itemSpace))
//        recyclerView?.setSelectPadding(35, 34, 35, 34)
            recyclerView?.adapter = MenuAdapter(context!!, Constant.ADD_NEW_NETWORK_OPTIONS)


            recyclerView?.addOnItemClickListener(object : OnItemClickListener {
                override fun onItemViewClick(position: Int, view: View) {
                    lastSelectedPosition = position
                    when (position) {
                        0 ->
                            setFragment(Constant.ADD_NEW_NETWORK_OPTIONS[0])
                        1 ->
                            setFragment(Constant.ADD_NEW_NETWORK_OPTIONS[1])
                        2 ->
                            setFragment(Constant.ADD_NEW_NETWORK_OPTIONS[2])
                        3 ->
                            setFragment(Constant.ADD_NEW_NETWORK_OPTIONS[3])

                    }
                }

                override fun onItemViewFocusChanged(gainFocus: Boolean, view: View, position: Int) {

                }
            })

            recyclerView.addOnLayoutChangeListener { view, i, i2, i3, i4, i5, i6, i7, i8 ->
                try {
                    var vPos: RecyclerView.ViewHolder?
                    if (view != null && lastSelectedPosition >= 0) {
                        vPos = recyclerView.findViewHolderForAdapterPosition(lastSelectedPosition)
                        vPos!!.itemView.requestFocus()
                        Log.d("CurrentFocusedView", "Focused")

                    } else {
                        Log.d("Change Lay Selected Pos", DetailsFragment.lastSelectedPosition.toString())
                    }


                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                }
            }


        } else {

            if (activity is DetailsActivity) {
                activity.hideImageHolder()
            }

            proxytext!!.visibility = View.VISIBLE

            val manager = GridLayoutManager(context, Constant.PROXY_OPTION.size)
            manager.orientation = LinearLayoutManager.HORIZONTAL
            recyclerView?.setLayoutManager(manager)
            val itemSpace = resources.getDimensionPixelSize(R.dimen.recyclerView_item_space)
            recyclerView?.addItemDecoration(SpaceItemDecoration(itemSpace))
//        recyclerView?.setSelectPadding(35, 34, 35, 34)
            recyclerView?.adapter = MenuAdapter(context!!, Constant.PROXY_OPTION)
            recyclerView?.addOnItemClickListener(object : OnItemClickListener {
                override fun onItemViewClick(position: Int, view: View) {
                    lastSelectedPosition = position
                    when (position) {
                        0 ->
                            Toast.makeText(activity, "Saved successfully!", Toast.LENGTH_LONG).show()
                        1 -> {
                            addManualProxyFragment("Proxy settings")
                        }
                    }
                }

                override fun onItemViewFocusChanged(gainFocus: Boolean, view: View, position: Int) {

                }
            })

            recyclerView.addOnLayoutChangeListener { view, i, i2, i3, i4, i5, i6, i7, i8 ->
                try {
                    var vPos: RecyclerView.ViewHolder?
                    if (view != null && lastSelectedPosition >= 0) {
                        vPos = recyclerView.findViewHolderForAdapterPosition(lastSelectedPosition)
                        vPos!!.itemView.requestFocus()
                        Log.d("CurrentFocusedView", "Focused")

                    } else {
                        Log.d("Change Lay Selected Pos", DetailsFragment.lastSelectedPosition.toString())
                    }


                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                }
            }
        }
        return view
    }

    private fun setFragment(title: String) {


        if(title=="None")
        {
            val intent = Intent(activity, WpsActivity::class.java)
            intent.putExtra("loader","None")
            intent.putExtra("SSID",wifiName)
            startActivity(intent)
        }
        else
        {
            val fm = activity?.supportFragmentManager
            val ft = fm?.beginTransaction()
            val llf = WifiConfigFragment.newInstance()
            val bundle: Bundle = Bundle()
            bundle.putString("title", title)
            bundle.putString(WIFI_KEY, wifiName)
            bundle.putString(WIFI_CAPABILITIES_KEY, title)
            llf.arguments = bundle
            ft?.replace(R.id.sub_details_container, llf, "WifiConfigFragment")
            ft?.addToBackStack("WifiConfigFragment")
            ft?.commit()

            val newBackStackLength: Int = fm?.getBackStackEntryCount()!!.plus(1);

            fm?.addOnBackStackChangedListener(object : FragmentManager.OnBackStackChangedListener {
                override fun onBackStackChanged() {
                    val nowCount = fm.getBackStackEntryCount()
                    if (newBackStackLength !== nowCount) {
                        if (!DetailsFragment.isStartNowClicked) {
                            // we don't really care if going back or forward. we already performed the logic here.
                            fm.removeOnBackStackChangedListener(this)

                            if (newBackStackLength > nowCount) { // user pressed back
                                //  fm.popBackStackImmediate()
                                header?.setText(arguments?.getString("title"))
                                /*    if (checkPermissions()) {
                                        startScanning()
                                        wifiDetailAdpter.notifyDataSetChanged()
                                    }*/
                            }
                        }
                    }
                }
            })
        }

    }



    inner class SpaceItemDecoration internal constructor(private val space: Int) : RecyclerView.ItemDecoration() {

        override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView,
                                    state: RecyclerView.State) {
            outRect.top = space
        }
    }

    fun addManualProxyFragment(title: String) {
        val header = activity?.findViewById<TextView>(R.id.headerText)
        header?.setText(title)
        val fm = activity?.supportFragmentManager
        val ft = fm?.beginTransaction()
        val llf = ManualProxyFragment.newInstance();
        val bundle: Bundle = Bundle()
        bundle.putString("title", title)
        llf.arguments = bundle
        ft?.replace(R.id.sub_details_container, llf, "ManualProxyFragment")
        ft?.addToBackStack("ManualProxyFragment")
        ft?.commit()

        val newBackStackLength: Int = fm?.getBackStackEntryCount()!!.plus(1);

        fm?.addOnBackStackChangedListener(object : FragmentManager.OnBackStackChangedListener {
            override fun onBackStackChanged() {
                val nowCount = fm.getBackStackEntryCount()
                if (newBackStackLength !== nowCount) {
                    if (!DetailsFragment.isStartNowClicked) {
                        // we don't really care if going back or forward. we already performed the logic here.
                        fm.removeOnBackStackChangedListener(this)

                        if (newBackStackLength > nowCount) { // user pressed back
                            //  fm.popBackStackImmediate()
                            header?.setText(arguments?.getString("title"))
                        }
                    }
                }
            }
        })
    }


    override fun onDestroy() {
        val activity = this.activity
        if (activity is DetailsActivity) {
            activity.showImageHolder()
        }
        super.onDestroy()
    }
}
