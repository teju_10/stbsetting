package com.rjil.jiostbsetting.fragments


import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.net.ProxyInfo
import android.net.wifi.WifiConfiguration
import android.net.wifi.WifiManager
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import com.rjil.jiostbsetting.DetailsActivity
import com.rjil.jiostbsetting.R
import com.rjil.jiostbsetting.utils.Constant
import kotlinx.android.synthetic.main.fragment_bypass_proxy.view.*


class BypassProxyFragment : Fragment() {

    companion object {
        fun newInstance(): BypassProxyFragment {
            return BypassProxyFragment()
        }

    }

    private val wifiManager: WifiManager
        get() = activity?.getSystemService(Context.WIFI_SERVICE) as WifiManager
    var proxy_bypass: EditText? = null
    var action_save_Proxy: Button? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.fragment_bypass_proxy, container, false)
        proxy_bypass = view.proxy_bypass
        action_save_Proxy = view.action_save_Proxy
        val activity = this.activity
        if (activity is DetailsActivity) {
            activity.hideImageHolder()
        }
        var hostname = arguments?.getString("proxeyHostName")
        var proxyPort = arguments?.getString("proxyPort")
        Log.e("host", hostname + proxyPort)
        action_save_Proxy?.setOnClickListener {
            var bypassProxy: List<String> = proxy_bypass?.text.toString().split(",").toList()
            Log.e("list", "" + bypassProxy)
            val proxyInfo = ProxyInfo.buildDirectProxy(hostname, proxyPort!!.toInt(), bypassProxy)
            Constant.setProxy(activity!!, proxyInfo)
            val intent = Intent(context, DetailsActivity::class.java)
            intent.putExtra("title", "Network")
            startActivity(intent)
            activity.finish()
        }
        proxy_bypass!!.setOnKeyListener { _, keyCode, keyEvent ->
            if (keyEvent.action == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                //  setWifiProxySettings()


                return@setOnKeyListener true
            }
            return@setOnKeyListener false
        }
        return view
    }

    private fun getCurrentWifiConfiguration(manager: WifiManager): WifiConfiguration? {
        if (!manager.isWifiEnabled)
            return null

        val configurationList = manager.configuredNetworks
        var configuration: WifiConfiguration? = null
        val networkId = manager.connectionInfo.networkId
        for (i in configurationList.indices) {
            val wifiConfiguration = configurationList[i]
            if (wifiConfiguration.networkId == networkId)
                configuration = wifiConfiguration
        }

        return configuration
    }

//    @SuppressLint("PrivateApi")
//    fun testSetProxy(): Boolean {
//        try {
//            val manager = context?.getSystemService(Context.WIFI_SERVICE) as WifiManager
//            val config = getCurrentWifiConfiguration(manager)
//            val proxySettings = Class.forName("android.net.IpConfiguration\$ProxySettings")
//
//            val setProxyParams = arrayOfNulls<Class<*>>(2)
//            setProxyParams[0] = proxySettings
//            setProxyParams[1] = ProxyInfo::class.java
//
//            val setProxy = WifiConfiguration::class.java.getDeclaredMethod("setProxy", *setProxyParams)
//            setProxy.isAccessible = true
//
//            val proxyInfo = ProxyInfo.buildDirectProxy("192.168.1.129", port)
//
//            val methodParams = arrayOfNulls<Any>(2)
//            methodParams[0] = java.lang.Enum.valueOf(proxySettings, "STATIC")
//            methodParams[1] = proxyInfo
//
//            setProxy.invoke(config, methodParams)
//
//            manager.updateNetwork(config)
//
//            val result = getCurrentWifiConfiguration(manager).toString()
//            val key = "Proxy settings: "
//            val start = result.indexOf(key) + key.length
//            if (result.substring(start, start + 4) == "NONE") {
//                throw RuntimeException("Can't update the Network, you should have the right WifiConfiguration")
//            }
//
//            manager.disconnect()
//            manager.reconnect()
//
//            return true
//        } catch (e: Exception) {
//            return false
//        }
//
//    }

    @Throws(SecurityException::class, NoSuchFieldException::class, IllegalArgumentException::class, IllegalAccessException::class)
    fun getField(obj: Any, name: String): Any {
        val f = obj.javaClass.getField(name)
        Log.e("tttt", "" + f.get(obj))
        return f.get(obj)
    }

    @Throws(SecurityException::class, NoSuchFieldException::class, IllegalArgumentException::class, IllegalAccessException::class)
    fun getDeclaredField(obj: Any, name: String): Any {
        val f = obj.javaClass.getDeclaredField(name)
        f.isAccessible = true
        return f.get(obj)
    }

    @Throws(SecurityException::class, NoSuchFieldException::class, IllegalArgumentException::class, IllegalAccessException::class)
    fun setEnumField(obj: Any, value: String, name: String) {
        val f = obj.javaClass.getField(name)
        f.set(obj, value)
        //f.set(obj, Enum.valueOf((Class<Enum>) f.getType(), value));

    }

    @SuppressLint("PrivateApi")
    fun setWifProxy() {
        val proxySettings = Class.forName("android.net.IpConfiguration\$ProxySettings")

        val setProxyParams = arrayOfNulls<Class<*>>(2)
        setProxyParams[0] = proxySettings
        setProxyParams[1] = ProxyInfo::class.java
        val wifiConfigurationClass = Class.forName("android.net.wifi.WifiConfiguration")

        val setProxy = wifiConfigurationClass.getDeclaredMethod("setProxy", *setProxyParams)
        setProxy.isAccessible = true

        val desiredProxy = ProxyInfo.buildDirectProxy("192.168.1.101", 8081)

        val methodParams = arrayOfNulls<Any>(2)
        //   methodParams[0] = Enum.valueOf<Enum>(proxySettings, "STATIC")
        methodParams[1] = desiredProxy

        setProxy.invoke(getCurrentWifiConfiguration(wifiManager), methodParams)
    }

    @Throws(SecurityException::class, IllegalArgumentException::class, NoSuchFieldException::class, IllegalAccessException::class)
    fun setProxySettings(assign: String, wifiConf: WifiConfiguration) {
        setEnumField(wifiConf, assign, "proxySettings")
    }

    fun GetCurrentWifiConfiguration(manager: WifiManager): WifiConfiguration? {
        if (!manager.isWifiEnabled)
            return null

        val configurationList = manager.configuredNetworks
        var configuration: WifiConfiguration? = null
        val cur = manager.connectionInfo.networkId
        var i = 0
        while (i < configurationList.size) {
            val wifiConfiguration = configurationList[i]
            if (wifiConfiguration.networkId == cur)
                configuration = wifiConfiguration
            i++
        }
        return configuration
    }


    @SuppressLint("PrivateApi")
    fun setWifiProxySettings() {
        //get the current wifi configuration
        val config = GetCurrentWifiConfiguration(wifiManager) ?: return

        try {
            //get the link properties from the wifi configuration
            val linkProperties = getField(config, "linkProperties") ?: return
            //get the setHttpProxy method for LinkProperties
            val ProxyInfoClass = Class.forName("android.net.ProxyInfo")
            val setHttpProxyParams = arrayOfNulls<Class<*>>(1)
            setHttpProxyParams[0] = ProxyInfoClass
            val lpClass = Class.forName("android.net.LinkProperties")
            val setHttpProxy = lpClass.getDeclaredMethod("setHttpProxy", *setHttpProxyParams)
            setHttpProxy.isAccessible = true
            //get ProxyProperties constructor
            val proxyPropertiesCtorParamTypes = arrayOfNulls<Class<*>>(3)
            proxyPropertiesCtorParamTypes[0] = String::class.java
            proxyPropertiesCtorParamTypes[1] = Int::class.javaPrimitiveType
            proxyPropertiesCtorParamTypes[2] = String::class.java

            val proxyPropertiesCtor = ProxyInfoClass.getConstructor(*proxyPropertiesCtorParamTypes)

            //create the parameters for the constructor
            val proxyPropertiesCtorParams = arrayOfNulls<Any>(3)
            proxyPropertiesCtorParams[0] = arguments?.getString("proxeyHostName")
            proxyPropertiesCtorParams[1] = arguments?.getString("proxyPort")!!.toInt()
            proxyPropertiesCtorParams[2] = proxy_bypass?.text.toString()

            //create a new object using the params
            val proxySettings = proxyPropertiesCtor.newInstance(proxyPropertiesCtorParams)

            //pass the new object to setHttpProxy
            val params = arrayOfNulls<Any>(1)
            params[0] = proxySettings
            setHttpProxy.invoke(linkProperties, params)

            setProxySettings("STATIC", config)

            //save the settings
            wifiManager!!.updateNetwork(config)
            wifiManager.disconnect()
            wifiManager.reconnect()
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

}
