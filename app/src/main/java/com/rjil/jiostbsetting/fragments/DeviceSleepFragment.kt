package com.rjil.jiostbsetting.fragments

import android.graphics.Rect
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.rjil.jiostbsetting.utils.Constant
import com.rjil.jiostbsetting.R
import com.rjil.jiostbsetting.adapters.DeviceSleepAdapter
import com.rjil.jiostbsetting.listener.OnItemClickListener
import com.rjil.jiostbsetting.listener.addOnItemClickListener
import com.rjil.jiostbsetting.utils.DreamInfo
import com.rjil.jiostbsetting.tvrecyclerview.CenterSmoothScroller
import java.lang.Exception


class DeviceSleepFragment : Fragment() {

    companion object {
        var itemCount: Int = 0
        var itemArray = arrayOf("")
        var itemValue = arrayOf("")
        var title: String = ""
        var lastSelectedPosition = 0

        fun newInstance(): DeviceSleepFragment {
            return DeviceSleepFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.fragment_sub_details_menu, container,
                false)
        val activity = activity
        val recyclerView = view.findViewById<RecyclerView>(R.id.sub_details_recycler_view) as RecyclerView
        recyclerView.layoutManager = GridLayoutManager(activity, 1) as RecyclerView.LayoutManager?
        val bundle = this.arguments
        title = bundle?.getString("title")!!
        var adapter: DeviceSleepAdapter

        itemArray = Constant.DEVICE_SLEEP
        itemValue = Constant.DEVICE_SLEEP_VALUE
        itemCount = Constant.DEVICE_SLEEP.size
        adapter = DeviceSleepAdapter(context!!, itemArray, itemValue, Constant.RADIO_ITEM)

        if (itemCount > 0) {

            val manager = GridLayoutManager(context, itemCount)
            manager.orientation = LinearLayoutManager.HORIZONTAL
            recyclerView.layoutManager = manager
            val itemSpace = resources.getDimensionPixelSize(R.dimen.recyclerView_item_space)
            recyclerView.addItemDecoration(SpaceItemDecoration(itemSpace))
            recyclerView.adapter = adapter

        }

        recyclerView.addOnItemClickListener(object : OnItemClickListener {
            override fun onItemViewClick(position: Int, view: View) {
                Log.d("Put Device to Sleep CLICK ", "Put Device to Sleep")
                lastSelectedPosition = recyclerView.getChildAdapterPosition(view)
                Constant.setSleepTime(activity!!.applicationContext, (itemValue[position].toLong()).toInt())
             //   adapter.setTrackPlaying(position)
                adapter.notifyDataSetChanged()

            }

            override fun onItemViewFocusChanged(gainFocus: Boolean, view: View, position: Int) {
                //  adapter.setTrackPlaying(position)

            }

        })

        recyclerView.addOnLayoutChangeListener(View.OnLayoutChangeListener { view, i, i2, i3, i4, i5, i6, i7, i8 ->
            try {
                var vPos: RecyclerView.ViewHolder?
                if (view != null && lastSelectedPosition >= 0) {
                    vPos = recyclerView.findViewHolderForAdapterPosition(lastSelectedPosition)
                    vPos!!.itemView.requestFocus()
                    Log.d("CurrentFocusedView", "Focused")
                } else {
                    Log.d("Change Lay Selected Pos", lastSelectedPosition.toString())
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        })
        return view
    }


    inner class SpaceItemDecoration internal constructor(private val space: Int) : RecyclerView.ItemDecoration() {

        override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView,
                                    state: RecyclerView.State) {
            outRect.top = space
        }
    }


}