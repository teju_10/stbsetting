package com.rjil.jiostbsetting.fragments

import android.content.Intent
import android.graphics.Rect
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.rjil.jiostbsetting.utils.Constant
import com.rjil.jiostbsetting.DetailsActivity
import com.rjil.jiostbsetting.R
import com.rjil.jiostbsetting.adapters.MenuAdapter
import com.rjil.jiostbsetting.listener.OnItemClickListener
import com.rjil.jiostbsetting.listener.addOnItemClickListener
import com.rjil.jiostbsetting.utils.SharedPreferance

class MenuFragment : Fragment() {
    lateinit var recyclerView: RecyclerView

    companion object {

        fun newInstance(): MenuFragment {
            return MenuFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.fragment_main_menu, container,
                false)
        val activity = activity
        val context = activity?.applicationContext
        recyclerView = view.findViewById<RecyclerView>(R.id.tv_recycler_view) as RecyclerView
        recyclerView.layoutManager = GridLayoutManager(activity, 1)
        val manager = GridLayoutManager(context, Constant.MENU_OPTION.size)
        manager.orientation = LinearLayoutManager.HORIZONTAL
        recyclerView.layoutManager = manager
        val itemSpace = resources.getDimensionPixelSize(R.dimen.recyclerView_item_space)
        recyclerView.addItemDecoration(SpaceItemDecoration(itemSpace))
//        recyclerView.setSelectPadding(35, 34, 35, 34)
        recyclerView.adapter = MenuAdapter(context!!, Constant.MENU_OPTION)

        recyclerView.addOnItemClickListener(object : OnItemClickListener {
            override fun onItemViewClick(position: Int, view: View) {
                val intent = Intent(context, DetailsActivity::class.java)
                intent.putExtra("title", view?.tag.toString())
                SharedPreferance.setItemPos(context,"1")
                startActivity(intent)
            }

            override fun onItemViewFocusChanged(gainFocus: Boolean, view: View, position: Int) {
                if (view != null) {
                    if (gainFocus) {
                        view!!.background = context!!.resources.getDrawable(R.drawable.rl_shape_corner)
                    } else {
                        view!!.background = context!!.resources.getDrawable(R.drawable.rv_focused_bg)
                    }
                }
            }
        })
        return view
    }

    override fun onResume() {
        super.onResume()

    }

    inner class SpaceItemDecoration internal constructor(private val space: Int) : RecyclerView.ItemDecoration() {

        override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView,
                                    state: RecyclerView.State) {
            outRect.top = space
        }
    }
}