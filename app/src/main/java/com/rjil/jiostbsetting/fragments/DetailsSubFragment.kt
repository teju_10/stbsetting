package com.rjil.jiostbsetting.fragments

import android.graphics.Rect
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.rjil.jiostbsetting.utils.Constant
import com.rjil.jiostbsetting.R
import com.rjil.jiostbsetting.adapters.DetailsSubAdapter
import com.rjil.jiostbsetting.listener.OnItemClickListener
import com.rjil.jiostbsetting.listener.addOnItemClickListener
import com.rjil.jiostbsetting.tvrecyclerview.TvRecyclerView


class DetailsSubFragment : Fragment() {
var lastSelectedPosition = 0
    companion object {
        var itemCount : Int = 0
        var itemArray = arrayOf("")
        var itemValue = arrayOf("","","","","")

        fun newInstance(): DetailsSubFragment {
            return DetailsSubFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.fragment_sub_details_menu, container,
                false)
        val activity = activity
        val recyclerView = view.findViewById<RecyclerView>(R.id.sub_details_recycler_view) as RecyclerView
        recyclerView.layoutManager = GridLayoutManager(activity, 1) as RecyclerView.LayoutManager?
        val bundle = this.arguments
        val title:String = bundle?.getString("title")!!
        var adapter:DetailsSubAdapter

        if(title == "Screensaver"){
            itemCount = Constant.INACTIVITY_SUB_OPTION.size
            itemArray = Constant.INACTIVITY_SUB_OPTION
            itemValue = Constant.INACTIVITY_SUB_OPTION_VALUE
            adapter = DetailsSubAdapter(context!!, itemArray, itemValue, Constant.RADIO_ITEM,title)

        }else {
            if(Constant.getIpAddress() == ""){
                itemValue[0] = "N/A"
            }else{
                itemValue[0] = Constant.getIpAddress()
            }
            itemValue[1] = Constant.getMacAddr()
            itemValue[2] = Constant.getBluetoothMacAddress()
            itemValue[3] = android.os.Build.SERIAL
            itemValue[4] = Constant.getDeviceUpTime()
            itemCount = Constant.STATUS_SUB_OPTION.size
            itemArray = Constant.STATUS_SUB_OPTION
            //   itemValue = Constant.STATUS_SUB_OPTION_VALUE
            adapter = DetailsSubAdapter(context!!, itemArray, itemValue, Constant.NORMAL_ITEM,title)
        }
        if(itemCount > 0) {
            val manager = GridLayoutManager(context, itemCount)
            manager.orientation = LinearLayoutManager.HORIZONTAL
            recyclerView.layoutManager = manager
            val itemSpace = resources.getDimensionPixelSize(R.dimen.recyclerView_item_space)
            recyclerView.addItemDecoration(SpaceItemDecoration(itemSpace))
        //    recyclerView.setSelectPadding(35, 34, 35, 34)
            recyclerView.adapter = adapter
        }
        recyclerView.addOnItemClickListener(object : OnItemClickListener {
            override fun onItemViewClick(position: Int, view: View) {
                lastSelectedPosition = position
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onItemViewFocusChanged(gainFocus: Boolean, view: View, position: Int) {
                println("DetailsSubFragment  $gainFocus")
                if (title == "Status")
                    recyclerView.isFocusable = false            }

        })


        return view
    }

    inner class SpaceItemDecoration internal constructor(private val space: Int) : RecyclerView.ItemDecoration() {

        override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView,
                                    state: RecyclerView.State) {
            outRect.top = space
        }
    }



}