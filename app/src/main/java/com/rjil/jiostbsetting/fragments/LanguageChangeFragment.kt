package com.rjil.jiostbsetting.fragments

import android.content.Context
import android.content.SharedPreferences
import android.content.res.Configuration
import android.graphics.Rect
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import com.iwedia.fti.rjio.fti.api.AsyncDataReceiver
import com.iwedia.fti.rjio.fti.entities.Language
import com.iwedia.fti.rjio.fti_sdk.RjioSdk
import com.iwedia.fti.rjio.fti_sdk.handler.RjioSharedPrefs
import com.rjil.jiostbsetting.R
import com.rjil.jiostbsetting.RJioSdk.RjioUtilityServiceHandler
import com.rjil.jiostbsetting.adapters.Language_Keyboard.LangChangeAdapter
import com.rjil.jiostbsetting.listener.OnItemClickListener
import com.rjil.jiostbsetting.listener.addOnItemClickListener
import com.rjil.jiostbsetting.utils.SharedPreferance
import kotlin.collections.ArrayList
import com.rjil.jiostbsetting.MainActivity
import com.rjil.jiostbsetting.DetailsActivity
import com.rjil.jiostbsetting.utils.Utils


class LanguageChangeFragment : Fragment() {

    private val ARG_PARAM1 = "param1"
    private val ARG_PARAM2 = "param2"
    lateinit var recyclerView_Adapter: LangChangeAdapter
    var arrayLang: ArrayList<String> = ArrayList()
    lateinit var mContext: Context
    var fragment: Fragment? = null
    lateinit var rb_lang: RadioButton
    var languages: ArrayList<Language> = ArrayList()
    var lastSelectedPosition = 0
    private var currentLanguage = 0
    private var mParam1: String? = null
    private var mParam2: String? = null
    var TAG = "LanguageChangeFragment "
    lateinit var lang_recyclerView: RecyclerView


    companion object {
        var isLanSelect = false

        fun newInstance(param1: String): LanguageChangeFragment {
            val fragment = LanguageChangeFragment()
            val args = Bundle()
            args.putString("param1", param1)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.fragment_languagechange, container,
                false)
        mContext = this!!.activity!!


//        val lang = RjioSdk.get().sharedPrefs.getValue(RjioSharedPrefs.LANGUAGE_KEY, "default") as String
//        if (lang != "default") {
//            Utils.changeLanguage(lang)
//        } else {
//            Log.d("SetupCompleteActivity", "This is wrong state. It should not happen")
//        }

        RjioSdk.get().languageHandler.refreshLangues()
        RjioSdk.get().languageHandler.getDefaultLanguage()

        lang_recyclerView = view.findViewById<RecyclerView>(R.id.lang_recy_view) as RecyclerView
        lang_recyclerView!!.layoutManager = GridLayoutManager(activity, 1) as RecyclerView.LayoutManager?
        val itemSpace = resources.getDimensionPixelSize(com.rjil.jiostbsetting.R.dimen.recyclerView_item_space)
        lang_recyclerView.addItemDecoration(SpaceItemDecoration(itemSpace))
//        lang_recyclerView.setSelectPadding(35, 34, 35, 34)

        val bundle = this.arguments
        StrorageAndResetFragment.title = bundle?.getString("title")!!

        val lm = LinearLayoutManager(context)
        lang_recyclerView!!.setLayoutManager(lm)
        recyclerView_Adapter = LangChangeAdapter(mContext, arrayLang)
        lang_recyclerView!!.setAdapter(recyclerView_Adapter);


        //getLanguages()

        lang_recyclerView.addOnItemClickListener(object : OnItemClickListener {

            override fun onItemViewFocusChanged(gainFocus: Boolean, view: View, position: Int) {
                println("LanguageChangeFragment  $gainFocus")

            }

            override fun onItemViewClick(position: Int, view: View) {
                lastSelectedPosition = position

                val lang = languages!![position].id
                RjioSdk.get().sharedPrefs.storeValue(RjioSharedPrefs.LANGUAGE_KEY, lang)
                Utils.setLanguage(languages!!.get(position))
                recyclerView_Adapter.checkItem(position)
                isLanSelect = true
//                Log.d(TAG, "SharedPrefs:stored: language key " + RjioSharedPrefs.LANGUAGE_KEY + ", value " + lang)
                //(activity as DetailsActivity).clearBackStackInclusive("Lang_Change_Frag") // tag (addToBackStack tag) should be the same which was used while transacting the F2 fragment
            }
        })

        lang_recyclerView.addOnLayoutChangeListener(View.OnLayoutChangeListener { view, i, i2, i3, i4, i5, i6, i7, i8 ->
            try {
                var vPos: RecyclerView.ViewHolder?
                if (view != null && lastSelectedPosition >= 0) {
                    vPos = lang_recyclerView.findViewHolderForAdapterPosition(lastSelectedPosition)
                    vPos!!.itemView.requestFocus()
                    Log.d("CurrentFocusedView", "Focused")
                } else {
                    Log.d("Change Lay Selected Pos", lastSelectedPosition.toString())
                }
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }


        })

        return view
    }

    override fun onResume() {
        super.onResume()
        getLang()
    }

    fun getLang() {
        try {
            arrayLang.clear()
            val supportLanguages = arrayOf("en-US", "en-IN")
            RjioSdk.get().getLanguageHandler().getLanguageList(supportLanguages, object : AsyncDataReceiver<ArrayList<Language>> {
                override fun onSuccess(data: ArrayList<Language>) {
                    languages = data
                    for (language in data) {
                        arrayLang.add(language.getName())
                    }
                    //  recyclerView_Adapter.refresh(arrayLang)
                }

                override fun onFailed() {

                }
            })

            RjioSdk.get().languageHandler.getCurrentLanguage(object : AsyncDataReceiver<Language> {
                override fun onSuccess(data: Language) {
                    Log.d("Current Language ", data.name)
                    currentLanguage = languages!!.indexOf(data)
                    recyclerView_Adapter.checkItem(currentLanguage)
                }

                override fun onFailed() {

                }
            })

            val lang = RjioSdk.get().sharedPrefs.getValue(RjioSharedPrefs.LANGUAGE_KEY, "default") as String
            Log.d(TAG, "Last selected lenguage: $lang")

            if (lang == "en-IN") {
                recyclerView_Adapter.checkItem(0)
                lang_recyclerView.postDelayed(Runnable {
                    if (lang_recyclerView.findViewHolderForAdapterPosition(0) != null) {
                        lang_recyclerView.findViewHolderForAdapterPosition(0)!!.itemView.requestFocus()
                    }
                }, 50)
            } else if (lang == "en-US") {
                recyclerView_Adapter.checkItem(1)
                lang_recyclerView.postDelayed(Runnable {
                    if (lang_recyclerView.findViewHolderForAdapterPosition(1) != null) {
                        lang_recyclerView.findViewHolderForAdapterPosition(1)!!.itemView.requestFocus()
                    }
                }, 50)
            } else {
                lang_recyclerView.requestFocus()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

//        if (lang == "en-US") {
//            checkListAdapter.checkItem(1)
//            checkList.postDelayed(Runnable {
//                if (checkList.findViewHolderForAdapterPosition(1) != null) {
//                    checkList.findViewHolderForAdapterPosition(1)!!.itemView.requestFocus()
//                }
//            }, 50)
//        } else {
//            checkList.requestFocus()
//        }
    }


    inner class SpaceItemDecoration internal constructor(private val space: Int) : RecyclerView.ItemDecoration() {
        override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView,
                                    state: RecyclerView.State) {
            outRect.top = space
        }
    }


}


