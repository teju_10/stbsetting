package com.rjil.jiostbsetting.fragments

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.graphics.Rect
import android.net.wifi.*
import android.os.Bundle
import android.os.Handler
import android.text.InputType
import android.text.TextUtils
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.iwedia.fti.rjio.fti.utils.information_bus.Event
import com.iwedia.fti.rjio.fti.utils.information_bus.EventListener
import com.iwedia.fti.rjio.fti.utils.information_bus.InformationBus
import com.iwedia.fti.rjio.fti.utils.information_bus.events.Events
import com.iwedia.fti.rjio.fti_sdk.RjioSdk
import com.rjil.jiostbsetting.DetailsActivity
import com.rjil.jiostbsetting.R
import com.rjil.jiostbsetting.WpsActivity
import com.rjil.jiostbsetting.adapters.network.ConnectedWifiAdapter
import com.rjil.jiostbsetting.fragments.NetworkDetailsFragment.Companion.NEW_NETWORK_KEY
import com.rjil.jiostbsetting.fragments.WifiFragment.Companion.WIFI_CAPABILITIES_KEY
import com.rjil.jiostbsetting.fragments.WifiFragment.Companion.WIFI_KEY
import com.rjil.jiostbsetting.listener.OnItemClickListener
import com.rjil.jiostbsetting.listener.addOnItemClickListener
import com.rjil.jiostbsetting.main.ProgressDialog
import com.rjil.jiostbsetting.utils.ConnectionReceiver
import com.rjil.jiostbsetting.utils.ConnectivityReceiver
import com.rjil.jiostbsetting.utils.Utils
import com.rjil.jiostbsetting.utils.WifiConfigHelper.removeDoubleQuotes
import com.rjil.stbwifilibrary.WiFiManager
import kotlinx.android.synthetic.main.activity_wifi_config.view.*
import java.lang.reflect.InvocationTargetException
import java.lang.reflect.Method


class WifiConfigFragment : Fragment(), ConnectivityReceiver.ConnectivityReceiverListener, /*OnWifiConnectListener,*/ TextView.OnEditorActionListener {


    override fun onNetworkConnectionChanged(isConnected: Boolean, connectedSsid: String) {
        if (isConnected) {
            if (removeDoubleQuotes(connectedSsid) == removeDoubleQuotes(scanResult!!.SSID)) {
                progress_dialog!!.visibility = GONE
//                activity!!.supportFragmentManager.popBackStack()
            } else {
                progress_dialog!!.visibility = GONE
//                Toast.makeText(activity, "Invalid Password", Toast.LENGTH_LONG).show()

            }
        }
    }

    var lastSelectedPosition = 0
    private var scanResult: ScanResult? = null
    var optionList: ArrayList<String>? = null
    var mview: View? = null
    var recyclerView: RecyclerView? = null
    var conn_layout: LinearLayout? = null
    var configure_layout: LinearLayout? = null
    var hide: CheckBox? = null
    private var mAddNewNetwork = false
    var wifi_name: TextView? = null
    var header: TextView? = null
    var title: TextView? = null
    var lbl: TextView? = null
//    var progress_ssdname: TextView? = null
    var pass_wifi: EditText? = null
    var ll_cancelconnect: LinearLayout? = null
//    var progress: FrameLayout? = null
    var progress_dialog: ProgressDialog? = null
    var action_connect: Button? = null
    var wifiCapabilities: String? = null
    var action_cancel: Button? = null
    private var mWiFiManager: WiFiManager? = null
    var mHandler: Handler? = null
    private val wifiManager: WifiManager
        get() = activity?.getSystemService(Context.WIFI_SERVICE) as WifiManager
    var info: WifiInfo? = null
    var wifiName: String? = null
    var isConnectedToOpen=false
    private var wifiConnectionStateObserver: WifiConnectionStateObserver? = null
    private var wifiConnectionStateObserverUnsecure: WifiConnectionStateObserver? = null
    private var connectionReceiver: ConnectionReceiver? = null
    private var networkEventListener: NetworkEventListener? = null
    var position : Int?=0
    companion object {

        fun newInstance(): WifiConfigFragment {
            return WifiConfigFragment()
        }

        val TAG = WifiConfigFragment.javaClass.simpleName
        var SECURITY_TYPE = "security_type"
    }


    override fun onPause() {
        super.onPause()
        // activity!!.unregisterReceiver(wifiReceiverss)
//        mWiFiManager!!.removeOnWifiConnectListener()
    }


    override fun onResume() {
        super.onResume()
        if(WpsActivity.isPasswordback)
            activity!!.supportFragmentManager.popBackStack()

//        mWiFiManager!!.setOnWifiConnectListener(this)

        WpsActivity.isPasswordback=false
        if(pass_wifi!!.visibility==VISIBLE)
        {
            pass_wifi!!.isFocusable = true
            pass_wifi!!.isFocusableInTouchMode=true
//            Utils.showKeyboardDefault(activity!!,pass_wifi!!)
        }
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.activity_wifi_config, container,
                false)
        recyclerView = view._recycler_view
        configure_layout = view.configure_layout
        conn_layout = view.conn_layout
        wifi_name = view.wifi_name
        pass_wifi = view.pass_wifi
//        progress_ssdname = view.tv_progress
//        progress = view.progress
        progress_dialog = view.progress_dialog
        title = view.title
        lbl = view.lbl
        ll_cancelconnect = view.ll_cancelconnect
        action_cancel = view.action_cancel
        action_connect = view.action_connect
        hide = view.hide
        val activity = this.activity
        mWiFiManager = WiFiManager(activity)
        WpsActivity.isnotify=false
        NetworkDetailsFragment.isRefreshList=false
        val imm = activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
        /*imm?.showSoftInput(pass_wifi!!,
                InputMethodManager.SHOW_IMPLICIT)*/
        if (activity is DetailsActivity) {
            activity.hideImageHolder()
        }
//        ConnectivityReceiver.connectivityReceiverListener = this

        val args = arguments
        if (args != null) {
            scanResult = args.getParcelable("scanResult")
            wifiCapabilities = args.getString(WIFI_CAPABILITIES_KEY)
            if (wifiCapabilities != null) {
                if (wifiCapabilities!!.contains("None") || wifiCapabilities!!.contains("NONE"))
                    connectToUnsecuredNetwork()
            }
        }
        header = activity?.findViewById(R.id.headerText) as TextView
//        progress_ssdname = view.findViewById(R.id.tv_progress) as TextView
        header?.text = activity.intent?.getStringExtra("title")
        // TODO when We clicked on Connected wifi
        if (args!!.getBoolean("Connected")) {
            conn_layout!!.visibility = VISIBLE
            configure_layout!!.visibility = GONE
            wifiName = scanResult!!.SSID
            setUpRecycleView(activity)
        } else {
            conn_layout!!.visibility = GONE
            configure_layout!!.visibility = VISIBLE
            if (scanResult != null) {
                wifi_name!!.visibility = VISIBLE
                wifi_name!!.text = "Enter the password for " + "\"" + scanResult!!.SSID + "\""
                wifiName = scanResult!!.SSID
                wifiCapabilities = scanResult!!.capabilities
                position=args.getInt("position")
                Utils.showKeyboard(activity,pass_wifi)
            }
            else
            {
                if (args.getBoolean(NEW_NETWORK_KEY, false)) {
                    title!!.visibility = GONE
                    wifi_name!!.visibility = GONE
                    lbl!!.visibility = GONE
                    hide!!.visibility = GONE
                    ll_cancelconnect!!.visibility = GONE
                    addNewNetwork(view)
                }
                else
                {
                    // Add new network-> network name-> none/wps->
                    wifiName=args.getString(WIFI_KEY)
                    wifi_name!!.text = "Enter the password for " + "\"" + wifiName + "\""
                    Utils.showKeyboard(activity,pass_wifi)
//                    enterPassword()
                }
            }

        }
        pass_wifi!!.setOnKeyListener { _, keyCode, keyEvent ->
            if (keyEvent.action == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                imm?.hideSoftInputFromWindow(
                        pass_wifi!!.windowToken, 0)
                return@setOnKeyListener true
            }
            return@setOnKeyListener false
        }

        action_connect!!.setOnClickListener {

            connectToWifi()

        }
        action_cancel!!.setOnClickListener {
            fragmentManager!!.popBackStack()
        }
        hide!!.setOnCheckedChangeListener { compoundButton, b ->
            if (b) {
                pass_wifi!!.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD

            } else {
                pass_wifi!!.inputType = InputType.TYPE_CLASS_TEXT

            }
        }
        return view
    }



    private fun connectToWifi() {
        if (pass_wifi!!.text.toString().isEmpty()) {
            Toast.makeText(activity, "Password must have at least 8 character", Toast.LENGTH_LONG).show()
        } else if (pass_wifi!!.text.toString().length < 8) {
            Toast.makeText(activity, "Password must have at least 8 character", Toast.LENGTH_LONG).show()
        } else {
//            configure_layout!!.visibility = GONE
//            progress_dialog!!.visibility = VISIBLE
//            progress_dialog!!.setDialogTitle("Connecting to $wifiName ")
//            progress_ssdname!!.text = "Connecting to $wifiName "
//                wifiManager.connectWPA2Network(scanResult!!.SSID, pass_wifi!!.text.toString())
//            mWiFiManager!!.connectWPA2Network(wifiName!!, pass_wifi!!.text.toString())

            val intent = Intent(activity, WpsActivity::class.java)
            intent.putExtra("loader","Password")
            intent.putExtra("SSID",wifiName)
            intent.putExtra("pass",pass_wifi!!.text.toString())
            intent.putExtra("position",position)
            startActivity(intent)

        }
    }

    private fun addNewNetwork(view: View) {
//        val title: TextView = view.findViewById(R.id.headerText)
//        title.setText(R.string.new_network_name)
        Utils.showKeyboard(activity,pass_wifi)
        pass_wifi = view.findViewById(R.id.pass_wifi)
        pass_wifi!!.setEnabled(true)
        pass_wifi!!.setHint(getString(R.string.wifi_name))
        pass_wifi!!.setOnEditorActionListener(this)
        pass_wifi!!.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
            // If EditText has "textPassword" IME option, then each time when next character
            // is entered, the focus is passed on a next view allowed to accept it.

            if (keyCode == KeyEvent.KEYCODE_ENTER) {
                Utils.showKeyboard(context, pass_wifi)
                return@OnKeyListener true
            }
            false
        })
        pass_wifi!!.setInputType(InputType.TYPE_CLASS_TEXT)
        mAddNewNetwork = true
    }

    private fun connectToUnsecuredNetwork() {
       /* Utils.hideKeyboard(context,pass_wifi)
        isConnectedToOpen=true
        wifiName=arguments!!.getString(WIFI_KEY)
        configure_layout!!.visibility= GONE
        progress_dialog!!.visibility= VISIBLE
        progress_dialog!!.setDialogTitle("Connecting to $wifiName")
        mWiFiManager!!.connectOpenNetwork(wifiName!!)*/
        val intent = Intent(activity,WpsActivity::class.java)
        intent.putExtra("loader","OpenNetwork")
        intent.putExtra("SSID",wifiName!!)
        startActivity(intent)
    }

    private fun setUpRecycleView(activity: FragmentActivity) {

        optionList = ArrayList()
        optionList!!.add("Forget Network")
        optionList!!.add("Proxy settings")
        optionList!!.add("IP Address")
        //        optionList.add("Subnet Mask");
        //        optionList.add("Router");
        optionList!!.add("Signal Strength")
//        optionList!!.add("Configure DNS")
        Utils.hideKeyboard(context,pass_wifi)

        if (activity is DetailsActivity) {
            activity.showImageHolder()
        }

        WpsActivity.isnotify=false

        //optionList.add("DNS");
        val manager = GridLayoutManager(activity, optionList!!.size)
        manager.orientation = LinearLayoutManager.HORIZONTAL
        recyclerView!!.layoutManager = manager
        val itemSpace = resources.getDimensionPixelSize(R.dimen.recyclerView_item_space)
        recyclerView!!.addItemDecoration(SpaceItemDecoration(itemSpace))
//        recyclerView!!.setSelectPadding(35, 34, 35, 34)
        val connectedWifiAdapter = ConnectedWifiAdapter(activity!!, optionList!!, scanResult!!)
        recyclerView!!.adapter = connectedWifiAdapter
        recyclerView!!.addOnItemClickListener(object : OnItemClickListener {
            override fun onItemViewClick(position: Int, view: View)
            {
                lastSelectedPosition = position

                when (position) {
                    0 ->
                    {
                        forgetWifiNetwork(context!!,scanResult!!.SSID)
                        Handler().postDelayed({
                            activity.supportFragmentManager.popBackStack()
                        },1500)
                    }
                    1 -> {
                        addProxeySettingFragment("Proxy settings")
                    }
                   /* 4 -> {
                        Toast.makeText(activity, "Coming Soon..", Toast.LENGTH_LONG).show()
                    }*/
                }
            }

            override fun onItemViewFocusChanged(gainFocus: Boolean, view: View, position: Int) {
                if (view != null) {

                    if (gainFocus) {
                        // view!!.background = context!!.resources.getDrawable(R.drawable.rl_shape_corner)
                    } else {
                        //  view!!.background = context!!.resources.getDrawable(R.drawable.rv_focused_bg)
                    }
                }
            }
        })

        recyclerView!!.addOnLayoutChangeListener(View.OnLayoutChangeListener { view, i, i2, i3, i4, i5, i6, i7, i8 ->
            try {
                var vPos: RecyclerView.ViewHolder?
                try {
                    if(view!=null && lastSelectedPosition>0)
                    {
                        vPos = recyclerView!!.findViewHolderForAdapterPosition(lastSelectedPosition)
                        vPos!!.itemView.requestFocus()
                    }

                }catch (e: java.lang.Exception)
                {
                    e.printStackTrace()
                }

            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }

        })



    }

    fun forgetWifiNetwork(context: Context?, ssid: String) {
        val mWifiManager = context!!.getSystemService(Context.WIFI_SERVICE) as WifiManager
//        val wifiInfo: WifiInfo? = mWifiManager.connectionInfo
        val list = mWifiManager.configuredNetworks


        for (network in list) {

            if(network!=null && !TextUtils.isEmpty(network.SSID))
            {

                if(network.SSID.equals("\"" + ssid + "\""))
                {
                        val networkId = network.networkId
                        if (networkId != -1) {
                            var method: Method? = null
                            try {
                                method = mWifiManager.javaClass.getMethod("removeNetwork", Int::class.java)
                                method.invoke(mWifiManager, networkId)
                            } catch (e: NoSuchMethodException) {
                                e.printStackTrace()
                            } catch (e: IllegalAccessException) {
                                e.printStackTrace()
                            } catch (e: InvocationTargetException) {
                                e.printStackTrace()
                            }
                        }
                }
            }
        }
    }

    // TODO forgot current connection
    fun forgetWifiNetwork(context: Context) {
        val mWifiManager = context.getSystemService(Context.WIFI_SERVICE) as WifiManager
        val wifiInfo = mWifiManager.connectionInfo
        if (wifiInfo != null) {
            val networkId = wifiInfo.networkId
            if (networkId != -1) {
                var method: Method? = null
                try {
                    method = mWifiManager.javaClass.getMethod("removeNetwork", Int::class.java)
                    method.invoke(mWifiManager, networkId)
                } catch (e: NoSuchMethodException) {
                    e.printStackTrace()
                } catch (e: IllegalAccessException) {
                    e.printStackTrace()
                } catch (e: InvocationTargetException) {
                    e.printStackTrace()
                }
            }
        }
    }

    fun addProxeySettingFragment(title: String) {

        val header = activity?.findViewById<TextView>(R.id.headerText)
        header?.text = title
        val fm = activity?.supportFragmentManager
        val ft = fm?.beginTransaction()
        val llf = ProxeySettingsFragment.newInstance()
        val bundle: Bundle = Bundle()
        bundle.putString("title", title)
        llf.arguments = bundle
        ft?.replace(R.id.sub_details_container, llf, "ProxeySettingsFragment")
        ft?.addToBackStack("ProxeySettingsFragment")
        ft?.commit()

        val newBackStackLength: Int = fm?.backStackEntryCount!!.plus(1)

        fm.addOnBackStackChangedListener(object : FragmentManager.OnBackStackChangedListener {
            override fun onBackStackChanged() {
                val nowCount = fm.backStackEntryCount
                if (newBackStackLength !== nowCount) {
                    if (!DetailsFragment.isStartNowClicked) {
                        // we don't really care if going back or forward. we already performed the logic here.
                        fm.removeOnBackStackChangedListener(this)

                        if (newBackStackLength > nowCount) { // user pressed back
                            //  fm.popBackStackImmediate()
                            header?.text = activity?.intent?.getStringExtra("title")
                        }
                    }
                }
            }
        })
    }

    override fun onEditorAction(p0: TextView?, p1: Int, p2: KeyEvent?): Boolean {
        if (mAddNewNetwork) {
            Utils.hideKeyboard(context, pass_wifi)
            pass_wifi!!.setEnabled(false)

            if (pass_wifi!!.text.toString().isNotEmpty()) {
//                Toast.makeText(context, "Add New Network Options", Toast.LENGTH_SHORT).show()
                val fm = activity!!.supportFragmentManager
                val ft = fm.beginTransaction()
                val llf = ProxeySettingsFragment.newInstance()
                val bundle: Bundle = Bundle()
                bundle.putString(WIFI_KEY, pass_wifi!!.text.toString())
                bundle.putString("title", "Type of security")
                bundle.putBoolean(SECURITY_TYPE, true)
                llf.arguments = bundle
                ft.replace(R.id.sub_details_container, llf, "ProxeySettingsFragment")
                ft.addToBackStack("ProxeySettingsFragment")
                ft.commit()

                val newBackStackLength: Int = fm?.getBackStackEntryCount()!!.plus(1)


                fm.addOnBackStackChangedListener(object : FragmentManager.OnBackStackChangedListener {
                    override fun onBackStackChanged() {
                        val nowCount = fm.getBackStackEntryCount()
                        if (newBackStackLength !== nowCount) {
                            if (!DetailsFragment.isStartNowClicked) {
                                // we don't really care if going back or forward. we already performed the logic here.
                                fm.removeOnBackStackChangedListener(this)

                                if (newBackStackLength > nowCount) { // user pressed back
                                    //  fm.popBackStackImmediate()
                                    header?.text = arguments?.getString("title")
                                    /* if (checkPermissions()) {
                                         startScanning()
                                         wifiDetailAdpter.notifyDataSetChanged()
                                     }*/
                                }
                            }
                        }
                    }
                })
            } else {
                Toast.makeText(context, R.string.name_length, Toast.LENGTH_SHORT).show()
                pass_wifi!!.setEnabled(true)
                pass_wifi!!.requestFocus()
                Utils.showKeyboard(context, pass_wifi!!)
            }
        } else {
            connectToWifi()
        }
        return true
    }

    inner class SpaceItemDecoration internal constructor(private val space: Int) : RecyclerView.ItemDecoration() {

        override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView,
                                    state: RecyclerView.State) {
            outRect.top = space
        }
    }

    override fun onDestroyView() {
        val activity = this.activity
        if (activity is DetailsActivity) {
            activity.showImageHolder()
        }
        super.onDestroyView()
    }

    val RESULT_UNKNOWN_ERROR = 1
    val RESULT_TIMEOUT = 2
    val RESULT_BAD_AUTHENTICATION = 3
    val RESULT_REJECTED_BY_AP = 4

    private inner class WifiConnectionStateObserver
    /**
     * Constructor
     *
     * @param callback wifi callback
     */
    (
            /**
             * Wifi Callback
             */
            private val callback: AsyncReceiverErrorCode) : BroadcastReceiver() {

        private var mWasAssociating: Boolean = false
        private var mWasAssociated: Boolean = false
        private var mWasHandshaking: Boolean = false
        private var mWasCompleted: Boolean = false

        override fun onReceive(context: Context, intent: Intent) {
            if (WifiManager.SUPPLICANT_STATE_CHANGED_ACTION == intent.action) {

                val state = intent.getParcelableExtra<SupplicantState>(

                        WifiManager.EXTRA_NEW_STATE)

                Log.d(TAG, "Got supplicant state: " + state.name)

                when (state) {
                    SupplicantState.ASSOCIATING -> mWasAssociating = true
                    SupplicantState.ASSOCIATED -> mWasAssociated = true
                    SupplicantState.COMPLETED -> {
                        // this just means the supplicant has connected, now
                        // we wait for the rest of the framework to catch up
                        mWasCompleted = true
                        callback.onSuccess()
                    }
                    SupplicantState.DISCONNECTED, SupplicantState.DORMANT -> if (mWasAssociated || mWasHandshaking) {
                        if (!mWasCompleted) {
                            callback.onFailed(if (mWasHandshaking)
                                RESULT_BAD_AUTHENTICATION
                            else
                                RESULT_UNKNOWN_ERROR)
                        } else
                            callback.onFailed(RESULT_UNKNOWN_ERROR)
                    }
                    SupplicantState.INTERFACE_DISABLED, SupplicantState.UNINITIALIZED -> callback.onFailed(RESULT_UNKNOWN_ERROR)
                    SupplicantState.FOUR_WAY_HANDSHAKE, SupplicantState.GROUP_HANDSHAKE -> mWasHandshaking = true
                    SupplicantState.INACTIVE -> {
                        if (mWasAssociating && !mWasAssociated) {
                            // If we go inactive after 'associating' without ever having
                            // been 'associated', the AP(s) must have rejected us.
                            callback.onFailed(RESULT_REJECTED_BY_AP)
                        }
                        return
                    }
                    SupplicantState.INVALID, SupplicantState.SCANNING -> return
                    else -> return
                }
            }
        }
    }

    interface AsyncReceiverErrorCode {

        /**
         * On success
         */
        fun onSuccess()

        /**
         * On failed
         */
        fun onFailed(err: Int)
    }

    private inner class NetworkEventListener : EventListener() {
        init {
            addType(Events.NETWORK_CONNECTION)
        }

        override fun callback(event: Event) {
            if (event.type == Events.NETWORK_CONNECTION) {
                val networkHandler = RjioSdk.get().networkHandler
                if (networkHandler.isEthernetConnected) {
                    networkHandler.requestInternetAvailabilityCheck(object : com.iwedia.fti.rjio.fti.api.AsyncReceiver {
                        override fun onSuccess() {

                            InformationBus.getInstance().unregisterEventListener(networkEventListener)

                            Log.d("FtiPasswordActivity", "NetworkEventListener onSuccess")
                            /*  val intent = Intent(this@FtiPasswordActivity, PrivacyPolicyActivity::class.java)
                              intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                              startActivity(intent)
                              finish()*/
                        }

                        override fun onFailed() {

                        }
                    })
                }
            }
        }
    }


    /*override fun onWiFiConnectLog(log: String?) {
        Log.d(TAG, "onWiFiConnectLog: $log")
    }

    override fun onWiFiConnectSuccess(SSID: String?) {
        progress_dialog!!.visibility = GONE
        configure_layout!!.visibility = VISIBLE

        Log.d(TAG, "onWiFiConnectLog: $SSID")
        Toast.makeText(activity, "$SSID  Connected Successfully!!", Toast.LENGTH_SHORT).show()
        activity!!.supportFragmentManager.popBackStack()

    }

    override fun onWiFiConnectFailure(SSID: String?) {
        progress_dialog!!.visibility = GONE
        Log.d(TAG, "onWiFiConnectFailure: $SSID")
        if(!isConnectedToOpen)
        {
            Toast.makeText(activity, "Invalid Password", Toast.LENGTH_SHORT).show()
            configure_layout!!.visibility = VISIBLE
//            mWiFiManager!!.removeNetwork(SSID)
        }
        else
        {
            Toast.makeText(activity, "Could not connected to network", Toast.LENGTH_SHORT).show()
            activity!!.supportFragmentManager.popBackStack()
        }
    }
*/
}