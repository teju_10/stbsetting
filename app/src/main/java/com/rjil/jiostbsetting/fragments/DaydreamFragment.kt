package com.rjil.jiostbsetting.fragments

import android.graphics.Rect
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Switch
import android.widget.Toast

import com.rjil.jiostbsetting.utils.Constant
import com.rjil.jiostbsetting.R
import com.rjil.jiostbsetting.adapters.DetailsSubAdapter
import com.rjil.jiostbsetting.adapters.DeviceSleepAdapter
import com.rjil.jiostbsetting.listener.OnItemClickListener
import com.rjil.jiostbsetting.listener.addOnItemClickListener
import com.rjil.jiostbsetting.tvrecyclerview.TvRecyclerView
import com.rjil.jiostbsetting.utils.DreamInfo
import com.rjil.jiostbsetting.tvrecyclerview.CenterSmoothScroller
import java.lang.Exception


class DaydreamFragment : Fragment() {

    companion object {
        var itemCount: Int = 0
        var itemArray = arrayOf("", "")
        var itemValue = arrayOf("")
        var dreamInfo: List<DreamInfo> = emptyList()
        var title: String = ""
        var lastSelectedPosition = 0

        fun newInstance(): DaydreamFragment {
            return DaydreamFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.fragment_sub_details_menu, container,
                false)
        val activity = activity
        val recyclerView = view.findViewById<RecyclerView>(R.id.sub_details_recycler_view) as RecyclerView
        recyclerView.layoutManager = GridLayoutManager(activity, 1) as RecyclerView.LayoutManager?
        val bundle = this.arguments
        title = bundle?.getString("title")!!
        var adapter: DetailsSubAdapter
        if (title == "Screensaver") {
            dreamInfo = Constant.getDreamInfos(activity!!.applicationContext)
            val arrayKey = arrayOfNulls<String>(dreamInfo.size)
            val arrayValue = arrayOfNulls<String>(dreamInfo.size)
            for (i in 0..dreamInfo.size - 1) {
                arrayKey[i] = dreamInfo[i].caption.toString()
                arrayValue[i] = "" + dreamInfo[i].componentName!!.packageName
            }
            itemArray = arrayKey.requireNoNulls()
            itemValue = arrayValue.requireNoNulls()
            itemCount = arrayKey.size
            adapter = DetailsSubAdapter(context!!, itemArray, itemValue, Constant.RADIO_ITEM, "Inactivity Timer & HDMI CEC")
        } else if (title == "When to Start Screensaver") {
            itemArray = Constant.WHEN_TO_START
            itemValue = Constant.WHEN_TO_START_VALUE
            itemCount = Constant.WHEN_TO_START.size
            adapter = DetailsSubAdapter(context!!, itemArray, itemValue, Constant.RADIO_ITEM, "Inactivity Timer & HDMI CEC")
        } else {
            itemCount = Constant.STATUS_SUB_OPTION.size
            itemArray = Constant.STATUS_SUB_OPTION
            itemValue = Constant.STATUS_SUB_OPTION_VALUE
            adapter = DetailsSubAdapter(context!!, itemArray, itemValue, Constant.NORMAL_ITEM, title)
        }
        if (itemCount > 0) {
            val manager = GridLayoutManager(context, itemCount)
            manager.orientation = LinearLayoutManager.HORIZONTAL
            recyclerView.layoutManager = manager
            val itemSpace = resources.getDimensionPixelSize(R.dimen.recyclerView_item_space)
            recyclerView.addItemDecoration(SpaceItemDecoration(itemSpace))
            recyclerView.adapter = adapter
        }

        recyclerView.addOnItemClickListener(object : OnItemClickListener {
            override fun onItemViewClick(position: Int, view: View) {

                if (title.equals("Screensaver") && dreamInfo != null) {
                    lastSelectedPosition = recyclerView.getChildAdapterPosition(view)
                    Constant.setActiveDream(dreamInfo.get(position).componentName!!)
                    adapter.notifyDataSetChanged()
                } else if (title == "When to Start Screensaver") {
                    if (Constant.isDaydreamEnabled(activity?.applicationContext!!)==0) {
                        Toast.makeText(getActivity(), "ScreenSaver is off.Turn on to start screensaver", Toast.LENGTH_SHORT).show()

                    }else{
                        Log.d("TAG", "XXonItemViewClick")
                        lastSelectedPosition = recyclerView.getChildAdapterPosition(view)
                        //     if(Constant.getActiveDreamComponent()?.packageName.equals())
                        Constant.setDreamTime(activity!!.applicationContext, (itemValue[position].toLong()).toInt())
                        adapter.notifyDataSetChanged()
                    }
                }
            }

            override fun onItemViewFocusChanged(gainFocus: Boolean, view: View, position: Int) {
                //  adapter.setTrackPlaying(position)

            }

        })


        recyclerView.addOnLayoutChangeListener(View.OnLayoutChangeListener { view, i, i2, i3, i4, i5, i6, i7, i8 ->
            try {
                var vPos: RecyclerView.ViewHolder?
                if (view != null && lastSelectedPosition >= 0) {
                    vPos = recyclerView.findViewHolderForAdapterPosition(lastSelectedPosition)
                    vPos!!.itemView.requestFocus()
                    Log.d("DayDreamFocused ", "Focused")
                } else {
                    Log.d("DayDreamFocused Lay Selected Pos", lastSelectedPosition.toString())
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }


        })
        return view
    }


    inner class SpaceItemDecoration internal constructor(private val space: Int) : RecyclerView.ItemDecoration() {

        override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView,
                                    state: RecyclerView.State) {
            outRect.top = space
        }
    }


}