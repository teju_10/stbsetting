package com.rjil.jiostbsetting.fragments


import android.graphics.Rect
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import com.rjil.jiostbsetting.DetailsActivity

import com.rjil.jiostbsetting.R
import com.rjil.jiostbsetting.adapters.MenuAdapter
import com.rjil.jiostbsetting.tvrecyclerview.TvRecyclerView
import com.rjil.jiostbsetting.utils.Constant
import kotlinx.android.synthetic.main.fragment_ip_settings.view.*

class IpSettingsFragment : Fragment() {
    companion object {
        fun newInstance(): IpSettingsFragment {
            return IpSettingsFragment()
        }
    }

    var recyclerView: TvRecyclerView? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.fragment_ip_settings, container, false)
        val activity = this.activity
        val context = activity?.applicationContext
        if (activity is DetailsActivity) {
            activity.hideImageHolder()
        }
        recyclerView = view.tv_recycler_view
        recyclerView?.layoutManager = GridLayoutManager(activity, 1)
        val manager = GridLayoutManager(context, Constant.IP_OPTION.size)
        manager.orientation = LinearLayoutManager.HORIZONTAL
        recyclerView?.setLayoutManager(manager)
        val itemSpace = resources.getDimensionPixelSize(R.dimen.recyclerView_item_space)
        recyclerView?.addItemDecoration(SpaceItemDecoration(itemSpace))
        recyclerView?.setSelectPadding(35, 34, 35, 34)
        recyclerView?.adapter = MenuAdapter(context!!, Constant.IP_OPTION)
        recyclerView?.setOnItemStateListener(object : TvRecyclerView.OnItemStateListener {
            override fun onItemViewClick(view: View?, position: Int) {
                when (position) {
                    0 ->
                        Toast.makeText(activity, "Click None", Toast.LENGTH_LONG).show()
                    1 -> {
                        addIpAddressFragment("IP SETTINGS")
                    }
                }
            }

            override fun onItemViewFocusChanged(gainFocus: Boolean, view: View?, position: Int) {
                if (view != null) {
                    if (gainFocus) {
                        view!!.background = context!!.resources.getDrawable(R.drawable.rl_shape_corner)
                    } else {
                        view!!.background = context!!.resources.getDrawable(R.drawable.rv_focused_bg)
                    }
                }
            }
        })
        return view
    }

    inner class SpaceItemDecoration internal constructor(private val space: Int) : RecyclerView.ItemDecoration() {

        override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView,
                                    state: RecyclerView.State) {
            outRect.top = space
        }
    }

    fun addIpAddressFragment(title: String) {
        val header = activity?.findViewById<TextView>(R.id.headerText)
        header?.setText(title)
        val fm = activity?.supportFragmentManager
        val ft = fm?.beginTransaction()
        val llf = IpAddressFragment.newInstance();
        val bundle: Bundle = Bundle()
        bundle.putString("title", title)
        llf.arguments = bundle
        ft?.replace(R.id.sub_details_container, llf, "IpAddressFragment")
        ft?.addToBackStack("IpAddressFragment")
        ft?.commit()

        val newBackStackLength: Int = fm?.getBackStackEntryCount()!!.plus(1);

        fm?.addOnBackStackChangedListener(object : FragmentManager.OnBackStackChangedListener {
            override fun onBackStackChanged() {
                val nowCount = fm.getBackStackEntryCount()
                if (newBackStackLength !== nowCount) {
                    if (!DetailsFragment.isStartNowClicked) {
                        // we don't really care if going back or forward. we already performed the logic here.
                        fm.removeOnBackStackChangedListener(this)

                        if (newBackStackLength > nowCount) { // user pressed back
                            //  fm.popBackStackImmediate()
                            header?.setText(arguments?.getString("title"))
                        }
                    }
                }
            }
        })
    }

    override fun onDestroy() {
        val activity = this.activity
        if (activity is DetailsActivity) {
            activity.showImageHolder()
        }
        super.onDestroy()
    }

}
