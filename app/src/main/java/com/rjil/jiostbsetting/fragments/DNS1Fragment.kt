package com.rjil.jiostbsetting.fragments


import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import com.rjil.jiostbsetting.DetailsActivity
import com.rjil.jiostbsetting.R
import kotlinx.android.synthetic.main.fragment_dns1.view.*

class DNS1Fragment : Fragment() {
    companion object {
        fun newInstance(): DNS1Fragment {
            return DNS1Fragment()
        }
    }

    var dns1: EditText? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.fragment_dns1, container, false)
        dns1 = view.dns1
        val activity = this.activity
        if (activity is DetailsActivity) {
            activity.hideImageHolder()
        }

        dns1!!.setOnKeyListener { _, keyCode, keyEvent ->
            if (keyEvent.action == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                addDNS2Fragment("IP SETTINGS", dns1?.text.toString())

                return@setOnKeyListener true
            }
            return@setOnKeyListener false
        }
        return view
    }

    fun addDNS2Fragment(title: String, dns1: String) {
        val header = activity?.findViewById<TextView>(R.id.headerText)
        header?.setText(title)
        val fm = activity?.supportFragmentManager
        val ft = fm?.beginTransaction()
        val llf = DNS2Fragment.newInstance();
        val bundle: Bundle = Bundle()
        bundle.putString("title", title)
        bundle.putString("ip_address", arguments?.getString("ip_address"))
        bundle.putString("getway", arguments?.getString("getway"))
        bundle.putString("prefixLength", arguments?.getString("prefixLength"))
        bundle.putString("dns1", dns1)
        llf.arguments = bundle
        ft?.replace(R.id.sub_details_container, llf, "DNS2Fragment")
        ft?.addToBackStack("DNS2Fragment")
        ft?.commit()

        val newBackStackLength: Int = fm?.getBackStackEntryCount()!!.plus(1);

        fm?.addOnBackStackChangedListener(object : FragmentManager.OnBackStackChangedListener {
            override fun onBackStackChanged() {
                val nowCount = fm.getBackStackEntryCount()
                if (newBackStackLength !== nowCount) {
                    if (!DetailsFragment.isStartNowClicked) {
                        // we don't really care if going back or forward. we already performed the logic here.
                        fm.removeOnBackStackChangedListener(this)

                        if (newBackStackLength > nowCount) { // user pressed back
                            //  fm.popBackStackImmediate()
                            header?.setText(arguments?.getString("title"))
                        }
                    }
                }
            }
        })
    }
}
