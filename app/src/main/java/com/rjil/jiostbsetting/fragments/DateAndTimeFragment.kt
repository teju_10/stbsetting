package com.rjil.jiostbsetting.fragments

import android.graphics.Rect
import android.os.Bundle
import android.provider.Settings

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import android.text.format.DateFormat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Switch

import com.rjil.jiostbsetting.DetailsActivity
import com.rjil.jiostbsetting.R
import com.rjil.jiostbsetting.adapters.DisplayOptionAdapter
import com.rjil.jiostbsetting.listener.OnItemClickListener
import com.rjil.jiostbsetting.listener.addOnItemClickListener
import com.rjil.jiostbsetting.utils.Constant
import com.rjil.jiostbsetting.utils.SharedPreferance

import java.util.ArrayList
import java.util.Calendar

class DateAndTimeFragment : Fragment() {

    private var mParam1: String? = null
    private var mParam2: String? = null
    private var mNowDate: String? = null
    private var mNowTime: String? = null
    private var switch24Format: Switch? = null
    internal lateinit var rootView: View
    internal var isStartNowClicked = false
    internal lateinit var valueArray: Array<String?>


    private var recyclerView: RecyclerView? = null
    private var valueList: ArrayList<String>? = null
    var fragment: Fragment? = null

    private val autoDateTime: String
        get() {
            try {
                val value = Settings.Global.getInt(activity!!.contentResolver, Settings.Global.AUTO_TIME)
                return if (value == 1) {
                    activity!!.getString(R.string.use_network_prov_time)
                } else {
                    activity!!.getString(R.string.off)
                }
            } catch (e: Settings.SettingNotFoundException) {
                e.printStackTrace()
                return ""
            }

        }

    val time24Hour: String
        get() {
            try {
                return Settings.System.getString(activity!!.contentResolver,
                        Settings.System.TIME_12_24)
            } catch (e: SecurityException) {
                e.printStackTrace()
                return "time_12_24"
            }

        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            mParam1 = arguments!!.getString(ARG_PARAM1)
            mParam2 = arguments!!.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        rootView = inflater.inflate(R.layout.fragment_display_option, container, false)
        //       ((MainActivity)getActivity()).header.setText(mParam1);
        recyclerView = rootView.findViewById<View>(R.id.display_recycler_view) as RecyclerView

        valueList = ArrayList()
        setUpRecycleView()
        return rootView
    }

    private fun setUpRecycleView() {

        valueArray = arrayOfNulls<String>(Constant.DATE_AND_TIME_SUB_OPTION.size)

        updateTimeAndDateStrings()
        //        valueList.add(getAutoDateTime());
        //        valueList.add(mNowDate);
        //        valueList.add(mNowTime);
        valueArray[0] = "SWITCH"
        valueArray[1] = autoDateTime
        valueArray[2] = mNowDate
        valueArray[3] = mNowTime
        //  valueArray[3]=">";


        val manager = GridLayoutManager(activity, 5)
        manager.orientation = LinearLayoutManager.HORIZONTAL
        recyclerView!!.layoutManager = manager
        val itemSpace = resources.getDimensionPixelSize(R.dimen.recyclerView_item_space)
        recyclerView!!.addItemDecoration(SpaceItemDecoration(itemSpace))
        val dateAndTimeSuboption: DisplayOptionAdapter
        dateAndTimeSuboption = DisplayOptionAdapter(activity!!, Constant.DATE_AND_TIME_SUB_OPTION, valueArray)
        recyclerView!!.adapter = dateAndTimeSuboption
        //update 24 format value

        recyclerView!!.addOnItemClickListener(object : OnItemClickListener{
            override fun onItemViewClick(position: Int, view: View) {
                when (position) {
                    1 -> {
                    }
                    2 -> {
                    }
                    3 -> {
                    }
                    0 -> {
                        switch24Format = view.findViewById<View>(R.id.switch_item) as Switch
                        if (switch24Format!!.isChecked) {
                            switch24Format!!.isChecked = false
                            SharedPreferance.setDateAndTime(activity, false)
                            setTime24Hour(false)
                            updateTimeAndDateStrings()
                            dateAndTimeSuboption.notifyItemChanged(1)
                            dateAndTimeSuboption.notifyItemChanged(2)
                            dateAndTimeSuboption.notifyItemChanged(3)
                        } else {
                            switch24Format!!.isChecked = true
                            SharedPreferance.setDateAndTime(activity, true)
                            setTime24Hour(true)
                            updateTimeAndDateStrings()
                            dateAndTimeSuboption.notifyItemChanged(1)
                            dateAndTimeSuboption.notifyItemChanged(2)
                            dateAndTimeSuboption.notifyItemChanged(3)
                          //  dateAndTimeSuboption.notifyDataSetChanged()
                        }
                    }
                }//switchFragments(position);
                //switchFragments(position);
                //switchFragments(position);            }
            }
            override fun onItemViewFocusChanged(gainFocus: Boolean, view: View, position: Int) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }



        })

    }

    internal inner class SpaceItemDecoration(var space: Int) : RecyclerView.ItemDecoration() {

        override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
            outRect.top = space
            //            super.getItemOffsets(outRect, view, parent, state);
        }
    }

    private fun setAutoDateTime(on: Boolean) {
        Settings.Global.putInt(activity!!.contentResolver, Settings.Global.AUTO_TIME, if (on) 1 else 0)
    }

    private fun setTime24Hour(is24Hour: Boolean) {
        try {
            Settings.System.putString(activity!!.contentResolver,
                    Settings.System.TIME_12_24,
                    if (is24Hour) HOURS_24 else HOURS_12)
        } catch (e: SecurityException) {
            e.printStackTrace()
        }

    }

    private fun updateTimeAndDateStrings() {
        val now = Calendar.getInstance()
        val dateFormat = DateFormat.getDateFormat(activity)
        mNowDate = dateFormat.format(now.time)
        val timeFormat = DateFormat.getTimeFormat(activity)
        mNowTime = timeFormat.format(now.time)
        valueArray[3] = mNowTime

    }

    fun switchFragments(position: Int) {

        when (position) {
            0 -> {
                fragment = DisplayConfrugationFragment.newInstance(activity!!.getString(R.string.auto_date_time))
                addDateAndTimeFragment(activity!!.getString(R.string.auto_date_time), fragment)
            }
            1 -> {
                fragment = DisplayConfrugationFragment.newInstance(activity!!.getString(R.string.set_date))
                addDateAndTimeFragment(activity!!.getString(R.string.set_date), fragment)
            }
            2 -> {
                fragment = DisplayConfrugationFragment.newInstance(activity!!.getString(R.string.set_time))
                addDateAndTimeFragment(activity!!.getString(R.string.set_time), fragment)
            }
            3 -> {
                fragment = DisplayConfrugationFragment.newInstance(activity!!.getString(R.string.set_time_zone))
                addDateAndTimeFragment(activity!!.getString(R.string.set_time_zone), fragment)
            }
        }

    }

    fun addDateAndTimeFragment(title: String, fragment: Fragment?) {
        val fm = activity!!.supportFragmentManager
        val ft = fm.beginTransaction()
        val llf = fragment
        val bundle = Bundle()
        bundle.putString("title", title)
        bundle.putString("param1", title)
        llf!!.arguments = bundle
        ft.replace(R.id.sub_details_container, llf, "Date_N_Time")
        ft.addToBackStack("Date_N_Time")
        ft.commit()

        val newBackStackLength = fm.backStackEntryCount + 1
        fm.addOnBackStackChangedListener(object : FragmentManager.OnBackStackChangedListener {
            override fun onBackStackChanged() {
                val nowCount = fm.backStackEntryCount
                if (newBackStackLength != nowCount) {
                    if (isStartNowClicked) {
                        // we don't really care if going back or forward. we already performed the logic here.
                        fm.removeOnBackStackChangedListener(this)
                        if (newBackStackLength > nowCount) { // user pressed back
                            //  fm.popBackStackImmediate()
                            (activity as DetailsActivity).header!!.text = activity!!.intent.getStringExtra("title")
                        }
                    }
                }
            }
        })
    }

    override fun onResume() {
        super.onResume()
        (activity as DetailsActivity).header!!.text = mParam1
    }

    companion object {

        private val ARG_PARAM1 = "param1"
        private val ARG_PARAM2 = "param2"
        private val HOURS_12 = "12"
        private val HOURS_24 = "24"

        fun newInstance(param1: String): DateAndTimeFragment {
            val fragment = DateAndTimeFragment()
            val args = Bundle()
            args.putString(ARG_PARAM1, param1)
            fragment.arguments = args
            return fragment
        }
    }
}// Required empty public constructor
