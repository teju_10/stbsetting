package com.rjil.jiostbsetting.fragments


import android.content.Context
import android.net.LinkAddress
import android.net.wifi.WifiConfiguration
import android.net.wifi.WifiManager
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import com.rjil.jiostbsetting.DetailsActivity
import com.rjil.jiostbsetting.R
import kotlinx.android.synthetic.main.fragment_dns2.view.*
import java.lang.reflect.InvocationTargetException
import java.net.InetAddress


class DNS2Fragment : Fragment() {
    companion object {
        fun newInstance(): DNS2Fragment {
            return DNS2Fragment()
        }

    }

    private val wifiManager: WifiManager?
        get() = activity?.getSystemService(Context.WIFI_SERVICE) as WifiManager?

    var dns2: EditText? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.fragment_dns2, container, false)
        dns2 = view.dns2
        val activity = this.activity
        if (activity is DetailsActivity) {
            activity.hideImageHolder()
        }

        dns2!!.setOnKeyListener { _, keyCode, keyEvent ->
            if (keyEvent.action == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {

                getWifiConfiguration()?.let { setIpConfig(it) }
                return@setOnKeyListener true
            }
            return@setOnKeyListener false
        }
        var ip = arguments?.getString("ip_address")
        var getway = arguments?.getString("getway")
        var prefixLength = arguments?.getString("prefixLength")
        var dns1 = arguments?.getString("dns1")
        Log.e("Val", ip + getway + prefixLength + dns1)

        return view
    }

    @Throws(SecurityException::class, IllegalArgumentException::class, NoSuchFieldException::class, IllegalAccessException::class)
    fun setIpAssignment(assign: String, wifiConf: WifiConfiguration) {
        setEnumField(wifiConf, assign, "ipAssignment")
    }

    @Throws(SecurityException::class, NoSuchFieldException::class, IllegalArgumentException::class, IllegalAccessException::class)
    private fun setEnumField(obj: Any, value: String, name: String) {
        val f = obj.javaClass.getField(name)
        f.set(obj, value)
       // f.set(obj, Enum.valueOf((Class<Enum>) f.getType(), value));

    }

    @Throws(SecurityException::class, IllegalArgumentException::class, NoSuchFieldException::class, IllegalAccessException::class, NoSuchMethodException::class, ClassNotFoundException::class, Fragment.InstantiationException::class, InvocationTargetException::class)
    fun setIpAddress(addr: InetAddress, prefixLength: Int, wifiConf: WifiConfiguration) {
        val linkProperties = getField(wifiConf, "linkProperties") ?: return
        val laClass = Class.forName("android.net.LinkAddress")
        val laConstructor = laClass.getConstructor(InetAddress::class.java, Int::class.javaPrimitiveType)
        val linkAddress = laConstructor.newInstance(addr, prefixLength)
        val mLinkAddresses = getDeclaredField(linkProperties, "mLinkAddresses") as ArrayList<*>
        mLinkAddresses.clear()
        mLinkAddresses.add(linkAddress as Nothing)

    }

    @Throws(SecurityException::class, NoSuchFieldException::class, IllegalArgumentException::class, IllegalAccessException::class)
    fun getDeclaredField(obj: Any, name: String): Any {
        val f = obj.javaClass.getDeclaredField(name)
        f.isAccessible = true
        return f.get(obj)
    }

    @Throws(SecurityException::class, IllegalArgumentException::class, NoSuchFieldException::class, IllegalAccessException::class)
    fun setDNS(dns: InetAddress, wifiConf: WifiConfiguration) {
        val linkProperties = getField(wifiConf, "linkProperties") ?: return

        val mDnses = getDeclaredField(linkProperties, "mDnses") as ArrayList<InetAddress>
        mDnses.clear() //or add a new dns address , here I just want to replace DNS1
        mDnses.add(dns)
    }

    @Throws(SecurityException::class, NoSuchFieldException::class, IllegalArgumentException::class, IllegalAccessException::class)
    fun getField(obj: Any, name: String): Any? {
        val f = obj.javaClass.getField(name)
        return f.get(obj)
    }

    @Throws(SecurityException::class, IllegalArgumentException::class, NoSuchFieldException::class, IllegalAccessException::class, ClassNotFoundException::class, NoSuchMethodException::class, Fragment.InstantiationException::class, InvocationTargetException::class)
    fun setGateway(gateway: InetAddress, wifiConf: WifiConfiguration) {
        val linkProperties = getField(wifiConf, "linkProperties") ?: return
        val mGateways = getDeclaredField(linkProperties, "mGateways") as ArrayList<*>
        mGateways.clear()
        mGateways.add(gateway as Nothing)
    }

    fun getWifiConfiguration(): WifiConfiguration? {
        var wifiConf: WifiConfiguration? = null
        val wifiManager = activity?.getSystemService(Context.WIFI_SERVICE) as WifiManager?

        val connectionInfo = wifiManager!!.connectionInfo
        val configuredNetworks = wifiManager!!.configuredNetworks
        for (conf in configuredNetworks) {
            if (conf.networkId == connectionInfo.networkId) {
                wifiConf = conf
                break
            }
        }
        return wifiConf;
    }

//    fun GetCurrentWifiConfiguration(manager: WifiManager): WifiConfiguration? {
//        if (!manager.isWifiEnabled)
//            return null
//
//        val configurationList = manager.configuredNetworks
//        var configuration: WifiConfiguration? = null
//        val cur = manager.connectionInfo.networkId
//        var i = 0
//        while (i < configurationList.size) {
//            val wifiConfiguration = configurationList[i]
//            if (wifiConfiguration.networkId == cur)
//                configuration = wifiConfiguration
//            i++
//        }
//
//        return configuration
//    }

    fun setIpConfig(wifiConf: WifiConfiguration) {
        try {
            setIpAssignment("STATIC", wifiConf) //or "DHCP" for dynamic setting
            setIpAddress(InetAddress.getByName(arguments?.getString("ip_address")), arguments?.getString("prefixLength")!!.toInt(), wifiConf)
            setGateway(InetAddress.getByName(arguments?.getString("getway")), wifiConf)
            setDNS(InetAddress.getByName(arguments?.getString("dns1")), wifiConf)
            wifiManager?.updateNetwork(wifiConf) //apply the setting
            wifiManager?.saveConfiguration() //Save it
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }


//    fun changeWifiConfiguration(dhcp: Boolean, ip: String, prefix: Int, dns1: String, gateway: String) {
//        val wm = activity?.getSystemService(Context.WIFI_SERVICE) as WifiManager?
//        if (!wm!!.isWifiEnabled) {
//            // wifi is disabled
//            return
//        }
//        // get the current wifi configuration
//        var wifiConf: WifiConfiguration? = null
//        val connectionInfo = wm.connectionInfo
//        val configuredNetworks = wm.configuredNetworks
//        if (configuredNetworks != null) {
//            for (conf in configuredNetworks) {
//                if (conf.networkId == connectionInfo.networkId) {
//                    wifiConf = conf
//                    break
//                }
//            }
//        }
//        if (wifiConf == null) {
//            // wifi is not connected
//            return
//        }
//        try {
//            val ipAssignment = wifiConf.javaClass.getMethod("getIpAssignment").invoke(wifiConf).javaClass
//            var staticConf: Any? = wifiConf.javaClass.getMethod("getStaticIpConfiguration").invoke(wifiConf)
//            if (dhcp) {
//                wifiConf.javaClass.getMethod("setIpAssignment", ipAssignment).invoke(wifiConf, Enum.valueOf<Enum>(ipAssignment as Class<Enum<*>>, "DHCP"))
//                staticConf?.javaClass?.getMethod("clear")?.invoke(staticConf)
//            } else {
//                wifiConf.javaClass.getMethod("setIpAssignment", ipAssignment).invoke(wifiConf, Enum.valueOf<Enum>(ipAssignment as Class<Enum<*>>, "STATIC"))
//                if (staticConf == null) {
//                    val staticConfigClass = Class.forName("android.net.StaticIpConfiguration")
//                    staticConf = staticConfigClass.newInstance()
//                }
//                // STATIC IP AND MASK PREFIX
//                val laConstructor = LinkAddress::class.java.getConstructor(InetAddress::class.java, Int::class.javaPrimitiveType)
//                val linkAddress = laConstructor.newInstance(
//                        InetAddress.getByName(ip),
//                        prefix) as LinkAddress
//                staticConf!!.javaClass.getField("ipAddress").set(staticConf, linkAddress)
//                // GATEWAY
//                staticConf.javaClass.getField("gateway").set(staticConf, InetAddress.getByName(gateway))
//                // DNS
//                val dnsServers = staticConf.javaClass.getField("dnsServers").get(staticConf) as List<InetAddress>
//                dnsServers.clear()
//                dnsServers.add(InetAddress.getByName(dns1))
//                dnsServers.add(InetAddress.getByName("8.8.8.8")) // Google DNS as DNS2 for safety
//                // apply the new static configuration
//                wifiConf.javaClass.getMethod("setStaticIpConfiguration", staticConf.javaClass).invoke(wifiConf, staticConf)
//            }
//            // apply the configuration change
//            var result = wm.updateNetwork(wifiConf) != -1 //apply the setting
//            if (result) result = wm.saveConfiguration() //Save it
//            if (result) wm.reassociate() // reconnect with the new static IP
//        } catch (e: Exception) {
//            e.printStackTrace()
//        }
//
//    }
}
