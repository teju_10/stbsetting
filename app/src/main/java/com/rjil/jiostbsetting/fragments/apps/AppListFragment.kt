package com.rjil.jiostbsetting.fragments

import android.content.pm.ApplicationInfo
import android.content.pm.PackageInfo
import android.content.pm.PackageItemInfo
import android.graphics.Rect
import android.nfc.Tag
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.rjil.jiostbsetting.R
import com.rjil.jiostbsetting.adapters.SubDetailsAdapter
import com.rjil.jiostbsetting.fragments.apps.AppDetailsFragment
import com.rjil.jiostbsetting.listener.OnItemClickListener
import com.rjil.jiostbsetting.listener.addOnItemClickListener
import com.rjil.jiostbsetting.models.AppList
import com.rjil.jiostbsetting.utils.SharedPreferance
import kotlinx.android.synthetic.main.activity_details.*
import java.util.*
import kotlin.collections.ArrayList


class AppListFragment : Fragment() {

    lateinit var listType: String
    lateinit var header: TextView
    var lastSelectedPosition = 0

    companion object {


        var app = ArrayList<PackageInfo>()
        // var app: MutableList<PackageInfo> = mutableListOf()

        fun newInstance(): AppListFragment {
            return AppListFragment()
        }

        var flag = false

        lateinit var subrecyclerView: RecyclerView
    }

    private lateinit var appAdapter: SubDetailsAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.fragment_app_details, container, false)


        var activity = activity
        subrecyclerView = view?.findViewById<RecyclerView>(R.id.Subdetails_recycler_view) as RecyclerView
        header = activity?.findViewById<TextView>(R.id.headerText) as TextView


        subrecyclerView.layoutManager = GridLayoutManager(activity, 1)

        listType = this.arguments!!.getString("title").toString()
        // var name = activity?.intent?.getStringExtra("apps")
        header?.text = listType

        subrecyclerView.addOnItemClickListener(object : OnItemClickListener {

            override fun onItemViewFocusChanged(gainFocus: Boolean, view: View, position: Int) {
            }

            override fun onItemViewClick(position: Int, view: View) {
                lastSelectedPosition = position
                flag = false
                AppListtoDetailsFragment("Application Details", app, position)
            }

        })
//        appAdapter.notifyDataSetChanged()

        subrecyclerView.addOnLayoutChangeListener { view, i, i2, i3, i4, i5, i6, i7, i8 ->
            try {
                var vPos: RecyclerView.ViewHolder?
                if (view != null && lastSelectedPosition < app.size - 1) {
                    if (SharedPreferance.getUninstall(activity)) {
                        SharedPreferance.setUninstall(activity, false)
                    }
                    vPos = subrecyclerView.findViewHolderForAdapterPosition(lastSelectedPosition)
                    vPos!!.itemView.requestFocus()

                } else {
                    if (SharedPreferance.getUninstall(activity)) {
                        vPos = subrecyclerView.findViewHolderForAdapterPosition(lastSelectedPosition-1)
                        vPos!!.itemView.requestFocus()
                  //  } else if (SharedPreferance.getUninstall(activity)) {
                        SharedPreferance.setUninstall(activity, false)
                      // vPos = subrecyclerView.findViewHolderForAdapterPosition(lastSelectedPosition - 1)
                       // vPos!!.itemView.requestFocus()
                    } else {
                        vPos = subrecyclerView.findViewHolderForAdapterPosition(lastSelectedPosition)
                        vPos!!.itemView.requestFocus()
                    }

                }
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }
        }

        if (flag)
            changeAppLogo()

        return view
    }

    fun changeAppLogo() {
        try {
            Handler().post() {
                val packageInfo: PackageInfo = app.get(0)
                Log.e("Info", packageInfo.packageName)
                val icon = context!!.packageManager.getApplicationIcon(packageInfo.packageName)
                activity!!.jiologo!!.visibility = View.GONE
                activity!!.applogo!!.visibility = View.VISIBLE
                activity!!.applogo?.setImageDrawable(icon)
            }

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


    fun getAppList() {

        if (listType == "Downloaded Apps") {
            app.clear()
            val packs = context?.packageManager?.getInstalledPackages(0)

            for (i in packs!!.indices) {
                val pak = packs[i]
                var src = pak.applicationInfo.sourceDir

                if (src.startsWith("/data/app/")) {
                    if (!(src == "/data/app/com.jio.store-2/base.apk" || src == "/data/app/com.android.app.jio.voiceassist-1/base.apk"
                                    ||src == "/data/app/com.iwedia.ui.rjio.launcher.reliance-1/base.apk"))
                    //   var appname = p.applicationInfo.loadLabel(activity?.getPackageManager()).toString()
                        app.add(pak)
                    //app.size
                }
                Log.e("Download SRC NAME  ", src)
            }
        } else {
            app.clear()
            val packs = context?.packageManager?.getInstalledPackages(0)
            for (i in packs!!.indices) {
                val p = packs[i]
                var src = p.applicationInfo.sourceDir
                Log.e("System SRC", src)
                if (src.startsWith("/system/") || src == "/data/app/com.jio.store-2/base.apk" || src == "/data/app/com.android.app.jio.voiceassist-1/base.apk"
                        || src == "/data/app/com.iwedia.ui.rjio.launcher.reliance-1/base.apk") {
                    //var appname = p.applicationInfo.loadLabel(activity?.getPackageManager()).toString()
                    app.add(p)

                }
            }
        }

        Collections.sort(app, object : Comparator<PackageInfo> {
            override fun compare(a: PackageInfo, b: PackageInfo): Int {
                return ("" + a!!.applicationInfo.loadLabel(context!!.packageManager)).compareTo("" + b!!.applicationInfo.loadLabel(context!!.packageManager), true)
            }
        })

        appAdapter = SubDetailsAdapter(context!!, app, activity!!)
        subrecyclerView.adapter = appAdapter
    }


    inner class SpaceItemDecoration internal constructor(private val space: Int) : RecyclerView.ItemDecoration() {

        override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView,
                                    state: RecyclerView.State) {
            outRect.top = space
        }
    }

    override fun onResume() {
        super.onResume()
        getAppList()
        //activity?.jiologo?.setImageDrawable(context?.getDrawable(R.drawable.jio_logo))
    }

    override fun onDestroyView() {
        super.onDestroyView()

    }

    override fun onDestroy() {
        super.onDestroy()
    }

    fun AppListtoDetailsFragment(title: String, p: ArrayList<PackageInfo>, position: Int) {
        var name = p.get(position)?.applicationInfo?.loadLabel(activity?.getPackageManager()).toString()
        var pkgname = p.get(position)?.applicationInfo?.packageName.toString()
        var classname = p.get(position)?.applicationInfo?.className.toString()
        var version = p.get(position)?.versionName.toString()
        var src = p.get(position)?.applicationInfo?.sourceDir.toString()
        var uid = p.get(position)?.applicationInfo?.uid

        var jiologo = activity?.findViewById<ImageView>(R.id.jiologo)
        var applogo = activity?.findViewById<ImageView>(R.id.applogo)

        header?.text = name
        val fm = activity?.supportFragmentManager
        val ft = fm?.beginTransaction()
        val llf = AppDetailsFragment.newInstance();
        val args = Bundle()
        args.putString("version", version)
        args.putString("pkgname", pkgname)
        args.putString("name", name)
        args.putString("class", classname)
        args.putString("src", src)
        args.putString("previous_title", listType)
        uid?.let { args.putInt("uid", it) }
        llf.arguments = args
        ft?.replace(R.id.sub_details_container, llf, "AppDetail_Frag")
        ft?.addToBackStack("AppDetail_Frag")
        ft?.commit()

        val newBackStackLength: Int = fm?.getBackStackEntryCount()!!.plus(1);

        fm?.addOnBackStackChangedListener(object : FragmentManager.OnBackStackChangedListener {
            override fun onBackStackChanged() {
                val nowCount = fm.getBackStackEntryCount()
                if (newBackStackLength !== nowCount) {
                    if (!DetailsFragment.isStartNowClicked) {
                        // we don't really care if going back or forward. we already performed the logic here.
                        fm.removeOnBackStackChangedListener(this)

                        if (newBackStackLength > nowCount) { // user pressed back
                            //  fm.popBackStackImmediate()
                            header?.setText(listType)
                            // jiologo?.setImageDrawable(context?.getDrawable(R.drawable.jio_logo))
                            appAdapter.notifyDataSetChanged()
                        }
                    }
                }
            }
        })
    }


    fun getInstalledApps(): ArrayList<AppList> {
        val res = ArrayList<AppList>();

        val packs = context?.getPackageManager()!!.getInstalledPackages(0)

        for (i in packs!!.indices) {
            val p: PackageInfo = packs.get(i)
            if ((isSystemPackage(p) == false)) {
                val appName = p.applicationInfo.loadLabel(context?.getPackageManager()).toString();
                var icon = p.applicationInfo.loadIcon(context?.getPackageManager())
                res.add(AppList(appName, icon))
            }
        }
        return res
    }

    fun isSystemPackage(pkgInfo: PackageInfo): Boolean {
        var res: Boolean
        res = ApplicationInfo.FLAG_SYSTEM == 1

        return res

        Log.d("isSystemPackage APP LIST ", res.toString())
    }

}