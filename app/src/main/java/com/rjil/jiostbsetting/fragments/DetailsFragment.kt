package com.rjil.jiostbsetting.fragments

import android.app.PendingIntent
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothManager
import android.bluetooth.BluetoothProfile
import android.content.*
import android.content.pm.PackageManager
import android.graphics.Rect
import android.hardware.usb.UsbDevice
import android.hardware.usb.UsbManager
import android.net.Uri
import android.net.wifi.WifiManager
import android.os.Bundle
import android.os.Handler
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.*
import androidx.core.content.ContextCompat.checkSelfPermission
import com.github.mjdev.libaums.UsbMassStorageDevice
import com.rjil.jiostbsetting.AlertFragment
import com.rjil.jiostbsetting.MainActivity
import com.rjil.jiostbsetting.R
import com.rjil.jiostbsetting.adapters.DetailsAdapter
import com.rjil.jiostbsetting.adapters.DetailsSubListAdapter
import com.rjil.jiostbsetting.listener.OnItemClickListener
import com.rjil.jiostbsetting.listener.addOnItemClickListener
import com.rjil.jiostbsetting.models.Bluetooth
import com.rjil.jiostbsetting.tvrecyclerview.CenterSmoothScroller
import com.rjil.jiostbsetting.utils.Const
import com.rjil.jiostbsetting.utils.Constant
import com.rjil.jiostbsetting.utils.Constant.Companion.TAG
import com.rjil.jiostbsetting.utils.SharedPreferance
import com.rjil.jiostbsetting.utils.StorageInformation
import kotlinx.android.synthetic.main.activity_details.*
import kotlinx.android.synthetic.main.fragment_sub_details_menu.*
import java.io.File


class DetailsFragment : Fragment() {

    private lateinit var filterusb: IntentFilter
    private lateinit var filter: IntentFilter
    var progress: FrameLayout? = null

    companion object {
        var itemCount: Int = 0
        var itemArray = arrayOf("")
        var itemValue = arrayOf("")

        var sub_itemCount: Int = 0
        var sub_itemArray = arrayListOf<String>()
        var sub_itemValue = arrayListOf<Bluetooth>()
        var lastSelectedPosition = 0
        var isStartNowClicked = false
        lateinit var recyclerView: RecyclerView
        lateinit var sublist_recyclerView: RecyclerView
     //   var isUSBConnected = false
        var mAdapter: DetailsAdapter? = null
        lateinit var header: TextView
        fun newInstance(): DetailsFragment {
            return DetailsFragment()
        }
        val ACTION_USB_PERMISSION = "com.rjil.jiostbsetting.USB_PERMISSION"

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.fragment_details_menu, container,
                false)
        val activity = activity
        recyclerView = view.findViewById(R.id.details_recycler_view) as RecyclerView
        progress = view.findViewById(R.id.frame_progress) as FrameLayout
        recyclerView.layoutManager = GridLayoutManager(activity!!, 1) as RecyclerView.LayoutManager?
        view.findViewById<LinearLayout>(R.id.sublist_layout).visibility = View.GONE
        sublist_recyclerView = view.findViewById(R.id.more_recycler_view) as RecyclerView
        header = activity?.findViewById(R.id.headerText) as TextView
        activity?.applogo!!.visibility = View.GONE
        activity?.jiologo!!.visibility = View.VISIBLE
        val smoothScroller = CenterSmoothScroller(recyclerView.context)
        smoothScroller.targetPosition = 1
        recyclerView.layoutManager!!.startSmoothScroll(smoothScroller)

        filter = IntentFilter()
        filterusb = IntentFilter(ACTION_USB_PERMISSION)
        filter.addAction(BluetoothDevice.ACTION_ACL_CONNECTED)
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED)

        filterusb.addAction(UsbManager.ACTION_USB_DEVICE_ATTACHED)
        filterusb.addAction(UsbManager.ACTION_USB_DEVICE_DETACHED)

        context!!.registerReceiver(mReceiver, filter)
        context!!.registerReceiver(usbReceiver, filterusb)

        if(activity?.intent?.getStringExtra("title").equals("About")) {
            itemCount = Constant.ABOUT_SUB_OPTION.size
            itemArray = Constant.ABOUT_SUB_OPTION
            itemValue = Constant.ABOUT_SUB_OPTION_VALUE
            itemValue[3] = Constant.getDeviceName(activity?.applicationContext!!)
            itemValue[7] = Constant.getKernelVersion()
        } else if (activity?.intent?.getStringExtra("title").equals("Network")) {
            itemCount = Constant.NETWORK_SUB_OPTION.size
            itemArray = Constant.NETWORK_SUB_OPTION
            itemValue = Constant.NETWORK_SUB_OPTION_VALUE
        } else if (activity?.intent?.getStringExtra("title").equals("Apps")) {
            itemCount = Constant.APPS_SUB_OPTION.size
            itemArray = Constant.APPS_SUB_OPTION
            itemValue = Constant.APPS_SUB_OPTION_VALUE
        } else if (activity?.intent?.getStringExtra("title").equals("Storage & Reset")) {
            itemArray = Constant.STORAGE_SUB_OPTION_WITH_EXTDEVICE
            itemCount = Constant.STORAGE_SUB_OPTION_WITH_EXTDEVICE.size
            itemValue = Constant.STORAGE_SUB_OPTION_VALUE_WITH_EXTDEVICE
            discoverDevice()
        } else if (activity?.intent?.getStringExtra("title").equals("Display, Keyboard & Language")) {
            itemCount = Constant.DISPLAY_SUB_OPTION.size
            itemArray = Constant.DISPLAY_SUB_OPTION
            itemValue = Constant.DISPLAY_SUB_OPTION_VALUE
            // itemValue[3] = Constant.getCurrentKeyboard(context!!)
        } else if (activity?.intent?.getStringExtra("title").equals("Developers & Location")) {
            itemCount = Constant.SECURITY_SUB_OPTION.size
            itemArray = Constant.SECURITY_SUB_OPTION
            itemValue = Constant.SECURITY_SUB_OPTION_VALUE
        } else if (activity?.intent?.getStringExtra("title").equals("Remote & Accessory")) {
            itemCount = Constant.REMOTE_SUB_OPTION.size
            itemArray = Constant.REMOTE_SUB_OPTION
            itemValue = Constant.REMOTE_SUB_OPTION_VALUE

            var bondedDevices: Set<BluetoothDevice> = Constant.getBLEDeviceList()!!
            sub_itemCount = bondedDevices.size
            sub_itemArray = Constant.getBLEDeviceName(bondedDevices)!!
            sub_itemValue = Constant.isDeviceConnected(bondedDevices)!!
            view.findViewById<LinearLayout>(R.id.sublist_layout).visibility = View.VISIBLE
            view.findViewById<LinearLayout>(R.id.sublist_layout).isFocusable = false
        } else if (activity?.intent?.getStringExtra("title").equals("Inactivity Timer & HDMI CEC")) {
            itemCount = Constant.OTHER_SUB_OPTION.size
            itemArray = Constant.OTHER_SUB_OPTION
            itemValue = Constant.OTHER_SUB_OPTION_VALUE
        } else {
            itemCount = 0
            itemArray = emptyArray()
            itemValue = emptyArray()
        }
        if (itemCount > 0) {
            val manager = GridLayoutManager(context, itemCount)
            manager.orientation = LinearLayoutManager.HORIZONTAL
            recyclerView.layoutManager = manager
            val itemSpace = resources.getDimensionPixelSize(R.dimen.recyclerView_item_space)
            recyclerView.addItemDecoration(SpaceItemDecoration(itemSpace))
//
//            recyclerView.setSelectPadding(35, 34, 35, 34)
            mAdapter = DetailsAdapter(context!!, itemArray, itemValue, activity?.intent?.getStringExtra("title")!!)
            recyclerView.adapter = mAdapter
        }
        header?.text = activity?.intent?.getStringExtra("title")

        recyclerView.addOnItemClickListener(object : OnItemClickListener {
            override fun onItemViewClick(position: Int, view: View) {
                lastSelectedPosition = position
//            recyclerView.setOnItemStateListener(object : TvRecyclerView.OnItemStateListener {
//                override fun onItemViewClick(view: View?, position: Int) {
                Log.d("TAG", "##" + activity?.intent?.getStringExtra("title"))
                if (activity?.intent?.getStringExtra("title").equals("About")) {
                    when (position) {

                        //  0 -> Constant.rebootBOX(activity?.applicationContext!!)
                        0 -> {
                            openAlert("Restart", "", AlertFragment.newInstance())

                        }
                        1 -> addSubDetailsFragment("Status")
                        2 -> Constant.startWebActivity(activity?.applicationContext!!, "Open-source licences")

                    }
                } else if (activity?.intent?.getStringExtra("title").equals("Network")) {
                    addNetWorkSettingFragment("Network")
                } else if (activity?.intent?.getStringExtra("title").equals("Display, Keyboard & Language")) {
                    when (position) {
                        0 -> {
                            isStartNowClicked = true
//                            Constant.startOverScanActivity(activity?.applicationContext!!)
                            try {
                                var intent = Intent()
                                intent.component = ComponentName("com.android.tv.settings", "com.android.tv.settings.OverscanActivity")
                                startActivity(intent)
                            } catch (e: ActivityNotFoundException) {
                                e.printStackTrace()
                                isStartNowClicked = false
                            }
                        }
                        1 -> addDateAndTimeFragment(getString(R.string.Date_and_time))
//                             1->{
//                                 Constant.getSelectedLanguage(context!!)
//                             }

                        2 -> addLanguageChangeFragment(getString(R.string.language))
                        3 -> addChangeKeyboard(getString(R.string.keyboard))
                        //  3 -> Constant.detechKeyboardLang(activity?.applicationContext!!)
                    }
                } else if (activity?.intent?.getStringExtra("title").equals("Apps")) {
                    when (position) {
                        0 -> addAppListFragment("Downloaded Apps")
                        1 -> addAppListFragment("System Apps")
                    }
                } else if (activity?.intent?.getStringExtra("title").equals("Storage & Reset")) {
                    when (position) {

                        0 -> addStorageAndResetFragment("Device Storage")
                        1 -> {
                            addStorageAndResetFragment("External Storage")
//                            } else Toast.makeText(context, "Please give permission to access the size", Toast.LENGTH_SHORT).show()
                        }
                        2 -> {
                            openAlert("Reset", "", AlertFragment.newInstance())

//                            val intent = Intent(context, AlertActivity::class.java)
//                            intent.putExtra("title", "Reset")
//                            startActivity(intent)
//                            MainActivity.devicePolicyManager?.wipeData(0);
                            //  Constant.factoryReset(activity?.applicationContext!!)
                        }
                    }
                } else if (activity?.intent?.getStringExtra("title").equals("Developers & Location")) {
                    when (position) {
                        0 -> addDeveloperFragment("Developer Options")
                        1 -> if (view?.tag.toString().equals("SWITCH")) {
                            var switch: Switch? = view?.findViewById(R.id.switch_item)!!
                            if (switch != null && !switch.isChecked) {
                                Constant.setLocationMode(activity?.applicationContext!!, !switch.isChecked)
                                switch!!.isChecked = true
                                SharedPreferance.setLocationValue(activity?.applicationContext!!, 1)
                                // switch.isChecked = Constant.getLocationMode(activity?.applicationContext!!)
                            } else {
                                SharedPreferance.setLocationValue(activity?.applicationContext!!, 0)
                                switch!!.isChecked = false
                            }
                        }
                    }
                } else if (activity?.intent?.getStringExtra("title").equals("Remote & Accessory")) {
                    when (position) {
                        0 -> Constant.startMobileRemoteDetailActivity(activity?.applicationContext!!)
//                        1 -> Constant.addBLEAccessory(activity?.applicationContext!!)
                       // 1 -> Toast.makeText(context,"Coming Soon..",Toast.LENGTH_SHORT).show()
                    }
                } else if (activity?.intent?.getStringExtra("title").equals("Inactivity Timer & HDMI CEC")) {
                    when (position) {
                        0 -> {
                            var switch: Switch? = view?.findViewById(R.id.switch_item)!!
                            if (switch != null) {
                                if(Constant.isDaydreamEnabled(activity?.applicationContext!!) == 1) {
                                    Constant.DayDreamEnable(activity?.applicationContext!!, 0)
                                    switch.isChecked = false
                                } else {
                                    Constant.DayDreamEnable(activity?.applicationContext!!, 1)
                                    switch.isChecked = true
                                }
                            }
                        }
                        1 -> addDaydreamFragment("Screensaver")
                        2 -> addDaydreamFragment("When to Start Screensaver")
                        3 -> addDeviceSleepFragment("Put Device to Sleep")
                        4 -> {

                            if(Constant.isDaydreamEnabled(activity?.applicationContext!!)==0){
                                Toast.makeText(getActivity(), "ScreenSaver is off.Turn on to start screensaver", Toast.LENGTH_SHORT).show()
                            }else{
                                isStartNowClicked = true
                                Constant.startDreaming()
                            }
                        }
                        5 -> if (view?.tag.toString() == "SWITCH") {
                            var switch: Switch? = view?.findViewById(R.id.switch_item)!!
                            if (switch != null) {
                                Constant.writeCecOption(activity?.applicationContext!!, "hdmi_control_enabled", !switch.isChecked)
                                switch.isChecked = Constant.readCecOption(activity.applicationContext, "hdmi_control_enabled")
                            }
                        }
                    }


                }
            }

            override fun onItemViewFocusChanged(gainFocus: Boolean, view: View, position: Int) {

            }
        })

        if (sub_itemCount > 0) {
            val manager = GridLayoutManager(context, sub_itemCount)
            manager.orientation = LinearLayoutManager.HORIZONTAL
            sublist_recyclerView.layoutManager = manager
            val itemSpace = resources.getDimensionPixelSize(R.dimen.recyclerView_item_space)
            sublist_recyclerView.addItemDecoration(SpaceItemDecoration(itemSpace))
//            sublist_recyclerView.setSelectPadding(35, 34, 35, 34)
            Log.d(TAG, "sub_itemValue.size.toString() " + sub_itemValue.size.toString())
            sublist_recyclerView.adapter = DetailsSubListAdapter(context!!, sub_itemArray, sub_itemValue, activity?.intent?.getStringExtra("title")!!)

        }

        recyclerView.addOnLayoutChangeListener { view, i, i2, i3, i4, i5, i6, i7, i8 ->
            try {
                if (SharedPreferance.getItemPos(activity!!.applicationContext).equals("0")) {
                    var vPos: RecyclerView.ViewHolder?
                    if (view != null && lastSelectedPosition >= 0) {
                        vPos = recyclerView.findViewHolderForAdapterPosition(lastSelectedPosition)
                        vPos!!.itemView.requestFocus()
                        Log.d("CurrentFocusedView", "Focused")
                      //  isUSBConnected = false
                        // context!!.unregisterReceiver(usbReceiver)
                    } else {
                        Log.d("Change Lay Selected Pos", lastSelectedPosition.toString())
                    }
                }

            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }
        }

        return view
    }


    override fun onDestroy() {
        super.onDestroy()
        try {
            if (mReceiver!=null) {
                context!!.unregisterReceiver(mReceiver)
            }
        } catch (ex:IllegalArgumentException) {
            ex.printStackTrace()
        }

        try {
            if (usbReceiver!=null) {
                context!!.unregisterReceiver(usbReceiver)
            }
        } catch (ex:IllegalArgumentException) {
            ex.printStackTrace()
        }
    }

    private fun openAlert(title: String, packagename: String, newInstance: Fragment) {
        val header = activity?.findViewById<TextView>(R.id.headerText)
        header?.setText(title)
        val fm = activity?.supportFragmentManager
        val ft = fm?.beginTransaction()
//        val llf = AlertFragment.newInstance()
        val llf = newInstance
        val args = Bundle()
        args.putString("title", title)
        args.putString("pkgname", packagename)
        llf.arguments = args
        ft?.replace(R.id.sub_details_container, llf, "alert_frag")
        ft?.addToBackStack("alert_frag")
        ft?.commit()

        val newBackStackLength: Int = fm?.getBackStackEntryCount()!!.plus(1);

        fm?.addOnBackStackChangedListener(object : FragmentManager.OnBackStackChangedListener {
            override fun onBackStackChanged() {
                val nowCount = fm.getBackStackEntryCount()
                if (newBackStackLength !== nowCount) {
                    fm.removeOnBackStackChangedListener(this)

                    if (newBackStackLength > nowCount) { // user pressed back
                        //  fm.popBackStackImmediate()
                        if (header != null) {
                            //header.setText(activity?.intent?.getStringExtra("title"))
//                            header!!.setText(arguments!!.getString("title").toString())
                        }

                    }
                }
            }
        })
    }




    fun checkPermission(): Boolean {
        var result = checkSelfPermission(context!!, ACTION_USB_PERMISSION);
        return result == PackageManager.PERMISSION_GRANTED
    }

    fun openLicence() {

        val DEFAULT_LICENSE_PATH = "/system/etc/NOTICE.html.gz"
        val PROPERTY_LICENSE_PATH = "ro.config.license_path"
        val fileName = System.getenv(PROPERTY_LICENSE_PATH) ?: DEFAULT_LICENSE_PATH
        val file = File(fileName)
        val intent = Intent(Intent.ACTION_VIEW)
        intent.setDataAndType(Uri.fromFile(file), "text/html");
        intent.putExtra(Intent.EXTRA_TITLE, "Open-source licences")
        intent.addCategory(Intent.CATEGORY_DEFAULT)
        intent.setPackage("com.android.htmlviewer")

        try {
            startActivity(intent)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


    inner class SpaceItemDecoration internal constructor(private val space: Int) : RecyclerView.ItemDecoration() {

        override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView,
                                    state: RecyclerView.State) {
            outRect.top = space
        }
    }

    fun addSubDetailsFragment(title: String) {
        SharedPreferance.setItemPos(activity!!.applicationContext, "0")

        header?.setText(title)
        val fm = activity?.supportFragmentManager
        val ft = fm?.beginTransaction()
        val llf = DetailsSubFragment.newInstance()
        val bundle: Bundle = Bundle()
        bundle.putString("title", title)
        llf.arguments = bundle
      //  ft!!.remove(fm.findFragmentById(R.id.sub_details_container)!!)
       ft?.replace(R.id.sub_details_container, llf, "details_sub_frag")
      //  ft?.add(fm.findFragmentById(R.id.sub_details_container)!!,"details_sub_frag")
        ft?.addToBackStack("details_sub_frag")
        ft?.commit()

        val newBackStackLength: Int = fm?.backStackEntryCount!!.plus(1)

        fm?.addOnBackStackChangedListener(object : FragmentManager.OnBackStackChangedListener {
            override fun onBackStackChanged() {
                val nowCount = fm.backStackEntryCount
                if (newBackStackLength !== nowCount) {
                    // we don't really care if going back or forward. we already performed the logic here.
                    fm.removeOnBackStackChangedListener(this)

                    if (newBackStackLength > nowCount) { // user pressed back
                        //  fm.popBackStackImmediate()
                        header?.setText(activity?.intent?.getStringExtra("title"))
                    }
                }
            }
        })
    }

    fun addDaydreamFragment(title: String) {
        SharedPreferance.setItemPos(activity!!.applicationContext, "0")
        val header = activity?.findViewById<TextView>(R.id.headerText)
        header?.setText(title)
        val fm = activity?.supportFragmentManager
        val ft = fm?.beginTransaction()
        val llf = DaydreamFragment.newInstance()
        val bundle: Bundle = Bundle()
        bundle.putString("title", title)
        llf.arguments = bundle
        ft?.replace(R.id.sub_details_container, llf, "daydream_frag")
        ft?.addToBackStack("daydream_frag")
        ft?.commit()

        val newBackStackLength: Int = fm?.getBackStackEntryCount()!!.plus(1);

        fm?.addOnBackStackChangedListener(object : FragmentManager.OnBackStackChangedListener {
            override fun onBackStackChanged() {
                val nowCount = fm.getBackStackEntryCount()
                if (newBackStackLength !== nowCount) {
                    if (!isStartNowClicked) {
                        // we don't really care if going back or forward. we already performed the logic here.
                        fm.removeOnBackStackChangedListener(this)
                        if (newBackStackLength > nowCount) { // user pressed back
                            //  fm.popBackStackImmediate()
                            header?.setText(activity?.intent?.getStringExtra("title"))

                        }
                    }
                }
            }
        })
    }

    fun addDeviceSleepFragment(title: String) {
        SharedPreferance.setItemPos(activity!!.applicationContext, "0")

        val header = activity?.findViewById<TextView>(R.id.headerText)
        header?.setText(title)
        val fm = activity?.supportFragmentManager
        val ft = fm?.beginTransaction()
        val llf = DeviceSleepFragment.newInstance()
        val bundle: Bundle = Bundle()
        bundle.putString("title", title)
        llf.arguments = bundle
        ft?.replace(R.id.sub_details_container, llf, "devicesleep_frag")
        ft?.addToBackStack("devicesleep_frag")
        ft?.commit()

        val newBackStackLength: Int = fm?.backStackEntryCount!!.plus(1)

        fm?.addOnBackStackChangedListener(object : FragmentManager.OnBackStackChangedListener {
            override fun onBackStackChanged() {
                val nowCount = fm.getBackStackEntryCount()
                if (newBackStackLength !== nowCount) {
                    if (!isStartNowClicked) {
                        // we don't really care if going back or forward. we already performed the logic here.
                        fm.removeOnBackStackChangedListener(this)

                        if (newBackStackLength > nowCount) { // user pressed back
                            //  fm.popBackStackImmediate()
                            header?.setText(activity?.intent?.getStringExtra("title"))
                        }
                    }
                }
            }
        })
    }

    fun addStorageAndResetFragment(title: String) {
//        progress?.visibility = View.VISIBLE
        activity!!.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE)
        val storageInformation = StorageInformation(context!!)
        var alSize = storageInformation.getpackageSize();

        Handler().postDelayed({
//            progress?.visibility = View.GONE
            val size = alSize.get(alSize.size - 1).cachedData
            val appSize = alSize.get(alSize.size - 1).appSize
            activity!!.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE)

            try {
                SharedPreferance.setItemPos(activity!!.applicationContext, "0")
                val header = activity?.findViewById<TextView>(R.id.headerText)
                header?.setText(title)
                val fm = activity?.supportFragmentManager
                val ft = fm?.beginTransaction()
                val llf = StrorageAndResetFragment.newInstance()
                val bundle: Bundle = Bundle()
                bundle.putString("title", title)
                bundle.putString("cachedData", size)
                bundle.putString("appSize", appSize)
                llf.arguments = bundle
                ft?.replace(R.id.sub_details_container, llf, "storageReset_frag")
                ft?.addToBackStack("storageReset_frag")
                ft?.commit()

                val newBackStackLength: Int = fm?.getBackStackEntryCount()!!.plus(1);

                fm?.addOnBackStackChangedListener(object : FragmentManager.OnBackStackChangedListener {
                    override fun onBackStackChanged() {
                        val nowCount = fm.getBackStackEntryCount()
                        if (newBackStackLength !== nowCount) {
                            if (!isStartNowClicked) {
                                // we don't really care if going back or forward. we already performed the logic here.
                                fm.removeOnBackStackChangedListener(this)

                                if (newBackStackLength > nowCount) { // user pressed back
                                    //  fm.popBackStackImmediate()
                                    header?.setText(activity?.intent?.getStringExtra("title"))
                                }
                            }
                        }
                    }
                })
            } catch (exc: Exception) {
                exc.printStackTrace()
            }

        }, 1500)
    }

    fun addDeveloperFragment(title: String) {
        SharedPreferance.setItemPos(activity!!.applicationContext, "0")

        val header = activity?.findViewById<TextView>(R.id.headerText)
        header?.setText(title)
        val fm = activity?.supportFragmentManager
        val ft = fm?.beginTransaction()
        val llf = DeveloperFragment.newInstance()
        val bundle: Bundle = Bundle()
        bundle.putString("title", title)
        llf.arguments = bundle
        ft?.replace(R.id.sub_details_container, llf, "developer_frag")
        ft?.addToBackStack("developer_frag")
        ft?.commit()

        val newBackStackLength: Int = fm?.getBackStackEntryCount()!!.plus(1);

        fm?.addOnBackStackChangedListener(object : FragmentManager.OnBackStackChangedListener {
            override fun onBackStackChanged() {
                val nowCount = fm.getBackStackEntryCount()
                if (newBackStackLength !== nowCount) {
                    if (!isStartNowClicked) {
                        // we don't really care if going back or forward. we already performed the logic here.
                        fm.removeOnBackStackChangedListener(this)

                        if (newBackStackLength > nowCount) { // user pressed back
                            //  fm.popBackStackImmediate()
                            header?.setText(activity?.intent?.getStringExtra("title"))
                        }
                    }
                }
            }
        })
    }

    fun addDisplayConfrugationFragment(title: String) {
        val header = activity?.findViewById<TextView>(R.id.headerText)
        header?.setText(title)
        val fm = activity?.supportFragmentManager
        val ft = fm?.beginTransaction()
        val llf = DisplayConfrugationFragment.newInstance(title);
        val bundle: Bundle = Bundle()
        bundle.putString("title", title)
        bundle.putString("param1", title)
        llf.arguments = bundle
        ft?.replace(R.id.sub_details_container, llf, "Display_Configuration")
        ft?.addToBackStack("Display_Configuration")
        ft?.commit()

        val newBackStackLength: Int = fm?.getBackStackEntryCount()!!.plus(1);

        fm?.addOnBackStackChangedListener(object : FragmentManager.OnBackStackChangedListener {
            override fun onBackStackChanged() {
                val nowCount = fm.getBackStackEntryCount()
                if (newBackStackLength !== nowCount) {
                    if (!isStartNowClicked) {
                        // we don't really care if going back or forward. we already performed the logic here.
                        fm.removeOnBackStackChangedListener(this)

                        if (newBackStackLength > nowCount) { // user pressed back
                            //  fm.popBackStackImmediate()
                            header?.setText(activity?.intent?.getStringExtra("title"))
                        }
                    }
                }
            }
        })
    }

    fun addLanguageChangeFragment(title: String) {
        SharedPreferance.setItemPos(activity!!.applicationContext, "0")

        val header = activity?.findViewById<TextView>(R.id.headerText)
        header?.setText(title)
        val fm = activity?.supportFragmentManager
        val ft = fm?.beginTransaction()
        val llf = LanguageChangeFragment.newInstance(title)
        val bundle: Bundle = Bundle()
        bundle.putString("title", title)
        bundle.putString("param1", title)
        llf.arguments = bundle
        //ft?.remove(fm.findFragmentById(R.id.sub_details_container)!!)
        if(fm!!.findFragmentByTag("Lang_Change_Frag")!=null){
            fm.popBackStackImmediate()
            ft!!.addToBackStack(null)
            ft?.remove(fm.findFragmentById(R.id.sub_details_container)!!)
        }else{
            ft?.addToBackStack("Lang_Change_Frag")
        }
        ft?.replace(R.id.sub_details_container, llf, "Lang_Change_Frag")
        ft?.commit()

        val newBackStackLength: Int = fm?.backStackEntryCount!!.plus(1)

        fm?.addOnBackStackChangedListener(object : FragmentManager.OnBackStackChangedListener {
            override fun onBackStackChanged() {
                val nowCount = fm.getBackStackEntryCount()
                if (newBackStackLength !== nowCount) {
                    if (!isStartNowClicked) {
                        fm.removeOnBackStackChangedListener(this)
                        if (newBackStackLength > nowCount) { // user pressed back
                            header?.setText(activity?.intent?.getStringExtra("title"))
                        }
                    }
                }
            }
        })
    }

    fun addChangeKeyboard(title: String) {
        SharedPreferance.setItemPos(activity!!.applicationContext, "0")

        val header = activity?.findViewById<TextView>(R.id.headerText)
        header?.setText(title)
        val fm = activity?.supportFragmentManager
        val ft = fm?.beginTransaction()
        val llf = KeyboardChangeFragment.newInstance(title);
        val bundle: Bundle = Bundle()
        bundle.putString("title", title)
        bundle.putString("param1", title)
        llf.arguments = bundle
        ft?.replace(R.id.sub_details_container, llf, "Keyboard_Change_Frag")
        ft?.addToBackStack("Keyboard_Change_Frag")
        ft?.commit()

        val newBackStackLength: Int = fm?.backStackEntryCount!!.plus(1)

        fm?.addOnBackStackChangedListener(object : FragmentManager.OnBackStackChangedListener {
            override fun onBackStackChanged() {
                val nowCount = fm.getBackStackEntryCount()
                if (newBackStackLength !== nowCount) {
                    if (!isStartNowClicked) {
                        // we don't really care if going back or forward. we already performed the logic here.
                        fm.removeOnBackStackChangedListener(this)

                        if (newBackStackLength > nowCount) { // user pressed back
                            //  fm.popBackStackImmediate()
                            header?.setText(activity?.intent?.getStringExtra("title"))
                        }
                    }
                }
            }
        })
    }

//    fun addSwitchKeyboard() {
//        val ime = activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
//        ime?.showInputMethodPicker()
//    }

    fun addDateAndTimeFragment(title: String) {
        SharedPreferance.setItemPos(activity!!.applicationContext, "0")

        val header = activity?.findViewById<TextView>(R.id.headerText)
        header?.setText(title)
        val fm = activity?.supportFragmentManager
        val ft = fm?.beginTransaction()
        val llf = DateAndTimeFragment.newInstance(title);
        val bundle: Bundle = Bundle()
        bundle.putString("title", title)
        bundle.putString("param1", title)
        llf.arguments = bundle
        ft?.replace(R.id.sub_details_container, llf, "Date_Time_frag")
        ft?.addToBackStack("Date_Time_frag")
        ft?.commit()

        val newBackStackLength: Int = fm?.getBackStackEntryCount()!!.plus(1);

        fm?.addOnBackStackChangedListener(object : FragmentManager.OnBackStackChangedListener {
            override fun onBackStackChanged() {
                val nowCount = fm.getBackStackEntryCount()
                if (newBackStackLength !== nowCount) {
                    if (!isStartNowClicked) {
                        // we don't really care if going back or forward. we already performed the logic here.
                        fm.removeOnBackStackChangedListener(this)

                        if (newBackStackLength > nowCount) { // user pressed back
                            //  fm.popBackStackImmediate()
                            header?.setText(activity?.intent?.getStringExtra("title"))
                        }
                    }
                }
            }
        })
    }

    fun addNetWorkSettingFragment(title: String) {
        val header = activity?.findViewById<TextView>(R.id.headerText)
        header?.setText(title)
        val fm = activity?.supportFragmentManager
        val ft = fm?.beginTransaction()
        val llf = NetworkDetailsFragment.newInstance(title);
        val bundle: Bundle = Bundle()
        bundle.putString("title", title)
        bundle.putString("param1", title)
        llf.arguments = bundle
        ft?.replace(R.id.sub_details_container, llf, "NetWork_Setting")
        ft?.addToBackStack("NetWork_Setting")
        ft?.commit()

        val newBackStackLength: Int = fm?.getBackStackEntryCount()!!.plus(1);

        fm?.addOnBackStackChangedListener(object : FragmentManager.OnBackStackChangedListener {
            override fun onBackStackChanged() {
                val nowCount = fm.getBackStackEntryCount()
                if (newBackStackLength !== nowCount) {
                    if (!isStartNowClicked) {
                        // we don't really care if going back or forward. we already performed the logic here.
                        fm.removeOnBackStackChangedListener(this)

                        if (newBackStackLength > nowCount) { // user pressed back
                            //  fm.popBackStackImmediate()
                            header?.setText(activity?.intent?.getStringExtra("title"))
                        }
                    }
                }
            }
        })
    }

    fun addAppListFragment(title: String) {
        SharedPreferance.setItemPos(activity!!.applicationContext, "0")
        val header = activity?.findViewById<TextView>(R.id.headerText)
        val jiologo = activity?.findViewById<ImageView>(R.id.jiologo)

        header?.setText(title)
        val fm = activity?.supportFragmentManager
        val ft = fm?.beginTransaction()
        val llf = AppListFragment.newInstance()
        AppListFragment.flag=true
        val bundle: Bundle = Bundle()
        bundle.putString("title", title)
        llf.arguments = bundle
        ft?.replace(R.id.sub_details_container, llf, "AppList_Frag")
        ft?.addToBackStack("AppList_Frag")
        ft?.commit()

        val newBackStackLength: Int = fm?.getBackStackEntryCount()!!.plus(1);

        fm?.addOnBackStackChangedListener(object : FragmentManager.OnBackStackChangedListener {
            override fun onBackStackChanged() {
                val nowCount = fm.getBackStackEntryCount()
                if (newBackStackLength !== nowCount) {
                    if (!isStartNowClicked) {
                        // we don't really care if going back or forward. we already performed the logic here.
                        fm.removeOnBackStackChangedListener(this)
                        if (newBackStackLength > nowCount) { // user pressed back
                            header?.setText(activity?.intent?.getStringExtra("title"))
                            activity?.applogo!!.visibility = View.GONE
                            activity?.jiologo!!.visibility = View.VISIBLE

                        }
                    }
                }
            }
        })
    }

    private val usbReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {

            try{
                var action = intent.action
                Log.d(TAG, "Received Broadcast: $action")

                if (ACTION_USB_PERMISSION == intent.action
                        || UsbManager.ACTION_USB_DEVICE_ATTACHED == intent.action) {
                    synchronized(this) {
                        val device: UsbDevice? = intent.getParcelableExtra(UsbManager.EXTRA_DEVICE)
                        //isUSBConnected = true
                        if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false) && device != null) {
                            device?.apply {
                                activity!!.runOnUiThread {
                                    itemValue[1] = Constant.setupUSBDevice(context)
                                    // recyclerView.adapter!!.notifyDataSetChanged()
                                    //  recyclerView.itemAnimator = null
                                    if(!itemValue[1].equals(1)) {
                                        var view = recyclerView.getChildAt(1)
                                        if (view != null) {
                                            view.isFocusable = true
                                            val Fram = recyclerView.getChildAt(1) as FrameLayout
                                            val rel = Fram.getChildAt(0) as RelativeLayout
                                            val text = rel.getChildAt(3) as TextView
                                            text.text = itemValue[1]
                                            val img = rel.getChildAt(0) as ImageView
                                            img.visibility = View.VISIBLE
                                            // mAdapter.notifyDataSetChanged()
                                        }
                                    }
                                    Log.d(TAG, "USB ATTACHED  $device")
                                }
                            }
                        } else {
                            val usbManager = context.getSystemService(Context.USB_SERVICE) as UsbManager
                            val massStorageDevices = UsbMassStorageDevice.getMassStorageDevices(context)
                            val permissionIntent = PendingIntent.getBroadcast(context, 0, Intent(
                                    ACTION_USB_PERMISSION), 0)
                            usbManager.requestPermission(massStorageDevices[0].usbDevice, permissionIntent)
                            Log.d(TAG, "permission denied for device $device")
                        }
                    }
                } else if (UsbManager.ACTION_USB_DEVICE_DETACHED == intent.action) {
                    val device: UsbDevice? = intent.getParcelableExtra(UsbManager.EXTRA_DEVICE)
                    device?.apply {
                        Log.e(TAG, "USB DETECHED  $device")
                        recyclerView.getChildAt(1).isFocusable = true
                        val Fram = recyclerView.getChildAt(1) as FrameLayout
                        val  rel = Fram.getChildAt(0) as RelativeLayout
                        val  text = rel.getChildAt(3) as TextView
                        text.text = "0 KB"
                        val img = rel.getChildAt(0) as ImageView
                        img.visibility = View.GONE
                        // mAdapter!!.notifyItemChanged(1)
//                    fragmentManager!!.popBackStack()
                    }
                }
            }catch (e: java.lang.Exception)
            {
                e.printStackTrace()
            }
          //  mAdapter!!.notifyDataSetChanged()
        }

    }


    private val mReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val action = intent.action
            Log.d(TAG, "mReceiver action = " + action!!)
            Log.d(TAG, "USB Device Attached $action")

            if (BluetoothDevice.ACTION_FOUND == action) {
                //isUSBConnected = false
            } else if (BluetoothDevice.ACTION_ACL_CONNECTED == action) {
                btDeviceStatus(true)
              //  isUSBConnected = false
            } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED == action) {

            } else if (BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED == action) {

            } else if (BluetoothDevice.ACTION_ACL_DISCONNECTED == action) {
                btDeviceStatus(false)

//            } else if (UsbManager.ACTION_USB_DEVICE_ATTACHED.equals(action)) {
//
//                val device: UsbDevice? = intent.getParcelableExtra(UsbManager.EXTRA_DEVICE)
//                if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
//                    device?.apply {
//                        itemValue[1] = Constant.setupUSBDevice(context!!)
//
//                    }
//                } else {
//                    Log.d(TAG, "permission denied for device $device")
//                    val permissionIntent = PendingIntent.getBroadcast(context, 0, Intent(
//                            ACTION_USB_PERMISSION), 0)
//                    //  usbManager.requestPermission(massStorageDevices[0].usbDevice, permissionIntent)
//                }
//                discoverDevice()
            }
        }
    }


    fun btDeviceStatus(flag: Boolean) {
        sub_itemValue.clear()
        Log.d("Remoted Connected Status", "btDeviceStatus:$flag")
        val bluetoothManager =
                context!!.getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager?
        val gattServerConnectedDevices =
                bluetoothManager!!.getConnectedDevices(BluetoothProfile.GATT)
        for (device in gattServerConnectedDevices) {
            Log.d("Remoted Connected Status ", "Bt Name:" + device.name)
            val bleName = arrayListOf<Bluetooth>()
            if (flag) {
                if (device.toString()
                                .substring(0, 8) == Constant.JIO_REMOTE_MAC) {

                    sub_itemValue.add(Bluetooth(true))
                } else {
                    sub_itemValue.add(Bluetooth(false))
                }
            } else {
                sub_itemValue.add(Bluetooth(false))

            }
            break

        }

        val bAdapter = BluetoothAdapter.getDefaultAdapter()
        val pairedDevices = bAdapter.bondedDevices
        if (pairedDevices.size > 0) {
            // There are paired devices. Get the name and address of each paired device.
            for (device in pairedDevices) {

                if (flag) {
                    if (device.toString()
                                    .substring(0, 8) == Constant.JIO_REMOTE_MAC) {
                        sub_itemValue.add(Bluetooth(true))
                    } else {
                        sub_itemValue.add(Bluetooth(true))
                    }
                } else {
                    sub_itemValue.add(Bluetooth(false))
                }
            }
            try {
                sublist_recyclerView.adapter!!.notifyDataSetChanged()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    override fun onResume() {
        super.onResume()
//        context!!.registerReceiver(mReceiver, filter)
        context!!.registerReceiver(usbReceiver, filterusb)
    }
    override fun onPause() {
        super.onPause()
        try {
            if (mReceiver != null)
//                context!!.unregisterReceiver(mReceiver)
            else {
                context!!.unregisterReceiver(usbReceiver)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onStop() {
        super.onStop()
        try {
            if (mReceiver != null)
                context!!.unregisterReceiver(mReceiver) else {
                context!!.unregisterReceiver(usbReceiver)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

//    override fun onDestroy() {
//        super.onDestroy()
//        try {
//            if (mReceiver != null)
//                context!!.unregisterReceiver(mReceiver)
//            else {
//                context!!.unregisterReceiver(usbReceiver)
//            }
//        } catch (e: Exception) {
//            e.printStackTrace()
//        }
//    }


    fun discoverDevice() {
        try {
            Log.d(TAG, "@@@@@@@@@@discoverDevice")
            val usbManager = context!!.getSystemService(Context.USB_SERVICE) as UsbManager
            val massStorageDevices = UsbMassStorageDevice.getMassStorageDevices(context!!)
            val usbDevice: UsbDevice? = activity!!.intent.getParcelableExtra(UsbManager.EXTRA_DEVICE)

            if (usbDevice != null && usbManager.hasPermission(usbDevice)) {
                Log.d(TAG, "received usb device via intent")
                itemValue[1] = Constant.setupUSBDevice(context!!)
                //} else if (usbDevice == null&&Constant.isStorageUSBConnected(context!!)) {
                //  itemValue[1] = Constant.setupUSBDevice(context!!)
            } else {
                val permissionIntent = PendingIntent.getBroadcast(context, 0, Intent(
                        ACTION_USB_PERMISSION), 0)
                usbManager.requestPermission(massStorageDevices[0].usbDevice, permissionIntent)
//                itemValue[1] = "0 KB"
            }
        } catch (e: Exception) {
            e.printStackTrace()
            itemValue[1] = "0 KB"

        }

    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.d("DetailActivity", "requestCode $requestCode && ResultCode $resultCode")
        if (requestCode == 101) {
            startActivity(Intent(context, MainActivity::class.java))
            activity!!.finish()
        }
    }

}