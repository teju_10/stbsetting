package com.rjil.jiostbsetting.fragments

import android.Manifest
import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Context.CONNECTIVITY_SERVICE
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Rect
import android.net.ConnectivityManager
import android.net.ConnectivityManager.CONNECTIVITY_ACTION
import android.net.Uri
import android.net.wifi.ScanResult
import android.net.wifi.WifiInfo
import android.net.wifi.WifiManager
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.provider.Settings
import android.text.format.Formatter
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.*
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SimpleItemAnimator
import com.iwedia.fti.rjio.fti.api.AsyncReceiver
import com.iwedia.fti.rjio.fti_sdk.RjioSdk
import com.iwedia.ui.rjio.launcher.reliance.internate.OnConnectionCallback
import com.rjil.jiostbsetting.DetailsActivity
import com.rjil.jiostbsetting.R
import com.rjil.jiostbsetting.SettingApplication
import com.rjil.jiostbsetting.WpsActivity
import com.rjil.jiostbsetting.adapters.network.LessOptionAdapter
import com.rjil.jiostbsetting.fragments.DetailsFragment.Companion.isStartNowClicked
import com.rjil.jiostbsetting.listener.OnItemClickListener
import com.rjil.jiostbsetting.listener.addOnItemClickListener
import com.rjil.jiostbsetting.network.CheckNetworkConnection
import com.rjil.jiostbsetting.utils.ConnectionReceiver
import com.rjil.jiostbsetting.utils.Utils
import kotlinx.android.synthetic.main.fragment_network_detail.view.*
import java.net.Inet4Address
import java.net.InetAddress
import java.net.NetworkInterface
import java.net.SocketException
import java.util.*
import kotlin.collections.ArrayList
import kotlin.concurrent.thread


class NetworkDetailsFragment : Fragment(), View.OnClickListener, View.OnFocusChangeListener, OnConnectionCallback {

    lateinit var optionList: ArrayList<String>
    lateinit var valueList: ArrayList<String>

    var connectedDevice: TextView? = null
    var val_ipaddress: TextView? = null
    var val_subnetmask: TextView? = null
    var lbl_signalstrenght: TextView? = null
    var val_signal: TextView? = null
    var header: TextView? = null
    var val_dns: TextView? = null
    var val_mac: TextView? = null
    var lbl_mac: TextView? = null
    var lbl_emac: TextView? = null
    var val_emac: TextView? = null
    var progress: FrameLayout? = null
    var connection: RelativeLayout? = null
    var connectedstrlabel: TextView? = null
    var ipinfo: LinearLayout? = null
    var itemWifiScaned: ArrayList<ScanResult>? = ArrayList()
    var itemWifiScaned1: ArrayList<ScanResult>? = ArrayList()
    lateinit var handler: Handler
    var conn_type: TextView? = null
    var CONECTION_TYPE: String? = null
    var lastSelectedPosition : Int=0
    var isNetWorkConnected = true
    private var toast: Toast? = null
    //recycleview
    var recyclerView: RecyclerView? = null
    var title: String? = null
    var mAdapter: LessOptionAdapter? = null
    var flag=false
    private val wifiManager: WifiManager
        get() = activity?.getSystemService(Context.WIFI_SERVICE) as WifiManager
    private var wifiReceiverRegistered: Boolean = false
    var info: WifiInfo? = null
    var isConnected: Boolean = false
    private var connectionReceiver: ConnectionReceiver? = null
    // TODO: Rename and change types and number of parameters
    companion object {
        private const val PERMISSIONS_REQUEST_CODE_ACCESS_COARSE_LOCATION = 120
        const val NEW_NETWORK_KEY: String = "new_network"
        public fun newInstance(param1: String): NetworkDetailsFragment {
            val fragment = NetworkDetailsFragment()
            val args = Bundle()
            args.putString("param1", param1)
            // args.putString(ARG_PARAM2, ConfriType);
            fragment.arguments = args
            return fragment
        }

        var isRefreshList : Boolean?=false
    }

    private val wifiReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val results = wifiManager.getScanResults()
            info = wifiManager!!.connectionInfo

            val connSSID = info?.ssid?.replace("\"", "")

            if (results != null && results.size > 0) {
                itemWifiScaned?.clear()
                itemWifiScaned1?.clear()
                itemWifiScaned?.addAll(results.distinctBy { it -> it.SSID })
//                itemWifiScaned=ArrayList<ScanResult>(itemWifiScaned1)

                try {
                    for (item in itemWifiScaned?.indices!!) {
                        if (item < itemWifiScaned!!.size)
                        {
                            val str = itemWifiScaned!![item].SSID
                            Log.d("ssid+bssid", str + itemWifiScaned?.get(item)?.BSSID)
                            if (isNullOrEmpty(str)) {
                                println("ssiddrop")
                                itemWifiScaned!!.remove(itemWifiScaned?.get(item))
                            } else {
                                if (itemWifiScaned?.get(item)?.SSID.equals(connSSID)) {
                                    Collections.swap(itemWifiScaned, item, 0)
                                }
                            }
                        }
                    }

                } catch (e: Exception) {
                    e.printStackTrace()
                }


            }

        }

    }


    fun isNullOrEmpty(str: String?): Boolean {
        if (str != null && !str.trim().isEmpty())
            return false
        return true
    }



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.fragment_network_detail, container,
                false)
        val activity = activity
        recyclerView = view.findViewById(R.id.netsubmenu_recy_view) as RecyclerView
//        recyclerView!!.disableItemAnimator()
        header = activity?.findViewById(R.id.headerText) as TextView
        header?.text = activity?.intent?.getStringExtra("title")
        title = activity.intent?.getStringExtra("title")
        handler = Handler()
        handler.post(runnableCode)
        optionSetting()

        //settingPermission()

        connectedDevice = view.findViewById<TextView>(R.id.connectedDevice)
        connectedstrlabel = view.findViewById<TextView>(R.id.connectedstrlabel)
        ipinfo = view.findViewById<LinearLayout>(R.id.ipinfo)
        val_ipaddress = view.findViewById<TextView>(R.id.val_ipaddress)
        // val_subnetmask = view.findViewById<TextView>(R.id.val_subnetmask)
        lbl_signalstrenght = view.findViewById<TextView>(R.id.lbl_signalstrenght)
        val_signal = view.findViewById<TextView>(R.id.val_signal)
        // val_dns = view.findViewById<TextView>(R.id.val_dns)
        lbl_mac = view.findViewById<TextView>(R.id.lbl_mac)
        lbl_emac = view.findViewById<TextView>(R.id.lbl_emac)
        val_mac = view.findViewById<TextView>(R.id.val_mac)
        val_emac = view.findViewById<TextView>(R.id.val_emac)
        conn_type = view.findViewById<TextView>(R.id.conn_type)
        connection = view.findViewById<RelativeLayout>(R.id.connection)
        connection?.setOnClickListener(this@NetworkDetailsFragment)
        progress = view.progress
        recyclerView!!.getItemAnimator()!!.setChangeDuration(0)
        if(activity is DetailsActivity)
        {
            activity.showImageHolder()
        }
        //check which type of connection
//        checkConnectionType()
        val intentFilter: IntentFilter = IntentFilter()
        intentFilter.addAction(CONNECTIVITY_ACTION)
        activity?.registerReceiver(connectivityChangeReciver, intentFilter)
        if (checkPermissions()) {
            startScanning()
        }
//        val settingsCanWrite = Settings.System.canWrite(context);
//        if (settingsCanWrite){
//            val cr = activity?.contentResolver
//            Settings.System.putInt(cr, Settings.System.WIFI_USE_STATIC_IP, 1);
//            Settings.System.putString(cr, Settings.System.WIFI_STATIC_IP, "you.re.ip.addr");
//        }

        return view
    }

    fun RecyclerView.disableItemAnimator() {
        (itemAnimator as? SimpleItemAnimator)?.supportsChangeAnimations = false
    }


    private fun startScanning() {
        activity?.registerReceiver(wifiReceiver, IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION))
        wifiManager.startScan()
        wifiReceiverRegistered = true
    }

    fun settingPermission() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!Settings.System.canWrite(activity)) {
                val intent = Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS, Uri.parse("package:" + activity?.getPackageName()));
                startActivityForResult(intent, 200);

            }
        }

    }

    private fun stopScanning() {
        if (wifiReceiverRegistered) {
            activity?.unregisterReceiver(wifiReceiver)
            wifiReceiverRegistered = false
        }
    }

    //Permission
    private fun checkPermissions(): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                && ActivityCompat.checkSelfPermission(this!!.context!!, Manifest.permission.ACCESS_COARSE_LOCATION) !== android.content.pm.PackageManager.PERMISSION_GRANTED

        ) {
            requestPermissions(
                    arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION),
                    PERMISSIONS_REQUEST_CODE_ACCESS_COARSE_LOCATION
            )
            return false
        } else {
            return true
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            PERMISSIONS_REQUEST_CODE_ACCESS_COARSE_LOCATION -> {
                startScanning()
            }
        }
    }

    fun optionSetting() {
        //optionList
        optionList = ArrayList()
        optionList.add("Wi-Fi")
        optionList.add("Ethernet")
        optionList.add("Connect via WPS")
        optionList.add("Add new network")
        optionList.add("Proxy settings")
        // optionList.add("IP Settings")
        //value list
        valueList = ArrayList()
        valueList.add("")
        valueList.add("")
        valueList.add("")
        valueList.add("")
        valueList.add("")
        //  valueList.add("")
        //recycler view
        recyclerView?.layoutManager = GridLayoutManager(activity, 1)
        isRefreshList=true

        val manager = GridLayoutManager(context, optionList.size)
        manager.orientation = LinearLayoutManager.HORIZONTAL
        recyclerView?.layoutManager = manager
        val itemSpace = resources.getDimensionPixelSize(R.dimen.recyclerView_item_space)
        recyclerView?.addItemDecoration(SpaceItemDecoration(itemSpace))
//        recyclerView?.setSelectPadding(35, 34, 35, 34)
        WpsActivity.isnotify=false
        //adapter
        recyclerView!!.setHasFixedSize(true)
        mAdapter = LessOptionAdapter(context!!, optionList, valueList, "ConnectedWifiDetails")
        recyclerView?.adapter = mAdapter

        recyclerView?.addOnItemClickListener(object : OnItemClickListener {
            override fun onItemViewClick(position: Int, view: View) {
                lastSelectedPosition=position
                when (position) {
                    0 ->
                        if (optionList.get(0).equals(getString(R.string.wifi))) {
                            //TODO #check
                            if (itemWifiScaned?.size != 0 && itemWifiScaned!!.isNotEmpty()) {
                                addWifiListFragment(getString(R.string.wifi))
                            } else {
                                progress?.visibility = VISIBLE
                                Handler().postDelayed({
                                    //Do something after 100ms
                                    if(itemWifiScaned?.size != 0 && itemWifiScaned!!.isNotEmpty())
                                    {
                                        addWifiListFragment(getString(R.string.wifi))
                                        progress?.visibility = GONE
                                    }
                                }, 3000)
                            }
                        }
                    1 -> {
                        if (optionList.get(1).equals(getString(R.string.wifi))) {
                            //TODO #check
                            if (itemWifiScaned?.size != 0 && itemWifiScaned!!.isNotEmpty()) {
                                addWifiListFragment(getString(R.string.wifi))
                            } else {
                                progress?.visibility = VISIBLE
                                Handler().postDelayed({
                                    //Do something after 100ms
                                    if(itemWifiScaned?.size != 0 && itemWifiScaned!!.isNotEmpty())
                                    {
                                        addWifiListFragment(getString(R.string.wifi))
                                        progress?.visibility = GONE
                                    }
                                }, 3000)
                            }
                            }
                        }
                    2 -> {
                        if(RjioSdk.get().networkHandler.isEthernetConnected)
                        {
                            Toast.makeText(activity, R.string.no_internt_access, Toast.LENGTH_LONG).show()
                        }
                        else
                        {
                            val intent = Intent(activity,WpsActivity::class.java)
                            intent.putExtra("loader","Wps")
                            startActivity(intent)
                        }

//                        startActivity(Intent(activity,WpsActivity::class.java))
//                        activity!!.window.enterTransition=null
                    }

                    3 -> {
                        Handler().postDelayed({
                            addNewNetwork()
                        },100)

                    }
                    4 -> {
                        addProxeySettingFragment("Proxy settings")
                    }
//                    5 -> {
//                        addIpSettingsFragment("IP SETTINGS")
//                    }

                }
            }
            override fun onItemViewFocusChanged(gainFocus: Boolean, view: View, position: Int) {
                println("Network Details Frag $gainFocus")
            }
        })


        recyclerView!!.addOnLayoutChangeListener { view, i, i2, i3, i4, i5, i6, i7, i8 ->
            try {
                var vPos: RecyclerView.ViewHolder?
                if (view != null && lastSelectedPosition >= 0) {
                    vPos = recyclerView!!.findViewHolderForAdapterPosition(lastSelectedPosition)
                    vPos!!.itemView.requestFocus()
                } else {
                }
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }
        }
    }


    private fun addNewNetwork() {
        header?.text = title

        if (RjioSdk.get().networkHandler.isEthernetConnected) {
            if (toast == null || !toast!!.view.isShown) {
                toast = Toast.makeText(context!!, R.string.no_internt_access, Toast.LENGTH_LONG)
                toast!!.show()
            }
        } else {
            val fm = activity!!.supportFragmentManager
            val ft = fm.beginTransaction()
            val llf = WifiConfigFragment.newInstance()
            val bundle: Bundle = Bundle()
            bundle.putBoolean(NEW_NETWORK_KEY, true)
//            bundle!!.putBoolean(NEW_NETWORK_KEY,true)
            bundle!!.putString("title", "Network")
            llf.arguments = bundle
            ft.replace(R.id.sub_details_container, llf, "WifiConfigFragment")
            ft.addToBackStack("WifiConfigFragment")
            ft.commit()

            val newBackStackLength: Int = fm?.backStackEntryCount!!.plus(1)


            fm.addOnBackStackChangedListener(object : FragmentManager.OnBackStackChangedListener {
                override fun onBackStackChanged() {
                    val nowCount = fm.backStackEntryCount
                    if (newBackStackLength !== nowCount) {
                        if (!isStartNowClicked) {
                            fm.removeOnBackStackChangedListener(this)

                            if (newBackStackLength > nowCount) {
                                //header?.text = arguments?.getString("title")
                                /* if (checkPermissions()) {
                                     startScanning()
                                     wifiDetailAdpter.notifyDataSetChanged()
                                 }*/
                            }
                        }
                    }
                }
            })
        }
    }


    fun getCurrentSsid(): String? {
        var ssid: String? = null
        var wifiInfo: WifiInfo? = null
        if (wifiManager != null)
            wifiInfo = wifiManager.connectionInfo
        if (wifiInfo != null) {
            ssid = wifiInfo.ssid
            updateStatus(true, wifiInfo, this!!.CONECTION_TYPE!!)
        } else {
            updateStatus(false, wifiInfo, this!!.CONECTION_TYPE!!)
        }
        val connSSID = ssid!!.replace("\"", "")
        valueList.add(0, connSSID)
        Log.d("Current IP Address", wifiInfo!!.ipAddress.toString())
        // TODO When we connect to wps and press back at the time It is necessary to refresh list
        /*if(SettingApplication.getIpAddress().replace("\"","")!=connSSID || connSSID=="0.0.0.0")
        {
            mAdapter?.updateMenu(valueList, this.CONECTION_TYPE!!)
        }*/
        mAdapter?.updateMenu(valueList, this.CONECTION_TYPE!!)

        return ssid
    }

    private fun updateStatus(isConnected: Boolean, wifiInfo: WifiInfo?, connType: String) {
        //Todo manage status for Ethernet
        if (isConnected && connType.equals("WIFI")) {
            lbl_mac!!.visibility = View.VISIBLE
            val_mac!!.visibility = View.VISIBLE
            lbl_emac!!.visibility = View.GONE
            val_emac!!.visibility = View.GONE
            lbl_signalstrenght!!.visibility = View.VISIBLE
            val_signal!!.visibility = View.VISIBLE
            val_ipaddress?.text = getIpAddress(wifiInfo!!)
            //val dhcp:DhcpInfo= wifiManager?.dhcpInfo!!
//            val_subnetmask?.text = "" +Formatter.formatIpAddres s(dhcp.netmask)
//            val_signal?.text = "" + wifiInfo?.hiddenSSID
//            val_dns?.text = "" + dhcp.dns1.toString()
            val_mac?.text = "" + Utils.getMACAddress("wlan0")

            if(getIpAddress(wifiInfo!!).equals("0.0.0.0")){
                val_signal?.text = "N/A"
            }else{
                val_signal?.text = getSignalStrength()
            }

//            val_signal?.text = getSignalStrength()

        } else if (isConnected && connType.equals("LAN")) {
            lbl_mac!!.visibility = GONE
            val_mac!!.visibility = GONE
            lbl_emac!!.visibility = VISIBLE
            val_emac!!.visibility = VISIBLE
            lbl_signalstrenght!!.visibility = GONE
            val_signal!!.visibility = GONE
            val_ipaddress?.text = getIpAddress()
            val_subnetmask?.text = "NA"
            val_signal?.text = "N/A"
            val_dns?.text = "N/A"
            val_mac?.text = "N/A"
            val_emac?.text = "" + Utils.getMACAddress("eth0")
        } else {
            val_ipaddress?.text = "N/A"
            val_subnetmask?.text = "N/A"
            val_signal?.text = "N/A"
            val_dns?.text = "N/A"
            val_mac?.text = "N/A"
        }


    }

    fun getIpAddress(wifiInfo: WifiInfo): String? {
        var ip: Int
        ip = wifiInfo.ipAddress
        var ipaddress: String = Formatter.formatIpAddress(ip)
        if (ipaddress == "0.0.0.0") {
            val_signal!!.text = "N/A"
        }
        return ipaddress
    }

    fun isEthernetConnected() {
        try {
            val cm = activity!!.getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
            if (cm != null && cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().getType() == ConnectivityManager.TYPE_ETHERNET) {
//          conn_type?.text=getString(R.string.ethernet)
//          connectedDevice?.setText("Connected")
                CONECTION_TYPE = "LAN"
                ethernetData()
                if (isConnected) {
                    valueList.add(0, "Connected")
                } else {
                    valueList.add(0, "Connected,no internet connection")
                }
                mAdapter?.updateMenu(valueList, this.CONECTION_TYPE!!)
                //updateStatus(true, null, this!!.CONECTION_TYPE!!)

            } else {

            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    private fun checkConnectionType() {
        val cm = activity!!.getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
        if (cm != null && cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().getType() == ConnectivityManager.TYPE_ETHERNET)
        {
//          conn_type?.text=getString(R.string.ethernet)
//          connectedDevice?.setText("Connected")
            CONECTION_TYPE = "LAN"
            ethernetData()
            if (isConnected) {
                valueList.add(0, "Connected")
            } else {
                valueList.add(0, "Connected,no internet connection")
            }
            mAdapter?.updateMenu(valueList, this!!.CONECTION_TYPE!!)
            updateStatus(true, null, this!!.CONECTION_TYPE!!)
        } else {
            CONECTION_TYPE = "WIFI"
            conn_type?.setText(R.string.wifi)
            getCurrentSsid()
        }

    }

    fun getLocalIpAddress(): String? {
        try {
            val en = NetworkInterface.getNetworkInterfaces()

            while (en.hasMoreElements()) {
                val intf = en.nextElement()
                val enumIpAddr = intf.getInetAddresses()
                while (enumIpAddr.hasMoreElements()) {
                    val inetAddress = enumIpAddr.nextElement()
                    if (!inetAddress.isLoopbackAddress()) {
                        return inetAddress.getHostAddress().toString()
                    }
                }
            }
        } catch (ex: SocketException) {
            Log.e("SocketException", ex.toString())
        }

        return null
    }

    private val connectivityChangeReciver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val results = wifiManager.getScanResults()
            Log.e("size", "" + results.size)

//            Toast.makeText(activity, "Connectivity change Reciver!", Toast.LENGTH_SHORT).show()
//            val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
//            if( connectivityManager != null && connectivityManager.getActiveNetworkInfo()!=null && connectivityManager.getActiveNetworkInfo().getType() == ConnectivityManager.TYPE_ETHERNET) {
//                Log.i("NET-Ethernet", "Connected")
//            }else
//                Log.i("NET-Wifi", "Not Connected")

            checkConnectionType()
        }
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.connection -> {
                //Todo #check
                if (conn_type?.text?.equals(getString(R.string.wifi))!!) {
//                    val intent = Intent(context, WifiListActivity::class.java)
//                    intent.putExtra("title", "Wi-Fi")
//                    startActivity(intent)
                    addWifiListFragment(getString(R.string.wifi))
                }
            }

        }

    }

    override fun onFocusChange(p0: View?, p1: Boolean) {

    }


    override fun onDestroy() {
        super.onDestroy()
        activity?.unregisterReceiver(connectivityChangeReciver)
        activity?.unregisterReceiver(wifiReceiver)
        handler.removeCallbacks(runnableCode)
    }

    inner class SpaceItemDecoration internal constructor(private val space: Int) : RecyclerView.ItemDecoration() {

        override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView,
                                    state: RecyclerView.State) {
            outRect.top = space
        }
    }


    fun ethernetData() {
//       val interfaces: Enumeration<NetworkInterface> = NetworkInterface.getNetworkInterfaces()
//
//        while (interfaces.hasMoreElements())
//        {
//             val networkInterface :NetworkInterface = interfaces.nextElement()
//
//           val  inetAddresses:Enumeration<InetAddress>  = networkInterface.getInetAddresses()


        thread {
            val localhost = InetAddress.getLocalHost()
            Log.i("inetLAN", "" + localhost)
        }.start()


        Log.i("inetLAN", "")
//        }
    }

    fun getIpAddress(): String? {
        try {
            val en = NetworkInterface.getNetworkInterfaces()
            while (en.hasMoreElements()) {
                val intf = en.nextElement()
                val enumIpAddr = intf.inetAddresses
                while (enumIpAddr.hasMoreElements()) {
                    val inetAddress = enumIpAddr.nextElement()
                    if (!inetAddress.isLoopbackAddress && inetAddress is Inet4Address) {
                        val ipAddress = inetAddress.getHostAddress().toString()
                        Log.e("IP address", "" + ipAddress)
                        return ipAddress
                    }
                }
            }
        } catch (ex: SocketException) {
            Log.e("Socket exception in GetIP Address of Utilities", ex.toString())
        }

        return null
    }

//    private fun getSignalStrength(): String {
//        val signalLevels = activity.resources.getStringArray(R.array.wifi_signal_strength)
//        val strength = mConnectivityListener.getWifiSignalStrength(signalLevels.size)
//        return signalLevels[strength]
//    }

    private fun getSignalStrength(): String {
        val signalLevels = activity?.resources?.getStringArray(R.array.wifi_signal_strength)
        //    val wifiManager = context.getSystemService(Context.WIFI_SERVICE) as WifiManager
        val numberOfLevels = 4
        val wifiInfo = wifiManager?.connectionInfo
        val level = WifiManager.calculateSignalLevel(wifiInfo!!.rssi, numberOfLevels)
        return signalLevels?.get(level)!!

    }

    private fun getWifiMACAddress(): String {
        val wInfo: WifiInfo = wifiManager?.getConnectionInfo()!!
        val macAddress: String = wInfo.getMacAddress();
        return macAddress
    }

    fun addWifiListFragment(title: String)
    {
        if(activity!=null)
        {
            val header = activity?.findViewById<TextView>(R.id.headerText)
            header?.setText(title)
            val fm = activity?.supportFragmentManager
            val ft = fm?.beginTransaction()
            val llf = WifiFragment.newInstance()
            val bundle: Bundle = Bundle()
            bundle.putString("title", title)
            bundle.putString("param1", title)
            bundle.putParcelableArrayList("list", itemWifiScaned)
            llf.arguments = bundle
            ft?.replace(R.id.sub_details_container, llf, "WifiFragment")
            ft?.addToBackStack("WifiFragment")
            ft?.commit()

            val newBackStackLength: Int = fm?.getBackStackEntryCount()!!.plus(1);

            fm?.addOnBackStackChangedListener(object : FragmentManager.OnBackStackChangedListener {
                override fun onBackStackChanged() {
                    val nowCount = fm.getBackStackEntryCount()
                    if (newBackStackLength !== nowCount) {
                        if (!isStartNowClicked) {
                            // we don't really care if going back or forward. we already performed the logic here.
                            fm.removeOnBackStackChangedListener(this)

                            if (newBackStackLength > nowCount) { // user pressed back
                                //  fm.popBackStackImmediate()
                                header?.setText(activity?.intent?.getStringExtra("title"))
                            }
                        }
                    }
                }
            })
        }

    }

    fun addIpSettingsFragment(title: String) {
        val header = activity?.findViewById<TextView>(R.id.headerText)
        header?.setText(title)
        val fm = activity?.supportFragmentManager
        val ft = fm?.beginTransaction()
        val llf = IpSettingsFragment.newInstance();
        val bundle: Bundle = Bundle()
        bundle.putString("title", title)
        llf.arguments = bundle
        ft?.replace(R.id.sub_details_container, llf, "IpSettingsFragment")
        ft?.addToBackStack("IpSettingsFragment")
        ft?.commit()

        val newBackStackLength: Int = fm?.getBackStackEntryCount()!!.plus(1);

        fm?.addOnBackStackChangedListener(object : FragmentManager.OnBackStackChangedListener {
            override fun onBackStackChanged() {
                val nowCount = fm.getBackStackEntryCount()
                if (newBackStackLength !== nowCount) {
                    if (!DetailsFragment.isStartNowClicked) {
                        // we don't really care if going back or forward. we already performed the logic here.
                        fm.removeOnBackStackChangedListener(this)

                        if (newBackStackLength > nowCount) { // user pressed back
                            //  fm.popBackStackImmediate()
                            header?.setText(activity?.intent?.getStringExtra("title"))
                        }
                    }
                }
            }
        })
    }

    fun addProxeySettingFragment(title: String) {
        val header = activity?.findViewById<TextView>(R.id.headerText)
        header?.setText(title)
        val fm = activity?.supportFragmentManager
        val ft = fm?.beginTransaction()
        val llf = ProxeySettingsFragment.newInstance();
        val bundle: Bundle = Bundle()
        bundle.putString("title", title)
        llf.arguments = bundle
        ft?.replace(R.id.sub_details_container, llf, "ProxeySettingsFragment")
        ft?.addToBackStack("ProxeySettingsFragment")
        ft?.commit()

        val newBackStackLength: Int = fm?.getBackStackEntryCount()!!.plus(1);

        fm?.addOnBackStackChangedListener(object : FragmentManager.OnBackStackChangedListener {
            override fun onBackStackChanged() {
                val nowCount = fm.getBackStackEntryCount()
                if (newBackStackLength !== nowCount) {
                    if (!DetailsFragment.isStartNowClicked) {
                        // we don't really care if going back or forward. we already performed the logic here.
                        fm.removeOnBackStackChangedListener(this)

                        if (newBackStackLength > nowCount) { // user pressed back
                            //  fm.popBackStackImmediate()
                            header?.setText(activity?.intent?.getStringExtra("title"))
                        }
                    }
                }
            }
        })
    }

    val runnableCode = object : Runnable {
        override fun run() {
            CheckNetworkConnection(context, this@NetworkDetailsFragment).execute()
            handler.postDelayed(this, 5000)
        }
    }

    /*override fun onConnectionSuccess() {
        Log.d("NetworkDetail Fragment ", "onConnectionSuccess")
        isConnected = true
        isEthernetConnected()
        LessOptionAdapter.isConnected = "Connected"
//        Toast.makeText(activity, "onConnectionSuccess", Toast.LENGTH_LONG).show()
        //checkConnectionLAN(true)
    }


    override fun onConnectionFail(errorMsg: String) {
        Log.d("NetworkDetail Fragment ", "onConnectionFail")
        isConnected = false
        isEthernetConnected()
        LessOptionAdapter.isConnected = "Connected,no internet connection"
        //   Toast.makeText(activity, errorMsg, Toast.LENGTH_LONG).show()
        //checkConnectionLAN(false)

    }*/

    override fun onConnectionSuccess() {
        Log.d("NetworkDetail Fragment ", "onConnectionSuccess")
        if (isNetWorkConnected) {
            //  Toast.makeText(context, "isNetWorkConnected SUCCESS", Toast.LENGTH_SHORT).show()
            isConnected = true
            LessOptionAdapter.isConnected = "Connected"

            isEthernetConnected()
//            mAdapter!!.notifyDataSetChanged()
//            (recyclerView!!.itemAnimator as SimpleItemAnimator).supportsChangeAnimations = false
        }
        //Toast.makeText(activity, "onConnectionSuccess", Toast.LENGTH_LONG).show()

        isNetWorkConnected = false
    }


    override fun onConnectionFail(errorMsg: String) {
        Log.d("NetworkDetail Fragment ", "onConnectionFail")
        if (!isNetWorkConnected) {
            //  Toast.makeText(context, "isNetWorkConnected FAILED", Toast.LENGTH_SHORT).show()
            isConnected = false
            LessOptionAdapter.isConnected = "Connected,no internet connection"
            isEthernetConnected()
//            mAdapter!!.notifyDataSetChanged()
//            (recyclerView!!.itemAnimator as SimpleItemAnimator).supportsChangeAnimations = false

        }
        isNetWorkConnected = true

    }

}