package com.rjil.jiostbsetting.fragments;

import android.app.AlarmManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.XmlResourceParser;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.os.LocaleList;
import android.provider.Settings;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.rjil.jiostbsetting.DetailsActivity;
import com.rjil.jiostbsetting.R;
import com.rjil.jiostbsetting.adapters.TimeZoneAdapter;
import com.rjil.jiostbsetting.models.Lcodeandnative;
import com.rjil.jiostbsetting.models.TimeZoneInfo;
import com.rjil.jiostbsetting.tvrecyclerview.TvRecyclerView;

import org.xmlpull.v1.XmlPullParserException;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import static android.content.Context.INPUT_METHOD_SERVICE;

public class DisplayConfrugationFragment extends Fragment {

    private static String TAG = "DisplayConfrugationFragment";
    private static final String XMLTAG_TIMEZONE = "timezone";
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    public String mParam1;
    private TvRecyclerView recyclerView;
    ArrayList<String> optionList, valueList;
    //autonmatic date andtime
    private LinearLayout autoTimeAnDdateLayout;
    private RadioGroup AutoDTGroup, LanguageRadioGrp;

    private Calendar mDummyDate;
    private boolean mIsResumed;

    private IntentFilter mIntentFilter;
    private View root;
    // private ArrayAdapter<LocalePicker.LocaleInfo> mLocales;
    ArrayList<Lcodeandnative> listcodes = null;

    public DisplayConfrugationFragment() {
        // Required empty public constructor
    }

    //Date annd Time
    DatePicker picker;
    TimePicker pickerTime;
    //keyboard related
    private InputMethodManager mInputMan;
    private static final String INPUT_METHOD_SEPARATOR = ":";
    TextView currentKeyboard;


    // TODO: Rename and change types and number of parameters
    public static DisplayConfrugationFragment newInstance(String param1) {
        DisplayConfrugationFragment fragment = new DisplayConfrugationFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
       // args.putString(ARG_PARAM2, ConfriType);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            ((DetailsActivity)getActivity()).getHeader().setText(mParam1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_display_configure, container, false);

        recyclerView = (TvRecyclerView) root.findViewById(R.id.configure_recycler_view);
        AutoDTGroup = (RadioGroup) root.findViewById(R.id.autodt_grp);
        LanguageRadioGrp = (RadioGroup) root.findViewById(R.id.language_radio_grp);
        optionList = new ArrayList<>();
        autoTimeAnDdateLayout = (LinearLayout) root.findViewById(R.id.autotimeanddate);
        mDummyDate = Calendar.getInstance();
        mIntentFilter = new IntentFilter();
        mIntentFilter.addAction(Intent.ACTION_TIME_TICK);
        mIntentFilter.addAction(Intent.ACTION_TIME_CHANGED);
        mIntentFilter.addAction(Intent.ACTION_TIMEZONE_CHANGED);
        mIntentFilter.addAction(Intent.ACTION_DATE_CHANGED);
        if (mParam1.equals(getActivity().getString(R.string.auto_date_time))) {
            autoTimeAnDdateLayout.setVisibility(View.VISIBLE);
            if (getAutoDateTime().equals(getActivity().getString(R.string.use_network_prov_time))) {
                AutoDTGroup.check(R.id.use_wifi);
            } else {
                AutoDTGroup.check(R.id.off);
            }

            AutoDTGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    // This will get the radiobutton that has changed in its check state
                    RadioButton checkedRadioButton = (RadioButton) group.findViewById(checkedId);
                    // This puts the value (true/false) into the variable
                    boolean isChecked = checkedRadioButton.isChecked();
                    // If the radiobutton that has changed in check state is now checked...
                    if (checkedRadioButton.getText().equals("Off")) {
                        setAutoDateTime(false);
                    } else {
                        setAutoDateTime(true);
                    }
                }
            });

        } else if (mParam1.equals(getActivity().getString(R.string.set_time_zone))) {
            recyclerView.setVisibility(View.VISIBLE);
            setUpTimeZeneRecycleView();
        } else if (mParam1.equals(getActivity().getResources().getString(R.string.set_time))) {
            setSystemTime();
        } else if (mParam1.equals(getActivity().getResources().getString(R.string.set_date))) {
            setSystemDate();
        } else if (mParam1.equals(getActivity().getResources().getString(R.string.language))) {
            launguageSetting();
        }else if(mParam1.equals(getActivity().getResources().getString(R.string.keyboard))){
            keyboardsSetting();
        }
//        setUpRecycleView();

        // Inflate the layout for this fragment
        return root;
    }

    private void setSystemTime() {
        LinearLayout setTimeLayout = root.findViewById(R.id.setTime_layout);
        setTimeLayout.setVisibility(View.VISIBLE);
        pickerTime = (TimePicker) root.findViewById(R.id.timePicker);
        pickerTime.setFocusable(true);
        // picker.requestFocus();
    }

    private void setSystemDate() {
        LinearLayout setDateLayout = root.findViewById(R.id.setdate_layout);
        setDateLayout.setVisibility(View.VISIBLE);

        picker = (DatePicker) root.findViewById(R.id.datePicker);
        picker.setFocusable(true);
       //picker.requestFocus();
        // Button setDate= root.findViewById(R.id.btn_setDate);
//        picker.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                setDate(getActivity(), picker.getDayOfMonth(), (picker.getMonth() + 1), picker.getYear());
//            }
//        });

    }

    public void setSystemDateFromActivity(){
        setDate(getActivity(), picker.getDayOfMonth(), (picker.getMonth() + 1), picker.getYear());
    }

    private void setTime(Context context, int hourOfDay, int minute) {
        Calendar c = Calendar.getInstance();

        c.set(Calendar.HOUR_OF_DAY, hourOfDay);
        c.set(Calendar.MINUTE, minute);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        long when = c.getTimeInMillis();

        if (when / 1000 < Integer.MAX_VALUE) {
            ((AlarmManager) context.getSystemService(Context.ALARM_SERVICE)).setTime(when);
        }
    }


    private void launguageSetting() {
        getLocaleSettingInAndroid();

        LinearLayout languageSetLayout = root.findViewById(R.id.language_layout);
        languageSetLayout.setVisibility(View.VISIBLE);
//        if (getAutoDateTime().equals(getActivity().getString(R.string.in_english))) {
//            LanguageRadioGrp.check(R.id.use_wifi);
//        } else {
//            LanguageRadioGrp.check(R.id.off);
//        }

        LanguageRadioGrp.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // This will get the radiobutton that has changed in its check state
                RadioButton checkedRadioButton = (RadioButton) group.findViewById(checkedId);
                // This puts the value (true/false) into the variable
                boolean isChecked = checkedRadioButton.isChecked();
                // If the radiobutton that has changed in check state is now checked...
                if (checkedRadioButton.getText().equals(getActivity().getResources().getString(R.string.in_english))) {
                    String codes = listcodes.get(0).getCodes().toString();
                    String codeqian = codes.substring(0, 2);
                    String codehou = codes.substring(codes.indexOf("_") + 1);
                    Locale deLocale = new Locale(codeqian, codehou);
                    updateLanguage(deLocale);
                } else {
                    String codes = listcodes.get(1).getCodes().toString();
                    String codeqian = codes.substring(0, 2);
                    String codehou = codes.substring(codes.indexOf("_") + 1);
                    Locale deLocale = new Locale(codeqian, codehou);
                    updateLanguage(deLocale);
                }
            }
        });
       /* //from setting app
        final String[] specialLocaleCodes = getResources().getStringArray(
                com.android.internal.R.array.special_locale_codes);
        final String[] specialLocaleNames = getResources().getStringArray(
                com.android.internal.R.array.special_locale_names);


        mLocales = LocalePicker.constructAdapter(this);
        mLocales.sort(new LocaleComparator());
        final String[] localeNames = new String[mLocales.getCount()];
        Locale currentLocale;
        try {
            Class<?> activityManagerNative = Class.forName("android.app.ActivityManager");
            Method method = activityManagerNative.getDeclaredMethod("getConfiguration",null);

            currentLocale = activityManagerNative.c().getConfiguration().locale;
        } catch (RemoteException e) {
            currentLocale = null;
        }
        for (int i = 0; i < localeNames.length; i++) {
            Locale target = mLocales.getItem(i).getLocale();
            localeNames[i] = getDisplayName(target, specialLocaleCodes, specialLocaleNames);

            // if this locale's label has a country, use the shortened version
            // instead
            if (mLocales.getItem(i).getLabel().contains("(")) {
                String country = target.getCountry();
                if (!TextUtils.isEmpty(country)) {
                    localeNames[i] = localeNames[i] + " (" + target.getCountry() + ")";
                }
            }

            // For some reason locales are not always first letter cased, for example for
            // in the Spanish locale.
            if (localeNames[i].length() > 0) {
                localeNames[i] = localeNames[i].substring(0, 1).toUpperCase(currentLocale) +
                        localeNames[i].substring(1);
            }

            StringBuilder sb = new StringBuilder();
            sb.append(KEY_LOCALE).append(i);
            mActions.add(new Action.Builder()
                    .key(sb.toString())
                    .title(localeNames[i])
                    .checked(target.equals(currentLocale))
                    .build());
        }*/
        listcodes = new ArrayList<Lcodeandnative>();
        listcodes.add(new Lcodeandnative(1, "en_IN", "English(India)"));
        listcodes.add(new Lcodeandnative(2, "en_GB", "English(United Kingdom)"));


    }

    private void setUpTimeZeneRecycleView() {

        GridLayoutManager manager = new GridLayoutManager(getActivity(), 1);
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(manager);
        int itemSpace = getResources().getDimensionPixelSize(R.dimen.recyclerView_item_space);
        recyclerView.addItemDecoration(new SpaceItemDecoration(itemSpace));
        recyclerView.setSelectPadding(35, 34, 35, 34);
        TimeZoneAdapter timeZoneAdapter = new TimeZoneAdapter(getActivity(), getTimeZones(getActivity()));
        recyclerView.setAdapter(timeZoneAdapter);
        recyclerView.setOnItemStateListener(new TvRecyclerView.OnItemStateListener() {
            @Override
            public void onItemViewClick(View view, int position) {
                RadioButton  sel_Zone=(RadioButton) view.findViewById(R.id.select_zone);
                if(sel_Zone.isChecked()){
                    sel_Zone.setChecked(false);
                    //system level code to settimezone
                }else{
                    sel_Zone.setChecked(true);
                    //system level code to settimezone
                }
                setTimeZone(getTimeZones(getActivity()).get(position).tzId);
                //updateTimeAndDateStrings();
                getActivity().onBackPressed();

            }

            @Override
            public void onItemViewFocusChanged(boolean gainFocus, View view, int position) {

            }
        });

    }


    class SpaceItemDecoration extends RecyclerView.ItemDecoration {
        int space;

        public SpaceItemDecoration(int space) {
            this.space = space;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            outRect.top = space;
            super.getItemOffsets(outRect, view, parent, state);
        }
    }


    //Auto confrogution date and time
    private String getAutoDateTime() {
        try {
            int value = Settings.Global.getInt(getActivity().getContentResolver(), Settings.Global.AUTO_TIME);
            if (value == 1) {
                return getActivity().getString(R.string.use_network_prov_time);
            } else {
                return getActivity().getString(R.string.off);
            }
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
            return "";
        }
    }

    private void setAutoDateTime(boolean on) {
        Settings.Global.putInt(getActivity().getContentResolver(), Settings.Global.AUTO_TIME, on ? 1 : 0);
    }

    private ArrayList<TimeZoneInfo> getTimeZones(Context context) {
        ArrayList<TimeZoneInfo> timeZones = new ArrayList<TimeZoneInfo>();
        final long date = Calendar.getInstance().getTimeInMillis();
        try {
            XmlResourceParser xrp = context.getResources().getXml(R.xml.timezones);
            while (xrp.next() != XmlResourceParser.START_TAG)
                continue;
            xrp.next();
            while (xrp.getEventType() != XmlResourceParser.END_TAG) {
                while (xrp.getEventType() != XmlResourceParser.START_TAG &&
                        xrp.getEventType() != XmlResourceParser.END_DOCUMENT) {
                    xrp.next();
                }

                if (xrp.getEventType() == XmlResourceParser.END_DOCUMENT) {
                    break;
                }

                if (xrp.getName().equals(XMLTAG_TIMEZONE)) {
                    String id = xrp.getAttributeValue(0);
                    String displayName = xrp.nextText();
                    TimeZone tz = TimeZone.getTimeZone(id);
                    long offset;
                    if (tz != null) {
                        offset = tz.getOffset(date);
                        timeZones.add(new TimeZoneInfo(id, displayName, offset));
                    } else {
                        continue;
                    }
                }
                while (xrp.getEventType() != XmlResourceParser.END_TAG) {
                    xrp.next();
                }
                xrp.next();
            }
            xrp.close();
        } catch (XmlPullParserException xppe) {
            Log.e(TAG, "Ill-formatted timezones.xml file");
        } catch (java.io.IOException ioe) {
            Log.e(TAG, "Unable to read timezones.xml file");
        }
        return timeZones;
    }


//    private void setSampleDate() {
//        Calendar now = Calendar.getInstance();
//        mDummyDate.setTimeZone(now.getTimeZone());
//        // We use December 31st because it's unambiguous when demonstrating the date format.
//        // We use 15:14 so we can demonstrate the 12/24 hour options.
//        mDummyDate.set(now.get(Calendar.YEAR), 11, 31, 15, 14, 0);
//    }

    private BroadcastReceiver mIntentReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (mIsResumed) {
//                switch ((ActionType) mState) {
//                    case DATE_TIME_OVERVIEW:
//                    case DATE:
//                    case TIME:
//                        updateTimeAndDateDisplay();
//                }
                String message = "";
                switch (intent.getAction()) {
                    case Intent.ACTION_DATE_CHANGED:
                        message = "Date Changed";
                        break;
                    case Intent.ACTION_TIME_CHANGED:
                        message = "Time changed";
                        break;


                }
                Toast.makeText(context.getApplicationContext(), message, Toast.LENGTH_SHORT).show();
            }

        }

    };


    static void setDate(Context context, int year, int month, int day) {
        Calendar c = Calendar.getInstance();

        c.set(Calendar.YEAR, year);
        c.set(Calendar.MONTH, month);
        c.set(Calendar.DAY_OF_MONTH, day);
        long when = c.getTimeInMillis();

        if (when / 1000 < Integer.MAX_VALUE) {
            ((AlarmManager) context.getSystemService(Context.ALARM_SERVICE)).setTime(when);
        }
    }

//    //for language setting related code
//    private static class LocaleComparator implements Comparator<LocalePicker.LocaleInfo> {
//        @Override
//        public int compare(LocalePicker.LocaleInfo l, LocalePicker.LocaleInfo r) {
//            Locale lhs = l.getLocale();
//            Locale rhs = r.getLocale();
//
//            String lhsCountry = "";
//            String rhsCountry = "";
//
//            try {
//                lhsCountry = lhs.getISO3Country();
//            } catch (MissingResourceException e) {
//                Log.e(TAG, "LocaleComparator cuaught exception, country set to empty.");
//            }
//
//            try {
//                rhsCountry = rhs.getISO3Country();
//            } catch (MissingResourceException e) {
//                Log.e(TAG, "LocaleComparator cuaught exception, country set to empty.");
//            }
//
//            String lhsLang = "";
//            String rhsLang = "";
//
//            try {
//                lhsLang = lhs.getISO3Language();
//            } catch (MissingResourceException e) {
//                Log.e(TAG, "LocaleComparator cuaught exception, language set to empty.");
//            }
//
//            try {
//                rhsLang = rhs.getISO3Language();
//            } catch (MissingResourceException e) {
//                Log.e(TAG, "LocaleComparator cuaught exception, language set to empty.");
//            }
//
//            // if they're the same locale, return 0
//            if (lhsCountry.equals(rhsCountry) && lhsLang.equals(rhsLang)) {
//                return 0;
//            }
//
//            // prioritize US over other countries
//            if ("USA".equals(lhsCountry)) {
//                // if right hand side is not also USA, left hand side is first
//                if (!"USA".equals(rhsCountry)) {
//                    return -1;
//                } else {
//                    // if one of the languages is english we want to prioritize
//                    // it, otherwise we don't care, just alphabetize
//                    if ("ENG".equals(lhsLang) && "ENG".equals(rhsLang)) {
//                        return 0;
//                    } else {
//                        return "ENG".equals(lhsLang) ? -1 : 1;
//                    }
//                }
//            } else if ("USA".equals(rhsCountry)) {
//                // right-hand side is the US and the other isn't, return greater than 1
//                return 1;
//            } else {
//                // neither side is the US, sort based on display language name
//                // then country name
//                int langEquiv = lhs.getDisplayLanguage(lhs).compareTo(rhs.getDisplayLanguage(rhs));
//                if (langEquiv == 0) {
//                    return lhs.getDisplayCountry(lhs).compareTo(rhs.getDisplayCountry(rhs));
//                } else {
//                    return langEquiv;
//                }
//            }
//        }
//    }
//    private static String getDisplayName(
//            Locale l, String[] specialLocaleCodes, String[] specialLocaleNames) {
//        String code = l.toString();
//
//        for (int i = 0; i < specialLocaleCodes.length; i++) {
//            if (specialLocaleCodes[i].equals(code)) {
//                return specialLocaleNames[i];
//            }
//        }
//
//        return l.getDisplayName(l);
//    }

//    private void setLanguagePreference(int offset) {
//        LocalePicker.updateLocale(mLocales.getItem(offset).getLocale());
//    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void getLocaleSettingInAndroid() {
        Locale currentLocale = null;
        try {
            Object objIActMagLoc = null;
            Class clzIActMag = Class.forName("android.app.IActivityManager");
            Class clzActMagNative = Class.forName("android.app.ActivityManagerNative");
            Method mtdActMagNative$getDefault = clzActMagNative.getDeclaredMethod("getDefault");

            objIActMagLoc = mtdActMagNative$getDefault.invoke(clzActMagNative);

            Method mtdIActMag$getConfiguration = clzIActMag.getDeclaredMethod("getConfiguration");
            mtdIActMag$getConfiguration.setAccessible(true);

            Class configClass = Class.forName("android.content.res.Configuration");

            Method localeList = configClass.getDeclaredMethod("getLocales", LocaleList.class);

            currentLocale = ((LocaleList) localeList.invoke(configClass)).get(0);
            Log.e(TAG, "locale : " + currentLocale);
        } catch (Exception e) {
            Log.e(TAG, "Could not retrieve locale", e);
        }


    }

    private void updateLanguage(Locale locale) {
        try {
            // Utils.shadowScreen(sw, null);
            Object objIActMag = null;
            Class clzIActMag = Class.forName("android.app.IActivityManager");
            Class clzActMagNative = Class.forName("android.app.ActivityManagerNative");
            Method mtdActMagNative$getDefault = clzActMagNative.getDeclaredMethod("getDefault");
            objIActMag = mtdActMagNative$getDefault.invoke(clzActMagNative);
            Method mtdIActMag$getConfiguration = clzIActMag.getDeclaredMethod("getConfiguration");
            // 2017/04/20
            mtdIActMag$getConfiguration.setAccessible(true);

            Configuration config = (Configuration) mtdIActMag$getConfiguration.invoke(objIActMag);
            // config.userSetLocale = true;
            Class configClass = config.getClass();
            java.lang.reflect.Field f = configClass.getField("userSetLocale");
            f.setBoolean(config, true);

            config.locale = locale;
            Class[] clzParams = {Configuration.class};
            Method mtdIActMag$updateConfiguration = clzIActMag.getDeclaredMethod("updateConfiguration", clzParams);
            // 2017/04/20
            mtdIActMag$updateConfiguration.setAccessible(true);

            mtdIActMag$updateConfiguration.invoke(objIActMag, config);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getTime24Hour() {
        return Settings.System.getString(getActivity().getContentResolver(),
                Settings.System.TIME_12_24);
    }

    public void onEnterKeyPressed() {
        if (mParam1.equals(getActivity().getResources().getString(R.string.set_date))) {
            setDate(getActivity(), picker.getDayOfMonth(), (picker.getMonth() + 1), picker.getYear());
        } else if (mParam1.equals(getActivity().getResources().getString(R.string.set_time))) {
            setTime(getActivity(), pickerTime.getHour(), pickerTime.getMinute());
        }
       getActivity().onBackPressed();
    }

    private void keyboardsSetting() {
        FrameLayout keyboardSetLayout=(FrameLayout) root.findViewById(R.id.keyboard_set_Layout);
        keyboardSetLayout.setVisibility(View.VISIBLE);
        currentKeyboard=(TextView) root.findViewById(R.id.current_key_value);
        mInputMan = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
        // enable all available input methods
        enableAllInputMethods();
    }

    //Keyboard settings
    private void enableAllInputMethods() {
        String keyBoardName = null;
        final PackageManager packageManager = getActivity().getPackageManager();
        final String defaultId = Settings.Secure.getString(getActivity().getContentResolver(),
                Settings.Secure.DEFAULT_INPUT_METHOD);
        List<InputMethodInfo> allInputMethodInfos =
                new ArrayList<InputMethodInfo>(mInputMan.getInputMethodList());
        List<InputMethodInfo> enabledInputMethodInfos = getEnabledSystemInputMethodList();

        for (final InputMethodInfo info : enabledInputMethodInfos) {
             keyBoardName = (String) info.loadLabel(packageManager);
            final String id = info.getId();
            //  values.add(id);
            if (TextUtils.equals(id, defaultId)) {
               break;
            }
        }
        currentKeyboard.setText(keyBoardName);



       Log.e(TAG, allInputMethodInfos.get(0).getServiceName());
        Log.e(TAG, allInputMethodInfos.get(0).getId());
        boolean needAppendSeparator = false;
        StringBuilder builder = new StringBuilder();
        for (InputMethodInfo imi : allInputMethodInfos) {
            if (needAppendSeparator) {
                builder.append(INPUT_METHOD_SEPARATOR);
            } else {
                needAppendSeparator = true;
            }
            builder.append(imi.getId());
        }
//        Settings.Secure.putString(getActivity().getContentResolver(),
//                Settings.Secure.ENABLED_INPUT_METHODS, builder.toString());
    }

    private Intent getInputMethodSettingsIntent(InputMethodInfo imi) {
        final Intent intent;
        final String settingsActivity = imi.getSettingsActivity();
        if (!TextUtils.isEmpty(settingsActivity)) {
            intent = new Intent(Intent.ACTION_MAIN);
            intent.setClassName(imi.getPackageName(), settingsActivity);
        } else {
            intent = null;
        }
        return intent;
    }

    private List<InputMethodInfo> getEnabledSystemInputMethodList() {
        List<InputMethodInfo> enabledInputMethodInfos =
                new ArrayList<InputMethodInfo>(mInputMan.getEnabledInputMethodList());
        // Filter auxiliary keyboards out
//        for (Iterator<InputMethodInfo> it = enabledInputMethodInfos.iterator(); it.hasNext(); ) {
//            if (it.next().isAuxiliaryIme()) {
//                it.remove();
//            }
//        }
        return enabledInputMethodInfos;
    }


//    private void setInputMethod(String imid) {
//            if (imid == null) {
//                throw new IllegalArgumentException("Null ID");
//            }
//
//            int userId;
//            try {
//                userId = ActivityManagerNative.getDefault().getCurrentUser().id;
//                Settings.Secure.putStringForUser(getActivity().getContentResolver(),
//                        Settings.Secure.DEFAULT_INPUT_METHOD, imid, userId);
//
//                if (ActivityManagerNative.isSystemReady()) {
//                    Intent intent = new Intent(Intent.ACTION_INPUT_METHOD_CHANGED);
//                    intent.addFlags(Intent.FLAG_RECEIVER_REPLACE_PENDING);
//                    intent.putExtra("input_method_id", imid);
//                    getActivity().sendBroadcastAsUser(intent, UserHandle.CURRENT);
//                }
//            } catch (RemoteException e) {
//                Log.d(TAG, "set default input method remote exception");
//            }
//}

// Updates the member strings to reflect the current date and time.
//private void updateTimeAndDateStrings() {
//    final Calendar now = Calendar.getInstance();
//    java.text.DateFormat dateFormat = DateFormat.getDateFormat(getActivity());
//    mNowDate = dateFormat.format(now.getTime());
//    java.text.DateFormat timeFormat = DateFormat.getTimeFormat(getActivity());
//    mNowTime = timeFormat.format(now.getTime());
//}
private void setSampleDate() {
    Calendar now = Calendar.getInstance();
    mDummyDate.setTimeZone(now.getTimeZone());
    // We use December 31st because it's unambiguous when demonstrating the date format.
    // We use 15:14 so we can demonstrate the 12/24 hour options.
    mDummyDate.set(now.get(Calendar.YEAR), 11, 31, 15, 14, 0);
}
private void setTimeZone(String tzId) {
    // Update the system timezone value
    final AlarmManager alarm = (AlarmManager) getActivity().getSystemService(Context.ALARM_SERVICE);
    alarm.setTimeZone(tzId);
    setSampleDate();
}

}



