package com.rjil.jiostbsetting

import android.content.Intent
import android.content.pm.IPackageDataObserver
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.Button
import android.widget.TextView
import com.rjil.jiostbsetting.utils.Constant
import com.rjil.jiostbsetting.utils.SharedPreferance
import java.io.File
import java.lang.reflect.InvocationTargetException
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.Fragment


class AlertFragment : Fragment() {

    private var packageName: String? = null
    private val REQUEST_UNINSTALL: Int = 0
    var btnReset: Button? = null
    var btnCancel: Button? = null
    //    var alertHeader: TextView? = null
    var tv_lbl: TextView? = null

    var pName = ArrayList<String>()

    companion object {
        fun newInstance(): AlertFragment {
            return AlertFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.alert_fragment, container, false)
        val context = activity?.applicationContext
        btnReset = view.findViewById(R.id.btnReset) as Button?
        btnCancel = view.findViewById(R.id.btnCancel) as Button?
        tv_lbl = view.findViewById(R.id.tv_lbl) as TextView?

//        alertHeader = view.findViewById(R.id.alertHeader) as TextView
        var strTitle: String = this.arguments!!.getString("title")
        packageName = this.arguments!!.getString("pkgname")
//        alertHeader?.setText("" + strTitle)
        btnReset?.setText("" + strTitle)

        val activity = this.activity
        if (activity is DetailsActivity) {
            activity.hideImageHolder()
        }

        if (strTitle.equals("Reset")) {
            tv_lbl?.text = "Select Reset to restore your JioGigaTV to factory settings and erase all settings and information."

        } else if (strTitle.equals("Restart")) {
            tv_lbl?.text = "Restart now?"

        } else if (strTitle.equals("Uninstall")) {
            tv_lbl?.text = "Select uninstall to remove this application from the JioGigaTv."

        } else if (strTitle.equals("Force stop")) {
//            tv_lbl?.text = "Select forced stop to stop this application from the JioGigaTv."
            tv_lbl?.text = "If you force stop an app, it may misbehave."

        } else if (strTitle.equals("Clear Data")) {
//            tv_lbl?.text = "Select clear data to clear all the history from this application."
            tv_lbl?.text = "All the app's data will be deleted permanently.\nThis includes all files, settings, accounts,\n databases, etc."

        } else if (strTitle.equals("Clear Cache")) {
            tv_lbl?.text = "Select clear cache to clear cache from this application."

        } else if (strTitle.equals("Uninstall Updates")) {
            tv_lbl?.text = "Select uninstall updates to move the application in the previous version from the JioGigaTv."

        } else if (strTitle.equals("Disable Application")) {
            tv_lbl?.text = "Do you want to disable this app?"

        } else if (strTitle.equals("Enable Application")) {
            tv_lbl?.text = "Do you want to enable this app?"

        }


        btnCancel?.setOnFocusChangeListener { view: View, b: Boolean ->
            if (b) {
                btnCancel!!.background = resources.getDrawable(R.drawable.cancel_shape_select)
                val anim = AnimationUtils.loadAnimation(context, R.anim.anim_scale_big)
                btnCancel?.startAnimation(anim)
                anim.fillAfter = true

            } else {
                btnCancel?.background = resources.getDrawable(R.drawable.rv_focused_bg)
                val anim = AnimationUtils.loadAnimation(context, R.anim.anim_scale_small)
                btnCancel?.startAnimation(anim)
                anim.fillAfter = true
            }
        }

        btnReset?.setOnFocusChangeListener { view: View, b: Boolean ->
            if (b) {
                btnReset!!.background = resources.getDrawable(R.drawable.reset_shape)
                val anim = AnimationUtils.loadAnimation(context, R.anim.anim_scale_big)
                btnReset?.startAnimation(anim)
                anim.fillAfter = true

            } else {
                btnReset?.background = resources.getDrawable(R.drawable.reset_shape_unselect)
                val anim = AnimationUtils.loadAnimation(context, R.anim.anim_scale_small)
                btnReset?.startAnimation(anim)
                anim.fillAfter = true
            }
        }

        btnCancel?.setOnClickListener {
            Log.e("Action", "Cancel")
            fragmentManager!!.popBackStack()
//            finish()

        }

        btnReset?.setOnClickListener {
            Log.e("Action", "Reset")
            Log.e("package:@@@@@: ", "" + packageName)

            if (strTitle.equals("Reset")) {
                Constant.factoryReset(context!!)

            } else if (strTitle.equals("Restart")) {
                Constant.rebootBOX(context!!)

            } else if (strTitle.equals("Uninstall")) {
//                var packagename: String = activity!!.intent.getStringExtra("packagename")
                val intent = Intent(Intent.ACTION_DELETE);
                intent.setData(Uri.parse("package:$packageName"));
                startActivity(intent)

                SharedPreferance.setUninstall(context!!, true)
                fragmentManager!!.popBackStack()

            } else if (strTitle.equals("Force stop")) {
//                var packagename: String = activity!!.intent.getStringExtra("packagename")
                var packagename: String = this.arguments!!.getString("pkgname")
                Constant.forceStopPackage(context!!, packagename)

//                val appSharedPrefs = PreferenceManager.getDefaultSharedPreferences(applicationContext!!)
//                val gson = Gson()
//                val json = appSharedPrefs.getString("packageName", "")
//                val type = object : TypeToken<List<String>>() {
//                }.type
//                if (json != null && !json.equals(""))
//                    pName = gson.fromJson(json, type)
//
//
//                pName.add(packagename);
//                val prefsEditor = appSharedPrefs.edit()
//                var pList = gson.toJson(pName)
//                prefsEditor.putString("packageName", pList)
//                prefsEditor.commit()

//                SharedPreferance.setForcePackage(applicationContext, packagename)
//                SharedPreferance.setForceFlag(context!!, true)
//                finish()
                fragmentManager!!.popBackStack()

            } else if (strTitle.equals("Clear Data")) {
//                var packagename: String = activity!!.intent.getStringExtra("packagename")
                Log.e("@@ PAckage Name: ", packageName)
                var comnd = "pm clear $packageName"
                val proc: Process? = Runtime.getRuntime().exec(comnd)
                proc!!.waitFor()
//                SharedPreferance.setUninstall(context!!, true)
//                finish()
                fragmentManager!!.popBackStack()

            } else if (strTitle.equals("Clear Cache")) {
                try {
                    val m = activity!!.packageManager
//                    var packagename: String = activity!!.intent.getStringExtra("packagename")
                    val p = m.getPackageInfo(packageName, 0)
                    var s: String = p.applicationInfo.dataDir
                    val file = File("$s/cache/")
                    println("SYSTEM PATH $file")
                    clearCache(m)
                } catch (e: Exception) {
                    e.printStackTrace()
                }


                fragmentManager!!.popBackStack()

            } else if (strTitle.equals("Uninstall Updates")) {
                var packagename: String = this.arguments!!.getString("pkgname")
                Constant.uninstallUpdates(context!!, packagename)
                SharedPreferance.setUninstall(context!!, true)
                fragmentManager!!.popBackStack()

            } else if (strTitle.equals("Enable Application")) {
                var packagename: String = this.arguments!!.getString("pkgname")
                Constant.EnableApp(activity!!, packagename)
                SharedPreferance.setUninstall(context!!, true)

                fragmentManager!!.popBackStack()

            } else if (strTitle.equals("Disable Application")) {
//                var packagename: String = activity!!.intent.getStringExtra("packagename")
                var packagename: String = this.arguments!!.getString("pkgname")
                Constant.DisableApp(activity!!, packagename)
                SharedPreferance.setUninstall(context!!, true)
//                finish()
                fragmentManager!!.popBackStack()
            }


        }

        return view
    }

    private val CACHE_APP = java.lang.Long.MAX_VALUE
    private var mClearCacheObserver: CachePackageDataObserver? = null

    fun clearCache(mPM: PackageManager) {

        if (mClearCacheObserver == null) {
            mClearCacheObserver = CachePackageDataObserver()
        }

        val classes = arrayOf(java.lang.Long.TYPE, IPackageDataObserver::class.java)
        val localLong = java.lang.Long.valueOf(CACHE_APP)
        try {
            val localMethod = mPM.javaClass.getMethod("freeStorageAndNotify", *classes)
            try {
                localMethod.invoke(mPM, localLong, mClearCacheObserver)
            } catch (e: IllegalArgumentException) {
                e.printStackTrace()
            } catch (e: IllegalAccessException) {
                e.printStackTrace()
            } catch (e: InvocationTargetException) {
                e.printStackTrace()
            }

        } catch (e1: NoSuchMethodException) {
            e1.printStackTrace()
        }

    }

    private inner class CachePackageDataObserver : IPackageDataObserver.Stub() {
        override fun onRemoveCompleted(packageName: String, succeeded: Boolean) {

        }
    }

    override fun onDestroyView() {
        val activity = this.activity
        if (activity is DetailsActivity) {
            activity.showImageHolder()
        }
        super.onDestroyView()
    }
}
