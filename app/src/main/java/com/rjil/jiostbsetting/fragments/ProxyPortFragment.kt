package com.rjil.jiostbsetting.fragments


import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import com.rjil.jiostbsetting.DetailsActivity
import com.rjil.jiostbsetting.R
import com.rjil.jiostbsetting.utils.Constant
import com.rjil.jiostbsetting.utils.Utils
import kotlinx.android.synthetic.main.fragment_proxy_port.view.*

class ProxyPortFragment : Fragment() {

    companion object {
        fun newInstance(): ProxyPortFragment {
            return ProxyPortFragment()
        }
    }

    var proxy_port: EditText? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.fragment_proxy_port, container, false)
        proxy_port = view.proxy_port
        val activity = this.activity
        if (activity is DetailsActivity) {
            activity.hideImageHolder()
        }
        Utils.showKeyboard(activity!!,proxy_port)

        proxy_port!!.setOnKeyListener { _, keyCode, keyEvent ->
            if (keyEvent.action == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                addBypassProxyFragment("Proxy settings", proxy_port?.text.toString())

                return@setOnKeyListener true
            }
            return@setOnKeyListener false
        }
        return view
    }

    fun addBypassProxyFragment(title: String, proxyPort: String) {

        val header = activity?.findViewById<TextView>(R.id.headerText)
        header?.setText(title)
        val fm = activity?.supportFragmentManager
        val ft = fm?.beginTransaction()
        val llf = BypassProxyFragment.newInstance()
        val bundle: Bundle = Bundle()
        bundle.putString("title", title)
        bundle.putString("proxeyHostName", arguments?.getString("proxeyHostName"))
        bundle.putString("proxyPort", proxyPort)
        llf.arguments = bundle
        ft?.replace(R.id.sub_details_container, llf, "BypassProxyFragment")
        ft?.addToBackStack("BypassProxyFragment")
        ft?.commit()

        val newBackStackLength: Int = fm?.getBackStackEntryCount()!!.plus(1);

        fm?.addOnBackStackChangedListener(object : FragmentManager.OnBackStackChangedListener {
            override fun onBackStackChanged() {
                val nowCount = fm.getBackStackEntryCount()
                if (newBackStackLength !== nowCount) {
                    if (!DetailsFragment.isStartNowClicked) {
                        // we don't really care if going back or forward. we already performed the logic here.
                        fm.removeOnBackStackChangedListener(this)

                        if (newBackStackLength > nowCount) { // user pressed back
                            //  fm.popBackStackImmediate()
                            header?.setText(arguments?.getString("title"))
                        }
                    }
                }
            }
        })
    }

}
