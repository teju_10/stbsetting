package com.rjil.jiostbsetting.main;

import android.content.Context;
import android.graphics.drawable.Animatable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.DrawableRes;
import androidx.annotation.StringRes;

import com.rjil.jiostbsetting.R;

/**
 * Custom progress dialog
 *
 * @author Dragan Krnjaic
 */
public class ProgressDialog extends RelativeLayout {

    /**
     * Dialog title text view
     */
    private TextView tvDialogTitle;

    /**
     * Dialog message text view
     */
    private TextView tvDialogMessage;

    /**
     * Icon
     */
    private ImageView ivDialogIcon;

    /**
     * Constructor
     *
     * @param context context
     */
    public ProgressDialog(Context context) {
        super(context);
        init();
    }

    /**
     * Constructor
     *
     * @param context context
     * @param attrs   attr set
     */
    public ProgressDialog(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    /**
     * Constructor
     *
     * @param context      context
     * @param attrs        attr set
     * @param defStyleAttr style attr
     */
    public ProgressDialog(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    /**
     * Init custom dialog view
     */
    private void init() {
        //Inflate layout
        inflate(getContext(), R.layout.progress_dialog, this);
        tvDialogTitle = findViewById(R.id.dialog_title);
        tvDialogMessage = findViewById(R.id.dialog_message);
        ivDialogIcon = findViewById(R.id.dialog_icon);
        ((Animatable) ((ImageView) findViewById(R.id.horizontal_progress)).getDrawable()).start();
    }

    /**
     * Show progress dialog
     */
    public void showProgressDialog() {
        setVisibility(VISIBLE);
    }

    /**
     * Hide progress dialog
     */
    public void hideProgressDialog() {
        setVisibility(GONE);
    }

    /**
     * Set dialog title
     *
     * @param dialogTitleLabel dialog title label
     */
    public void setDialogTitle(String dialogTitleLabel) {
        tvDialogTitle.setText(dialogTitleLabel);
    }

    /**
     * Set dialog title
     *
     * @param resource  string resource id
     */
    public void setDialogTitle(@StringRes int resource) {
        tvDialogTitle.setText(resource);
    }

    /**
     * Set dialog message text
     *
     * @param text  message text
     */
    public void setDialogMessage(String text) {
        tvDialogMessage.setVisibility(View.VISIBLE);
        tvDialogMessage.setText(text);
    }

    /**
     * Set icon resource id
     *
     * @param resource  drawable resource id
     */
    public void setDialogIcon(@DrawableRes int resource) {
        ivDialogIcon.setVisibility(View.VISIBLE);
        ivDialogIcon.setImageResource(resource);
    }

    public void hideWps() {
        ivDialogIcon.setVisibility(View.INVISIBLE);
        tvDialogMessage.setVisibility(View.INVISIBLE);
    }

}
