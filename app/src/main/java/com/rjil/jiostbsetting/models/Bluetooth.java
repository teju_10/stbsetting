package com.rjil.jiostbsetting.models;

public class Bluetooth {

    public String value;

    public Bluetooth(boolean isConnected) {
        this.value = value;
        this.isConnected = isConnected;
    }

    public boolean isConnected;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public boolean isConnected() {
        return isConnected;
    }

    public void setConnected(boolean connected) {
        isConnected = connected;
    }
}
