package com.rjil.jiostbsetting.models;

public class TimeZoneInfo implements Comparable<TimeZoneInfo> {
    public String tzId;
    public String tzName;
    public long tzOffset;

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public boolean isSelected;


    public TimeZoneInfo(String id, String name, long offset) {
        tzId = id;
        tzName = name;
        tzOffset = offset;
    }

    @Override
    public int compareTo(TimeZoneInfo another) {
        return (int) (tzOffset - another.tzOffset);
    }
}
