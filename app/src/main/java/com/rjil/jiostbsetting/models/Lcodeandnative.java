package com.rjil.jiostbsetting.models;

public class Lcodeandnative {

    private int codesid;
    private String codes;
    private String language;

    public Lcodeandnative() {
        super();
    }

    public Lcodeandnative(int codesid, String codes, String language) {
        super();
        this.codesid = codesid;
        this.codes = codes;
        this.language = language;
    }

    public int getCodesid() {
        return codesid;
    }

    public void setCodesid(int codesid) {
        this.codesid = codesid;
    }

    public String getCodes() {
        return codes;
    }

    public void setCodes(String codes) {
        this.codes = codes;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    @Override
    public String toString() {
        // TODO Auto-generated method stub
        return "codesid==" + codesid + "codes==" + codes + "language==" + language;
    }
}