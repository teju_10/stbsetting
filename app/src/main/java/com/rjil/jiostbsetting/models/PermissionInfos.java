package com.rjil.jiostbsetting.models;

public class PermissionInfos {
    public String groupName;
    public String permissionName;
    public Boolean isPGranted;

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getPermissionName() {
        return permissionName;
    }

    public void setPermissionName(String permissionName) {
        this.permissionName = permissionName;
    }

    public Boolean getPGranted() {
        return isPGranted;
    }

    public void setPGranted(Boolean granted) {
        isPGranted = granted;
    }
}
