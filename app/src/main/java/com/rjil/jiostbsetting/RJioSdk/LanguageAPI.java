package com.rjil.jiostbsetting.RJioSdk;

import java.util.ArrayList;

/**
 * Language API
 *
 * @author Dejan Nadj
 */
public interface LanguageAPI<T extends Language> {

    /**
     * Get language count
     *
     * @param receiver language count receiver
     */
    void getLanguageCount(AsyncDataReceiver<Integer> receiver);

    /**
     * Get language by id
     *
     * @param id       language id
     * @param receiver language receiver
     */
    void getLanguage(String id, AsyncDataReceiver<T> receiver);

    /**
     * Get language by index
     *
     * @param index    language index
     * @param receiver language receiver
     */
    void getLanguage(int index, AsyncDataReceiver<T> receiver);

    /**
     * Get language list
     *
     * @param receiver  language list receiver
     */
    void getLanguageList(AsyncDataReceiver<ArrayList<T>> receiver);

    /**
     * Get current language
     *
     * @param receiver current language receiver
     */
    void getCurrentLanguage(AsyncDataReceiver<T> receiver);

    /**
     * Set language
     * <p>
     * Update language resource
     *
     * @param language new language
     * @param receiver receiver
     */
    void setLanguage(T language);
}
