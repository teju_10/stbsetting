package com.rjil.jiostbsetting.RJioSdk;

/**
 * Async data receiver
 *
 * @author Veljko Ilkic
 */
public interface AsyncDataReceiver<T> {

    /**
     * On success
     *
     * @param data data
     */
    void onSuccess(T data);

    /**
     * On failed
     */
    void onFailed();
}
