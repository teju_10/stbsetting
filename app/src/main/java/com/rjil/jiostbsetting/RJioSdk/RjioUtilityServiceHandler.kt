package com.rjil.jiostbsetting.RJioSdk

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.net.wifi.ScanResult
import android.net.wifi.WifiInfo
import android.os.IBinder
import android.os.RemoteException

import com.iwedia.utility.IUtilityService
import com.iwedia.utility.bluetooth.IBluetoothManager
import com.iwedia.utility.language.ILanguageManager
import com.iwedia.utility.language.Language
import com.iwedia.utility.network.INetworkManager
import com.iwedia.utility.power.ILauncherPowerManager
import com.iwedia.utility.settings.ISettingsManager

import java.util.ArrayList

/**
 * Rjio utility service handler
 *
 * @author Dejan Nadj
 */
internal class RjioUtilityServiceHandler
/**
 * Constructor
 */
private constructor() {

    /**
     * Application context
     */
    private var context: Context? = null
    var serviceConnection: ServiceConnection? = null
    /**
     * Utility service
     */
    private var utilityService: IUtilityService? = null

    /**
     * Network manager
     */
    private val networkManager: INetworkManager? = null

    /**
     * Settings manager
     */
    private val settingsManager: ISettingsManager? = null

    /**
     * Power manager
     */
    private val launcherPowerManager: ILauncherPowerManager? = null

    /**
     * Bluetooth manager
     */
    private val bluetoothManager: IBluetoothManager? = null

    /**
     * Location manager
     */
    private var languageManager: ILanguageManager? = null

    private var languageList: List<Language> = ArrayList()


    /**
     * Is Wifi network duplicated in list
     *
     * @param Ssid
     * @param wifiNetworks
     * @return
     */


    /**
     * Is device interactive
     *
     * @return  true if device is interactive otherwise false
     */
    val isInteractive: Boolean
        get() {
            val isInteractive: Boolean
            try {
                isInteractive = launcherPowerManager!!.isInteractive
                return isInteractive
            } catch (e: RemoteException) {
                e.printStackTrace()
                return false
            }

        }

    val currentLanguage: String?
        get() {
            if (utilityService == null || languageManager == null) {
                return null
            }
            try {
                return languageManager!!.currentLanguage
            } catch (e: RemoteException) {
                e.printStackTrace()
            }

            return null
        }

    /**
     * Connect to the utility service
     *
     * @param context   application context
     */
    fun connect(context: Context) {
        this.context = context

        serviceConnection = object : ServiceConnection {
            override fun onServiceConnected(componentName: ComponentName, iBinder: IBinder) {

                utilityService = IUtilityService.Stub.asInterface(iBinder)
                try {
                    languageManager = utilityService!!.languageManager

                } catch (e: RemoteException) {
                    e.printStackTrace()
                }

            }

            override fun onServiceDisconnected(componentName: ComponentName) {
                utilityService = null
            }
        }

        val intent = Intent()
        //        intent.setPackage(SERVICE_PACKAGE_NAME);
        intent.component = ComponentName(SERVICE_PACKAGE_NAME, SERVICE_CLASS_NAME)
        context.bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE)
    }

    fun unBindService(context: Context) {
        this.context = context
        context.unbindService(serviceConnection)
    }

    private fun convertMaskToNetworkPrefix(mask: String): Int {

        val arr = mask.split("\\.".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        var bitCount = 0
        for (s in arr) {
            bitCount += Integer.bitCount(Integer.valueOf(s))
        }
        return bitCount
    }


    fun getLanguages(callback: AsyncDataReceiver<List<Language>>) {
        if (utilityService == null || languageManager == null) {
            callback.onFailed()
            return
        }

        try {
            languageList = languageManager!!.languages
            callback.onSuccess(languageList)
        } catch (e: RemoteException) {
            e.printStackTrace()
        }

    }

    fun setLanguage(languageTag: String) {
        if (utilityService == null || languageManager == null) {
            return
        }
        try {
            languageManager!!.setLanguage(languageTag)
        } catch (e: RemoteException) {
            e.printStackTrace()
        }

    }

    /**
     * Disable HDMI CEC
     */
    fun disableHdmiCec() {
        if (utilityService == null || settingsManager == null) {
            return
        }
        try {
            settingsManager.disableHdmiCec()
        } catch (e: RemoteException) {
            e.printStackTrace()
        }

    }

    companion object {

        private val SERVICE_PACKAGE_NAME = "com.iwedia.utility"

        private val SERVICE_CLASS_NAME = "com.iwedia.utility.UtilityService"

        private var instance: RjioUtilityServiceHandler? = null

        /**
         * Get RjioUtilityService instance
         *
         * @return  singleton class instance
         */
        fun getInstance(): RjioUtilityServiceHandler {
            if (instance == null) {
                instance = RjioUtilityServiceHandler()
            }
            return instance as RjioUtilityServiceHandler
        }
    }


}
