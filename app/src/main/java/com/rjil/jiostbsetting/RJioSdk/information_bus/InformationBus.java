package com.rjil.jiostbsetting.RJioSdk.information_bus;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Information bus
 *
 * @author Dragan Krnjaic
 */
public class InformationBus {

    /**
     * Singleton instance
     */
    private static final InformationBus instance = new InformationBus();

    /**
     * Get singleton instance
     *
     * @return information bus instance
     */
    public static InformationBus getInstance() {
        return instance;
    }

    /**
     * Singleton constructor
     */
    private InformationBus() {
    }

    /**
     * Registered listeners
     */
    private List<EventListener> listeners = Collections.synchronizedList(new ArrayList<EventListener>());

    /**
     * Register event listener
     *
     * @param listener event listener
     */
    public synchronized void registerEventListener(EventListener listener) {
        if (listener != null) {
            listeners.add(listener);
        }
    }

    /**
     * Unregister event listener
     *
     * @param listener event listener
     */
    public synchronized void unregisterEventListener(EventListener listener) {
        for (int i = 0; i < listeners.size(); i++) {
            if (listeners.get(i) == listener) {
                listeners.remove(i);
                break;
            }
        }
    }

    /**
     * Submit event
     *
     * @param event event
     */
    public synchronized void submitEvent(Event event) {
        for (EventListener listener : listeners) {
            if (listener != null) {
                if (listener.hasTypes()) {
                    if (listener.isContainsType(event.getType())) {
                        listener.callback(event);
                    }
                } else {
                    listener.callback(event);
                }
            }
        }
    }
}
