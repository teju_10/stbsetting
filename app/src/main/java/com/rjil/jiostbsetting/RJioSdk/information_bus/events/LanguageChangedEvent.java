package com.rjil.jiostbsetting.RJioSdk.information_bus.events;


import com.rjil.jiostbsetting.RJioSdk.Language;
import com.rjil.jiostbsetting.RJioSdk.information_bus.Event;

/**
 * Language changed event
 *
 * @author Dejan Nadj
 */
public class LanguageChangedEvent extends Event {

    /**
     * Constructor
     *
     * @param language language
     */
    public LanguageChangedEvent(Language language) {
        super(Events.LANGUAGE_CHANGED, language);
    }
}
