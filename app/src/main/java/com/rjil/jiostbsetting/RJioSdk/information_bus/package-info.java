/**
 * Provides Information bus events feature
 * <p>
 * <p>
 * <code>InformationBus</code> provides support for sending and receiving events between different app modules.
 * <p>
 * Every <code>Event</code> has id and data object.
 * Event can be submitted and received by using InformationBus
 *
 * @see com.iwedia.fti.rjio.fti.utils.information_bus.Event
 * @see com.iwedia.fti.rjio.fti.utils.information_bus.EventListener
 * @see com.iwedia.fti.rjio.fti.utils.information_bus.InformationBus
 * @since 1.0
 */
package com.rjil.jiostbsetting.RJioSdk.information_bus;