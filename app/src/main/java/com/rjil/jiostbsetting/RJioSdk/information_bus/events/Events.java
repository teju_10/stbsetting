package com.rjil.jiostbsetting.RJioSdk.information_bus.events;

/**
 * Event ids
 *
 * @author Nikola Aleksic
 */
public class Events {

    /**
     * Inactivity warning id
     */
    public static final int INACTIVITY_WARNING = 1;

    /**
     * Inactivity remaining time id
     */
    public static final int INACTIVITY_REMAINING_TIME = 2;

    /**
     * Inactivity shutdown id
     */
    public static final int INACTIVITY_SHUTDOWN = 3;


    /**
     * New notification event id
     */
    public static final int NEW_NOTIFICATION = 4;

    /**
     * Update progress event id
     */
    public static final int UPDATE_PROGRESS = 5;

    /**
     * Network connection event id
     */
    public static final int NETWORK_CONNECTION = 6;

    /**
     * Language changed event id
     */
    public static final int LANGUAGE_CHANGED = 7;

    /**
     * Recommendation changed event id
     */
    public static final int RECOMMENDATION_CHANGED = 8;

    /**
     * Promotion changed event id
     */
    public static final int PROMOTION_CHANGED = 9;

    /**
     * Jio apps list changed
     */
    public static final int JIO_APPS_CHANGED = 10;

    /**
     * Apps list changed
     */
    public static final int APPS_CHANGED = 11;

    /**
     * Games list changed
     */
    public static final int GAMES_CHANGED = 12;

    /**
     * Time changed event
     */
    public static final int TIME_CHANGED = 13;

    /**
     * Start timer event
     */
    public static final int BACK_FROM_SLEEP = 14;

    /**
     * Visibility changed event
     */
    public static final int VISIBILITY_CHANGED = 15;
}
