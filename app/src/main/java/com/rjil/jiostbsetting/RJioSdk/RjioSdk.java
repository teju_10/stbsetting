package com.rjil.jiostbsetting.RJioSdk;

import android.content.Context;

/**
 * Rjio sdk
 *
 * @author Veljko Ilkic
 */
public class RjioSdk {

    /**
     * Singleton instance
     */
    private static final RjioSdk instance = new RjioSdk();

    /**
     * Context
     */
    private Context context;

    /**
     * Singleton constructor
     */
    private RjioSdk() {
    }

    /**
     * Get singleton instance
     *
     * @return singleton instance
     */
    public static RjioSdk get() {
        return instance;
    }


    private RjioSharedPrefs sharedPrefs;

    RjioLanguageHandler languageHandler;


    /**
     * Setup sdk
     */
    public void setup(final Context context) {
        this.context = context;


        sharedPrefs = new RjioSharedPrefs();
        languageHandler = new RjioLanguageHandler();
    }

    /**
     * Get application context
     *
     * @return context
     */
    public Context getContext() {
        return context;
    }

    public RjioSharedPrefs getSharedPrefs() {
        return sharedPrefs;
    }

    public RjioLanguageHandler getLanguageHandler() {
        return languageHandler;
    }

}
