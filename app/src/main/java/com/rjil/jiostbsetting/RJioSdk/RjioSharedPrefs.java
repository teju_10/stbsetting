package com.rjil.jiostbsetting.RJioSdk;

import android.content.Context;
import android.content.SharedPreferences;


/**
 * Rjio Shared Prefs
 *
 * @author Nikola Aleksic
 */
public class RjioSharedPrefs implements PrefsAPI {

    /**
     * Shared prefs key
     */
    private static String SHARED_PREFS_KEY = "SHARED_PREFS_KEY";

    /**
     * Username key
     */
    private static final String USERNAME_KEY = "USERNAME_KEY";

    /**
     * Password key
     */
    private static final String PASSWORD_KEY = "PASSWORD_KEY";

    /**
     * Language key
     */
    public static final String LANGUAGE_KEY = "language";

    /**
     * Is fti setup procedure completed flag
     */
    public static final String IS_SETUP_COMPLETED = "isSetupCompleted";

    /**
     * Location key
     */
    public static final String LOCATION = "location";

    /**
     * Shared preferences
     */
    private SharedPreferences sharedPreferences;

    /**
     * Constructor
     */
    public RjioSharedPrefs() {
        setup();
    }

    /**
     * Setup method
     */
    private void setup() {
        sharedPreferences = RjioSdk.get().getContext().getSharedPreferences(SHARED_PREFS_KEY, Context.MODE_PRIVATE);
    }

    @Override
    public void storeValue(String tag, Object value) {
        if (value instanceof String) {
            sharedPreferences.edit().putString(tag, (String) value).commit();
        } else if (value instanceof Boolean) {
            sharedPreferences.edit().putBoolean(tag, (Boolean) value).commit();
        } else if (value instanceof Integer) {
            sharedPreferences.edit().putInt(tag, (Integer) value).commit();
        } else if (value instanceof Float) {
            sharedPreferences.edit().putFloat(tag, (Float) value).commit();
        } else if (value instanceof Long) {
            sharedPreferences.edit().putLong(tag, (Long) value).commit();
        }
    }

    @Override
    public Object getValue(String tag, Object defaultValue) {
        if (defaultValue instanceof String) {
            return sharedPreferences.getString(tag, (String) defaultValue);
        } else if (defaultValue instanceof Boolean) {
            return sharedPreferences.getBoolean(tag, (Boolean) defaultValue);
        } else if (defaultValue instanceof Integer) {
            return sharedPreferences.getInt(tag, (Integer) defaultValue);
        } else if (defaultValue instanceof Float) {
            return sharedPreferences.getFloat(tag, (Float) defaultValue);
        } else if (defaultValue instanceof Long) {
            return sharedPreferences.getLong(tag, (Long) defaultValue);
        }

        return defaultValue;
    }

    /**
     * Save user name
     *
     * @param username username
     */
    public void saveUserName(String username) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(USERNAME_KEY, username);
        editor.apply();
    }

    /**
     * Save password
     *
     * @param password password
     */
    public void savePassword(String password) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(PASSWORD_KEY, password);
        editor.apply();
    }

    /**
     * Load username data
     *
     * @return saved username
     */
    public String loadUserName() {
        return sharedPreferences.getString(USERNAME_KEY, "");
    }

    /**
     * Load password data
     *
     * @return saved password
     */
    public String loadPassword() {
        return sharedPreferences.getString(PASSWORD_KEY, "");
    }

    /**
     * Remove username data
     */
    public void removeUserName() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(USERNAME_KEY);
        editor.apply();
    }

    /**
     * Remove password data
     */
    public void removePassword() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(PASSWORD_KEY);
        editor.apply();
    }
}
