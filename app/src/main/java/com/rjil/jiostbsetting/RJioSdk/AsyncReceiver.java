package com.rjil.jiostbsetting.RJioSdk;

/**
 * Async receiver
 *
 * @author Veljko Ilkic
 */
public interface AsyncReceiver {

    /**
     * On success
     */
    void onSuccess();

    /**
     * On failed
     */
    void onFailed();
}
