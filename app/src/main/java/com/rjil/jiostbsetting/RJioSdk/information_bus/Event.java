package com.rjil.jiostbsetting.RJioSdk.information_bus;

/**
 * Event on information bus
 *
 * @author Dragan Krnjaic
 */
public class Event {

    /**
     * Event type
     */
    private int type = -1;

    /**
     * Event data
     */
    private Object data;

    /**
     * Constructor
     *
     * @param type event type
     */
    public Event(int type) {
        this.type = type;
    }

    /**
     * Constructor
     *
     * @param type event type
     * @param data event data
     */
    public Event(int type, Object data) {
        this.type = type;
        this.data = data;
    }

    /**
     * Get event type
     *
     * @return event type
     */
    public int getType() {
        return this.type;
    }

    /**
     * Get event data
     *
     * @return event data
     */
    public Object getData() {
        return this.data;
    }
}
