package com.rjil.jiostbsetting.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreferance
{

    private static SharedPreferences getSharedPreferences(Context context) {
        return context.getSharedPreferences("AppPermission", Context.MODE_PRIVATE);
    }

    public static Integer getLocationValue(Context context) {
        return getSharedPreferences(context).getInt("location",-1);
    }

    public static void setLocationValue(Context context, int newValue) {
        final SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putInt("location" , newValue);
        editor.commit();
    }
    public static Integer getContactValue(Context context) {
        return getSharedPreferences(context).getInt("contact",-1);
    }

    public static void setContactValue(Context context, int newValue) {
        final SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putInt("contact" , newValue);
        editor.commit();
    }
    public static Integer getStorageValue(Context context) {
        return getSharedPreferences(context).getInt("storage",-1);
    }

    public static void setStorageValue(Context context, int newValue) {
        final SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putInt("storage" , newValue);
        editor.commit();
    }

    public static Integer getMicrophoneValue(Context context) {
        return getSharedPreferences(context).getInt("microphone",-1);
    }

    public static void setMicrophoneValue(Context context, int newValue) {
        final SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putInt("storage" , newValue);
        editor.commit();
    }

    public static boolean getDateAndTimeFormat(Context context) {
        return getSharedPreferences(context).getBoolean("DateTimeFormat",false);
    }

    public static void setDateAndTime(Context context, Boolean newValue) {
        final SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putBoolean("DateTimeFormat" , newValue);
        editor.commit();
    }

    public static void setItemPos(Context context, String Value) {
        final SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString("setItemPos" , Value);
        editor.commit();
    }

    public static String getItemPos(Context context) {
        return getSharedPreferences(context).getString("setItemPos","0");
    }

    public static void setLang(Context context, String Value) {
        final SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString("setLang" , "English (India)");
        editor.commit();
    }

    public static String getLang(Context context) {
        return getSharedPreferences(context).getString("setLang","English (India)");
    }


    public static void setUninstall(Context context, Boolean newValue) {
        final SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putBoolean("uninstall", newValue);
        editor.commit();
    }

    public static boolean getUninstall(Context context) {
        return getSharedPreferences(context).getBoolean("uninstall", false);
    }

    public static void setForce(Context context, Boolean newValue) {
        final SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putBoolean("force", newValue);
        editor.commit();
    }

    public static boolean getForce(Context context) {
        return getSharedPreferences(context).getBoolean("force", false);
    }

    public static void setForcePackage(Context context, String packagename) {
        final SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString("packagename", packagename);
        editor.commit();
    }

    public static String getForcePackage(Context context) {
        return getSharedPreferences(context).getString("packagename", "");
    }


    public static void setForceFlag(Context context, Boolean newValue) {
        final SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putBoolean("forceStop", newValue);
        editor.commit();
    }

    public static boolean getForceFlag(Context context) {
        return getSharedPreferences(context).getBoolean("forceStop", false);
    }
}
