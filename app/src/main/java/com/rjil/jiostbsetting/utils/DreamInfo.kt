package com.rjil.jiostbsetting.utils

import android.content.ComponentName
import android.graphics.drawable.Drawable


class DreamInfo {
    var caption: CharSequence? = null
    var icon: Drawable? = null
    var isActive: Boolean = false
    var componentName: ComponentName? = null
    var settingsComponentName: ComponentName? = null

    override fun toString(): String {
        val sb = StringBuilder(DreamInfo::class.java.simpleName)
        sb.append('[').append(caption)
        if (isActive)
            sb.append(",active")
        sb.append(',').append(componentName)
        if (settingsComponentName != null)
            sb.append("settings=").append(settingsComponentName)
        return sb.append(']').toString()
    }
}
