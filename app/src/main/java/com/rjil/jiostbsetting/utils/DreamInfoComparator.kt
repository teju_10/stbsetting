package com.rjil.jiostbsetting.utils

import android.content.ComponentName


class DreamInfoComparator(private val mDefaultDream: ComponentName) : Comparator<DreamInfo> {

    override fun compare(lhs: DreamInfo, rhs: DreamInfo): Int {
        return sortKey(lhs).compareTo(sortKey(rhs))
    }

    private fun sortKey(di: DreamInfo): String {
        val sb = StringBuilder()
        sb.append(if (di.componentName == mDefaultDream) '0' else '1')
        sb.append(di.caption)
        return sb.toString()
    }
}