package com.rjil.jiostbsetting.utils;

import android.content.Context;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;

import com.rjil.jiostbsetting.R;

public enum WifiSecurity {
    WEP(R.string.wifi_security_type_wep),
    PSK(R.string.wifi_security_type_wpa),
    EAP(R.string.wifi_security_type_eap),
    NONE(R.string.wifi_security_type_none);

    public static WifiSecurity getSecurity(ScanResult result) {
        if (result.capabilities.contains("WEP")) {
            return WEP;
        } else if (result.capabilities.contains("PSK")) {
            return PSK;
        } else if (result.capabilities.contains("EAP")) {
            return EAP;
        }
        return NONE;
    }

    public static WifiSecurity getSecurity(WifiConfiguration config) {
        if (config.allowedKeyManagement.get(WifiConfiguration.KeyMgmt.WPA_PSK)) {
            return PSK;
        }
        if (config.allowedKeyManagement.get(WifiConfiguration.KeyMgmt.WPA_EAP)
                || config.allowedKeyManagement.get(WifiConfiguration.KeyMgmt.IEEE8021X)) {
            return EAP;
        }
        return (config.wepKeys[0] != null) ? WEP : NONE;
    }

    public static WifiSecurity getSecurity(int type) {
        switch (type) {
            case 0:
                return WEP;
            case 1:
                return PSK;
            case 2:
                return EAP;
            case 3:
                return NONE;
        }

        throw new IllegalArgumentException("Unknown security type");
    }

    private final int mNameResourceId;

    private WifiSecurity(int nameResourceId) {
        mNameResourceId = nameResourceId;
    }

    public String getName(Context context) {
        return context.getString(mNameResourceId);
    }

    public boolean isOpen() {
        return this == NONE;
    }
}
