package com.rjil.jiostbsetting.utils

import android.annotation.SuppressLint
import android.app.ActivityManager
import android.app.AppOpsManager
import android.app.ProgressDialog
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.ActivityNotFoundException
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.pm.ApplicationInfo
import android.content.pm.PackageManager
import android.content.pm.PackageManager.COMPONENT_ENABLED_STATE_DISABLED
import android.content.pm.PackageManager.COMPONENT_ENABLED_STATE_DISABLED_USER
import android.content.pm.ResolveInfo
import android.content.res.TypedArray
import android.content.res.XmlResourceParser
import android.hardware.usb.UsbDevice
import android.hardware.usb.UsbManager
import android.net.ProxyInfo
import android.net.Uri
import android.net.wifi.WifiConfiguration
import android.net.wifi.WifiManager
import android.os.*
import android.provider.Settings
import android.provider.Settings.System.SCREEN_OFF_TIMEOUT
import android.service.dreams.DreamService
import android.service.dreams.IDreamManager
import android.text.TextUtils
import android.text.format.DateUtils
import android.util.AttributeSet
import android.util.Log
import android.util.Xml
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import com.github.mjdev.libaums.UsbMassStorageDevice
import com.rjil.jiostbsetting.ControlPanelQrCodeActivity
import com.rjil.jiostbsetting.PrivacyPolicyActivity
import com.rjil.jiostbsetting.R
import com.rjil.jiostbsetting.fragments.AppListFragment
import com.rjil.jiostbsetting.models.Bluetooth
import com.rjil.jiostbsetting.utils.Const.getFileSize
import org.xmlpull.v1.XmlPullParser
import org.xmlpull.v1.XmlPullParserException
import java.io.*
import java.lang.Process
import java.lang.reflect.InvocationTargetException
import java.net.NetworkInterface
import java.net.SocketException
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Matcher
import java.util.regex.Pattern

class Constant {

    companion object {
        private lateinit var pd: ProgressDialog
        val TAG = "Constant TAG"

        var lastselectedposition : Int?=0

        val JIO_REMOTE: String = "JIO BLE Remote"
        val JIO_REMOTE_MAC: String = "D4:B8:FF"

        val NORMAL_ITEM = 0

        val RADIO_ITEM = 1

        val JIO_REMOTE_NAME: String = "JIO BLE Remote"

        val MENU_OPTION = arrayOf("Network", "Apps", "Storage & Reset", "Display, Keyboard & Language", "Developers & Location", "Remote & Accessory", "Inactivity Timer & HDMI CEC", "About")
        val PROXY_OPTION = arrayOf("None", "Manual")
        val IP_OPTION = arrayOf("DHCP", "Static")

        val ABOUT_SUB_OPTION = arrayOf("Restart", "Status", "Jio Privacy", "Device Name", "Model", "Version & Build", "Android Security Patch Level", "Kernel Version")
        val ABOUT_SUB_OPTION_VALUE = arrayOf("", "", "", "", Build.MODEL, getBuildDetails(), Build.VERSION.SECURITY_PATCH, "")

        val REMOTE_SUB_OPTION = arrayOf("Mobile Remote")
        val REMOTE_SUB_OPTION_VALUE = arrayOf("")

        val OTHER_SUB_OPTION = arrayOf("Turn screensaver Off","Screensaver", "When to Start Screensaver", "Put Device to Sleep", "Start Screensaver Now", "HDMI CEC - One key power off")
        val OTHER_SUB_OPTION_VALUE = arrayOf("SWITCH","", "", "", "", "SWITCH")

        val STATUS_SUB_OPTION = arrayOf("IP Address", "MAC Address", "Bluetooth Address", "Serial Number", "Uptime")
        val STATUS_SUB_OPTION_VALUE = arrayOf(getIpAddress(), getMacAddr(), getBluetoothMacAddress(), android.os.Build.SERIAL, getDeviceUpTime())

        val INACTIVITY_SUB_OPTION = arrayOf("Screensaver", "When to Start Screensaver", "Put Device to Sleep", "Start Screensaver Now")
        val INACTIVITY_SUB_OPTION_VALUE = arrayOf("", "", "", "")

        val WHEN_TO_START = arrayOf("5 Minutes", "15 Minutes", "30 Minutes", "1 Hour", "2 Hours")
        val WHEN_TO_START_VALUE = arrayOf("300000", "900000", "1800000", "3600000", "7200000")

        val DEVICE_SLEEP = arrayOf("10 Minutes", "30 Minutes", "1 Hour", "3 Hours", "6 Hours", "12 Hours", "24 Hours", "Never")
        val DEVICE_SLEEP_VALUE = arrayOf("600000", "1800000", "3600000", "10800000", "21600000", "43200000", "86400000", "0")

        val DEFAULT_DREAM_TIME_MS = (30 * DateUtils.MINUTE_IN_MILLIS)
        val DEFAULT_SLEEP_TIME_MS = (3 * DateUtils.HOUR_IN_MILLIS)

        val ADD_NEW_NETWORK_OPTIONS = arrayOf("None", "WEP", "WPA/WPA2 PSK", "802.1x EAP")

        val SECURITY_SUB_OPTION = arrayOf("Developer Options", "Use Wi-Fi To Estimate Location")
        val SECURITY_SUB_OPTION_VALUE = arrayOf("", "SWITCH")

        val DEVELOPER_SUB_OPTION = arrayOf("Enable Developer options", "Stay awake", "Enable Bluetooth HCI snoop log",
                "USB debugging")
        //, "Revoke USB debugging authorisations","Logger buffer sizes"
//               , "Show layout bounds","Show CPU usage")
        val DEVELOPER_SUB_OPTION_VALUE = arrayOf("SWITCH", "SWITCH", "SWITCH", "SWITCH")//, "SWITCH", "SWITCH")

        val DISPLAY_SUB_OPTION = arrayOf("Overscan", "Date & Time", "Language", "Keyboard")
        //val DISPLAY_SUB_OPTION_VALUE = arrayOf("","",Locale.getDefault().getDisplayLanguage(), "")
        val DISPLAY_SUB_OPTION_VALUE = arrayOf("", "", "", "")
        //date and time
        // val DATE_AND_TIME_SUB_OPTION = arrayOf("Automatic date & time", "Set date","Set time","Set time zone","Use 24-hour format.")
        val DATE_AND_TIME_SUB_OPTION = arrayOf("Use 24-hour format", "Automatic date & time", "Current date", "Current time")
        val DATE_AND_TIME_SUB_OPTION_VALUE = arrayOf("", "", "", "")

        val STORAGE_SUB_OPTION = arrayOf("Device Storage", "Factory Reset")
        val STORAGE_SUB_OPTION_WITH_EXTDEVICE = arrayOf("Device Storage", "Removable Storage", "Factory Reset")
        val STORAGE_SUB_OPTION_VALUE_WITH_EXTDEVICE = arrayOf("", "", "")
        val STORAGE_SUB_OPTION_VALUE = arrayOf("", "")

        val APPS_SUB_OPTION = arrayOf("Downloaded Apps", "System Apps")
        val APPS_SUB_OPTION_VALUE = arrayOf("", "")

        val NETWORK_SUB_OPTION = arrayOf("Wifi", "Add new Network", "Connect via WPS", "Ethernet")
        val NETWORK_SUB_OPTION_VALUE = arrayOf("", "", "", "")

        val EXTERNAL_STORAGE = arrayOf("Misc", "Available")
        val DEVICE_STORAGE = arrayOf("Internal Shared Storage", "Cached Data", "Apps", "Downloads", "Available")

        val APP_MENU = arrayOf("Clear Data", "Uninstall", "Force Stop", "Clear Cache", "Permissions")

        val APP_SYSTEM_MENU_DISABLE = arrayOf("Clear Data", "Disable", "Force Stop", "Clear Cache", "Permissions")
        val APP_SYSTEM_MENU_FORCED_DISABLE = arrayOf("Clear Data", "Disable", "Clear Cache", "Permissions")
        val APP_SYSTEM_UNINSTALL_DISABLE = arrayOf("Clear Data", "Disable", "Uninstall Updates", "Force Stop", "Clear Cache", "Permissions")
        val APP_SYSTEM_MENU = arrayOf("Clear Data", "Enable", "Force Stop", "Clear Cache", "Permissions")
        val APP_SYSTEM_MENU_FORCED = arrayOf("Clear Data", "Enable", "Clear Cache", "Permissions")
        val APP_SYSTEM_UNINSTALL = arrayOf("Clear Data", "Enable", "Uninstall Updates", "Force Stop", "Clear Cache", "Permissions")
        val APP_PERMISSION = arrayOf("Location", "Contacts", "Storage", "Microphone")
        val PERMISSION_VALUE = arrayOf("SWITCH", "SWITCH", "SWITCH", "SWITCH")

        val APP_MENU_FORCED = arrayOf("Clear Data", "Uninstall", "Clear Cache", "Permissions")

        val APP_SYSTEM_UNINSTALL_MENU = arrayOf("Clear Data", "Uninstall Updates", "Force Stop", "Clear Cache", "Permissions")

        fun getBuildDetails(): String {
            var data = android.os.Build.VERSION.RELEASE + " & " + android.os.Build.DISPLAY
            val firstLine = data.substring(0, 18)
            return firstLine + "\n" + data.substring(18, data.length)
        }

        fun getSecurityPatch(): String? {
            var patchDate = android.os.Build.VERSION.SECURITY_PATCH
            var spf = SimpleDateFormat("yyyy-mm-dd")
            var newDate = spf.parse(patchDate)
            spf = SimpleDateFormat("dd MMMM yyyy");
            Log.d("TAG", "Date:" + spf.format(newDate).toString())
            return spf.format(newDate).toString()
        }

        fun getDeviceName(context: Context): String? {
            getSecurityPatch()
            return Settings.Global.getString(context.contentResolver, Settings.Global.DEVICE_NAME)
        }

        fun rebootBOX(context: Context) {
            try {
                val pm = context.getSystemService(Context.POWER_SERVICE) as PowerManager
                pm.reboot("deviceowner")
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        fun uninstallUpdates(context: Context, packageName: String) {
            var packageURI = Uri.parse("package:" + packageName);
            var uninstallIntent = Intent(Intent.ACTION_UNINSTALL_PACKAGE, packageURI)
            uninstallIntent.putExtra(Intent.EXTRA_RETURN_RESULT, true)
            context.startActivity(uninstallIntent)
        }

        fun getKernelVersion(): String {
            val FILENAME_PROC_VERSION = "/proc/version";
            val rawKernelVersion: String? = readLine(FILENAME_PROC_VERSION)
            val PROC_VERSION_REGEX: String = "Linux version (\\S+) " + "\\((\\S+?)\\) " + "(?:\\(gcc.+? \\)) " + "(#\\d+) " +
                    "(?:.*?)?" + "((Sun|Mon|Tue|Wed|Thu|Fri|Sat).+)"

            val matcher: Matcher = Pattern.compile(PROC_VERSION_REGEX).matcher(rawKernelVersion)
            if (!matcher.matches()) {
                return "Unavailable"
            } else if (matcher.groupCount() < 4) {
                return "Unavailable";
            }
            return matcher.group(1) + " " +
                    matcher.group(2) + " " + matcher.group(3) + "\n" +
                    matcher.group(4)
        }

        fun isScreensaverActive(context: Context): Boolean {
            var isactivity = false
            val pm = context.packageManager
            val dreamIntent = Intent(DreamService.SERVICE_INTERFACE)
            val resolveInfos = pm.queryIntentServices(dreamIntent,
                    PackageManager.GET_META_DATA)
            val dreamInfos = arrayListOf<DreamInfo>()
            for (resolveInfo: ResolveInfo in resolveInfos) {
                if (resolveInfo.serviceInfo == null)
                    continue
                val dreamInfo = DreamInfo()
                dreamInfo.caption = resolveInfo.loadLabel(pm)
                dreamInfo.icon = resolveInfo.loadIcon(pm)
                dreamInfo.componentName = getDreamComponentName(resolveInfo)
                dreamInfo.isActive = dreamInfo.componentName == getActiveDreamComponent()
                isactivity = dreamInfo.isActive
            }
            return isactivity
        }





        fun readLine(filename: String): String? {
            val reader = BufferedReader(FileReader(filename), 256)
            try {
                return reader.readLine()
            } finally {
                reader.close()
            }
        }

        fun startWebActivity(context: Context, type: String) {
            val intent = Intent(context, PrivacyPolicyActivity::class.java)
            intent.putExtra("title", type)
            context.startActivity(intent)
        }

        fun getIpAddress(): String {
            var ip = ""
            try {
                val enumNetworkInterfaces = NetworkInterface
                        .getNetworkInterfaces()
                while (enumNetworkInterfaces.hasMoreElements()) {
                    val networkInterface = enumNetworkInterfaces
                            .nextElement()
                    val enumInetAddress = networkInterface
                            .inetAddresses
                    while (enumInetAddress.hasMoreElements()) {
                        val inetAddress = enumInetAddress.nextElement()

                        if (inetAddress.isSiteLocalAddress) {
                            ip = inetAddress.hostAddress
                        }
                    }
                }
            } catch (e: SocketException) {
                e.printStackTrace()
                return "Not Available"
            }
            return ip
        }

        fun getMacAddr(): String {
            try {
                val all = Collections.list(NetworkInterface.getNetworkInterfaces())
                for (nif in all) {
                    if (!nif.name.equals("wlan0", ignoreCase = true)) continue

                    val macBytes = nif.hardwareAddress ?: return ""

                    val res1 = StringBuilder()
                    for (b in macBytes) {
                        res1.append(String.format("%02X:", b))
                    }

                    if (res1.length > 0) {
                        res1.deleteCharAt(res1.length - 1)
                    }
                    return res1.toString()
                }
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
            return "02:00:00:00:00:00"
        }

        fun getBluetoothMacAddress(): String {
            val bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
            var bluetoothMacAddress = ""
            if (bluetoothAdapter != null) {
                try {
                    val mServiceField = bluetoothAdapter.javaClass.getDeclaredField("mService")
                    mServiceField.isAccessible = true

                    val btManagerService = mServiceField.get(bluetoothAdapter)

                    if (btManagerService != null) {
                        bluetoothMacAddress = btManagerService.javaClass.getMethod("getAddress").invoke(btManagerService) as String
                    }
                } catch (ignore: NoSuchFieldException) {
                } catch (ignore: NoSuchMethodException) {
                } catch (ignore: IllegalAccessException) {
                } catch (ignore: InvocationTargetException) {
                }
            }
            return bluetoothMacAddress
        }

        fun showSoftKeyboard(context: Context,view: View) {
        if (view.requestFocus()) {
            val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT)
        }
    }

         fun getDeviceUpTime(): String {
            val secs = (SystemClock.elapsedRealtime() / 1000).toFloat()
            val hours = secs / 60 / 60
            val mins = secs / 60
            //val days = hours / 24 ## df.format(days) + " days\nOr\n" +
            val df = DecimalFormat("#####")

            if (mins > 60) {
                if (df.format(hours) == "1") {
                    return (df.format(hours) + " hour")
                } else {
                    return (df.format(hours) + " hours")
                }

            } else {
                if (df.format(mins) == "1") {
                    return (df.format(mins) + " minute")
                } else {
                    return (df.format(mins) + " minutes")
                }
            }
        }

        fun readCecOption(context: Context, key: String): Boolean {
            try {
                return Settings.Global.getInt(context.contentResolver, key, 1) == 1
            } catch (e: SecurityException) {
                e.printStackTrace()
                return false
            }
        }

        fun readPrivacyOption(context: Context, key: String): Boolean {
            try {
                return Settings.Global.getInt(context.contentResolver, key, 0) == 1
            } catch (e: SecurityException) {
                e.printStackTrace()
                return false
            }
        }

        fun writePrivacyOption(context: Context, key: String, value: Boolean) {
            try {
                Settings.Global.putInt(context.contentResolver, key, if (value) 1 else 0)
            } catch (e: SecurityException) {
                e.printStackTrace()
            }
        }

        fun writeCecOption(context: Context, key: String, value: Boolean) {
            try {
                Settings.Global.putInt(context.getContentResolver(), key, if (value) 1 else 0)
            } catch (e: SecurityException) {
                e.printStackTrace()
            }
        }

        fun getDreamInfos(mContext: Context): List<DreamInfo> {
            val pm = mContext.getPackageManager()
            val dreamIntent = Intent(DreamService.SERVICE_INTERFACE)
            val resolveInfos = pm.queryIntentServices(dreamIntent,
                    PackageManager.GET_META_DATA)
            val dreamInfos = arrayListOf<DreamInfo>()
            for (resolveInfo: ResolveInfo in resolveInfos) {
                if (resolveInfo.serviceInfo == null)
                    continue
                val dreamInfo = DreamInfo()
                dreamInfo.caption = resolveInfo.loadLabel(pm)
                dreamInfo.icon = resolveInfo.loadIcon(pm)
                dreamInfo.componentName = getDreamComponentName(resolveInfo)
                dreamInfo.isActive = dreamInfo.componentName == getActiveDreamComponent()
                dreamInfo.settingsComponentName = getSettingsComponentName(pm, resolveInfo)
                dreamInfos.add(dreamInfo)
            }
            return dreamInfos
        }

        fun getDreamComponentName(resolveInfo: ResolveInfo?): ComponentName? {
            return if (resolveInfo == null || resolveInfo.serviceInfo == null) null else ComponentName(resolveInfo.serviceInfo.packageName, resolveInfo.serviceInfo.name)
        }

        fun getSettingsComponentName(pm: PackageManager, resolveInfo: ResolveInfo): ComponentName? {
            if (resolveInfo == null
                    || resolveInfo.serviceInfo == null
                    || resolveInfo.serviceInfo.metaData == null)
                return null

            var cn: String? = null
            var parser: XmlResourceParser? = null
            var caughtException: Exception? = null
            try {
                parser = resolveInfo.serviceInfo.loadXmlMetaData(pm, DreamService.DREAM_META_DATA)
                if (parser == null) {
                    Log.w(TAG, "No " + DreamService.DREAM_META_DATA + " meta-data")
                    return null
                }
                val res = pm.getResourcesForApplication(resolveInfo.serviceInfo.applicationInfo)
                var attrs: AttributeSet = Xml.asAttributeSet(parser)
                var type: Int = parser.next()
                while (type != XmlPullParser.END_DOCUMENT
                        && type != XmlPullParser.START_TAG) {
                    val nodeName = parser.name
                    if (!"dream".equals(nodeName)) {
                        Log.w(TAG, "Meta-data does not start with dream tag");
                        return null
                    }
                    val sa: TypedArray = res.obtainAttributes(attrs, R.styleable.Dream);
                    cn = sa.getString(R.styleable.Dream_settingsActivity);
                    sa.recycle();
                    type = parser.next()
                }
            } catch (e: PackageManager.NameNotFoundException) {
                caughtException = e
            } catch (e: IOException) {
                caughtException = e
            } catch (e: XmlPullParserException) {
                caughtException = e
            } finally {
                if (parser != null)
                    parser.close()
            }
            if (caughtException != null) {
                Log.w(TAG, "Error parsing : " + resolveInfo.serviceInfo.packageName, caughtException);
                return null;
            }
            if (cn != null && cn.indexOf('/') < 0) {
                cn = resolveInfo.serviceInfo.packageName + "/" + cn;
            }
            return if (cn == null) null else ComponentName.unflattenFromString(cn)
        }

        fun getActiveDreamComponent(): ComponentName? {
            var binder: IBinder = this!!.androidOsServiceManagerGetService("dreams")!!
            //get the interface object to call the service functions
            var dreamManager: IDreamManager = IDreamManager.Stub.asInterface(binder)
            if (dreamManager != null)
                try {
                    var dreams = dreamManager.dreamComponents
                    if (dreams != null && dreams.size > 0) {
                        return dreams.get(0)
                    }
                } catch (e: RemoteException) {
                    Log.w(TAG, "Failed to get active dream", e)
                } catch (e: SecurityException) {
                    Log.w(TAG, "Failed to get active dream", e)
                }
            return null
        }

        fun setActiveDream(dream: ComponentName) {
            var binder: IBinder = this!!.androidOsServiceManagerGetService("dreams")!!
            //get the interface object to call the service functions
            var dreamManager: IDreamManager = IDreamManager.Stub.asInterface(binder)
            if (dreamManager == null)
                return
            try {
                var cmpName = arrayOf(dream)
                dreamManager.setDreamComponents(cmpName)
            } catch (e: RemoteException) {
                Log.w(TAG, "Failed to set active dream to " + dream, e)
            } catch (e: SecurityException) {
                Log.w(TAG, "Failed to set active dream to " + dream, e)
            }
        }

        fun startDreaming() {
            var binder: IBinder = this!!.androidOsServiceManagerGetService("dreams")!!
            //get the interface object to call the service functions
            var dreamManager: IDreamManager = IDreamManager.Stub.asInterface(binder)
            if (dreamManager == null)
                return
            try {
                dreamManager.dream()
            } catch (e: RemoteException) {
                Log.w(TAG, "Failed to start dream " + e)
            } catch (e: SecurityException) {
                e.printStackTrace()
            }
        }

        //https://omerjerk.in/index.php/2017/07/29/getting-the-instance-of-a-system-service-without-an-application-context-in-android/

        fun androidOsServiceManagerGetService(service: String): IBinder? {
            try {
                var serviceClass = Class.forName("android.os.ServiceManager")
                var serviceMethod = serviceClass.getDeclaredMethod("getService", String::class.java)
                return serviceMethod.invoke(serviceClass, service) as IBinder
            } catch (e: Exception) {
                Log.w("android.os.ServiceManager.getService", e)
            } catch (e: SecurityException) {
                e.printStackTrace()
            }
            return null
        }

        fun forceStopPackage(context: Context, packageName: String) {

            try {
                var activityManagerClass = Class.forName("android.app.ActivityManager")
                var mActivityManager: ActivityManager = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
                var currentUser = activityManagerClass.getMethod("forceStopPackage", String::class.java)
                currentUser.setAccessible(true)
                currentUser.invoke(mActivityManager, packageName)


                //Log.d(TAG, "getCurrentUser:" + result)
            } catch (e: ClassNotFoundException) {
                Log.i("MyActivityManager", "No Such Class Found Exception", e);
            } catch (e: Exception) {
                Log.i("MyActivityManager", "General Exception occurred", e);
            }
        }

        fun factoryReset(context: Context) {
//            val devicePolicyManager = context.getSystemService(Context.DEVICE_POLICY_SERVICE) as DevicePolicyManager
//            devicePolicyManager.wipeData(0)
            val intent = Intent("android.intent.action.MASTER_CLEAR")
            intent.putExtra("shutdown", true)
            context.sendBroadcast(intent)
        }


        @SuppressLint("PrivateApi")
        fun setProxy(context: Context, proxyInfo: ProxyInfo) {
            try {
                val wifiConfigurationClass = Class.forName("android.net.wifi.WifiConfiguration")
                val wifimanager: WifiManager = context?.getSystemService(Context.WIFI_SERVICE) as WifiManager
                val currentUser = wifiConfigurationClass.getMethod("setHttpProxy", ProxyInfo::class.java)
                currentUser.isAccessible = true
                currentUser.invoke(getCurrentWifiConfiguration(wifimanager), proxyInfo)

                Log.e("Constent App", "Proxy Setting Called")
            } catch (e: ClassNotFoundException) {
                Log.i("WifiConfiguration", "No Such Class Found Exception", e);

            } catch (e: Exception) {
                Log.i("WifiConfiguration", "General Exception occurred", e);
            }


        }


        private fun getCurrentWifiConfiguration(manager: WifiManager): WifiConfiguration? {
            if (!manager.isWifiEnabled)
                return null

            val configurationList = manager.configuredNetworks
            var configuration: WifiConfiguration? = null
            val networkId = manager.connectionInfo.networkId
            for (i in configurationList.indices) {
                val wifiConfiguration = configurationList[i]
                if (wifiConfiguration.networkId == networkId)
                    configuration = wifiConfiguration
            }

            return configuration
        }

        fun setDreamTime(context: Context, ms: Int) {
            try {
                Settings.System.putInt(context.getContentResolver(), SCREEN_OFF_TIMEOUT, ms);
            } catch (e: SecurityException) {
                e.printStackTrace()
            }
        }

        fun setSleepTime(context: Context, ms: Int) {
            try {
                Settings.Secure.putInt(context.contentResolver, "sleep_timeout", ms)
            } catch (e: SecurityException) {
                e.printStackTrace()
            }
        }

        fun getDreamTime(context: Context): Int {
            try {
                return Settings.System.getInt(context.getContentResolver(), SCREEN_OFF_TIMEOUT,
                        DEFAULT_DREAM_TIME_MS.toInt())
            } catch (e: SecurityException) {
                e.printStackTrace()
                return DEFAULT_DREAM_TIME_MS.toInt()
            }
        }

        fun getSleepTime(context: Context): Int {
            try {
                return Settings.Secure.getInt(context.getContentResolver(), "sleep_timeout",
                        DEFAULT_SLEEP_TIME_MS.toInt());
            } catch (e: SecurityException) {
                e.printStackTrace()
                return DEFAULT_SLEEP_TIME_MS.toInt()
            }
        }

        fun getLocationMode1(cx: Context): Boolean {

            if (Build.VERSION.SDK_INT >= 21) {
                var locationMode = Settings.Secure.getInt(cx.getContentResolver(), Settings.Secure.LOCATION_MODE, 0)
                return locationMode != Settings.Secure.LOCATION_MODE_OFF
            } else {
                var locationProviders = Settings.Secure.getString(cx.contentResolver, Settings.Secure.LOCATION_PROVIDERS_ALLOWED)
                return !TextUtils.isEmpty(locationProviders);
            }
        }

        fun getStopped(context: Context?, packagename: String): Boolean {
            val stoppedPackages = ArrayList<String>()
            val pm = context?.getPackageManager()
            val packages = pm?.getInstalledApplications(PackageManager.GET_META_DATA)
            try {
                for (packageInfo in packages!!)
                    if (packageInfo.packageName.equals(packagename)) {
                        if ((packageInfo.flags and ApplicationInfo.FLAG_STOPPED) != 0) {
                            Log.e("Force Stopped apps: ", pm.getApplicationLabel(packageInfo) as String)
                            return true
                        }
                        break
                    }
            } catch (e: Exception) {
            }
            return false
        }

        fun setLocationMode(context: Context, mode: Boolean) {
            try {
                Settings.Global.putInt(context.contentResolver, "location_mode", if (mode) 1 else 0)
            } catch (e: SecurityException) {
                e.printStackTrace()
            }
        }

        fun getLocationMode(context: Context): Boolean {
            try {
                var locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE, 0)
                if (locationMode == Settings.Secure.LOCATION_MODE_HIGH_ACCURACY) {
                    return true
                }
                return false
            } catch (e: Settings.SettingNotFoundException) {
                e.printStackTrace()
                return false
            }
        }

        fun enableDevMode(context: Context, mode: Boolean) {
            try {
                Settings.Global.putInt(context.getContentResolver(), Settings.Global.DEVELOPMENT_SETTINGS_ENABLED, if (mode) 1 else 0)
            } catch (e: SecurityException) {
                e.printStackTrace()
            }
        }

        fun getDevMode(context: Context): Boolean {
            try {
                return Settings.Global.getInt(context.getContentResolver(), Settings.Global.DEVELOPMENT_SETTINGS_ENABLED, 0) == 1
            } catch (e: Exception) {
                e.printStackTrace()
                return false
            }
        }

        fun setStayAwake(context: Context, mode: Boolean) {
            try {
                Settings.Global.putInt(context.getContentResolver(), Settings.Global.STAY_ON_WHILE_PLUGGED_IN, if (mode) 1 else 0)
            } catch (e: SecurityException) {
                e.printStackTrace()
            }
        }

        fun getStayAwake(context: Context): Boolean {
            try {
                return Settings.Global.getInt(context.getContentResolver(), Settings.Global.STAY_ON_WHILE_PLUGGED_IN, 0) == 1
            } catch (e: SecurityException) {
                e.printStackTrace()
                return false
            }
        }

        fun setBLEHCILog(context: Context, mode: Boolean) {
            try {
                Settings.Global.putInt(context.getContentResolver(), "bluetooth_hci_log", if (mode) 1 else 0)
            } catch (e: SecurityException) {
                e.printStackTrace()
            }
        }

        fun getBLEHCILog(context: Context): Boolean {
            try {
                return Settings.Global.getInt(context.getContentResolver(), "bluetooth_hci_log", 0) == 1
            } catch (e: SecurityException) {
                e.printStackTrace()
                return false
            }
        }

        fun setAdbDebug(context: Context, mode: Boolean) {
            try {
                Settings.Global.putInt(context.getContentResolver(), Settings.Global.ADB_ENABLED, if (mode) 1 else 0)
            } catch (e: SecurityException) {
                e.printStackTrace()
            }
        }

        fun getAdbDebug(context: Context): Boolean {
            try {
                return Settings.Global.getInt(context.getContentResolver(), Settings.Global.ADB_ENABLED, 0) == 1
            } catch (e: SecurityException) {
                e.printStackTrace()
                return false
            }
        }

        fun startMobileRemoteDetailActivity(context: Context) {
            /*try {
                var intent = Intent()
                intent.setComponent(ComponentName("com.iwedia.remotemobileapp", "com.iwedia.remotemobileapp.MainActivity"))
                context.startActivity(intent)
            } catch (e: ActivityNotFoundException) {
                e.printStackTrace()
            }*/

            val intent = Intent(context,ControlPanelQrCodeActivity::class.java)
            context.startActivity(intent)

        }


        fun DayDreamEnable(context: Context, value: Int) {
            try {
                Settings.Secure.putInt(context.contentResolver, "screensaver_enabled", value)
            } catch (e: SecurityException) {
                e.printStackTrace()
            }
        }
        fun isDaydreamEnabled(c: Context): Int {
            try {
                return Settings.Secure.getInt(c.contentResolver, "screensaver_enabled", 0)
            }catch (e: SecurityException) {
                e.printStackTrace()
                return 0
            }
        }

        fun startOverScanActivity(context: Context) {
            try {
                var intent = Intent()
                intent.setComponent(ComponentName("com.android.tv.settings", "com.android.tv.settings.OverscanActivity"))
                context.startActivity(intent)
            } catch (e: ActivityNotFoundException) {
                e.printStackTrace()
            }
        }

        fun addBLEAccessory(context: Context) {
            try {
                var intent = Intent()
                intent.setComponent(ComponentName("com.android.tv.settings", "com.android.tv.settings.accessories.AddAccessoryActivity"))
                context.startActivity(intent)
            } catch (e: ActivityNotFoundException) {
                e.printStackTrace()
            }
        }

        fun getBLEDeviceList(): Set<BluetoothDevice>? {
            var btAdapter = BluetoothAdapter.getDefaultAdapter();
            if (btAdapter != null) {
                var bondedDevices: Set<BluetoothDevice> = btAdapter.getBondedDevices();
                for (device in bondedDevices) {
                    Log.d("TAG", "Bonded Device:" + device.address + " & Device Name " + bondedDevices)
                }
                return bondedDevices;
            }
            return null
        }

        fun getBLEDeviceName(device: Set<BluetoothDevice>): ArrayList<String>? {
            if (device != null) {

                val bleName = arrayListOf<String>()
                bleName.clear()
                bleName.add("JIO BLE Remote")
//                for (i in device.iterator()) {
//                    val devicearray = device.toString().replace("[","").replace("]","")
//
//                    if (devicearray.substring(0, 8) == JIO_REMOTE_MAC) {
//                        bleName.add("JIO BLE Remote")
//                    } else {
//                      //  bleName.add("JIO BLE Remote")
//                        // bleName.add(i.name)
//                    }
//                }
                return bleName
            }
            return null

        }


        fun isDeviceConnected(device: Set<BluetoothDevice>): ArrayList<Bluetooth>? {
            if (device != null) {
                val bleName = arrayListOf<Bluetooth>()
                for (i in device.iterator()) {
                    if (i.bondState == 12)
                        bleName.add(Bluetooth(true))
                    else
                        bleName.add(Bluetooth(false))

//                    if (i.bondState == 12) {
//                        if (device.toString()
//                                        .substring(0, 8) == JIO_REMOTE_MAC) {
//                            bleName.add(Bluetooth(true))
//                        } else
//                            bleName.add(Bluetooth(true))
//                    } else
//                        bleName.add(Bluetooth(false))
                }
                return bleName
            }
            return null
        }

        fun setupUSBDevice(context: Context): String {
            var mValue = ""
            var devices: Array<UsbMassStorageDevice> = UsbMassStorageDevice.getMassStorageDevices(context!!)
            for (device in devices) {
                device.init()

                // Only uses the first partition on the device
                val currentFs = device.partitions[0].fileSystem
                Log.d("StorageFrag", "Capacity: " + currentFs.capacity)
                Log.d("StorageFrag", "Occupied Space: " + currentFs.occupiedSpace)
                Log.d("StorageFrag", "Free Space: " + currentFs.freeSpace)
                Log.d("StorageFrag", "Chunk size: " + currentFs.chunkSize)
                val availabesize = getFileSize(currentFs.capacity)
                println("Get File Size " + getFileSize(currentFs.capacity))

                mValue = getFileSize(currentFs.capacity)
            }
            return mValue
        }


        fun Cache(context: Context): String {
            val storageInformation = StorageInformation(context!!)
            val alSize = storageInformation.getpackageSize();
            val size = alSize.get(alSize.size - 1).cachedData

            return size

//            val useSize = context.cacheDir.usableSpace
//            val totalSize = context.cacheDir.totalSpace
//            val cacheSize = totalSize - useSize
//            val totalCache = cacheSize / 10000000
////            sb.append(totalCache).append(b)
//            val ca = formatSize(totalCache)
//            return ca
////            val useSize = context.cacheDir.usableSpace
////            val totalSize = context.cacheDir.totalSpace
////            val cacheSize = totalSize - useSize
////
////            val sb = StringBuilder()
////            var b = " MB"
////            val totalCache = cacheSize / 10000000
////            sb.append(totalCache).append(b)
////            val ca = sb.toString()
////            return ca
        }

        fun App(context: Context): String {
            var f = File("/data/app/");
            var app = f.length()
            val sb = StringBuilder()
            var b = " MB"
            val appSize = app / 10
            sb.append(appSize).append(b)
            val capp = sb.toString()
            return capp
        }

        fun Downloads(context: Context): String {
            var input: BufferedReader
            val file: File
            val url: File = Environment.getDownloadCacheDirectory()
            var absolutePath = url.absolutePath
            println("url====== " + url)
            //file = File(""); // Pass getFilesDir() and "MyFile" to read file
            input = BufferedReader(InputStreamReader(FileInputStream(url) as InputStream?))
            var line: String
            line = input.readLine()
            val buffer: StringBuffer = StringBuffer()
            while (line != null) {
                buffer.append(line)
            }
            Log.d(TAG, buffer.toString())
            val dapp = buffer.toString()

            return dapp
        }


        fun getDownloadStorage(): String {
            var g = ""
            var f = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)

//            val stat = StatFs(f.path)
//            val blockSize = stat.blockSize.toLong()
//            val totalBlocks = stat.blockCount.toLong()
//            val size = totalBlocks * blockSize
//            val Internal = size / 1000000000
//            val sb = StringBuilder()
//            var Size = " GB"
//            sb.append(Internal).append(Size)
//            val internalStorage = sb.toString()
            if (f != null) {
                var bytesAvailable = getFolderSize(f)
                g = formatSize(bytesAvailable)
            } else {
                g = "0"
            }
            return g
        }

        fun getCurrentUser(context: Context) {
            try {
                var activityManagerClass = Class.forName("android.app.ActivityManager")
                var mActivityManager: ActivityManager = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
                var currentUser = activityManagerClass.getMethod("getCurrentUser")
                currentUser.setAccessible(true)
                var result: Int = currentUser.invoke(mActivityManager) as Int
                Log.d(TAG, "getCurrentUser:" + result)
            } catch (e: ClassNotFoundException) {
                Log.i("MyActivityManager", "No Such Class Found Exception", e);
            } catch (e: Exception) {
                Log.i("MyActivityManager", "General Exception occurred", e);
            }
        }

        fun getFolderSize(file: File): Long {
            var size: Long
            if (file.isDirectory()) {
                size = 0
                if (file.listFiles() != null) {
                    for (child: File in file.listFiles()) {
                        size += getFolderSize(child)
                    }
                }

            } else {
                size = file.length()
            }
            return size
        }

        fun getReadableSize(size: Long): String {
            if (size <= 0)
                return "0 B"
            var units = arrayOf("B", "KB", "MB", "GB", "TB")
            var digitGroups = (Math.log10(size.toDouble()) / Math.log10(1024.0))
            return DecimalFormat("#,##0.#").format(size / Math.pow(1024.0, digitGroups)) + " " + units[digitGroups.toInt()]
        }

        fun InternalStorage(context: Context): String {
            val path = Environment.getDataDirectory()
            val stat = StatFs(path.path)
            val blockSize = stat.blockSize.toLong()
            val totalBlocks = stat.blockCount.toLong()
            val size = totalBlocks * blockSize
            val Internal = size / 1000000000
            val sb = StringBuilder()
            var Size = " GB"
            sb.append(Internal).append(Size)
            val internalStorage = sb.toString()
            return internalStorage
        }

        fun getTotalExternalMemorySize(): String {

            if (externalMemoryAvailable()) {
                val stat = StatFs(Environment.getExternalStorageDirectory().path);
                val bytesAvailable: Long
                if (android.os.Build.VERSION.SDK_INT >=
                        android.os.Build.VERSION_CODES.JELLY_BEAN_MR2) {
                    bytesAvailable = stat.getBlockSizeLong() * stat.getAvailableBlocksLong();
                } else {
                    bytesAvailable = stat.getBlockSize().toLong() * stat.getAvailableBlocks()
                }
                val megAvailable = bytesAvailable / (1024 * 1024)
                Log.e("", "Available MB : $megAvailable")
//                var path = Environment.getExternalStorageDirectory()
//                var stat = StatFs(path.getPath())
//                var BlockSize: Long = stat.blockSize.toLong()
//                val totalBlocks = stat.blockCount.toLong()
//                val size = totalBlocks * BlockSize
//                val Internal = size / 1000000000
                val sb = StringBuilder()
                var Size = " GB"
                sb.append(megAvailable).append(Size)
                val internalStorage = sb.toString()
                Log.e(TAG, "Total Ext Size $internalStorage")
                return internalStorage
            } else {
                return "0 B"
            }
        }

        fun externalMemoryAvailable(): Boolean {
            return android.os.Environment.getExternalStorageState().equals(
                    android.os.Environment.MEDIA_MOUNTED);
        }

        fun formatSize(size: Long): String {

            var BufferSize: StringBuilder
            var suffixSize = ""
            var kbSizes: Long
            var mbSizes: Long
            var gbSizes: Long
            if (size >= 1024) {
                suffixSize = " KB"
                kbSizes = size / 1024

                BufferSize = StringBuilder(kbSizes.toString())


                if (kbSizes >= 1024) {
                    suffixSize = " MB"
                    mbSizes = kbSizes / 1024
                    BufferSize = StringBuilder(mbSizes.toString())

                    if (mbSizes > 1024) {
                        suffixSize = " GB"
                        gbSizes = mbSizes / 1024
                        BufferSize = StringBuilder(gbSizes.toString())
                    }
                }
            } else {
                suffixSize = " B"
                BufferSize = StringBuilder(size.toString())
            }


            var commaOffset: Int = BufferSize.length - 3
            while (commaOffset > 0) {
                BufferSize.insert(commaOffset, ',')
                commaOffset -= 3
            }
            if (suffixSize != null) BufferSize.append(suffixSize)
            return BufferSize.toString()
        }

//        fun setupDevice(context: Context) {
//            val usbDevice = getActivity().intent?.getParcelableExtra(UsbManager.EXTRA_DEVICE) as? UsbDevice
//            val usbManager: UsbManager = context!!.getSystemService(Context.USB_SERVICE) as UsbManager
//            if (usbDevice != null) {
//                Log.d("Kabir ", "received usb device via intent")
//                for (device in devices) {
//                    device.init()
//
//                    // Only uses the first partition on the device
//                    val currentFs = device.partitions[0].fileSystem
//                    Log.d("StorageFrag", "Capacity: " + currentFs.capacity)
//                    Log.d("StorageFrag", "Occupied Space: " + currentFs.occupiedSpace)
//                    Log.d("StorageFrag", "Free Space: " + currentFs.freeSpace)
//                    Log.d("StorageFrag", "Chunk size: " + currentFs.chunkSize)
//
//                    println("Get File Size "+getFileSize(currentFs.capacity))
//
//                }
//            } else {
//                val permissionIntent = PendingIntent.getBroadcast(context, 0, Intent("com.github.mjdev.libaums.USB_PERMISSION"), 0);
//                usbManager.requestPermission(devices[0].usbDevice, permissionIntent)
//            }
//
//        }


//            var suffixSize: String = ""
//            var sizes: Long = 0
//            if (size >= 1024) {
//                suffixSize = " KB"
//                sizes = size / 1024
//                if (size >= 1024) {
//                    suffixSize = " MB"
//                    sizes = size / 1024
//
//                    if (size > 1024) {
//                        suffixSize = " GB"
//                        sizes = size / 1024
//                    }
//                }
//            }
//
//            var BufferSize = StringBuilder(sizes.toString())
//
//            var commaOffset: Int = BufferSize.length - 3
//            while (commaOffset > 0) {
//                BufferSize.insert(commaOffset, ',')
//                commaOffset -= 3
//            }
//            if (suffixSize != null) BufferSize.append(suffixSize)
//            return BufferSize.toString()

        fun enableDisable(pkgName: String): Boolean {
            var b = false
            val arrayPkg = arrayOf("com.jio.adc.embedded", "com.rjil.jiostblogservice", "com.jio.pushservices")
            b = arrayPkg.contains(pkgName)
            return b
        }

        fun getCurrentKeyboard(context: Context): String {
            try {
                val packageName = Settings.Secure.getString(context.getContentResolver(), "default_input_method")
                var ai: ApplicationInfo? = null
                try {
                    ai = context.packageManager.getApplicationInfo(packageName.split("/")[0], 0)
                } catch (e: PackageManager.NameNotFoundException) {
                    ai = null
                }
                return context.packageManager.getApplicationLabel(ai) as String
            } catch (e: SecurityException) {
                e.printStackTrace()
                return "NA"
            }
        }


        fun isStorageUSBConnected(context: Context): Boolean {
            Log.d(TAG, "isStorageUSBConnected")
            var value = false
            val usbManager = context.getSystemService(Context.USB_SERVICE) as UsbManager
            var clef: UsbDevice? = null
            if (usbManager != null) {
                val deviceList: HashMap<String, UsbDevice> = usbManager.deviceList
                if (deviceList != null && deviceList.size > 0) {
                    value = true
                    val deviceIterator: Iterator<UsbDevice> = deviceList.values.iterator()
                    while (deviceIterator.hasNext()) {
                        clef = deviceIterator.next()
                    }
                    Log.d(TAG, "Connected List $clef")
                }
            }
            return value
        }

        fun isAppStatus(context: Context, pkgname: String): Boolean {
            val ai = context!!.packageManager.getApplicationInfo(pkgname, 0)
            val appStatus = ai.enabled
            Log.e("DISABLE ENABLED ", appStatus.toString())
            return appStatus
        }


        fun getSystemUninstallUpdatesStatus(context: Context?, packagename: String): Boolean {
            val pm: PackageManager = context!!.getPackageManager()
            var listOfAppInfo = pm.getInstalledApplications(PackageManager.GET_META_DATA)
            for (app in listOfAppInfo) {

                if (pm.getLaunchIntentForPackage(app.packageName) != null) {
                    println("ApplicationInfo.FLAG_UPDATED_SYSTEM_APP " + ApplicationInfo.FLAG_UPDATED_SYSTEM_APP + pm.getApplicationLabel(app) as String)
                    // apps with launcher intent||

                    if (packagename == "com.jio.store"||packagename=="com.iwedia.ui.rjio.launcher.reliance") {
                        Log.e("updated system apps: ", pm.getApplicationLabel(app) as String)
//                        if (packagename == app.packageName)
                        return true
                        break

                    } else if ((app.flags and ApplicationInfo.FLAG_UPDATED_SYSTEM_APP) != 0) {

//                    if (app.flags and ApplicationInfo.FLAG_UPDATED_SYSTEM_APP == 1) {
                        // updated system apps
                        Log.e("updated system apps: ", pm.getApplicationLabel(app) as String)
                        if (packagename == app.packageName)
                            return true
                        break

                    } else if (app.flags and ApplicationInfo.FLAG_SYSTEM == 1) {
                        // system apps
                        Log.e("system apps: ", pm.getApplicationLabel(app) as String)
                    } else {
                        // user installed apps
                        Log.e("user installed apps: ", pm.getApplicationLabel(app) as String)
//                    nameOfAppsArray.add(pm.getApplicationLabel(app) as String)
                    }

                }
            }
//        return nameOfAppsArray
            return false
        }


        fun EnableApp(context: Context, pkgName: String) {
            try {
                var comnd = "pm enable $pkgName"
                val proc: Process? = Runtime.getRuntime().exec(comnd)
                proc!!.waitFor()
//                Settings.Secure.putString(context.contentResolver,
//                        Settings.Secure.ENABLED_ACCESSIBILITY_SERVICES, pkgName)
                val ai = context!!.packageManager.getApplicationInfo(pkgName, 0)
                val appStatus = ai.enabled
                Log.e("ENABLE ", appStatus.toString())
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }


        fun ClearCache(context: Context, pkgName: String) {
            try {
                var comnd = "m su -c \"rm -rf /data/data/$pkgName/cache/*"
                val proc: Process? = Runtime.getRuntime().exec(comnd)
                proc!!.waitFor()
//                Settings.Secure.putString(context.contentResolver,
//                        Settings.Secure.ENABLED_ACCESSIBILITY_SERVICES, pkgName)
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }

        fun DisableApp(context: Context, pkgName: String) {
            try {
                var comnd = "pm disable $pkgName"
                val proc: Process? = Runtime.getRuntime().exec(comnd)
                proc!!.waitFor()
//                Settings.Secure.putString(context.contentResolver,
//                        Settings.Secure.ENABLED_ACCESSIBILITY_SERVICES, pkgName)
                val ai = context!!.packageManager.getApplicationInfo(pkgName, 0)
                val appStatus = ai.enabled
                Log.e("DisableApp ", appStatus.toString())
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }


    }


}