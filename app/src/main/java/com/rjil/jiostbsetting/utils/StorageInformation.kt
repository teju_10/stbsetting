package com.rjil.jiostbsetting.utils

import android.content.Context
import android.content.pm.IPackageStatsObserver
import android.content.pm.PackageStats
import android.os.RemoteException
import android.util.Log
import com.rjil.jiostbsetting.pojo.AppMemoryDetails
import java.lang.reflect.InvocationTargetException
import java.lang.reflect.Method
import java.text.DecimalFormat

//import android.content.pm.IPackageStatsObserver;

/**
 * This class will perform data operation
 */
class StorageInformation(internal var context: Context) {

    internal var appSize: Long = 0
    internal var cachedData: Long = 0
    internal var downloadSize: Long = 0
    internal lateinit var cAppDetails: AppDetails
    var res: ArrayList<AppDetails.PackageInfoStruct>? = null
    var arrayList = arrayListOf<AppMemoryDetails>()
    var position: Int = 0

    fun getpackageDetails(pName: String): ArrayList<AppMemoryDetails> {
        arrayList = arrayListOf()
        cAppDetails = AppDetails(context)
        res = cAppDetails.packages

        if (res != null)
            for (m in res!!.indices) {

                if (res!![m].pname.equals(pName)) {
                    Log.e("Packagename@@@@: ", res!![m].pname.toString())

                    // Create object to access Package Manager
                    val pm = context.packageManager
                    val getPackageSizeInfo: Method
                    try {
                        getPackageSizeInfo = pm.javaClass.getMethod("getPackageSizeInfo", String::class.java,
                                IPackageStatsObserver::class.java)
                        getPackageSizeInfo.invoke(pm, res!![m].pname,
                                PackageState()) //Call the inner class
                    } catch (e: SecurityException) {
                        e.printStackTrace()
                    } catch (e: NoSuchMethodException) {
                        e.printStackTrace()
                    } catch (e: IllegalArgumentException) {
                        e.printStackTrace()
                    } catch (e: IllegalAccessException) {
                        e.printStackTrace()
                    } catch (e: InvocationTargetException) {
                        e.printStackTrace()
                    }

                    break
                }
            }

        return arrayList
    }

    /*
    * Inner class which will get the data size for application
    * */
    private inner class PackageState : IPackageStatsObserver.Stub() {
        @Throws(RemoteException::class)
        override fun onGetStatsCompleted(pStats: PackageStats, succeeded: Boolean) {
//            arrayList = ArrayList()
            arrayList.add(AppMemoryDetails(
                    pStats.packageName + "",
                    getDynamicSpace(pStats.cacheSize) + "",
                    getDynamicSpace(pStats.dataSize) + "",
                    getDynamicSpace(pStats.cacheSize) + "",
                    getDynamicSpace(pStats.codeSize + pStats.dataSize) + ""))

        }
    }


    fun getpackageSize(): ArrayList<AppMemoryDetails> {
        arrayList = arrayListOf<AppMemoryDetails>()
        cAppDetails = AppDetails(context)
        res = cAppDetails.packages

        if (res != null)
//            return
            for (m in res!!.indices) {
                // Create object to access Package Manager
                val pm = context.packageManager
                val getPackageSizeInfo: Method
                try {
                    getPackageSizeInfo = pm.javaClass.getMethod("getPackageSizeInfo", String::class.java,
                            IPackageStatsObserver::class.java)
                    getPackageSizeInfo.invoke(pm, res!![m].pname,
                            cachePackState()) //Call the inner class
                } catch (e: SecurityException) {
                    e.printStackTrace()
                } catch (e: NoSuchMethodException) {
                    e.printStackTrace()
                } catch (e: IllegalArgumentException) {
                    e.printStackTrace()
                } catch (e: IllegalAccessException) {
                    e.printStackTrace()
                } catch (e: InvocationTargetException) {
                    e.printStackTrace()
                }

            }

        return arrayList
    }

    /*
     * Inner class which will get the data size for application
     * */
    private inner class cachePackState : IPackageStatsObserver.Stub() {
        @Throws(RemoteException::class)
        override fun onGetStatsCompleted(pStats: PackageStats, succeeded: Boolean) {
            cachedData = cachedData + pStats.cacheSize
            appSize = appSize + pStats.codeSize + pStats.dataSize
//            downloadSize = downloadSize + pStats.externalMediaSize

            arrayList.add(AppMemoryDetails(
                    pStats.packageName + "",
                    getDynamicSpace(pStats.cacheSize) + "",
                    getDynamicSpace(pStats.dataSize) + "",
                    getDynamicSpace(cachedData) + "",
                    getDynamicSpace(appSize) + ""))

//            Log.e("Downloads Size", getDynamicSpace(downloadSize) + "")
//            Log.e("Total Cache Size", " " + getDynamicSpace(cachedData))
//            Log.e("App Size", getDynamicSpace(appSize) + "")
        }
    }

    companion object {
        fun getDynamicSpace(diskSpaceUsed: Long): String {
            if (diskSpaceUsed <= 0) {
                return "0 B"
            }
            val units = arrayOf("B", "KB", "MB", "GB", "TB")
            val digitGroups = (Math.log10(diskSpaceUsed.toDouble()) / Math.log10(1024.0)).toInt()
            return (DecimalFormat("#,##0.#").format(diskSpaceUsed / Math.pow(1024.0, digitGroups.toDouble()))
                    + " " + units[digitGroups])
        }
    }

}