package com.rjil.jiostbsetting.utils

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import com.iwedia.fti.rjio.fti.api.AsyncReceiver
import com.iwedia.fti.rjio.fti_sdk.RjioSdk

class ConnectionReceiver : BroadcastReceiver {

    /**
     * Wifi Callback
     */
    private var callback: AsyncReceiver? = null

    /**
     * Constructor
     *
     * @param callback wifi callback
     */
    constructor(callback: AsyncReceiver) {
        this.callback = callback
    }

    constructor() {}

    override fun onReceive(context: Context, intent: Intent) {
        val networkHandler = RjioSdk.get().networkHandler
        if (intent.action == ConnectivityManager.CONNECTIVITY_ACTION) {
            if (callback != null && (networkHandler.isEthernetConnected || networkHandler.isWifiConnected)) {
                networkHandler.requestInternetAvailabilityCheck(callback)
            }
        }
    }
}