package com.rjil.jiostbsetting.utils;

import android.content.Context;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.text.TextUtils;
import android.util.Log;

import com.rjil.jiostbsetting.R;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class WifiConfigHelper {
    private static final String TAG = "WifiConfigHelper";
    private static final boolean DEBUG = true;
    private static final String REGEX_HEX_BSSID = "[a-fA-F0-9]{12}";

    // Allows underscore char to supports proxies that do not
    // follow the spec
    private static final String HC = "a-zA-Z0-9\\_";

    // Matches blank input, ips, and domain names
    private static final String HOSTNAME_REGEXP = "^$|^[" + HC + "]+(\\-[" + HC + "]+)*(\\.[" + HC + "]+(\\-[" + HC + "]+)*)*$";
    private static final Pattern HOSTNAME_PATTERN;
    private static final String EXCLUSION_REGEXP = "$|^(\\*)?\\.?[" + HC + "]+(\\-[" + HC + "]+)*(\\.[" + HC + "]+(\\-[" + HC + "]+)*)*$";
    private static final Pattern EXCLUSION_PATTERN;

    static {
        HOSTNAME_PATTERN = Pattern.compile(HOSTNAME_REGEXP);
        EXCLUSION_PATTERN = Pattern.compile(EXCLUSION_REGEXP);
    }

    public static void setConfigSsid(WifiConfiguration config, String ssid) {
        // if this looks like a BSSID, don't quote it
        if (!Pattern.matches(REGEX_HEX_BSSID, ssid)) {
            config.SSID = convertToQuotedString(ssid);
        } else {
            config.SSID = ssid;
        }
    }

    public static String convertToQuotedString(String string) {
        return "\"" + string + "\"";
    }

    public static void setConfigKeyManagementBySecurity(
            WifiConfiguration config, WifiSecurity security) {
        config.allowedKeyManagement.clear();
        config.allowedAuthAlgorithms.clear();
        switch (security) {
            case NONE:
                config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
                break;
            case WEP:
                config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
                config.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.OPEN);
                config.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.SHARED);
                break;
            case PSK:
                config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
                break;
            case EAP:
                config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_EAP);
                config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.IEEE8021X);
                break;
        }
    }

    public static int validate(String hostname, String port, String exclList) {
        Matcher match = HOSTNAME_PATTERN.matcher(hostname);
        String exclListArray[] = exclList.split(",");

        if (!match.matches()) return R.string.proxy_error_invalid_host;

        for (String excl : exclListArray) {
            Matcher m = EXCLUSION_PATTERN.matcher(excl);
            if (!m.matches()) return R.string.proxy_error_invalid_exclusion_list;
        }

        if (hostname.length() > 0 && port.length() == 0) {
            return R.string.proxy_error_empty_port;
        }

        if (port.length() > 0) {
            if (hostname.length() == 0) {
                return R.string.proxy_error_empty_host_set_port;
            }
            int portVal = -1;
            try {
                portVal = Integer.parseInt(port);
            } catch (NumberFormatException ex) {
                return R.string.proxy_error_invalid_port;
            }
            if (portVal <= 0 || portVal > 0xFFFF) {
                return R.string.proxy_error_invalid_port;
            }
        }
        return 0;
    }

    public static WifiConfiguration getWifiConfiguration(WifiManager wifiManager, int networkId) {
        List<WifiConfiguration> configuredNetworks = wifiManager.getConfiguredNetworks();
        if (configuredNetworks != null) {
            for (WifiConfiguration configuredNetwork : configuredNetworks) {
                if (configuredNetwork.networkId == networkId) {
                    return configuredNetwork;
                }
            }
        }
        return null;
    }

    public static boolean isNetworkSaved(WifiConfiguration config) {
        return config != null && config.networkId > -1;
    }

    public static WifiConfiguration getConfiguration(Context context, String ssid,
                                                     WifiSecurity security) {
        WifiConfiguration config = getFromConfiguredNetworks(context, ssid, security);

        if (config == null) {
            // No configured network found; populate a new one with the provided ssid / security.
            config = new WifiConfiguration();
            setConfigSsid(config, ssid);
            setConfigKeyManagementBySecurity(config, security);
        }
        return config;
    }

    public static boolean saveConfiguration(Context context, WifiConfiguration config) {
        if (config == null) {
            return false;
        }

        WifiManager wifiMan = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        int networkId = wifiMan.addNetwork(config);
        if (networkId == -1) {
            if (DEBUG) Log.e(TAG, "failed to add network: " + config.toString());
            return false;
        }

        if (!wifiMan.enableNetwork(networkId, false)) {
            if (DEBUG) Log.e(TAG, "enable network failed: " + networkId + "; " + config.toString());
            return false;
        }

        if (!wifiMan.saveConfiguration()) {
            if (DEBUG) Log.e(TAG, "failed to save: " + config.toString());
            return false;
        }

        if (DEBUG) Log.d(TAG, "saved network: " + config.toString());
        return true;
    }

    public static void forgetConfiguration(Context context, WifiConfiguration config) {
        if (config == null) {
            return;
        }

        WifiManager wifiMan = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        List<WifiConfiguration> configuredNetworks = wifiMan.getConfiguredNetworks();
        if (configuredNetworks == null) {
            if (DEBUG) Log.e(TAG, "failed to get configured networks");
            return;
        }

        for (WifiConfiguration wc : configuredNetworks) {
            if (wc != null && wc.SSID != null && TextUtils.equals(wc.SSID, config.SSID)) {
                wifiMan.removeNetwork(wc.networkId);
                if (DEBUG) Log.d(TAG, "forgot network config: " + wc.toString());
                break;
            }
        }
    }

    public static void forgetWifiNetwork(Context context) {
        WifiManager mWifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInfo = mWifiManager.getConnectionInfo();
        if (wifiInfo != null) {
            int networkId = wifiInfo.getNetworkId();
            if (networkId != -1) {
                mWifiManager.removeNetwork(networkId);
            }
        }
    }

    private static WifiConfiguration getFromConfiguredNetworks(Context context, String ssid,
                                                               WifiSecurity security) {
        WifiManager wifiMan = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        List<WifiConfiguration> configuredNetworks = wifiMan.getConfiguredNetworks();
        if (configuredNetworks != null) {
            for (WifiConfiguration configuredNetwork : configuredNetworks) {
                if (configuredNetwork == null || configuredNetwork.SSID == null) {
                    continue;  // Does this ever really happen?
                }

                // If the SSID and the security match, that's our network.
                String configuredSsid = removeDoubleQuotes(configuredNetwork.SSID);
                if (TextUtils.equals(configuredSsid, ssid)) {
                    WifiSecurity configuredSecurity = WifiSecurity.getSecurity(configuredNetwork);
                    if (configuredSecurity.equals(security)) {
                        return configuredNetwork;
                    }
                }
            }
        }

        return null;
    }

    public static String removeDoubleQuotes(String string) {
        if (string == null) return null;
        final int length = string.length();
        if ((length > 1) && (string.charAt(0) == '"') && (string.charAt(length - 1) == '"')) {
            return string.substring(1, length - 1);
        }
        return string;
    }

    private WifiConfigHelper() {
    }
}
