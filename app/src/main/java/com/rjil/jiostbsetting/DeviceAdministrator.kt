package com.rjil.jiostbsetting

import android.app.admin.DeviceAdminReceiver
import android.content.Context
import android.widget.Toast
import android.content.Intent

class DeviceAdministrator : DeviceAdminReceiver() {

    override fun onDisabled(context: Context, intent: Intent) {
        super.onDisabled(context, intent)
        Toast.makeText(context, "Device administration has been disabled.", Toast.LENGTH_SHORT).show()
    }

    override fun onEnabled(context: Context, intent: Intent) {
        super.onEnabled(context, intent)
        Toast.makeText(context, "Device administration has been enabled.", Toast.LENGTH_SHORT).show()
    }
}