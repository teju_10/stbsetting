package com.rjil.jiostbsetting

import android.content.Context
import android.net.wifi.WifiManager
import android.os.Bundle
import android.os.Handler
import android.text.TextUtils
import android.util.Log
import android.view.KeyEvent
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.iwedia.fti.rjio.fti.api.AsyncReceiver
import com.iwedia.fti.rjio.fti_sdk.RjioSdk
import com.rjil.jiostbsetting.fragments.NetworkDetailsFragment
import com.rjil.jiostbsetting.main.ProgressDialog
import com.rjil.jiostbsetting.utils.ConnectionReceiver
import com.rjil.jiostbsetting.utils.Constant
import com.rjil.stbwifilibrary.WiFiManager
import com.rjil.stbwifilibrary.listner.OnWifiConnectListener
import java.lang.String
import java.lang.reflect.InvocationTargetException
import java.lang.reflect.Method

class WpsActivity : AppCompatActivity(), OnWifiConnectListener {

    private var toast: Toast? = null
    private var connectionReceiver: ConnectionReceiver? = null
//    var progress: FrameLayout?=null
    var progressDialog: ProgressDialog?=null
    lateinit var loader : kotlin.String
    private var mWiFiManager: WiFiManager? = null
    var position : Int? = 0
    lateinit var SSID : kotlin.String
    lateinit var pass : kotlin.String

    companion object{
        var isnotify = false
        var isWpsCancelled = false
        var isPasswordback = false
//        var isPasswordnotify=false
        val TAG: kotlin.String = WpsActivity.javaClass.simpleName
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_wps)
        progressDialog=findViewById(R.id.progress_dialog)
        mWiFiManager = WiFiManager(this)
        var bundle :Bundle ?=intent.extras
        NetworkDetailsFragment.isRefreshList=false

        if(bundle!=null)
        {
            loader = intent.getStringExtra("loader") as kotlin.String
            if(!loader.equals("Wps"))
            {
                SSID = intent.getStringExtra("SSID") as kotlin.String
            }
        }

        if(loader.equals("SavedNetwork"))
        {
            progressDialog!!.setDialogTitle("Connecting to $SSID")
            position=intent.getIntExtra("position",0)
        }
        else if(loader.equals("OpenNetwork"))
        {
            progressDialog!!.setDialogTitle("Connecting to $SSID")
            position=intent.getIntExtra("position",0)
            mWiFiManager!!.connectOpenNetwork(SSID)
        }
        else if(loader.equals("Password"))
        {
            progressDialog!!.setDialogTitle("Connecting to $SSID")
            pass=intent.getStringExtra("pass") as kotlin.String
            position=intent.getIntExtra("position",0)
            mWiFiManager!!.connectWPA2Network(SSID, pass)
        }
        else if(loader.equals("None"))
        {
            progressDialog!!.setDialogTitle("Connecting to $SSID")
            mWiFiManager!!.connectOpenNetwork(SSID)
        }
        else
        {
            connectToWps()
        }

    }


    override fun onResume() {
        super.onResume()
        mWiFiManager!!.setOnWifiConnectListener(this)
    }

    override fun onPause() {
        super.onPause()
        mWiFiManager!!.removeOnWifiConnectListener()
    }

    private fun connectToWps() {
        if (RjioSdk.get().networkHandler.isEthernetConnected) {
            if (toast == null || !toast!!.view.isShown) {
                toast = Toast.makeText(this, R.string.no_internt_access, Toast.LENGTH_LONG)
                toast!!.show()
            }
        } else {
            progressDialog!!.showProgressDialog()
            progressDialog!!.setDialogTitle(R.string.searching_for_wps_router)
            progressDialog!!.setDialogMessage(String.format("%s\n%s", getString(R.string.press_wps_button), getString(R.string.press_wps_button_hint)))
            progressDialog!!.setDialogIcon(R.drawable.ic_wifi_wps_in_progress)
            progressDialog!!.requestFocus()
            progressDialog!!.setOnKeyListener { view, i, keyEvent ->
                if (i == KeyEvent.KEYCODE_BACK) {
//                    progressDialog!!.hideWps()
                    RjioSdk.get().networkHandler.cancleWps(object : AsyncReceiver {
                        override fun onSuccess() {
                            isWpsCancelled=true
                        }

                        override fun onFailed() {

                        }
                    })
                }
                false
            }

            RjioSdk.get().networkHandler.connectWpsByButton(object : AsyncReceiver {
                override fun onSuccess() {
                    onWpsConnectionFinish(true)
                }

                override fun onFailed() {
                    runOnUiThread(Runnable {
//                        finish()
                                                progressDialog!!.hideWps()
                    })
                    onWpsConnectionFinish(false)
                }
            })
        }
    }

    private fun onWpsConnectionFinish(isConnected: Boolean) {
        runOnUiThread {
            if (!RjioSdk.get().networkHandler.isEthernetConnected) {

                progressDialog!!.hideProgressDialog()
                Toast.makeText(this,
                        getString(if (isConnected) R.string.wps_connected else R.string.wps_failed),
                        Toast.LENGTH_SHORT).show()
                if (isConnected) {
                    if (connectionReceiver != null) unregisterReceiver(connectionReceiver)
                    connectionReceiver = null
//                    Toast.makeText(context,"Connected",Toast.LENGTH_SHORT).show()
                    /*val successActivity = Intent(context, FtiWifiSuccessActivity::class.java)
                    startActivity(successActivity)
                    finish()*/
                } else {
                    RjioSdk.get().networkHandler.cancleWps(object : AsyncReceiver {
                        override fun onSuccess() {}
                        override fun onFailed() {}
                    })
                    if (connectionReceiver != null) unregisterReceiver(connectionReceiver)
                    connectionReceiver = null
                }
            } else {
                Toast.makeText(this,
                        getString(R.string.please_remove_eth_cable), Toast.LENGTH_SHORT).show()
            }
            progressDialog!!.hideProgressDialog()
            finish()
            window.exitTransition=null
//            overridePendingTransition(0, 0)
        }
    }

    fun forgetWifiNetwork(context: Context?, ssid: kotlin.String) {
        val mWifiManager = context!!.getSystemService(Context.WIFI_SERVICE) as WifiManager
//        val wifiInfo: WifiInfo? = mWifiManager.connectionInfo
        val list = mWifiManager.configuredNetworks

        for (network in list) {

            if(network!=null && !TextUtils.isEmpty(network.SSID))
            {
                Log.d(TAG,"NetworkSSID${network.SSID} && SSID=$ssid")
                if(network.SSID==ssid)
                {
                    Log.d(TAG,"Condition true")
                    val networkId = network.networkId
                    if (networkId != -1) {
                        var method: Method? = null
                        try {
                            method = mWifiManager.javaClass.getMethod("removeNetwork", Int::class.java)
                            method.invoke(mWifiManager, networkId)
                        } catch (e: NoSuchMethodException) {
                            e.printStackTrace()
                        } catch (e: IllegalAccessException) {
                            e.printStackTrace()
                        } catch (e: InvocationTargetException) {
                            e.printStackTrace()
                        }

                    }
                }
            }
        }
    }

    override fun onBackPressed() {
        if(loader.equals("Wps"))
        {
            RjioSdk.get().networkHandler.cancleWps(object : AsyncReceiver {
                override fun onSuccess() {
                    runOnUiThread(Runnable {
                        isWpsCancelled=true
                    })
                }

                override fun onFailed() {
                    runOnUiThread(Runnable { })
                }
            })
        }

        Handler().postDelayed({
            super.onBackPressed()
        },1000)

    }


    override fun onWiFiConnectLog(log: kotlin.String?) {

    }

    override fun onWiFiConnectSuccess(SSID: kotlin.String?) {
        Toast.makeText(this, "$SSID  Connected Successfully!!", Toast.LENGTH_SHORT).show()
        if(loader=="Password")
        {
            isnotify=true
            isPasswordback=true
            Constant.lastselectedposition=position
        }
        else if(loader=="SavedNetwork" || loader=="OpenNetwork")
        {
            isnotify=true
            Constant.lastselectedposition=position
        }


        Handler().postDelayed(Runnable {
            onBackPressed()
        },1000)
    }

    override fun onWiFiConnectFailure(SSID: kotlin.String?) {

        if(loader=="Password")
        {
            Toast.makeText(this, "Invalid Password", Toast.LENGTH_SHORT).show()
            isPasswordback=false
//            isPasswordnotify=false
            isnotify=false
            forgetWifiNetwork(this,SSID!!)
            Handler().postDelayed({
                onBackPressed()
            },100)
        }
        else
            Toast.makeText(this, "Connection Failed", Toast.LENGTH_SHORT).show()


        if(loader=="SavedNetwork" || loader=="OpenNetwork")
            isnotify=false

        if(loader!="Password")
        {
            Handler().postDelayed(Runnable {
                onBackPressed()
            },1000)
        }
    }




}
