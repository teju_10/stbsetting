package com.rjil.jiostbsetting

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class ControlPanelQrCodeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activtiy_control_panel_activation)
        val okBtn = findViewById<Button>(R.id.done)
        okBtn.setOnClickListener{
            finish()
        }
    }
}
