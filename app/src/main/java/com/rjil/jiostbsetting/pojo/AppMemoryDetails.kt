package com.rjil.jiostbsetting.pojo

class AppMemoryDetails {

    var packageName: String
    var cacheSize: String
    var dataSize: String
    var cachedData: String
    var appSize: String

    constructor(packageName: String, cacheSize: String, dataSize: String, cachedData: String, appSize: String) {
        this.cacheSize = cacheSize
        this.dataSize = dataSize
        this.packageName = packageName
        this.cachedData = cachedData
        this.appSize = appSize
    }

    override fun toString(): String {
        return "AppMemoryDetails(cacheSize='$cacheSize', dataSize='$dataSize',cachedData='$cachedData',appSize='$appSize', packageName='$packageName')"
    }

}
