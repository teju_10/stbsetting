package com.rjil.stbwifilibrary.listner;

/**
 * Created by Rahul1.Ranjan on 03-04-2017.
 */
public interface OnWifiEnabledListener {

    void onWifiEnabled(boolean enabled);
}
