package com.rjil.stbwifilibrary.listner;

/**
 * Created by Rahul1.Ranjan on 03-04-2017.
 */
public interface OnWifiConnectListener {

    void onWiFiConnectLog(String log);

    void onWiFiConnectSuccess(String SSID);

    void onWiFiConnectFailure(String SSID);
}
