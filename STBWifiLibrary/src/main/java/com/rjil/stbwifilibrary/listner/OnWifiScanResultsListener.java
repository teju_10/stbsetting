package com.rjil.stbwifilibrary.listner;

import android.net.wifi.ScanResult;

import java.util.List;

/**
 * Created by Rahul1.Ranjan on 03-04-2017.
 */
public interface OnWifiScanResultsListener {

    void onScanResults(List<ScanResult> scanResults);
}
