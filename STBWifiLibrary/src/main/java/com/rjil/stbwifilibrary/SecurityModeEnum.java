package com.rjil.stbwifilibrary;

/**
 * Created by Rahul1.Ranjan on 03-04-2017.
 */

public enum SecurityModeEnum {
    OPEN, WEP, WPA, WPA2
}
