package com.rjil.stbwifilibrary;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.ScanResult;
import android.net.wifi.SupplicantState;
import android.net.wifi.WifiInfo;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import androidx.annotation.NonNull;

import com.rjil.stbwifilibrary.listner.OnWifiConnectListener;
import com.rjil.stbwifilibrary.listner.OnWifiEnabledListener;
import com.rjil.stbwifilibrary.listner.OnWifiScanResultsListener;

import java.util.List;

/**
 * Created by Rahul1.Ranjan on 03-04-2017.
 */

public class WiFiManager extends BaseWiFiManager {

    private static final String TAG = "WiFiManager";
    private static CallBackHandler mCallBackHandler;
    private static final int WIFI_STATE_ENABLED = 0;
    private static final int WIFI_STATE_DISABLED = 1;
    private static final int SCAN_RESULTS_UPDATED = 3;
    private static final int WIFI_CONNECT_LOG = 4;
    private static final int WIFI_CONNECT_SUCCESS = 5;
    private static final int WIFI_CONNECT_FAILURE = 6;

    public WiFiManager(Context context) {
        super(context);

        mCallBackHandler = new CallBackHandler();
    }

    public void openWiFi() {
        if (!isWifiEnabled() && null != mWifiManager) {
            mWifiManager.setWifiEnabled(true);
        }
    }

    public void closeWiFi() {
        if (isWifiEnabled() && null != mWifiManager) {
            mWifiManager.setWifiEnabled(false);
        }
    }

    public boolean connectOpenNetwork(@NonNull String ssid) {
        int networkId = setOpenNetwork(ssid);
        if (-1 != networkId) {
            boolean isSave = saveConfiguration();
            boolean isEnable = enableNetwork(networkId);
            return isSave && isEnable;
        }
        return false;
    }

    public boolean connectWEPNetwork(@NonNull String ssid, @NonNull String password) {
        int networkId = setWEPNetwork(ssid, password);
        if (-1 != networkId) {
            boolean isSave = saveConfiguration();
            boolean isEnable = enableNetwork(networkId);
            return isSave && isEnable;
        }
        return false;
    }

    public boolean connectWPA2Network(@NonNull String ssid, @NonNull String password) {
        int networkId = setWPA2Network(ssid, password);
        if (-1 != networkId) {
            boolean isSave = saveConfiguration();
            boolean isEnable = enableNetwork(networkId);
            return isSave && isEnable;
        }
        return false;
    }


    public static class NetworkBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {

            android.net.wifi.WifiManager wifiManager = (android.net.wifi.WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);

            switch (intent.getAction()) {
                case android.net.wifi.WifiManager.WIFI_STATE_CHANGED_ACTION:
                    switch (intent.getIntExtra(android.net.wifi.WifiManager.EXTRA_WIFI_STATE, android.net.wifi.WifiManager.WIFI_STATE_UNKNOWN)) {
                        case android.net.wifi.WifiManager.WIFI_STATE_ENABLING:
                            Log.i(TAG, "onReceive: enabling WIFI...");
                            break;
                        case android.net.wifi.WifiManager.WIFI_STATE_ENABLED:
                            Log.i(TAG, "onReceive: WIFI state enabled");
                            if(mCallBackHandler!=null)
                                mCallBackHandler.sendEmptyMessage(WIFI_STATE_ENABLED);
                            break;
                        case android.net.wifi.WifiManager.WIFI_STATE_DISABLING:
                            Log.i(TAG, "onReceive: disabling WIFI...");
                            break;
                        case android.net.wifi.WifiManager.WIFI_STATE_DISABLED:
                            Log.i(TAG, "onReceive: WIFI state disabled");
                            if(mCallBackHandler!=null)
                                mCallBackHandler.sendEmptyMessage(WIFI_STATE_DISABLED);
                            break;
                        case android.net.wifi.WifiManager.WIFI_STATE_UNKNOWN:
                        default:
                            Log.i(TAG, "onReceive: WIFI staus unknown!");
                            break;
                    }
                    break;
                case android.net.wifi.WifiManager.SCAN_RESULTS_AVAILABLE_ACTION:
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                        boolean isUpdated = intent.getBooleanExtra(android.net.wifi.WifiManager.EXTRA_RESULTS_UPDATED, false);
                        Log.i(TAG, "onReceive: WIFI Scan  " + (isUpdated ? "Complete" : "Incomplete"));
                    } else {
                        Log.i(TAG, "onReceive: WIFI scan is complete");
                    }

                    Message scanResultsMessage = Message.obtain();
                    scanResultsMessage.what = SCAN_RESULTS_UPDATED;
                    scanResultsMessage.obj = wifiManager.getScanResults();
                    if(mCallBackHandler!=null)
                        mCallBackHandler.sendMessage(scanResultsMessage);
                    break;
                case android.net.wifi.WifiManager.NETWORK_STATE_CHANGED_ACTION:
//                    Log.i(TAG, "onReceive: WIFI connection status has changed");
//                    NetworkInfo networkInfo = intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
//                    if (null != networkInfo && ConnectivityManager.TYPE_WIFI == networkInfo.getType()) {
//                    }
                    WifiInfo wifiInfo = intent.getParcelableExtra(android.net.wifi.WifiManager.EXTRA_WIFI_INFO);
                    if (null != wifiInfo && wifiInfo.getSupplicantState() == SupplicantState.COMPLETED) {
                        String ssid = wifiInfo.getSSID();
                        Log.i(TAG, "onReceive: The network connection is successful ssid = " + ssid);
                    }
                    break;
                case android.net.wifi.WifiManager.SUPPLICANT_CONNECTION_CHANGE_ACTION:
                    boolean isConnected = intent.getBooleanExtra(android.net.wifi.WifiManager.EXTRA_SUPPLICANT_CONNECTED, false);
                    Log.i(TAG, "onReceive: SUPPLICANT_CONNECTION_CHANGE_ACTION  isConnected = " + isConnected);
                    break;
                case android.net.wifi.WifiManager.SUPPLICANT_STATE_CHANGED_ACTION:
                    SupplicantState supplicantState = intent.getParcelableExtra(android.net.wifi.WifiManager.EXTRA_NEW_STATE);

                    Message logMessage = Message.obtain();
                    logMessage.what = WIFI_CONNECT_LOG;
                    logMessage.obj = supplicantState.toString();
                    logMessage.obj = supplicantState.toString();
                    if(mCallBackHandler!=null)
                        mCallBackHandler.sendMessage(logMessage);

                    switch (supplicantState) {
                        case INTERFACE_DISABLED:
                            Log.i(TAG, "onReceive: INTERFACE_DISABLED");
                            break;
                        case DISCONNECTED:
//                            Log.i(TAG, "onReceive: DISCONNECTED:// Disconnected");
//                            break;
                        case INACTIVE:
                            WifiInfo connectFailureInfo = wifiManager.getConnectionInfo();
                            Log.i(TAG, "onReceive: INACTIVE  connectFailureInfo = " + connectFailureInfo);
                            if (null != connectFailureInfo) {
                                Message wifiConnectFailureMessage = Message.obtain();
                                wifiConnectFailureMessage.what = WIFI_CONNECT_FAILURE;
                                wifiConnectFailureMessage.obj = connectFailureInfo.getSSID();
                                if(mCallBackHandler!=null)
                                    mCallBackHandler.sendMessage(wifiConnectFailureMessage);
                                int networkId = connectFailureInfo.getNetworkId();
                                boolean isDisable = wifiManager.disableNetwork(networkId);
                                boolean isDisconnect = wifiManager.disconnect();
                                Log.i(TAG, "onReceive: Disonnected  =  " + (isDisable && isDisconnect));
                            }
                            break;
                        case SCANNING:
                            Log.i(TAG, "onReceive: SCANNING");
                            break;
                        case AUTHENTICATING:
                            Log.i(TAG, "onReceive: AUTHENTICATING: // Authenticating");
                            break;
                        case ASSOCIATING:
                            Log.i(TAG, "onReceive: ASSOCIATING: // Associating");
                            break;
                        case ASSOCIATED:
                            Log.i(TAG, "onReceive: ASSOCIATED: // Associated");
                            break;
                        case FOUR_WAY_HANDSHAKE:
                            Log.i(TAG, "onReceive: FOUR_WAY_HANDSHAKE:");
                            break;
                        case GROUP_HANDSHAKE:
                            Log.i(TAG, "onReceive: GROUP_HANDSHAKE:");
                            break;
                        case COMPLETED:
                            Log.i(TAG, "onReceive: WIFI_CONNECT_SUCCESS: // Carry Out");
                            WifiInfo connectSuccessInfo = wifiManager.getConnectionInfo();
                            if (null != connectSuccessInfo) {
                                Message wifiConnectSuccessMessage = Message.obtain();
                                wifiConnectSuccessMessage.what = WIFI_CONNECT_SUCCESS;
                                wifiConnectSuccessMessage.obj = connectSuccessInfo.getSSID();
                                if(mCallBackHandler!=null)
                                    mCallBackHandler.sendMessage(wifiConnectSuccessMessage);
                            }
                            break;
                        case DORMANT:
                            Log.i(TAG, "onReceive: DORMANT:");
                            break;
                        case UNINITIALIZED:
                            Log.i(TAG, "onReceive: UNINITIALIZED: // Uninitialized");
                            break;
                        case INVALID:
                            Log.i(TAG, "onReceive: INVALID: // Invalid");
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }
        }
    }


    private static class CallBackHandler extends Handler {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case WIFI_STATE_ENABLED:
                    if (null != mOnWifiEnabledListener) {
                        mOnWifiEnabledListener.onWifiEnabled(true);
                    }
                    break;
                case WIFI_STATE_DISABLED:
                    if (null != mOnWifiEnabledListener) {
                        mOnWifiEnabledListener.onWifiEnabled(false);
                    }
                    break;
                case SCAN_RESULTS_UPDATED:
                    if (null != mOnWifiScanResultsListener) {
                        @SuppressWarnings("unchecked")
                        List<ScanResult> scanResults = (List<ScanResult>) msg.obj;
                        mOnWifiScanResultsListener.onScanResults(scanResults);
                    }
                    break;
                case WIFI_CONNECT_LOG:
                    if (null != mOnWifiConnectListener) {
                        String log = (String) msg.obj;
                        mOnWifiConnectListener.onWiFiConnectLog(log);
                    }
                    break;
                case WIFI_CONNECT_SUCCESS:
                    if (null != mOnWifiConnectListener) {
                        String ssid = (String) msg.obj;
                        mOnWifiConnectListener.onWiFiConnectSuccess(ssid);
                    }
                    break;
                case WIFI_CONNECT_FAILURE:
                    if (null != mOnWifiConnectListener) {
                        String ssid = (String) msg.obj;
                        mOnWifiConnectListener.onWiFiConnectFailure(ssid);
                    }
                    break;
                default:
                    break;
            }
        }
    }

    private static OnWifiEnabledListener mOnWifiEnabledListener;

    private static OnWifiScanResultsListener mOnWifiScanResultsListener;

    private static OnWifiConnectListener mOnWifiConnectListener;

    public void setOnWifiEnabledListener(OnWifiEnabledListener listener) {
        mOnWifiEnabledListener = listener;
    }

    public void removeOnWifiEnabledListener() {
        mOnWifiEnabledListener = null;
    }

    public void setOnWifiScanResultsListener(OnWifiScanResultsListener listener) {
        mOnWifiScanResultsListener = listener;
    }

    public void removeOnWifiScanResultsListener() {
        mOnWifiScanResultsListener = null;
    }

    public void setOnWifiConnectListener(OnWifiConnectListener listener) {
        mOnWifiConnectListener = listener;
    }

    public void removeOnWifiConnectListener() {
        mOnWifiConnectListener = null;
    }
}

