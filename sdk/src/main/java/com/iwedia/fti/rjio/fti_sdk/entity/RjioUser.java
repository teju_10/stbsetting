package com.iwedia.fti.rjio.fti_sdk.entity;

import android.graphics.drawable.Drawable;

import com.iwedia.fti.rjio.fti.entities.User;

/**
 * Rjio User Item
 *
 * @author Nikola Aleksic
 */
public class RjioUser extends User {

    /**
     * Profile image
     */
    private Drawable image;

    /**
     * Constructor
     *
     * @param id       id
     * @param username username
     * @param password password
     * @param image    image
     */
    public RjioUser(int id, String username, String password, Drawable image) {
        super(id, username, password);
        this.image = image;
    }

    /**
     * Get image
     *
     * @return profile image
     */
    public Drawable getImage() {
        return image;
    }

}
