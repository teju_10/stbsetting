package com.iwedia.fti.rjio.fti_sdk.entity;

import com.iwedia.fti.rjio.fti.entities.WiFiItem;

/**
 * Rjio wifi item
 *
 * @author Nikola Aleksic
 */
public class RjioWiFiItem extends WiFiItem {

    /**
     * Constructor
     *
     * @param id          wifi id
     * @param name        wifi name
     * @param capabilities wifi capabilities
     */
    public RjioWiFiItem(int id, String name, String capabilities) {
        super(id, name, capabilities);
    }
}
