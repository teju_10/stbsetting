package com.iwedia.fti.rjio.fti_sdk.handler;

import com.iwedia.fti.rjio.fti.api.AsyncReceiver;
import com.iwedia.fti.rjio.fti.handler.UserProfileHandler;
import com.iwedia.fti.rjio.fti_sdk.entity.RjioUser;

/**
 * Rjio user profile handler
 *
 * @author Dragan Krnjaic
 */
public class RjioUserProfileHandler extends UserProfileHandler<RjioUser> {

    @Override
    public void requestUserData(RjioUser user, AsyncReceiver callback) {
        this.user = user;

        if(callback != null) callback.onSuccess();
    }
}
