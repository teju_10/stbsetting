package com.iwedia.fti.rjio.fti_sdk.handler;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;

import com.iwedia.fti.rjio.fti.api.AsyncDataReceiver;
import com.iwedia.fti.rjio.fti.api.AsyncReceiver;
import com.iwedia.fti.rjio.fti.handler.BluetoothHandler;
import com.iwedia.fti.rjio.fti_sdk.RjioSdk;
import com.iwedia.fti.rjio.fti_sdk.entity.RjioBluetoothDevice;

import java.lang.reflect.Method;
import java.util.Set;

/**
 * Rjio bluetooth handler
 *
 * @author Dejan Nadj
 */
public class RjioBluetoothHandler extends BluetoothHandler<RjioBluetoothDevice> {

    /**
     * Is bluetooth supported
     */
    private boolean isBluetoothSupported;

    private BluetoothManager mBluetoothManager = null;
    private BluetoothAdapter mBluetoothAdapter = null;

    public RjioBluetoothHandler(Context context) {
        isBluetoothSupported = RjioSdk.get().getContext().getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH);

        mBluetoothManager = (BluetoothManager) context.getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = mBluetoothManager.getAdapter();

    }

    public void unpairDevice(RjioBluetoothDevice bluetoothDevice, AsyncReceiver callback) {

        if (callback == null) {
            throw new IllegalArgumentException("Callback is null!");
        }
        if (!isBluetoothSupported) {

            callback.onSuccess();
            return;
        }
        Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
        for (BluetoothDevice bt : pairedDevices) {
            removeBond(bt);
        }

        if(pairedDevices.isEmpty()) {
            callback.onSuccess();
        }
        else{
            callback.onFailed();
        }
    }

    private void removeBond(BluetoothDevice device) {
        try {
            Method method = device.getClass()
                    .getMethod("removeBond", (Class[]) null);
            method.invoke(device, (Object[]) null);
        } catch (Exception e) {
        }
    }

    /**
     * Is remote controll device connected
     *
     * @param callback  callback
     */
    public void isRCUConnected(AsyncDataReceiver<Intent> callback) {
        if (callback == null) {
            throw new IllegalArgumentException("Callback is null!");
        }
        if (!isBluetoothSupported) {
            callback.onFailed();
            return;
        }
        Set<BluetoothDevice> pairedDevices = BluetoothAdapter.getDefaultAdapter().getBondedDevices();

        if (pairedDevices.size() > 0) {
            // There are paired devices. Check the device name matches with RCU name.
            for (BluetoothDevice device : pairedDevices) {
                if(device.getType() == BluetoothDevice.DEVICE_TYPE_LE){
                    Intent intent = new Intent();
                    intent.putExtra("Name", device.getName());
                    intent.putExtra("Address", device.getAddress());
                    callback.onSuccess(intent);
                    return;
                }
            }
        }

        callback.onFailed();
    }
}
