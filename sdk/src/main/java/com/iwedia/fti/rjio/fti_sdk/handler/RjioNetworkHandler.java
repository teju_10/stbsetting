package com.iwedia.fti.rjio.fti_sdk.handler;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.net.wifi.WpsInfo;
import android.util.Log;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.List;

import com.iwedia.fti.rjio.fti.api.AsyncDataReceiver;
import com.iwedia.fti.rjio.fti.api.AsyncReceiver;
import com.iwedia.fti.rjio.fti.handler.NetworkHandler;
import com.iwedia.fti.rjio.fti.utils.information_bus.InformationBus;
import com.iwedia.fti.rjio.fti.utils.information_bus.events.NetworkConnectionChangedEvent;
import com.iwedia.fti.rjio.fti_sdk.entity.RjioEthernetEntity;
import com.iwedia.fti.rjio.fti_sdk.entity.RjioWiFiItem;

/**
 * Rjio network handler
 *
 * @author Nikola Aleksic
 */
public class RjioNetworkHandler extends NetworkHandler<RjioWiFiItem> {

    /**
     * Ethernet entity
     */
    private RjioEthernetEntity ethernetEntity;

    /**
     * Context
     */
    private Context context;

    /**
     * WiFi manager
     */
    private WifiManager wifiManager;

    /**
     * Constructor
     *
     * @param context application context
     */
    public RjioNetworkHandler(Context context) {
        this.context = context;
        ethernetEntity = new RjioEthernetEntity();

        //Register connection receiver
        IntentFilter intentFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        context.registerReceiver(new ConnectionReceiver(), intentFilter);

        //Connect to the utility service
        RjioUtilityServiceHandler.getInstance().connect(context);

        wifiManager = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);

        requestWifiScan();
    }

    @Override
    public void requestNetworks(AsyncDataReceiver<List<RjioWiFiItem>> callback) {
        RjioUtilityServiceHandler.getInstance().startWiFiSearch(callback);
    }

    @Override
    public void connectToWiFi(final String wifiName, final String wifiPassword, final String capabilities, final AsyncReceiver callback) {
        RjioUtilityServiceHandler.getInstance().connectToWiFi(wifiName, wifiPassword, capabilities, callback);
    }

    @Override
    public void setIPEthernet(String ipAddress, AsyncReceiver callback) {
        //todo check if ip address is valid
        ethernetEntity.setIpAddress(ipAddress);
        callback.onSuccess();
    }

    @Override
    public void setMaskEthernet(String maskAddress, AsyncReceiver callback) {
        //todo check if mask address is valid
        ethernetEntity.setMask(maskAddress);
        callback.onSuccess();
    }

    @Override
    public void setGatewayEthernet(String gatewayAddress, AsyncReceiver callback) {
        //todo check if gateway address is valid
        ethernetEntity.setGateway(gatewayAddress);
        callback.onSuccess();
    }

    @Override
    public void setDNSEthernet(String dnsAddress, AsyncReceiver callback) {
        //todo check if dns address is valid
        ethernetEntity.setDns1(dnsAddress);
        RjioUtilityServiceHandler.getInstance().setDNSEthernet(ethernetEntity, callback);
    }

    /**
     * Is ethernet connected
     *
     * @return
     */
    public boolean isEthernetConnected() {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = null;
        if (connectivityManager != null) {
            networkInfo = connectivityManager.getActiveNetworkInfo();
        }
        if (networkInfo != null && networkInfo.getType() == ConnectivityManager.TYPE_ETHERNET && networkInfo.isConnected()) {
            return true;
        }
        return false;
    }

    /**
     * Check if device has wifi connection
     *
     * @return is wifi connected
     */
    public boolean isWifiConnected() {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = null;
        if (connectivityManager != null) {
            networkInfo = connectivityManager.getActiveNetworkInfo();
        }
        if (networkInfo != null && networkInfo.getType() == ConnectivityManager.TYPE_WIFI && networkInfo.isConnected()) {
            return true;
        }
        return false;
    }

    /**
     * Connect to Wi-Fi with WPS
     *
     * @param callback
     */
    public void connectWpsByButton(final AsyncReceiver callback){
        WpsInfo wpsInfo = new WpsInfo();
        wpsInfo.setup = WpsInfo.PBC;
        wifiManager.startWps(wpsInfo, new WifiManager.WpsCallback() {
            @Override
            public void onStarted(String pin) {
            }

            @Override
            public void onSucceeded() {
                callback.onSuccess();
            }

            @Override
            public void onFailed(int reason) {
                callback.onFailed();
            }
        });
    }

    public void cancleWps(final AsyncReceiver callback){
        wifiManager.cancelWps(new WifiManager.WpsCallback() {
            @Override
            public void onStarted(String pin) {
            }

            @Override
            public void onSucceeded() {
                callback.onSuccess();
            }

            @Override
            public void onFailed(int reason) {
                callback.onFailed();
            }
        });
    }


    /**
     * Check if device has internet available (blocking call)
     */
    public boolean isInternetAvailable() {
        try (Socket socket = new Socket()) {
            socket.connect(new InetSocketAddress("www.google.com", 80), 2000);
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean isOnline() {
        Runtime runtime = Runtime.getRuntime();
        try {
            Process ipProcess = runtime.exec("/system/bin/ping -c 1 -w 1000 8.8.8.8");
            int exitValue = ipProcess.waitFor();
            return (exitValue == 0);
        }
        catch (IOException e)          { e.printStackTrace(); }
        catch (InterruptedException e) { e.printStackTrace(); }

        return false;
    }

     public boolean isWifiOnline() {
         ConnectivityManager cm =
                 (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);

         NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
         boolean isConnected = activeNetwork != null &&
                 activeNetwork.isConnectedOrConnecting();
         if(isConnected)
             return true;
         else
             return false;

    }




    /**
     * Start internet availability check
     */
    public void requestInternetAvailabilityCheck(final AsyncReceiver callback) {
        if (callback != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (isOnline()) {
                        callback.onSuccess();
                    } else {
                        callback.onFailed();
                    }
                }
            }).start();
        }
    }

    /**
     * Request wifi scan
     */
    public void requestWifiScan() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                wifiManager.setWifiEnabled(true);
                wifiManager.startScan();
            }
        }).start();
    }

    public void requestWifiDisconnect(String ssid, final AsyncReceiver callback) {
        List<WifiConfiguration> list = wifiManager.getConfiguredNetworks();
        for (int i = 0; i < list.size(); i++) {
            WifiConfiguration conf = list.get(i);
            if (conf.SSID != null && conf.SSID.replaceAll("\"", "").equals(ssid)) {
                final int id = conf.networkId;
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        wifiManager.disconnect();
                        wifiManager.removeNetwork(id);
                        RjioUtilityServiceHandler.getInstance().forgetWifiNetwork(id, new AsyncReceiver() {
                            @Override
                            public void onSuccess() {
                                wifiManager.saveConfiguration();
                                wifiManager.reconnect();
                                if (callback != null) {
                                    callback.onSuccess();
                                }
                            }

                            @Override
                            public void onFailed() {

                            }
                        });

                    }
                }).start();
            }
        }
    }

    public String getWiFiName() {
        return wifiManager.getConnectionInfo().getSSID();
    }

    /**
     * Connection receiver
     * Detects network connected/disconnected event
     */
    private class ConnectionReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction() == ConnectivityManager.CONNECTIVITY_ACTION) {

                ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
                boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
                boolean isWiFi = activeNetwork != null && activeNetwork.getType() == ConnectivityManager.TYPE_WIFI;
                boolean isEthernet = activeNetwork != null && activeNetwork.getType() == ConnectivityManager.TYPE_ETHERNET;

                if (isConnected) {
                    if (isWiFi) {
                        InformationBus.getInstance().submitEvent(new NetworkConnectionChangedEvent(true));
                    } else if (isEthernet) {
                        InformationBus.getInstance().submitEvent(new NetworkConnectionChangedEvent(true));
                    }
                } else {
                    InformationBus.getInstance().submitEvent(new NetworkConnectionChangedEvent(false));
                }
            }
        }
    }
}
