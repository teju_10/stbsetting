package com.iwedia.fti.rjio.fti_sdk.handler;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.ArrayMap;
import android.util.Log;

import com.iwedia.fti.rjio.fti.api.AsyncDataReceiver;
import com.iwedia.fti.rjio.fti.api.AsyncReceiver;
import com.iwedia.fti.rjio.fti_sdk.RjioSdk;
import com.iwedia.fti.rjio.fti_sdk.entity.RjioEthernetEntity;
import com.iwedia.fti.rjio.fti_sdk.entity.RjioWiFiItem;
import com.iwedia.utility.IUtilityService;
import com.iwedia.utility.bluetooth.IBluetoothManager;
import com.iwedia.utility.language.ILanguageManager;
import com.iwedia.utility.network.INetworkManager;
import com.iwedia.utility.settings.ISettingsManager;
import com.iwedia.utility.power.ILauncherPowerManager;
import com.iwedia.utility.language.Language;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Rjio utility service handler
 *
 * @author Dejan Nadj
 */
public class RjioUtilityServiceHandler {
    ServiceConnection serviceConnection;
    /**
     * Utility service package name
     */
    private static final String SERVICE_PACKAGE_NAME = "com.iwedia.utility";

    /**
     * Utility service class name
     */
    private static final String SERVICE_CLASS_NAME = "com.iwedia.utility.UtilityService";

    /**
     * Application context
     */
    private Context context;

    /**
     * Utility service
     */
    private IUtilityService utilityService;

    /**
     * Network manager
     */
    private INetworkManager networkManager;

    /**
     * Settings manager
     */
    private ISettingsManager settingsManager;

    /**
     * Power manager
     */
    private ILauncherPowerManager launcherPowerManager;

    /**
     * Bluetooth manager
     */
    private IBluetoothManager bluetoothManager;

    /**
     * Location manager
     */
    private ILanguageManager languageManager;

    /**
     * RjioUtilityService singleton instance
     */
    private static RjioUtilityServiceHandler instance;

    private List<Language> languageList = new ArrayList<>();

    /**
     * Constructor
     */
    private RjioUtilityServiceHandler() {
    }

    /**
     * Get RjioUtilityService instance
     *
     * @return singleton class instance
     */
    public static RjioUtilityServiceHandler getInstance() {
        if (instance == null) {
            instance = new RjioUtilityServiceHandler();
        }
        return instance;
    }

    /**
     * Connect to the utility service
     *
     * @param context application context
     */
    public void connect(Context context) {
        try {
            this.context = context;

            serviceConnection = new ServiceConnection() {
                @Override
                public void onServiceConnected(ComponentName componentName, IBinder iBinder) {

                    utilityService = IUtilityService.Stub.asInterface(iBinder);
                    try {
                        networkManager = utilityService.getNetworkManager();
                        settingsManager = utilityService.getSettingsManager();
                        launcherPowerManager = utilityService.getPowerManager();
                        bluetoothManager = utilityService.getBluetoothManager();
                        languageManager = utilityService.getLanguageManager();

                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onServiceDisconnected(ComponentName componentName) {
                    utilityService = null;
                }
            };

            Intent intent = new Intent();
//        intent.setPackage(SERVICE_PACKAGE_NAME);
            intent.setComponent(new ComponentName(SERVICE_PACKAGE_NAME, SERVICE_CLASS_NAME));
            context.bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void unBindService(Context cx) {
        this.context = cx;
        context.unbindService(serviceConnection);

    }

    /**
     * Is Wifi network duplicated in list
     *
     * @param Ssid
     * @param wifiNetworks
     * @return
     */

    private boolean isWifiDuplicated(String Ssid, ArrayList<RjioWiFiItem> wifiNetworks) {
        for (RjioWiFiItem currentWifi : wifiNetworks) {
            if (currentWifi.getSsid().equals(Ssid))
                return true;
        }
        return false;
    }

    /**
     * Start wifi search
     *
     * @param callback callback
     */
    public void startWiFiSearch(AsyncDataReceiver<List<RjioWiFiItem>> callback) {
        if (utilityService == null || networkManager == null) {
            callback.onFailed();
            return;
        }
        ArrayList<RjioWiFiItem> networks = new ArrayList<>();
        try {
            List<ScanResult> list = networkManager.getWifiScanResults();
            WifiInfo wifiInfo = networkManager.getConnectionInfo();

            if (list != null)
                for (ScanResult result : list) {
                    int i = 0;
                    boolean isConnected = wifiInfo.getSSID().equals(result.SSID) ? true : false;

                    if (result.SSID != null && !result.SSID.trim().isEmpty() && !isWifiDuplicated(result.SSID, networks))
                        networks.add(new RjioWiFiItem(i++, result.SSID, result.capabilities));
                }

            callback.onSuccess(networks);
        } catch (RemoteException e) {
            e.printStackTrace();
            callback.onFailed();
        }
    }

    /**
     * Connect to WiFi access point
     *
     * @param wifiName     wifi name
     * @param wifiPassword wifi password
     * @param capabilities capabilities
     * @param callback     callback
     */
    public void connectToWiFi(final String wifiName, final String wifiPassword, final String capabilities, final AsyncReceiver callback) {
        if (utilityService == null || networkManager == null) {
            callback.onFailed();
            return;
        }
        final String securityType;
        if (capabilities.contains("WEP"))
            securityType = "WEP";
        else if (capabilities.contains("WPA"))
            securityType = "WPA";
        else if (capabilities.contains("802.1x EAP"))
            securityType = "802.1x EAP";
        else
            securityType = "NONE";
        new Thread() {
            @Override
            public void run() {
                super.run();
                try {
                    networkManager.connectToWiFi(wifiName, wifiPassword, securityType);
                    callback.onSuccess();
                } catch (RemoteException e) {
                    e.printStackTrace();
                    callback.onFailed();
                }
            }
        }.start();
    }

    /**
     * Forget wifi network
     *
     * @param networkId
     * @param callback
     */
    public void forgetWifiNetwork(int networkId, AsyncReceiver callback) {
        if (utilityService == null || networkManager == null) {
            if (callback != null) {
                callback.onFailed();
                return;
            }
        }
        try {
            networkManager.forget(networkId);
            if (callback != null)
                callback.onSuccess();
        } catch (RemoteException e) {
            e.printStackTrace();
            if (callback != null)
                callback.onFailed();
        }
    }

    /**
     * Set DNS Ethernet
     *
     * @param ethernetEntity ethernet entity
     * @param callback       callback
     */
    public void setDNSEthernet(RjioEthernetEntity ethernetEntity, AsyncReceiver callback) {
        if (utilityService == null || networkManager == null) {
            callback.onFailed();
            return;
        }

        try {
            networkManager.setIPConfiguration(ethernetEntity.getIpAssignment(),
                    ethernetEntity.getIpAddress(),
                    ethernetEntity.getGateway(),
                    ethernetEntity.getDns1(),
                    ethernetEntity.getDns1(),
                    convertMaskToNetworkPrefix(ethernetEntity.getMask()));
            callback.onSuccess();
        } catch (RemoteException e) {
            e.printStackTrace();
            callback.onFailed();
        }
    }

    /**
     * Set the first time setup completed flag. When set to true will
     * allow the user to press home button.
     *
     * @param complete FTI state
     * @param callback result callback
     */
    public void setUserSetupComplete(boolean complete, AsyncReceiver callback) {
        if (utilityService == null || settingsManager == null) {
            callback.onFailed();
            return;
        }

        try {
            settingsManager.setUserSetupComplete(complete);
            callback.onSuccess();
        } catch (RemoteException e) {
            e.printStackTrace();
            callback.onFailed();
        }
    }

    /**
     * Send device into stand by mode
     */
    public void goToSleep() {
        try {
            launcherPowerManager.goToSleep();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    /**
     * Is device interactive
     *
     * @return true if device is interactive otherwise false
     */
    public boolean isInteractive() {
        boolean isInteractive;
        try {
            isInteractive = launcherPowerManager.isInteractive();
            return isInteractive;
        } catch (RemoteException e) {
            e.printStackTrace();
            return false;
        }

    }

    /**
     * Convert mask to network prefix
     *
     * @param mask mask
     * @return network prefix
     */
    private int convertMaskToNetworkPrefix(String mask) {

        String[] arr = mask.split("\\.");
        int bitCount = 0;
        for (String s : arr) {
            bitCount += Integer.bitCount(Integer.valueOf(s));
        }
        return bitCount;
    }

    /**
     * Set device name
     *
     * @param name new device name
     */
    public void setDeviceName(String name, AsyncReceiver callback) {
        if (utilityService == null || bluetoothManager == null) {
            callback.onFailed();
            return;
        }

        try {
            bluetoothManager.setDeviceName(name);
            callback.onSuccess();
        } catch (RemoteException e) {
            callback.onFailed();
            e.printStackTrace();
        }
    }

    public void getLanguages(AsyncDataReceiver<List<Language>> callback) {
        if (utilityService == null || languageManager == null) {
            callback.onFailed();
            return;
        }

        try {
            languageList = languageManager.getLanguages();
            callback.onSuccess(languageList);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public String getCurrentLanguage() {
        if (utilityService == null || languageManager == null) {
            return null;
        }
        try {
            String currentLanguage = languageManager.getCurrentLanguage();
            return currentLanguage;
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void setLanguage(String languageTag) {
        if (utilityService == null || languageManager == null) {
            return;
        }
        try {
            languageManager.setLanguage(languageTag);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    /**
     * Disable HDMI CEC
     */
    public void disableHdmiCec() {
        if (utilityService == null || settingsManager == null) {
            return;
        }
        try {
            settingsManager.disableHdmiCec();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }
}
