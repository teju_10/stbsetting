/**
 * Provides entities integrated inside application UI logic.
 * <p>
 * <p>
 * <code>RjioAppItem</code> extends from <code>AppItem</code> with four fields to represent application type: <i>APP, GAME, RJIO_APP, SYSTEM_APP</i>.
 * <p>
 * <code>RjioDeviceItem</code> extends from <code>AppItem</code> to represent external USB drive mounted to board.
 * <p>
 * <code>RjioUser</code> extends from <code>AppItem</code> to represent User account with username and password credentials.
 *
 * @see com.iwedia.ui.rjio.launcher.sdk.entity.RjioAppItem
 * @see com.iwedia.ui.rjio.launcher.sdk.entity.RjioDeviceItem
 * @see com.iwedia.ui.rjio.launcher.sdk.entity.RjioUserItem
 * @since 1.0
 */
package com.iwedia.fti.rjio.fti_sdk.entity;
