package com.iwedia.fti.rjio.fti_sdk.handler;

import com.iwedia.fti.rjio.fti.api.AsyncReceiver;
import com.iwedia.fti.rjio.fti.handler.AccountHandler;
import com.iwedia.fti.rjio.fti_sdk.RjioSdk;
import com.iwedia.fti.rjio.fti_sdk.entity.RjioUser;

/**
 * Rjio Sign Handler
 *
 * @author Nikola Aleksic
 * @author Dragan Krnjaic
 */
public class RjioAccountHandler extends AccountHandler<RjioUser> {


    @Override
    public void signUp(RjioUser user, AsyncReceiver callback) {

    }

    @Override
    public void signIn(RjioUser user, AsyncReceiver callback) {

        //TODO
        this.user = user;
        callback.onSuccess();
    }

    @Override
    public void signOut(RjioUser user, AsyncReceiver callback) {

        if(user != null && user.getUsername() != null && !user.getUsername().isEmpty()) {
            if(user.getUsername().equals(RjioSdk.get().getSharedPrefs().loadUserName())){
                RjioSdk.get().getSharedPrefs().removeUserName();
                if (callback != null) callback.onSuccess();
            } else {
                if (callback != null) callback.onFailed();
            }
        }

         if (user.getPassword() != null && !user.getPassword().isEmpty()){
             if(user.getPassword().equals(RjioSdk.get().getSharedPrefs().loadPassword())){
                 RjioSdk.get().getSharedPrefs().removePassword();
                 if (callback != null) callback.onSuccess();
             } else {
                 if (callback != null) callback.onFailed();
             }
         }
    }
}
