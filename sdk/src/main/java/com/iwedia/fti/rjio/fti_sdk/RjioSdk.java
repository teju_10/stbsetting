package com.iwedia.fti.rjio.fti_sdk;

import android.content.Context;

import com.iwedia.fti.rjio.fti_sdk.handler.RjioAccountHandler;

import com.iwedia.fti.rjio.fti_sdk.handler.RjioBluetoothHandler;

import com.iwedia.fti.rjio.fti_sdk.handler.RjioLanguageHandler;
import com.iwedia.fti.rjio.fti_sdk.handler.RjioNetworkHandler;

import com.iwedia.fti.rjio.fti_sdk.handler.RjioSharedPrefs;
import com.iwedia.fti.rjio.fti_sdk.handler.RjioSystemSettingsHandler;

import com.iwedia.fti.rjio.fti_sdk.handler.RjioUserProfileHandler;

/**
 * Rjio sdk
 *
 * @author Veljko Ilkic
 */
public class RjioSdk {

    /**
     * Singleton instance
     */
    private static final RjioSdk instance = new RjioSdk();

    /**
     * Context
     */
    private Context context;

    /**
     * Singleton constructor
     */
    private RjioSdk() {
    }

    /**
     * Get singleton instance
     *
     * @return singleton instance
     */
    public static RjioSdk get() {
        return instance;
    }


    /**
     * Account handler
     */
    private RjioAccountHandler accountHandler;

    /**
     * Shared prefs
     */
    private RjioSharedPrefs sharedPrefs;

    /**
     * User profile handler
     */
    private RjioUserProfileHandler userProfileHandler;

    /**
     * Rjio network handler
     */
    private RjioNetworkHandler networkHandler;

    /**
     * Rjio language handler
     */
    RjioLanguageHandler languageHandler;

    /**
     * Bluetooth handler
     */
    private RjioBluetoothHandler bluetoothHandler;

    /**
     * Rjio system settings handler
     */
    private RjioSystemSettingsHandler systemSettingsHandler;

    /**
     * Setup sdk
     */
    public void setup(final Context context) {
        this.context = context;

        networkHandler = new RjioNetworkHandler(context);
        bluetoothHandler = new RjioBluetoothHandler(context);
        accountHandler = new RjioAccountHandler();
        sharedPrefs = new RjioSharedPrefs();
        userProfileHandler = new RjioUserProfileHandler();
        languageHandler = new RjioLanguageHandler();
        systemSettingsHandler = new RjioSystemSettingsHandler();
    }

    /**
     * Get application context
     *
     * @return context
     */
    public Context getContext() {
        return context;
    }

    /**
     * Get account handler
     *
     * @return account handler
     */
    public RjioAccountHandler getAccountHandler() {
        return accountHandler;
    }

    /**
     * Get shared prefs
     *
     * @return shared prefs
     */
    public RjioSharedPrefs getSharedPrefs() {
        return sharedPrefs;
    }

    /**
     * Get User profile handler
     *
     * @return user profile handler
     */
    public RjioUserProfileHandler getUserProfileHandler() {
        return userProfileHandler;
    }

    /**
     * Get network handler
     *
     * @return network handler
     */
    public RjioNetworkHandler getNetworkHandler() {
        return networkHandler;
    }

    /**
     * Get language handler
     *
     * @return language handler
     */
    public RjioLanguageHandler getLanguageHandler() {
        return languageHandler;
    }

    /**
     * Get bluetooth handler
     *
     * @return bluetooth handler
     */
    public RjioBluetoothHandler getBluetoothHandler() {
        return bluetoothHandler;
    }

    /**
     * Get system settings handler
     *
     * @return system settings handler
     */
    public RjioSystemSettingsHandler getSystemSettingsHandler() {
        return systemSettingsHandler;
    }
}
