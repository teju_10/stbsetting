/**
 * Provides instances of all launcher handlers.
 * <p>
 * <p>
 * Most of handlers methods are asynchronous and data will be retrieved through AsyncDataReceiver callback.
 * <p>
 * <p>
 * <code>RjioAppsHandler</code> that extends from <code>AppsHandler</code> and overrides these methods:
 * <p>
 * <ul>
 * <li><code><font color="#4169E1">void requestInstalledApps(AsyncDataReceiver<ArrayList<RjioAppItem>> callback)</font></code> -
 * Requests for all installed apps and sorts them based on application's type</li>
 * <li><code><font color="#4169E1">void launchApp(RjioAppItem appItem, AsyncReceiver callback)</font></code> -
 * Handles the applications launching based on package name with given launch intent</li>
 * <li><code><font color="#4169E1">void removeApp(RjioAppItem appItem, AsyncReceiver callback)</font></code> -
 * Handles applications uninstall intent with provided package name and data scheme</li>
 * </ul>
 * <p>
 * It also holds UninstallHandlerListener interface to trigger onUninstall application event on demand.
 * <p>
 * <p>
 * <code>RjioDeviceHandler</code> extends from <code>DeviceHandler</code> and overrides these methods:
 * <p>
 * <ul>
 * <li><code><font color="#4169E1">void onMounted(RjioDeviceItem device)</font></code> - Mounts USB device and collects its data</li>
 * <li><code><font color="#4169E1">void onUnmounted(RjioDeviceItem device)</font></code> - Removes USB device and data</li>
 * </ul>
 * <p>
 * <p>
 * <code>RjioOverscanHandler</code> extend from <code>OverscanHandler</code> and overrides these methods:
 * <p>
 * <ul>
 * <li><code><font color="#4169E1">void onSizeChanged(int width, int height)</font></code> - Collects screen frame width and height values</li>
 * <li><code><font color="#4169E1">void onPositionChanged(int x, int y)</font></code> - Collects screen x and y axis values</li>
 * </ul>
 * <p>
 * <p>
 * <code>RjioAccountHandler</code> extend from <code>AccountHandler</code> and overrides these methods:
 * <ul>
 * <li><code><font color="#4169E1">void signIn(User user, AsyncReceiver callback)</font></code> - Handles user sign in feature</li>
 * <li><code><font color="#4169E1">void signOut(User user, AsyncReceiver callback)</font></code> - Handles user sign out feature</li>
 * <li><code><font color="#4169E1">void void signUp(User user, AsyncReceiver callback)</font></code> - Handles user sign up feature</li>
 * </ul>
 * <p>
 * <code>RjioUpdateHandler</code> extends from <code>UpdateHandler</code> and overrides these methods:
 * <ul>
 * <li><code><font color="#4169E1">void requestUpdateAvailable(AsyncReceiver callback)</font></code> - Handles when update is available</li>
 * <li><code><font color="#4169E1">void requestUpdateStart(AsyncReceiver callback)</font></code> - Handles when will download start</li>
 * <li><code><font color="#4169E1">void requestUpdateCancel(AsyncReceiver callback)</font></code> - Handles canceling update</li>
 * <li><code><font color="#4169E1">void requestUpdateApply(AsyncReceiver callback)</font></code> - Handles applying update after downloading</li>
 * </ul>
 *
 * @see com.iwedia.ui.rjio.launcher_sdk.handler.RjioAppsHandler
 * @see com.iwedia.ui.rjio.launcher_sdk.handler.RjioDeviceHandler
 * @see com.iwedia.ui.rjio.launcher_sdk.handler.RjioOverscanHandler
 * @see com.iwedia.ui.rjio.launcher_sdk.handler.RjioAccountHandler
 * @see com.iwedia.ui.rjio.launcher_sdk.handler.RjioUpdateHandler
 * @since 1.0
 */
package com.iwedia.fti.rjio.fti_sdk.handler;
