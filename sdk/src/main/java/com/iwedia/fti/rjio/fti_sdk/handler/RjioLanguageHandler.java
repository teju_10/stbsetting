package com.iwedia.fti.rjio.fti_sdk.handler;

import android.util.Log;

import com.iwedia.fti.rjio.fti.api.AsyncDataReceiver;
import com.iwedia.fti.rjio.fti.api.AsyncReceiver;
import com.iwedia.fti.rjio.fti.entities.Language;
import com.iwedia.fti.rjio.fti.handler.LanguageHandler;
import com.iwedia.fti.rjio.fti_sdk.RjioSdk;

import java.util.List;

/**
 * Rjio language handler
 *
 * @author Dejan Nadj
 */
public class RjioLanguageHandler extends LanguageHandler<Language> {

    /**
     * English language code
     */
    public static final String ENGLISH_LANGUAGE = "en";

    /**
     * Constructor
     */
    public RjioLanguageHandler() {

        //Read app language from shared prefs
        final String lang = (String) RjioSdk.get().getSharedPrefs().getValue(RjioSharedPrefs.LANGUAGE_KEY, ENGLISH_LANGUAGE);

        for (int i = 0; i < languages.size(); i++) {
            if (languages.get(i).getId().equals(lang)) {
                //currentLanguage = languages.get(i);
                break;
            }
        }
    }

    private void updateLanguagesList(List<com.iwedia.utility.language.Language> data) {
        languages.add(new Language("en-IN", "English (India)"));
        for (com.iwedia.utility.language.Language language : data) {
            if (language.tag.equals("en-IN")) {

            } else {
                languages.add(new Language(language.tag,language.name));
            }
        }
    }

    public void refreshLangues() {
        languages.clear();
        RjioUtilityServiceHandler.getInstance().getLanguages(new AsyncDataReceiver<List<com.iwedia.utility.language.Language>>() {
            @Override
            public void onSuccess(List<com.iwedia.utility.language.Language> data) {
                updateLanguagesList(data);
            }

            @Override
            public void onFailed() {
            }
        });
    }


    public void getDefaultLanguage() {
        String currentLanguageTag = "en-IN";
//        languages.add(new Language("en-IN", "English (India)"));
        if (currentLanguageTag == null) {
            return;
        }
        for (Language language : languages) {
            if (language.getId().equals(currentLanguageTag)) {
                currentLanguage = language;
                break;
            }
        }
    }

    @Override
    public void setLanguage(Language language) {
        super.setLanguage(language);
        RjioUtilityServiceHandler.getInstance().setLanguage(language.getId());

    }
}
