package com.iwedia.fti.rjio.fti_sdk.handler;


import android.app.AlarmManager;
import android.content.Context;

import com.iwedia.fti.rjio.fti.api.AsyncReceiver;
import com.iwedia.fti.rjio.fti.handler.SystemSettingsHandler;
import com.iwedia.fti.rjio.fti_sdk.RjioSdk;

/**
 * Rjio System Settings Handler
 *
 * @author Vadzim Dambrouski
 */
public class RjioSystemSettingsHandler extends SystemSettingsHandler {

    @Override
    public void setUserSetupComplete(boolean complete, AsyncReceiver callback) {
        RjioUtilityServiceHandler.getInstance().setUserSetupComplete(complete, callback);
    }

    @Override
    public void setDeviceName(String deviceName,AsyncReceiver callback) {
        RjioUtilityServiceHandler.getInstance().setDeviceName(deviceName,callback);
    }

    @Override
    public void setDefaultTimeZone() {
        final AlarmManager alarm = (AlarmManager)
                RjioSdk.get().getContext().getSystemService(Context.ALARM_SERVICE);
        if (alarm != null) {
            alarm.setTimeZone("Asia/Calcutta");
        }
    }

    @Override
    public void disableHdmiCec() {
        RjioUtilityServiceHandler.getInstance().disableHdmiCec();
    }


}
