/**
 * Provides Presenter implementation of base launcher functionalities.
 * <p>
 * <p>
 * SDK support these packages: <i>entity package, handler package</i>.
 * <p>
 * <code>RjioSdk</code> is singleton class which contains instances of all handlers.
 * <p>
 * In order to ask for SDK handlers, use RjioSdk.get() method.
 *
 * @see com.iwedia.ui.rjio.launcher.sdk.RjioSdk
 * @see com.iwedia.ui.rjio.launcher.sdk.entity.RjioAppItem
 * @see com.iwedia.ui.rjio.launcher.sdk.entity.RjioDeviceItem
 * @see com.iwedia.ui.rjio.launcher.sdk.entity.RjioUserItem
 * @see com.iwedia.ui.rjio.launcher.sdk.handler.RjioAppsHandler
 * @see com.iwedia.ui.rjio.launcher.sdk.handler.RjioDeviceHandler
 * @see com.iwedia.ui.rjio.launcher.sdk.handler.RjioOverscanHandler
 * @see com.iwedia.ui.rjio.launcher.sdk.handler.RjioSignHandler
 * @since 1.0
 */
package com.iwedia.fti.rjio.fti_sdk;
