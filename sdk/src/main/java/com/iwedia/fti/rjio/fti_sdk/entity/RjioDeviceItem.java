package com.iwedia.fti.rjio.fti_sdk.entity;

import com.iwedia.fti.rjio.fti.entities.DeviceEntity;

/**
 * Rjio device item
 *
 * @author Nikola Aleksic
 */
public class RjioDeviceItem extends DeviceEntity {

    /**
     * Constructor
     *
     * @param id           id
     * @param name         name
     * @param product      product
     * @param version      version
     * @param serialNumber serial number
     */
    public RjioDeviceItem(int id, String name, String product, String version, String serialNumber) {
        super(id, name, product, version, serialNumber);
    }
}
