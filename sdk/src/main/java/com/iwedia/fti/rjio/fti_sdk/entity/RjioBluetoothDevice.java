package com.iwedia.fti.rjio.fti_sdk.entity;

import com.iwedia.fti.rjio.fti.entities.BluetoothDevice;

/**
 * Rjio bluetooth device
 *
 * @author Dejan Nadj
 */
public class RjioBluetoothDevice extends BluetoothDevice {

    /**
     * Constructor
     *
     * @param deviceId      device id
     * @param deviceName    device name
     */
    public RjioBluetoothDevice(String deviceId, String deviceName) {
        super(deviceId, deviceName);
    }
}
