package com.iwedia.fti.rjio.fti_sdk.entity;

/**
 * Rjio Ethernet Entity
 *
 * @author Yahor Hryniuk
 */
public class RjioEthernetEntity {

    /**
     * Ip assigment (default static)
     */
    private String ipAssignment = "STATIC";

    /**
     * Ip address
     */
    private String ipAddress;

    /**
     * Mask
     */
    private String mask;

    /**
     * Gateway
     */
    private String gateway;

    /**
     * Dns1
     */
    private String dns1;

    /**
     * Dns2
     */
    private String dns2;

    /**
     * Get ip assignment
     * @return
     */
    public String getIpAssignment() {
        return ipAssignment;
    }

    /**
     * Set ip assingment
     *
     * @param ipAssignment
     */
    public void setIpAssignment(String ipAssignment) {
        this.ipAssignment = ipAssignment;
    }

    /**
     * Get ip Address
     *
     * @return
     */
    public String getIpAddress() {
        return ipAddress;
    }

    /**
     * Set ip address
     *
     * @param ipAddress
     */
    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    /**
     * Get mask
     *
     * @return
     */
    public String getMask() {
        return mask;
    }

    /**
     * Set mask
     *
     * @param mask
     */
    public void setMask(String mask) {
        this.mask = mask;
    }

    /**
     * Get gateway
     *
     * @return
     */
    public String getGateway() {
        return gateway;
    }

    /**
     * Set gateway
     *
     * @param gateway
     */
    public void setGateway(String gateway) {
        this.gateway = gateway;
    }

    /**
     * Get dns1
     *
     * @return
     */
    public String getDns1() {
        return dns1;
    }

    /**
     * Set dns1
     *
     * @param dns1
     */
    public void setDns1(String dns1) {
        this.dns1 = dns1;
    }

    /**
     * Get dns2
     *
     * @return
     */
    public String getDns2() {
        return dns2;
    }

    /**
     * Set dns2
     *
     * @param dns2
     */
    public void setDns2(String dns2) {
        this.dns2 = dns2;
    }
}
